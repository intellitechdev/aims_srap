Module modValidateString

    '=========================================================
    'system validation that will not accept special characters
    '=========================================================
    'debug.print(e.keychar)'this will show what key do you strike to return value
    Public Function alphanumeric_Validate(ByVal C As Char) As Boolean
        If Not (Char.IsLetterOrDigit(C)) And Not (C = "A") And Not (C = "(") And Not (C = "'") And Not (C = ";") And Not (C = ")") _
            And Not (C = ".") And Not (C = ",") And Not (C = "-") And Not (C = "/") And Not (Microsoft.VisualBasic.AscW(C) = 8) _
            And Not (Microsoft.VisualBasic.AscW(C) = 13) And Not (Microsoft.VisualBasic.AscW(C) = 32) Then
            MessageBox.Show("Invalid Input! This field allows letters and 0-9 only.", "System Validation")
            Return False
        Else
            Return True
        End If
    End Function

    '=========================================================
    'system validation that will accept numbers only with "." and "-"
    '=========================================================
    Public Function numeric_Validate(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = ".") And Not (C = "-") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MessageBox.Show("Invalid Input! This field allows  0-9 only.", "System Validation")
            Return False
        Else
            Return True
        End If
    End Function

    '=========================================================
    'system validation that will accept numbers only with
    '=========================================================
    Public Function numeric_Validation(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            Return False
        Else
            Return True
        End If
    End Function

End Module
