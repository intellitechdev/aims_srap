<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_acc_reconadjustment
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_acc_reconadjustment))
        Me.txtdiscripancy = New System.Windows.Forms.TextBox
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.btnReturn = New System.Windows.Forms.Button
        Me.btnLeave = New System.Windows.Forms.Button
        Me.btnAdjust = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'txtdiscripancy
        '
        Me.txtdiscripancy.BackColor = System.Drawing.SystemColors.Control
        Me.txtdiscripancy.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtdiscripancy.Location = New System.Drawing.Point(6, 12)
        Me.txtdiscripancy.Multiline = True
        Me.txtdiscripancy.Name = "txtdiscripancy"
        Me.txtdiscripancy.Size = New System.Drawing.Size(442, 39)
        Me.txtdiscripancy.TabIndex = 0
        Me.txtdiscripancy.Text = "reconcile descripancy"
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.SystemColors.Control
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox1.Location = New System.Drawing.Point(6, 54)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(442, 55)
        Me.TextBox1.TabIndex = 1
        Me.TextBox1.Text = resources.GetString("TextBox1.Text")
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.SystemColors.Control
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox2.Location = New System.Drawing.Point(6, 105)
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(442, 32)
        Me.TextBox2.TabIndex = 2
        Me.TextBox2.Text = "- Click Leave Reconcile to complete reconciliation later. CLICKSOFTWARE@Accountin" & _
            "g will save your changes."
        '
        'TextBox3
        '
        Me.TextBox3.BackColor = System.Drawing.SystemColors.Control
        Me.TextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox3.Location = New System.Drawing.Point(6, 141)
        Me.TextBox3.Multiline = True
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(442, 48)
        Me.TextBox3.TabIndex = 3
        Me.TextBox3.Text = resources.GetString("TextBox3.Text")
        '
        'btnReturn
        '
        Me.btnReturn.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReturn.Location = New System.Drawing.Point(460, 54)
        Me.btnReturn.Name = "btnReturn"
        Me.btnReturn.Size = New System.Drawing.Size(147, 23)
        Me.btnReturn.TabIndex = 4
        Me.btnReturn.Text = "Return to Reconcile"
        Me.btnReturn.UseVisualStyleBackColor = True
        '
        'btnLeave
        '
        Me.btnLeave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLeave.Location = New System.Drawing.Point(460, 98)
        Me.btnLeave.Name = "btnLeave"
        Me.btnLeave.Size = New System.Drawing.Size(147, 23)
        Me.btnLeave.TabIndex = 5
        Me.btnLeave.Text = "Leave Reconcile"
        Me.btnLeave.UseVisualStyleBackColor = True
        '
        'btnAdjust
        '
        Me.btnAdjust.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdjust.Location = New System.Drawing.Point(460, 141)
        Me.btnAdjust.Name = "btnAdjust"
        Me.btnAdjust.Size = New System.Drawing.Size(147, 23)
        Me.btnAdjust.TabIndex = 6
        Me.btnAdjust.Text = "Enter Adjustment"
        Me.btnAdjust.UseVisualStyleBackColor = True
        '
        'frm_acc_reconadjustment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(619, 195)
        Me.Controls.Add(Me.btnAdjust)
        Me.Controls.Add(Me.btnLeave)
        Me.Controls.Add(Me.btnReturn)
        Me.Controls.Add(Me.TextBox3)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.txtdiscripancy)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frm_acc_reconadjustment"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Reconcile Adjustment"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtdiscripancy As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents btnReturn As System.Windows.Forms.Button
    Friend WithEvents btnLeave As System.Windows.Forms.Button
    Friend WithEvents btnAdjust As System.Windows.Forms.Button
End Class
