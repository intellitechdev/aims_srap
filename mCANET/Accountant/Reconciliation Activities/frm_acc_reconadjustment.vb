Public Class frm_acc_reconadjustment

    Private Sub frm_acc_reconadjustment_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtdiscripancy.Text = "There is a " + CStr(gReconcileDiscripancy) + " discrepancy between your statement and the transactions you have selected."
    End Sub

    Private Sub btnReturn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        gReconcileDiscripancy = ""
        Me.Close()
    End Sub

    Private Sub btnLeave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLeave.Click
        frm_acc_reconcile2.Close()
        Me.Close()
    End Sub
End Class