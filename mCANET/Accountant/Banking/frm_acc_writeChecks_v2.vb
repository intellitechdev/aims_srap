Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_acc_writeChecks_v2
    Private gCon As New Clsappconfiguration

    Private paybillsID As String
    Public Property GetPayBillsID() As String
        Get
            Return paybillsID
        End Get
        Set(ByVal value As String)
            paybillsID = value
        End Set
    End Property

    Private writeChecksID As String
    Public Property GetWriteChecksID() As String
        Get
            Return writeChecksID
        End Get
        Set(ByVal value As String)
            writeChecksID = value
        End Set
    End Property

    Private IsVoid As Boolean
    Public Property GetIsVoid() As Boolean
        Get
            Return IsVoid
        End Get
        Set(ByVal value As Boolean)
            IsVoid = value
        End Set
    End Property


    Private Sub lblAmount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblAmount.Click
        txtAmount.Focus()
    End Sub

    Private Sub txtAmount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAmount.TextChanged

        Dim moneyConverted As New WPMs

        Try
            lblAmount.Text = Format(CDec(txtAmount.Text), "##,##0.00")
            lblAmountInWords.Text = moneyConverted.gGenerateCurrencyInWords(lblAmount.Text)
        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Dispose()
        Close()
    End Sub

    Private Sub frm_acc_writeChecks_v2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call LoadComboboxControls()
        Call LoadCheckDetails()
        Call EvaluateIfCheckIsVoid()
    End Sub

    Private Sub EvaluateIfCheckIsVoid()
        If GetIsVoid() Then
            LblTransStatus.Text = "Voided!"
            LblTransStatus.Visible = True
            btnPrint.Visible = False
            btnVoid.Visible = False

            cboBankList.Visible = False
            cboSupplierList.Visible = False
            dtePaydate.Visible = False

            lblMember.Visible = False
            lblBank.Visible = False
            lblPaydate.Visible = False
        Else
            LblTransStatus.Text = ""
            LblTransStatus.Visible = False
            btnPrint.Visible = True
            btnVoid.Visible = True

            cboBankList.Visible = True
            cboSupplierList.Visible = True
            dtePaydate.Visible = True

            lblMember.Visible = True
            lblBank.Visible = True
            lblPaydate.Visible = True
        End If
    End Sub

    Private Sub InitializedForm()
        LblTransStatus.Visible = False
    End Sub

    Private Sub LoadCheckDetails()
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "usp_t_WriteChecks_LoadDetails", _
                        New SqlParameter("@writeCheckID", GetWriteChecksID()))
                If rd.Read() Then
                    cboSupplierList.SelectedValue = rd.Item("fcSupplierName").ToString()
                    cboBankList.SelectedValue = rd.Item("acnt_name").ToString()

                    lblAccountName.Text = rd.Item("co_legalname").ToString()
                    lblPayToOrder.Text = rd.Item("fcSupplierName").ToString()
                    lblBankAccount.Text = rd.Item("acnt_desc").ToString()
                    lblCheckNo.Text = rd.Item("fcCheckNo").ToString()
                    lblDate.Text = Date.Parse(rd.Item("fdCheckDate").ToString()).ToShortDateString
                    lblAmount.Text = Format(CDec(rd.Item("fdCheckAmount").ToString()), "##,##0.00")
                    lblAmountInWords.Text = rd.Item("fcAmountInWords").ToString()

                    GetIsVoid() = rd.Item("fb_IsVoid")
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub LoadComboboxControls()
        LoadMemberList()
        LoadBankAccountList()
    End Sub

    Private Sub LoadMemberList()

        Dim DTAccounts As New DataTable("Accounts")
        Dim DR As DataRow

        DTAccounts.Columns.Add("fxKeySupplier")
        DTAccounts.Columns.Add("fcSupplierName")

        DR = DTAccounts.NewRow()
        DR("fcSupplierName") = ""
        DTAccounts.Rows.Add(DR)

        DR = DTAccounts.NewRow()
        DR("fcSupplierName") = "New"
        DTAccounts.Rows.Add(DR)

        DR = DTAccounts.NewRow()
        DR("fcSupplierName") = "------"
        DTAccounts.Rows.Add(DR)

        Dim storedProc As String = "usp_m_msupplier_getname"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, storedProc, _
                        New SqlParameter("@coid", gCompanyID()))
                While rd.Read
                    DR = DTAccounts.NewRow()
                    DR("fxKeySupplier") = rd.Item("fxKeySupplier").ToString
                    DR("fcSupplierName") = rd.Item("fcSupplierName").ToString
                    DTAccounts.Rows.Add(DR)
                End While
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        With cboSupplierList
            .Items.Clear()
            .LoadingType = MTGCComboBox.CaricamentoCombo.DataTable
            .SourceDataString = New String(1) {"fcSupplierName", "fxKeySupplier"}
            .SourceDataTable = DTAccounts
            If .Items.Count <> 0 Then
                .SelectedIndex = 0
            End If
        End With

    End Sub

    Private Sub LoadBankAccountList()
        Call GetSpecifiedAcnt(cboBankList, "Bank Account")
    End Sub

    Private Sub VoidCheck()
        LblTransStatus.Text = "Voided!"
        LblTransStatus.Visible = True
    End Sub

    Private Sub DisplayDialogVoidType()
        frm_acc_writeChecks_v2_Void.ShowDialog()
    End Sub

    Private Sub ReverseVoidCheck()
        LblTransStatus.Text = ""
        LblTransStatus.Visible = False
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        With frmCheckVoucherReport
            .GetWriteCheckID() = Me.GetWriteChecksID()
            .GetPayBillsID = Me.GetPayBillsID()
            .ShowDialog()
        End With
    End Sub

    Private Sub btnVoid_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVoid.Click
        With frm_acc_writeChecks_v2_Void
            .GetWriteChecksID() = Me.GetWriteChecksID()
            .GetPaybillsID() = Me.GetPayBillsID()
            .GetUsername() = gUserName
        End With

        If MessageBox.Show("Are you sure you want to void this check?", "Void Check", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            DisplayDialogVoidType()
        End If
    End Sub

End Class