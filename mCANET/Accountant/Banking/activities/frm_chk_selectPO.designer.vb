<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_chk_selectPO
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.ListView1 = New System.Windows.Forms.ListView
        Me.btn_chk_poOK = New System.Windows.Forms.Button
        Me.btn_chk_poCancel = New System.Windows.Forms.Button
        Me.btn_chk_poHelp = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(5, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Supplier"
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(53, 11)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(207, 21)
        Me.ComboBox1.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(5, 44)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(174, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Select a Purchase Order to receive"
        '
        'ListView1
        '
        Me.ListView1.Location = New System.Drawing.Point(8, 60)
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(364, 170)
        Me.ListView1.TabIndex = 3
        Me.ListView1.UseCompatibleStateImageBehavior = False
        '
        'btn_chk_poOK
        '
        Me.btn_chk_poOK.Location = New System.Drawing.Point(182, 236)
        Me.btn_chk_poOK.Name = "btn_chk_poOK"
        Me.btn_chk_poOK.Size = New System.Drawing.Size(93, 23)
        Me.btn_chk_poOK.TabIndex = 12
        Me.btn_chk_poOK.Text = "OK"
        Me.btn_chk_poOK.UseVisualStyleBackColor = True
        '
        'btn_chk_poCancel
        '
        Me.btn_chk_poCancel.Location = New System.Drawing.Point(279, 236)
        Me.btn_chk_poCancel.Name = "btn_chk_poCancel"
        Me.btn_chk_poCancel.Size = New System.Drawing.Size(93, 23)
        Me.btn_chk_poCancel.TabIndex = 13
        Me.btn_chk_poCancel.Text = "Cancel"
        Me.btn_chk_poCancel.UseVisualStyleBackColor = True
        '
        'btn_chk_poHelp
        '
        Me.btn_chk_poHelp.Location = New System.Drawing.Point(279, 9)
        Me.btn_chk_poHelp.Name = "btn_chk_poHelp"
        Me.btn_chk_poHelp.Size = New System.Drawing.Size(93, 23)
        Me.btn_chk_poHelp.TabIndex = 14
        Me.btn_chk_poHelp.Text = "Help"
        Me.btn_chk_poHelp.UseVisualStyleBackColor = True
        '
        'frm_chk_selectPO
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(382, 266)
        Me.Controls.Add(Me.btn_chk_poHelp)
        Me.Controls.Add(Me.btn_chk_poCancel)
        Me.Controls.Add(Me.btn_chk_poOK)
        Me.Controls.Add(Me.ListView1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frm_chk_selectPO"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Open Purchase Orders"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ListView1 As System.Windows.Forms.ListView
    Friend WithEvents btn_chk_poOK As System.Windows.Forms.Button
    Friend WithEvents btn_chk_poCancel As System.Windows.Forms.Button
    Friend WithEvents btn_chk_poHelp As System.Windows.Forms.Button
End Class
