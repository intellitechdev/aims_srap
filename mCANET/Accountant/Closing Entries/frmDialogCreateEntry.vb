Public Class frmDialogCreateEntry

    Private Sub btnCreate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        If MessageBox.Show("Are you sure you want to close the selected period?", "Close Period", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
            frmClosingEntries.GetPeriodFrom() = dteDateFrom.Value.Date
            frmClosingEntries.GetPeriodTo() = dtePeriodTo.Value.Date

            Call frmClosingEntries.ExecuteClosingProcess()

            Close()
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Close()
    End Sub
End Class