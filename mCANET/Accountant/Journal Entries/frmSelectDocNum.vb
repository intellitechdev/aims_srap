﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading

Public Class frmSelectDocNum

    Private gCon As New Clsappconfiguration()
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring

    Private Sub frmSelectDocNum_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call ViewDocNum(frmGeneralJournalEntries.cboDoctype.Text)
    End Sub

    Private Sub ViewDocNum(ByVal DocType As String)
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "usp_m_DocumentNumber_List_Unused", _
                                   New SqlParameter("@fcDocType", DocType),
                                   New SqlParameter("@coName", frmMain.lblCompanyName.Text))
        With Me.listDocNumber
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Document Number", 300, HorizontalAlignment.Center)
            '.Columns.Add("Date Used", 150, HorizontalAlignment.Center)
            Try
                While rd.Read
                    With .Items.Add(rd.Item(0))
                        '.SubItems.Add(rd.Item(1))
                    End With
                End While
                rd.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "View Document Number")
            End Try
        End With
    End Sub

    Private Sub listDocNumber_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles listDocNumber.DoubleClick
        Call selectdocnum()
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Call SearchDocNum(txtSearch.Text, frmGeneralJournalEntries.cboDoctype.Text)
    End Sub

    Private Sub SearchDocNum(ByVal DocNum As String, ByVal DocType As String)
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "usp_m_DocumentNumber_Search_Unused", _
                                   New SqlParameter("@fcDocNumber", DocNum),
                                   New SqlParameter("@fcDocType", DocType),
                                   New SqlParameter("@coName", frmMain.lblCompanyName.Text))
        With Me.listDocNumber
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Document Number", 300, HorizontalAlignment.Center)
            .Columns.Add("Date Used", 150, HorizontalAlignment.Center)
            Try
                While rd.Read
                    With .Items.Add(rd.Item(0))
                    End With
                End While
                'rd.Close()
            Catch ex As Exception
                'MessageBox.Show(ex.Message, "View Document Number")
            Finally
                rd.Close()
            End Try
        End With
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        Call selectdocnum()
    End Sub

    Private Sub listDocNumber_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles listDocNumber.KeyDown
        If e.KeyCode = Keys.Enter Then
            Call selectdocnum()
        End If
    End Sub

    Private Sub selectdocnum()
        If listDocNumber.SelectedItems.Item(0).Text = "" Then
            Exit Sub
        Else
            If CheckIfExistingInTemp() = "Existing" Then
                MsgBox("Document Number currently in use by other user. Please select other document number.", MsgBoxStyle.Exclamation, "Document Number")
                Exit Sub
            Else
                Try
                    SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_temp_TransactionEntriesDocNum_delete",
                                                New SqlParameter("@FcDocnum", frmGeneralJournalEntries.txtGeneralJournalNo.Text),
                                                New SqlParameter("@FcDocType", frmGeneralJournalEntries.cboDoctype.SelectedValue),
                                                New SqlParameter("@coid", gCompanyID))
                    frmGeneralJournalEntries.txtGeneralJournalNo.Text = listDocNumber.SelectedItems.Item(0).Text
                    SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_temp_TransactionEntriesDocNum_insert",
                                                New SqlParameter("@FcDocnum", frmGeneralJournalEntries.txtGeneralJournalNo.Text),
                                                New SqlParameter("@FcDocType", frmGeneralJournalEntries.cboDoctype.SelectedValue),
                                                New SqlParameter("@coid", gCompanyID))
                    Me.Close()
                Catch ex As Exception
                    Exit Sub
                End Try
            End If
        End If
    End Sub

    Private Function CheckIfExistingInTemp()
        Dim str As String
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_TempTransactionEntriesDocnum_Select",
                                                            New SqlParameter("@FcDocnum", listDocNumber.SelectedItems.Item(0).Text),
                                                            New SqlParameter("@FcDocType", frmGeneralJournalEntries.cboDoctype.SelectedValue),
                                                            New SqlParameter("@coid", gCompanyID))
        If rd.Read = True Then
            str = "Existing"
        Else
            str = ""
        End If
        Return str
    End Function
End Class