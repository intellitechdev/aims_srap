﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Public Class frmRecurringEntryList
    Private Sub frmRecurringEntryList_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ViewDocNum()
    End Sub
    Private Sub ViewDocNum()
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "spu_RecurringList", _
                                   New SqlParameter("@coid", gCompanyID),
                                   New SqlParameter("@filter", txtSearch.Text))
        With Me.listDocNumber
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Document Number", 300, HorizontalAlignment.Center)
            Try
                While rd.Read
                    With .Items.Add(rd.Item(1))
                    End With
                End While

                rd.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "View Document Number")
            End Try
        End With
    End Sub

    Private Sub listDocNumber_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles listDocNumber.DoubleClick
        Me.DialogResult = Windows.Forms.DialogResult.Yes
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        Me.DialogResult = Windows.Forms.DialogResult.Yes
    End Sub

    Private Sub txtSearch_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtSearch.TextChanged
        ViewDocNum()
    End Sub
End Class