﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading

Public Class frmMemberSetup

    Private gCon As New Clsappconfiguration()
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring
    Public xModule As String
    Dim isdebit As Boolean

    Private Sub frmMemberSetup_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        LoadGrid()
    End Sub

    Private Sub LoadGrid()
        Dim keyCompany As New Guid(gCompanyID)
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(gCon.cnstring, "spu_Cashiering_Memeber_View")
            GrdAccounts.Rows.Clear()
            GrdAccounts.DataSource = ds.Tables(0)
            GrdAccounts.Columns(0).Width = 100
            GrdAccounts.Columns(0).HeaderText = "Member ID"
            GrdAccounts.Columns(0).ReadOnly = True
            GrdAccounts.Columns(1).HeaderText = "Member Name"
            GrdAccounts.Columns(1).ReadOnly = True
            GrdAccounts.Columns(1).Width = 200
            GrdAccounts.Columns(2).HeaderText = "Non-Member"

        Catch ex As Exception
            'MessageBox.Show(ex.ToString, "Account List Description", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub UpdateInfo(ByVal ID As String, ByVal Mode As Boolean)

        SqlHelper.ExecuteNonQuery(gCon.cnstring, "spu_Cashiering_Memeber_Update",
                                  New SqlParameter("@MemberID", ID),
                                  New SqlParameter("@NonMember", Mode))
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnUpdate_Click(sender As System.Object, e As System.EventArgs) Handles btnUpdate.Click
        Try
            For Each row As DataGridViewRow In GrdAccounts.Rows
                UpdateInfo(row.Cells(0).Value.ToString, row.Cells(2).Value.ToString)
            Next

            LoadGrid()
            MessageBox.Show("Record Succesfully Updated!")
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString)
        End Try
    End Sub
End Class