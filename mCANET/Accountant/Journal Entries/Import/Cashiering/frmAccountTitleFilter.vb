﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading

Public Class frmAccountTitleFilter

    Private gCon As New Clsappconfiguration()
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring
    Public xModule As String
    Dim isdebit As Boolean

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub
    Private Sub btnSelect_Click(sender As System.Object, e As System.EventArgs) Handles btnSelect.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub frmAccountTitleFilter_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        loadAccounts()
    End Sub

    Private Sub loadAccounts()
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "spu_Cashiering_DebitCredit_View")
        dgvAccount.DataSource = ds.Tables(0)

        With dgvAccount
            .Columns("fxkey_AccountID").Visible = False
            .Columns("fcAccountCode").Width = 80
            .Columns("fcAccountCode").HeaderText = "Code"
            .Columns("fcAccountCode").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("fcAccountName").Width = 420
            .Columns("fcAccountName").HeaderText = "Account Title"
            .Columns("fcAccountName").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("fcAccountType").Visible = False
        End With
    End Sub

End Class