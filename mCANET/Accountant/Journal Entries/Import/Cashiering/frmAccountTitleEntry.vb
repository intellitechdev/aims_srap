﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading

Public Class frmAccountTitleEntry

    Private gCon As New Clsappconfiguration()
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring
    Public xModule As String
    Dim isdebit As Boolean

    Private Sub frmAccountTitleEntry_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        LoadGrid()
    End Sub

    Private Sub LoadGrid()
        Dim keyCompany As New Guid(gCompanyID)
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(gCon.cnstring, "spu_Cashiering_AccountEntry_View")
            GrdAccounts.Rows.Clear()
            GrdAccounts.DataSource = ds.Tables(0)
            GrdAccounts.Columns(0).Width = 100
            GrdAccounts.Columns(0).HeaderText = "Entry"
            GrdAccounts.Columns(0).ReadOnly = True
            GrdAccounts.Columns(1).HeaderText = "Account Code"
            GrdAccounts.Columns(1).ReadOnly = True
            GrdAccounts.Columns(1).Width = 100
            GrdAccounts.Columns(2).HeaderText = "Account Title"
            GrdAccounts.Columns(2).ReadOnly = True
            GrdAccounts.Columns(2).Width = 200
            GrdAccounts.Columns(3).Visible = False
            'GrdAccounts.Columns(4).Visible = False
            'GrdAccounts.Columns(7).Visible = False
            'GrdAccounts.Columns(5).HeaderText = "Account Code"
            'GrdAccounts.Columns(5).ReadOnly = True
            'GrdAccounts.Columns(5).Width = 100
            'GrdAccounts.Columns(6).HeaderText = "Account Title"
            'GrdAccounts.Columns(6).ReadOnly = True
            'GrdAccounts.Columns(6).Width = 200

            Dim btn As New DataGridViewButtonColumn()
            GrdAccounts.Columns.Add(btn)
            btn.HeaderText = "Edit Account"
            btn.Text = "Edit"
            btn.Name = "btn"
            btn.UseColumnTextForButtonValue = True

            'Dim btn2 As New DataGridViewButtonColumn()
            'GrdAccounts.Columns.Add(btn2)
            'btn2.HeaderText = "Debit/Credit"
            'btn2.Text = "Edit"
            'btn2.Name = "btn"
            'btn2.UseColumnTextForButtonValue = True

        Catch ex As Exception
            'MessageBox.Show(ex.ToString, "Account List Description", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub GrdAccounts_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GrdAccounts.CellClick
        If e.ColumnIndex = 4 Or e.ColumnIndex = 0 Then
            Dim xRowIndex As Integer = GrdAccounts.CurrentRow.Index
            'MsgBox(("Row : " + e.RowIndex.ToString & "  Col : ") + e.ColumnIndex.ToString)
            frmCOAFilter.xModule = "Entry"
            frmCOAFilter.ShowDialog()
            If frmCOAFilter.DialogResult = DialogResult.OK Then
                Dim cAccountTitle As String = frmCOAFilter.grdCoAList.SelectedRows(0).Cells("acnt_name").Value.ToString()
                Dim cAccountCode As String = frmCOAFilter.grdCoAList.SelectedRows(0).Cells("acnt_code").Value.ToString()
                Dim cAccounts As String = frmCOAFilter.grdCoAList.SelectedRows(0).Cells("acnt_id").Value.ToString()

                GrdAccounts.Item(1, xRowIndex).Value = cAccountCode
                GrdAccounts.Item(2, xRowIndex).Value = cAccountTitle
                GrdAccounts.Item(3, xRowIndex).Value = cAccounts
                'GrdAccounts.Item(7, xRowIndex).Value = 1
            End If
        End If

        'If e.ColumnIndex = 9 Or e.ColumnIndex = 1 Then
        '    Dim xRowIndex As Integer = GrdAccounts.CurrentRow.Index
        '    'MsgBox(("Row : " + e.RowIndex.ToString & "  Col : ") + e.ColumnIndex.ToString)
        '    frmAccountTitleFilter.ShowDialog()
        '    If frmAccountTitleFilter.DialogResult = DialogResult.OK Then
        '        Dim cAccountTitle As String = frmAccountTitleFilter.dgvAccount.SelectedRows(0).Cells("fcAccountName").Value.ToString()
        '        Dim cAccountCode As String = frmAccountTitleFilter.dgvAccount.SelectedRows(0).Cells("fcAccountCode").Value.ToString()
        '        Dim cAccounts As String = frmAccountTitleFilter.dgvAccount.SelectedRows(0).Cells("fxkey_AccountID").Value.ToString()

        '        GrdAccounts.Item(5, xRowIndex).Value = cAccountCode
        '        GrdAccounts.Item(6, xRowIndex).Value = cAccountTitle
        '        GrdAccounts.Item(4, xRowIndex).Value = cAccounts
        '        GrdAccounts.Item(7, xRowIndex).Value = 2
        '    End If
        'End If
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnUpdate_Click(sender As System.Object, e As System.EventArgs) Handles btnUpdate.Click
        Try
            For Each row As DataGridViewRow In GrdAccounts.Rows

                'If row.Cells(7).Value = 1 Then
                UPDATEENTRY(row.Cells(0).Value.ToString, 1, row.Cells(3).Value.ToString)
                'ElseIf row.Cells(7).Value = 2 Then
                '    UPDATEENTRY(row.Cells(0).Value.ToString, 2, row.Cells(4).Value.ToString)
                'End If
            Next

            LoadGrid()
            MessageBox.Show("Record Succesfully Updated!")
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString)
        End Try
        

    End Sub

    Private Sub UPDATEENTRY(ByVal ID As String, ByVal Mode As Integer, ByVal AccountID As String)

        Dim dAccountID As New Guid(AccountID)
        SqlHelper.ExecuteNonQuery(gCon.cnstring, "spu_Cashiering_AccountEntry_Update",
                                  New SqlParameter("@ID", ID),
                                  New SqlParameter("@Mode", Mode),
                                  New SqlParameter("@AccountID", dAccountID))
    End Sub
End Class