﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading

Public Class frmCashiering

    Private gCon As New Clsappconfiguration()
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring
    Public OKPress As Boolean = False
    'Public Acc_ID As String
    'Public Acc_Name As String
    'Public Acc_Code As String
    'Dim dtType As DataTable

    Private Sub AccountTitleToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AccountTitleToolStripMenuItem.Click
        frmAccountTitleEntry.ShowDialog()
    End Sub

    'Private Sub LoadType(ByVal Type As String)
    '    Dim ds As DataTable = SqlHelper.ExecuteDataset(gCon.cnstring, "spu_Cashiering_AccountEntry_View").Tables(0)

    '    Dim expression As String
    '    expression = "PkCashieringAccountID = '" + Type + "'"

    '    Dim foundRows() As DataRow
    '    foundRows = ds.Select(expression)

    '    Dim i As Integer
    '    For i = 0 To foundRows.GetUpperBound(0)
    '        lblTitle.Text = foundRows(i)(2).ToString()
    '        Acc_ID = foundRows(i)(3).ToString()
    '        Acc_Code = foundRows(i)(1).ToString()
    '        Acc_Name = foundRows(i)(2).ToString()
    '    Next i

    'End Sub

    Private Sub frmCashiering_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        LoadGridDate()
    End Sub

    Private Sub cboType_SelectedIndexChanged(sender As System.Object, e As System.EventArgs)
        'LoadType()
        'LoadGrid()
    End Sub

    Private Sub LoadGrid(ByVal Deyt As Date)
        Dim keyCompany As New Guid(gCompanyID)
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(gCon.cnstring, "spu_Cashiering_DailySales_View",
                                                         New SqlParameter("@Date", Deyt))

            GrdAccounts.DataSource = ds.Tables(0)
            GrdAccounts.ReadOnly = True
            GrdAccounts.Columns(0).Visible = False
            GrdAccounts.Columns(1).Visible = False
            GrdAccounts.Columns(2).Visible = False
            'GrdAccounts.Columns(6).Visible = False
            GrdAccounts.Columns(7).Visible = False
            GrdAccounts.Columns(8).Visible = False
            GrdAccounts.Columns(3).HeaderText = "Members ID"
            GrdAccounts.Columns(3).Width = 150
            GrdAccounts.Columns(4).HeaderText = "Date"
            GrdAccounts.Columns(4).Width = 100
            GrdAccounts.Columns(5).HeaderText = "Sales Amount"
            GrdAccounts.Columns(5).Width = 150
            GrdAccounts.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            GrdAccounts.Columns(6).HeaderText = "Credit"
        Catch ex As Exception
            'MessageBox.Show(ex.ToString, "Account List Description", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub LoadGridDate()
        Dim keyCompany As New Guid(gCompanyID)
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(gCon.cnstring, "spu_Cashiering_GetPostedDate")

            dgvDate.DataSource = ds.Tables(0)
            dgvDate.ReadOnly = True
            dgvDate.Columns(1).HeaderText = "Date"
            dgvDate.Columns(1).ReadOnly = True
            dgvDate.Columns(1).Width = 100

        Catch ex As Exception
            'MessageBox.Show(ex.ToString, "Account List Description", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        OKPress = False
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        LoadGridDate()
    End Sub

    Private Sub btnSelect_Click(sender As System.Object, e As System.EventArgs) Handles btnSelect.Click

        OKPress = True
        Me.Close()

    End Sub

    Private Sub GrdAccounts_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GrdAccounts.CellClick

    End Sub

    Private Sub dgvDate_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDate.CellClick
        For Each row As DataGridViewRow In dgvDate.SelectedRows            
            LoadGrid(row.Cells(0).Value.ToString)
        Next
    End Sub

    Private Sub CashieringMemberIDToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CashieringMemberIDToolStripMenuItem.Click
        frmMemberSetup.ShowDialog()
    End Sub
End Class