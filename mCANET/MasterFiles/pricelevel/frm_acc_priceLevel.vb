Public Class frm_acc_priceLevel

    Private Sub NewToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_New.Click
        frm_MF_priceLevelAddEdit.MdiParent = Me.MdiParent
        frm_MF_priceLevelAddEdit.Show()
    End Sub

    Private Sub EditPriceLevelToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_Edit.Click
        frm_MF_priceLevelAddEdit.MdiParent = Me.MdiParent
        frm_MF_priceLevelAddEdit.Text = "Edit Price Level"
        frm_MF_priceLevelAddEdit.Show()
    End Sub

    Private Sub DuplicateToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_Duplicate.Click
        frm_MF_priceLevelAddEdit.MdiParent = Me.MdiParent
        frm_MF_priceLevelAddEdit.Show()
    End Sub
End Class