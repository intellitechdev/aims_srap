﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmCostCenter_AddEdit
    Private CostCenterID As String

    Private Sub AEDDoctype_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadCostCenter()
    End Sub
    Private Sub LoadCostCenter()
        Dim gcon As New Clsappconfiguration

        Try
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, "usp_m_CostCenter_Load", _
                                           New SqlParameter("@companyID", gCompanyID()))

            grdCostCenter.DataSource = ds.Tables(0)
            grdCostCenter.Columns("pkCostCenter").Visible = False
            grdCostCenter.Columns("fxKeyCompany").Visible = False
            grdCostCenter.Columns("fcCostCenter").HeaderText = "Cost Center"


        Catch ex As Exception
            MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK)
        End Try
        
    End Sub
    Private Sub AddEdit(ByVal CostCenterID As String, ByVal CostCenterName As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "usp_m_CostCenter_Insert_Update", _
                New SqlParameter("@pkCostCenter", CostCenterID), _
                New SqlParameter("@costCenter", CostCenterName), _
                New SqlParameter("@companyID", gCompanyID()))
            trans.Commit()

        Catch ex As Exception
            trans.Rollback()
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
    Private Sub deleteDoctype(ByVal costCenterID As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "usp_m_CostCenter_Delete", _
                                      New SqlParameter("@pkCostCenter", costCenterID))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Delete Cost Center")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If Me.btnClose.Text = "Cancel" Then
            Me.txtName.Clear()
            Me.btnEdit.Enabled = True
            Me.btnDelete.Enabled = True
            btnNew.Enabled = True
            btnNew.Text = "New"
            btnClose.Text = "Close"
            LoadCostCenter()
        Else
            Me.Close()
        End If
    End Sub
    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        If Me.btnNew.Text = "New" Then
            Me.txtName.Enabled = True
            Me.txtName.Clear()
            Me.btnNew.Text = "Update"
            Me.btnEdit.Enabled = False
            Me.btnDelete.Enabled = False
            Me.btnClose.Text = "Cancel"

            CostCenterID = Nothing
        ElseIf Me.btnNew.Text = "Update" Then
            If txtName.Text = "" Then
                MessageBox.Show("Please fill up the Fields.", "Information", MessageBoxButtons.OK)
            Else
                Call AddEdit(CostCenterID, Me.txtName.Text)
                btnNew.Text = "New"
                Me.txtName.Enabled = False
                Me.btnClose.Text = "Close"
                Me.btnEdit.Enabled = True
                Me.btnDelete.Enabled = True
                Me.txtName.Clear()
                Call LoadCostCenter()
                MessageBox.Show("Successfully added.", "Information", MessageBoxButtons.OK)
            End If
        End If
    End Sub
    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If Me.grdCostCenter.RowCount = 0 Then
            MessageBox.Show("There is no Data Display", "User ERROR", MessageBoxButtons.OK)
        Else

            Try
                CostCenterID = Me.grdCostCenter.CurrentRow.Cells("pkCostCenter").Value.ToString()

                Dim x As New DialogResult
                x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
                If x = System.Windows.Forms.DialogResult.OK Then
                    Call deleteDoctype(CostCenterID)
                    Call LoadCostCenter()
                ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
                    MessageBox.Show("Cancelled", "Information", MessageBoxButtons.OK)
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Error Deleting Data")
            End Try
        End If
    End Sub
    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If Me.grdCostCenter.RowCount = 0 Then
            MessageBox.Show("There is no Data Display", "User ERROR", MessageBoxButtons.OK)
        ElseIf Me.grdCostCenter.CurrentRow.Cells("pkCostCenter").Value = Nothing Then
            MessageBox.Show("Please Select the Row you want to Edit", "Information", MessageBoxButtons.OK)
        Else
            btnNew.Text = "Update"
            btnEdit.Enabled = False
            btnDelete.Enabled = False
            btnClose.Text = "Cancel"

            txtName.Enabled = True

            CostCenterID = grdCostCenter.CurrentRow.Cells("pkCostCenter").Value.ToString()
            txtName.Text = grdCostCenter.CurrentRow.Cells("fcCostCenter").Value
        End If
    End Sub
End Class