<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_acc_salesRep
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_acc_salesRep))
        Me.grdSalesRep = New System.Windows.Forms.DataGridView
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.ToolStripDropDownButton1 = New System.Windows.Forms.ToolStripDropDownButton
        Me.ts_New = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_Edit = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_Delete = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_MkInactive = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_ShowInactive = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_customizeCol = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_Use = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_Find = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_Print = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_Report = New System.Windows.Forms.ToolStripButton
        Me.chkIncludeInactive = New System.Windows.Forms.CheckBox
        CType(Me.grdSalesRep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'grdSalesRep
        '
        Me.grdSalesRep.AllowUserToAddRows = False
        Me.grdSalesRep.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdSalesRep.Location = New System.Drawing.Point(0, 49)
        Me.grdSalesRep.Name = "grdSalesRep"
        Me.grdSalesRep.Size = New System.Drawing.Size(421, 150)
        Me.grdSalesRep.TabIndex = 0
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripDropDownButton1, Me.ts_Report})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(421, 25)
        Me.ToolStrip1.TabIndex = 1
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripDropDownButton1
        '
        Me.ToolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripDropDownButton1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_New, Me.ts_Edit, Me.ts_Delete, Me.ToolStripSeparator1, Me.ts_MkInactive, Me.ts_ShowInactive, Me.ts_customizeCol, Me.ToolStripSeparator2, Me.ts_Use, Me.ts_Find, Me.ToolStripSeparator3, Me.ts_Print})
        Me.ToolStripDropDownButton1.Image = CType(resources.GetObject("ToolStripDropDownButton1.Image"), System.Drawing.Image)
        Me.ToolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripDropDownButton1.Name = "ToolStripDropDownButton1"
        Me.ToolStripDropDownButton1.Size = New System.Drawing.Size(67, 22)
        Me.ToolStripDropDownButton1.Text = "S&ales Rep"
        '
        'ts_New
        '
        Me.ts_New.Name = "ts_New"
        Me.ts_New.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.ts_New.Size = New System.Drawing.Size(189, 22)
        Me.ts_New.Text = "New"
        '
        'ts_Edit
        '
        Me.ts_Edit.Name = "ts_Edit"
        Me.ts_Edit.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.ts_Edit.Size = New System.Drawing.Size(189, 22)
        Me.ts_Edit.Text = "Edit"
        '
        'ts_Delete
        '
        Me.ts_Delete.Name = "ts_Delete"
        Me.ts_Delete.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.ts_Delete.Size = New System.Drawing.Size(189, 22)
        Me.ts_Delete.Text = "Delete"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(186, 6)
        '
        'ts_MkInactive
        '
        Me.ts_MkInactive.Name = "ts_MkInactive"
        Me.ts_MkInactive.Size = New System.Drawing.Size(189, 22)
        Me.ts_MkInactive.Text = "Make Inactive "
        '
        'ts_ShowInactive
        '
        Me.ts_ShowInactive.Name = "ts_ShowInactive"
        Me.ts_ShowInactive.Size = New System.Drawing.Size(189, 22)
        Me.ts_ShowInactive.Text = "Show Inactive "
        '
        'ts_customizeCol
        '
        Me.ts_customizeCol.Name = "ts_customizeCol"
        Me.ts_customizeCol.Size = New System.Drawing.Size(189, 22)
        Me.ts_customizeCol.Text = "Customize Columns..."
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(186, 6)
        Me.ToolStripSeparator2.Visible = False
        '
        'ts_Use
        '
        Me.ts_Use.Name = "ts_Use"
        Me.ts_Use.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.U), System.Windows.Forms.Keys)
        Me.ts_Use.Size = New System.Drawing.Size(189, 22)
        Me.ts_Use.Text = "Use"
        Me.ts_Use.Visible = False
        '
        'ts_Find
        '
        Me.ts_Find.Name = "ts_Find"
        Me.ts_Find.Size = New System.Drawing.Size(189, 22)
        Me.ts_Find.Text = "Find in Transactions"
        Me.ts_Find.Visible = False
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(186, 6)
        '
        'ts_Print
        '
        Me.ts_Print.Name = "ts_Print"
        Me.ts_Print.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.ts_Print.Size = New System.Drawing.Size(189, 22)
        Me.ts_Print.Text = "Print List..."
        '
        'ts_Report
        '
        Me.ts_Report.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ts_Report.Image = CType(resources.GetObject("ts_Report.Image"), System.Drawing.Image)
        Me.ts_Report.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_Report.Name = "ts_Report"
        Me.ts_Report.Size = New System.Drawing.Size(49, 22)
        Me.ts_Report.Text = "Re&ports"
        Me.ts_Report.Visible = False
        '
        'chkIncludeInactive
        '
        Me.chkIncludeInactive.AutoSize = True
        Me.chkIncludeInactive.Location = New System.Drawing.Point(14, 29)
        Me.chkIncludeInactive.Name = "chkIncludeInactive"
        Me.chkIncludeInactive.Size = New System.Drawing.Size(118, 17)
        Me.chkIncludeInactive.TabIndex = 2
        Me.chkIncludeInactive.Text = "Include Inactive"
        Me.chkIncludeInactive.UseVisualStyleBackColor = True
        '
        'frm_acc_salesRep
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(421, 198)
        Me.Controls.Add(Me.chkIncludeInactive)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.grdSalesRep)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(429, 232)
        Me.Name = "frm_acc_salesRep"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Sales Rep"
        CType(Me.grdSalesRep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents grdSalesRep As System.Windows.Forms.DataGridView
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripDropDownButton1 As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents ts_New As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_Edit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_Delete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_MkInactive As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_ShowInactive As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_customizeCol As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_Print As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_Use As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_Find As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_Report As System.Windows.Forms.ToolStripButton
    Friend WithEvents chkIncludeInactive As System.Windows.Forms.CheckBox
End Class
