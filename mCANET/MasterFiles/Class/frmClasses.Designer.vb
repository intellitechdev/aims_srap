<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmClasses
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmClasses))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.DGVClasses = New System.Windows.Forms.DataGridView
        Me.TreeViewClass = New System.Windows.Forms.TreeView
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.RefreshClassesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.AddClassToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.EditClassToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.DeleteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.ClassToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.DeleteClaseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.EditClassToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AddClassToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Panel1.SuspendLayout()
        CType(Me.DGVClasses, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.DGVClasses)
        Me.Panel1.Controls.Add(Me.TreeViewClass)
        Me.Panel1.Controls.Add(Me.MenuStrip1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(491, 293)
        Me.Panel1.TabIndex = 2
        '
        'DGVClasses
        '
        Me.DGVClasses.AllowUserToAddRows = False
        Me.DGVClasses.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DGVClasses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVClasses.Location = New System.Drawing.Point(3, 177)
        Me.DGVClasses.Name = "DGVClasses"
        Me.DGVClasses.Size = New System.Drawing.Size(484, 80)
        Me.DGVClasses.TabIndex = 5
        Me.DGVClasses.Visible = False
        '
        'TreeViewClass
        '
        Me.TreeViewClass.ContextMenuStrip = Me.ContextMenuStrip1
        Me.TreeViewClass.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TreeViewClass.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeViewClass.HideSelection = False
        Me.TreeViewClass.HotTracking = True
        Me.TreeViewClass.Location = New System.Drawing.Point(0, 0)
        Me.TreeViewClass.Name = "TreeViewClass"
        Me.TreeViewClass.ShowNodeToolTips = True
        Me.TreeViewClass.Size = New System.Drawing.Size(491, 269)
        Me.TreeViewClass.TabIndex = 0
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuItem
        Me.ContextMenuStrip1.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RefreshClassesToolStripMenuItem, Me.ToolStripSeparator1, Me.AddClassToolStripMenuItem1, Me.EditClassToolStripMenuItem1, Me.DeleteToolStripMenuItem})
        Me.ContextMenuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Table
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(163, 98)
        '
        'RefreshClassesToolStripMenuItem
        '
        Me.RefreshClassesToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.reload
        Me.RefreshClassesToolStripMenuItem.Name = "RefreshClassesToolStripMenuItem"
        Me.RefreshClassesToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.RefreshClassesToolStripMenuItem.Text = "Refresh Classes"
        Me.RefreshClassesToolStripMenuItem.ToolTipText = "Reload classes from the database."
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(159, 6)
        '
        'AddClassToolStripMenuItem1
        '
        Me.AddClassToolStripMenuItem1.Enabled = False
        Me.AddClassToolStripMenuItem1.Image = Global.CSAcctg.My.Resources.Resources.add
        Me.AddClassToolStripMenuItem1.Name = "AddClassToolStripMenuItem1"
        Me.AddClassToolStripMenuItem1.Size = New System.Drawing.Size(162, 22)
        Me.AddClassToolStripMenuItem1.Text = "Add Class"
        Me.AddClassToolStripMenuItem1.ToolTipText = "Let you to add or create new classes"
        '
        'EditClassToolStripMenuItem1
        '
        Me.EditClassToolStripMenuItem1.Enabled = False
        Me.EditClassToolStripMenuItem1.Image = Global.CSAcctg.My.Resources.Resources.edit
        Me.EditClassToolStripMenuItem1.Name = "EditClassToolStripMenuItem1"
        Me.EditClassToolStripMenuItem1.Size = New System.Drawing.Size(162, 22)
        Me.EditClassToolStripMenuItem1.Text = "Edit Class"
        Me.EditClassToolStripMenuItem1.ToolTipText = "Let you edit the selected Class if it has no subclass."
        '
        'DeleteToolStripMenuItem
        '
        Me.DeleteToolStripMenuItem.Enabled = False
        Me.DeleteToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.delete
        Me.DeleteToolStripMenuItem.Name = "DeleteToolStripMenuItem"
        Me.DeleteToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.DeleteToolStripMenuItem.Text = "Delete Class"
        Me.DeleteToolStripMenuItem.ToolTipText = "let you to delete selected class and all of its subclasses."
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClassToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 269)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.ShowItemToolTips = True
        Me.MenuStrip1.Size = New System.Drawing.Size(491, 24)
        Me.MenuStrip1.TabIndex = 1
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ClassToolStripMenuItem
        '
        Me.ClassToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DeleteClaseToolStripMenuItem, Me.EditClassToolStripMenuItem, Me.AddClassToolStripMenuItem})
        Me.ClassToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.configure
        Me.ClassToolStripMenuItem.Name = "ClassToolStripMenuItem"
        Me.ClassToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.ClassToolStripMenuItem.Text = "Class"
        '
        'DeleteClaseToolStripMenuItem
        '
        Me.DeleteClaseToolStripMenuItem.Enabled = False
        Me.DeleteClaseToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.delete
        Me.DeleteClaseToolStripMenuItem.Name = "DeleteClaseToolStripMenuItem"
        Me.DeleteClaseToolStripMenuItem.Size = New System.Drawing.Size(144, 22)
        Me.DeleteClaseToolStripMenuItem.Text = "Delete Class"
        Me.DeleteClaseToolStripMenuItem.ToolTipText = "let you to delete selected class and all of its subclasses"
        '
        'EditClassToolStripMenuItem
        '
        Me.EditClassToolStripMenuItem.Enabled = False
        Me.EditClassToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.edit
        Me.EditClassToolStripMenuItem.Name = "EditClassToolStripMenuItem"
        Me.EditClassToolStripMenuItem.Size = New System.Drawing.Size(144, 22)
        Me.EditClassToolStripMenuItem.Text = "Edit Class"
        Me.EditClassToolStripMenuItem.ToolTipText = "Let you edit the selected Class if it has no subclass"
        '
        'AddClassToolStripMenuItem
        '
        Me.AddClassToolStripMenuItem.Checked = True
        Me.AddClassToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.AddClassToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.add
        Me.AddClassToolStripMenuItem.Name = "AddClassToolStripMenuItem"
        Me.AddClassToolStripMenuItem.Size = New System.Drawing.Size(144, 22)
        Me.AddClassToolStripMenuItem.Text = "Add Class"
        Me.AddClassToolStripMenuItem.ToolTipText = "Let you to add or create new classes"
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 10000
        Me.ToolTip1.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ToolTip1.InitialDelay = 500
        Me.ToolTip1.ReshowDelay = 100
        Me.ToolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info
        Me.ToolTip1.ToolTipTitle = "Class Information"
        '
        'frmClasses
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(491, 293)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmClasses"
        Me.Text = "Classes"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DGVClasses, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents TreeViewClass As System.Windows.Forms.TreeView
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ClassToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeleteClaseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditClassToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddClassToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DGVClasses As System.Windows.Forms.DataGridView
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents AddClassToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditClassToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeleteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RefreshClassesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
End Class
