Imports Microsoft.ApplicationBlocks.Data
Imports system.Data.SqlClient
Imports System.Text.RegularExpressions

Public Class frmStatutoryAllocations_AddEdit
    Private gCon As New Clsappconfiguration()

#Region "Properties"

    Private mode As String
    Public Property GetMode() As String
        Get
            Return mode
        End Get
        Set(ByVal value As String)
            mode = value
        End Set
    End Property

    Private allocationID As String
    Public Property GetAllocationID() As String
        Get
            Return allocationID
        End Get
        Set(ByVal value As String)
            allocationID = value
        End Set
    End Property

    Private allocation As String
    Public Property GetAllocationName() As String
        Get
            Return allocation
        End Get
        Set(ByVal value As String)
            allocation = value
        End Set
    End Property

    Private percentage As Decimal
    Public Property GetPercentage() As Decimal
        Get
            Return percentage
        End Get
        Set(ByVal value As Decimal)
            percentage = value
        End Set
    End Property

    Private category As String
    Public Property GetCategory() As String
        Get
            Return category
        End Get
        Set(ByVal value As String)
            category = value
        End Set
    End Property
#End Region

#Region "Events"
    Private Sub frmStatutoryAllocations_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call InitializeMode()
        Call LoadInitialData()
    End Sub

    Private Sub btnDone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDone.Click

        Try
            If txtAllocation.Text.Trim <> "" Or txtPercentage.Text.Trim <> "" Or cboCategory.Text.Trim <> "" Then
                Call InsertUpdateStatutoryAllocations()
                MessageBox.Show(GetMode().ToString() & _
                        " Allocation Successful!", "Statutory Allocation", _
                        MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

            frmStatutoryAllocations.Activate()
            Close()
        Catch ex As Exception
            MessageBox.Show("User Error: Invalid Inputs. Please check your entries. ", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Close()
    End Sub

#End Region

#Region "Functions and Subroutines"
    Public Sub InitializeMode()
        Me.Text = GetMode() + " Statutory Allocations"
        Me.btnDone.Text = GetMode()
    End Sub

    Public Sub LoadInitialData()
        cboCategory.Items.Clear()

        Using rd As SqlDataReader = (SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "Masterfile_StatutoryAllocations_LoadCategories", _
                    New SqlParameter("@companyID", gCompanyID())))
            While rd.Read
                cboCategory.Items.Add(rd.Item("fcCategory"))
            End While
        End Using

    End Sub

    Public Sub InsertUpdateStatutoryAllocations()
        Try
            Dim allocationID As String = GetAllocationID()
            Dim allocation As String = txtAllocation.Text
            Dim percent As Decimal = Decimal.Parse(txtPercentage.Text)
            Dim category As String = cboCategory.Text

            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "Masterfile_StatutoryAllocations_InsertUpdate", _
                New SqlParameter("@companyID", gCompanyID()), _
                New SqlParameter("@allocationID", GetAllocationID()), _
                New SqlParameter("@fcStatutoryAllocations", allocation), _
                New SqlParameter("@fnPercentAllocation", percent), _
                New SqlParameter("@fcCategory", category))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

    
    Private Sub txtPercentage_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPercentage.KeyPress
        Dim rex As Regex = New Regex("^[0-9]*[.]{0,1}[0-9]{0,1}$")
        If (Char.IsDigit(e.KeyChar) Or e.KeyChar.ToString() = "." Or e.KeyChar = CChar(ChrW(Keys.Back))) Then
            If (txtPercentage.Text.Trim() <> "") Then
                If (rex.IsMatch(txtPercentage.Text) = False And e.KeyChar <> CChar(ChrW(Keys.Back))) Then
                    e.Handled = True
                    MessageBox.Show("You are Not Allowed To Enter More then 2 Decimal!!")
                Else
                End If
            End If
        Else
            e.Handled = True
        End If
    End Sub
End Class