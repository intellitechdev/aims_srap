<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChartOfAccounts_Edit_Multiple
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmChartOfAccounts_Edit_Multiple))
        Me.PanelAccountType = New System.Windows.Forms.Panel()
        Me.cboProject = New MTGCComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtAccountTypeDesc = New System.Windows.Forms.TextBox()
        Me.cboAccountTypes = New MTGCComboBox()
        Me.lbAccountType_Instruction = New System.Windows.Forms.Label()
        Me.lblAccountType = New System.Windows.Forms.Label()
        Me.PanelButtons = New System.Windows.Forms.Panel()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.PanelMain = New System.Windows.Forms.Panel()
        Me.chDebit = New System.Windows.Forms.CheckBox()
        Me.lblDescription = New System.Windows.Forms.Label()
        Me.txtAccountDesc = New System.Windows.Forms.TextBox()
        Me.chkIsSubAccountOf = New System.Windows.Forms.CheckBox()
        Me.cboSubAccountOf = New MTGCComboBox()
        Me.lblAccountSubOf = New System.Windows.Forms.Label()
        Me.lblAccountStructure = New System.Windows.Forms.Label()
        Me.chkIsActive = New System.Windows.Forms.CheckBox()
        Me.cboLevel = New System.Windows.Forms.ComboBox()
        Me.lblLevel = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblAccountCode = New System.Windows.Forms.Label()
        Me.txtAccountCode = New System.Windows.Forms.TextBox()
        Me.txtAccountName = New System.Windows.Forms.TextBox()
        Me.lblAccount = New System.Windows.Forms.Label()
        Me.PanelAccountType.SuspendLayout()
        Me.PanelButtons.SuspendLayout()
        Me.PanelMain.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelAccountType
        '
        Me.PanelAccountType.Controls.Add(Me.cboProject)
        Me.PanelAccountType.Controls.Add(Me.Label2)
        Me.PanelAccountType.Controls.Add(Me.txtAccountTypeDesc)
        Me.PanelAccountType.Controls.Add(Me.cboAccountTypes)
        Me.PanelAccountType.Controls.Add(Me.lbAccountType_Instruction)
        Me.PanelAccountType.Controls.Add(Me.lblAccountType)
        Me.PanelAccountType.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelAccountType.Location = New System.Drawing.Point(0, 0)
        Me.PanelAccountType.Name = "PanelAccountType"
        Me.PanelAccountType.Size = New System.Drawing.Size(489, 165)
        Me.PanelAccountType.TabIndex = 0
        '
        'cboProject
        '
        Me.cboProject.ArrowBoxColor = System.Drawing.SystemColors.Control
        Me.cboProject.ArrowColor = System.Drawing.Color.Black
        Me.cboProject.BindedControl = CType(resources.GetObject("cboProject.BindedControl"), MTGCComboBox.ControlloAssociato)
        Me.cboProject.BorderStyle = MTGCComboBox.TipiBordi.Fixed3D
        Me.cboProject.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.cboProject.ColumnNum = 2
        Me.cboProject.ColumnWidth = "100;0"
        Me.cboProject.DisabledArrowBoxColor = System.Drawing.SystemColors.Control
        Me.cboProject.DisabledArrowColor = System.Drawing.Color.LightGray
        Me.cboProject.DisabledBackColor = System.Drawing.SystemColors.Control
        Me.cboProject.DisabledBorderColor = System.Drawing.SystemColors.InactiveBorder
        Me.cboProject.DisabledForeColor = System.Drawing.SystemColors.GrayText
        Me.cboProject.DisplayMember = "Text"
        Me.cboProject.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cboProject.DropDownBackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.cboProject.DropDownForeColor = System.Drawing.Color.Black
        Me.cboProject.DropDownStyle = MTGCComboBox.CustomDropDownStyle.DropDown
        Me.cboProject.DropDownWidth = 267
        Me.cboProject.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboProject.GridLineColor = System.Drawing.Color.LightGray
        Me.cboProject.GridLineHorizontal = False
        Me.cboProject.GridLineVertical = False
        Me.cboProject.LoadingType = MTGCComboBox.CaricamentoCombo.ComboBoxItem
        Me.cboProject.Location = New System.Drawing.Point(146, 10)
        Me.cboProject.ManagingFastMouseMoving = True
        Me.cboProject.ManagingFastMouseMovingInterval = 30
        Me.cboProject.Name = "cboProject"
        Me.cboProject.SelectedItem = Nothing
        Me.cboProject.SelectedValue = Nothing
        Me.cboProject.Size = New System.Drawing.Size(267, 24)
        Me.cboProject.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.MediumBlue
        Me.Label2.Location = New System.Drawing.Point(12, 13)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 15)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Project:"
        '
        'txtAccountTypeDesc
        '
        Me.txtAccountTypeDesc.BackColor = System.Drawing.SystemColors.Window
        Me.txtAccountTypeDesc.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtAccountTypeDesc.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAccountTypeDesc.Location = New System.Drawing.Point(146, 101)
        Me.txtAccountTypeDesc.Multiline = True
        Me.txtAccountTypeDesc.Name = "txtAccountTypeDesc"
        Me.txtAccountTypeDesc.ReadOnly = True
        Me.txtAccountTypeDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtAccountTypeDesc.Size = New System.Drawing.Size(267, 48)
        Me.txtAccountTypeDesc.TabIndex = 3
        '
        'cboAccountTypes
        '
        Me.cboAccountTypes.ArrowBoxColor = System.Drawing.SystemColors.Control
        Me.cboAccountTypes.ArrowColor = System.Drawing.Color.Black
        Me.cboAccountTypes.BindedControl = CType(resources.GetObject("cboAccountTypes.BindedControl"), MTGCComboBox.ControlloAssociato)
        Me.cboAccountTypes.BorderStyle = MTGCComboBox.TipiBordi.Fixed3D
        Me.cboAccountTypes.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.cboAccountTypes.ColumnNum = 2
        Me.cboAccountTypes.ColumnWidth = "100;0"
        Me.cboAccountTypes.DisabledArrowBoxColor = System.Drawing.SystemColors.Control
        Me.cboAccountTypes.DisabledArrowColor = System.Drawing.Color.LightGray
        Me.cboAccountTypes.DisabledBackColor = System.Drawing.SystemColors.Control
        Me.cboAccountTypes.DisabledBorderColor = System.Drawing.SystemColors.InactiveBorder
        Me.cboAccountTypes.DisabledForeColor = System.Drawing.SystemColors.GrayText
        Me.cboAccountTypes.DisplayMember = "Text"
        Me.cboAccountTypes.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cboAccountTypes.DropDownBackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.cboAccountTypes.DropDownForeColor = System.Drawing.Color.Black
        Me.cboAccountTypes.DropDownStyle = MTGCComboBox.CustomDropDownStyle.DropDown
        Me.cboAccountTypes.DropDownWidth = 267
        Me.cboAccountTypes.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAccountTypes.GridLineColor = System.Drawing.Color.LightGray
        Me.cboAccountTypes.GridLineHorizontal = False
        Me.cboAccountTypes.GridLineVertical = False
        Me.cboAccountTypes.LoadingType = MTGCComboBox.CaricamentoCombo.ComboBoxItem
        Me.cboAccountTypes.Location = New System.Drawing.Point(146, 68)
        Me.cboAccountTypes.ManagingFastMouseMoving = True
        Me.cboAccountTypes.ManagingFastMouseMovingInterval = 30
        Me.cboAccountTypes.Name = "cboAccountTypes"
        Me.cboAccountTypes.SelectedItem = Nothing
        Me.cboAccountTypes.SelectedValue = Nothing
        Me.cboAccountTypes.Size = New System.Drawing.Size(267, 24)
        Me.cboAccountTypes.TabIndex = 2
        '
        'lbAccountType_Instruction
        '
        Me.lbAccountType_Instruction.AutoSize = True
        Me.lbAccountType_Instruction.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbAccountType_Instruction.Location = New System.Drawing.Point(143, 46)
        Me.lbAccountType_Instruction.Name = "lbAccountType_Instruction"
        Me.lbAccountType_Instruction.Size = New System.Drawing.Size(270, 15)
        Me.lbAccountType_Instruction.TabIndex = 1
        Me.lbAccountType_Instruction.Text = "Choose the account type suited for this account."
        '
        'lblAccountType
        '
        Me.lblAccountType.AutoSize = True
        Me.lblAccountType.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccountType.ForeColor = System.Drawing.Color.MediumBlue
        Me.lblAccountType.Location = New System.Drawing.Point(12, 46)
        Me.lblAccountType.Name = "lblAccountType"
        Me.lblAccountType.Size = New System.Drawing.Size(84, 15)
        Me.lblAccountType.TabIndex = 0
        Me.lblAccountType.Text = "Account Type:"
        '
        'PanelButtons
        '
        Me.PanelButtons.Controls.Add(Me.btnClose)
        Me.PanelButtons.Controls.Add(Me.btnSave)
        Me.PanelButtons.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelButtons.Location = New System.Drawing.Point(0, 485)
        Me.PanelButtons.Name = "PanelButtons"
        Me.PanelButtons.Size = New System.Drawing.Size(489, 41)
        Me.PanelButtons.TabIndex = 2
        '
        'btnClose
        '
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Image = Global.CSAcctg.My.Resources.Resources.button_cancel1
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose.Location = New System.Drawing.Point(380, 5)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(104, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Image = Global.CSAcctg.My.Resources.Resources.apply
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.Location = New System.Drawing.Point(270, 6)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(104, 29)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'PanelMain
        '
        Me.PanelMain.Controls.Add(Me.chDebit)
        Me.PanelMain.Controls.Add(Me.lblDescription)
        Me.PanelMain.Controls.Add(Me.txtAccountDesc)
        Me.PanelMain.Controls.Add(Me.chkIsSubAccountOf)
        Me.PanelMain.Controls.Add(Me.cboSubAccountOf)
        Me.PanelMain.Controls.Add(Me.lblAccountSubOf)
        Me.PanelMain.Controls.Add(Me.lblAccountStructure)
        Me.PanelMain.Controls.Add(Me.chkIsActive)
        Me.PanelMain.Controls.Add(Me.cboLevel)
        Me.PanelMain.Controls.Add(Me.lblLevel)
        Me.PanelMain.Controls.Add(Me.Label1)
        Me.PanelMain.Controls.Add(Me.lblAccountCode)
        Me.PanelMain.Controls.Add(Me.txtAccountCode)
        Me.PanelMain.Controls.Add(Me.txtAccountName)
        Me.PanelMain.Controls.Add(Me.lblAccount)
        Me.PanelMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelMain.Location = New System.Drawing.Point(0, 165)
        Me.PanelMain.Name = "PanelMain"
        Me.PanelMain.Size = New System.Drawing.Size(489, 320)
        Me.PanelMain.TabIndex = 1
        '
        'chDebit
        '
        Me.chDebit.AutoSize = True
        Me.chDebit.Location = New System.Drawing.Point(371, 233)
        Me.chDebit.Name = "chDebit"
        Me.chDebit.Size = New System.Drawing.Size(51, 17)
        Me.chDebit.TabIndex = 14
        Me.chDebit.Text = "Debit"
        Me.chDebit.UseVisualStyleBackColor = True
        '
        'lblDescription
        '
        Me.lblDescription.AutoSize = True
        Me.lblDescription.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(152, 111)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(73, 15)
        Me.lblDescription.TabIndex = 5
        Me.lblDescription.Text = "Description:"
        '
        'txtAccountDesc
        '
        Me.txtAccountDesc.Location = New System.Drawing.Point(155, 129)
        Me.txtAccountDesc.Multiline = True
        Me.txtAccountDesc.Name = "txtAccountDesc"
        Me.txtAccountDesc.Size = New System.Drawing.Size(298, 43)
        Me.txtAccountDesc.TabIndex = 6
        '
        'chkIsSubAccountOf
        '
        Me.chkIsSubAccountOf.AutoSize = True
        Me.chkIsSubAccountOf.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIsSubAccountOf.Location = New System.Drawing.Point(218, 233)
        Me.chkIsSubAccountOf.Name = "chkIsSubAccountOf"
        Me.chkIsSubAccountOf.Size = New System.Drawing.Size(147, 19)
        Me.chkIsSubAccountOf.TabIndex = 11
        Me.chkIsSubAccountOf.Text = "Is this a sub-account?"
        Me.chkIsSubAccountOf.UseVisualStyleBackColor = True
        '
        'cboSubAccountOf
        '
        Me.cboSubAccountOf.ArrowBoxColor = System.Drawing.SystemColors.Control
        Me.cboSubAccountOf.ArrowColor = System.Drawing.Color.Black
        Me.cboSubAccountOf.BindedControl = CType(resources.GetObject("cboSubAccountOf.BindedControl"), MTGCComboBox.ControlloAssociato)
        Me.cboSubAccountOf.BorderStyle = MTGCComboBox.TipiBordi.Fixed3D
        Me.cboSubAccountOf.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.cboSubAccountOf.ColumnNum = 3
        Me.cboSubAccountOf.ColumnWidth = "250;120;0"
        Me.cboSubAccountOf.DisabledArrowBoxColor = System.Drawing.SystemColors.Control
        Me.cboSubAccountOf.DisabledArrowColor = System.Drawing.Color.LightGray
        Me.cboSubAccountOf.DisabledBackColor = System.Drawing.SystemColors.Control
        Me.cboSubAccountOf.DisabledBorderColor = System.Drawing.SystemColors.InactiveBorder
        Me.cboSubAccountOf.DisabledForeColor = System.Drawing.SystemColors.GrayText
        Me.cboSubAccountOf.DisplayMember = "Text"
        Me.cboSubAccountOf.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cboSubAccountOf.DropDownBackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.cboSubAccountOf.DropDownForeColor = System.Drawing.Color.Black
        Me.cboSubAccountOf.DropDownStyle = MTGCComboBox.CustomDropDownStyle.DropDown
        Me.cboSubAccountOf.DropDownWidth = 390
        Me.cboSubAccountOf.GridLineColor = System.Drawing.Color.LightGray
        Me.cboSubAccountOf.GridLineHorizontal = False
        Me.cboSubAccountOf.GridLineVertical = False
        Me.cboSubAccountOf.LoadingType = MTGCComboBox.CaricamentoCombo.ComboBoxItem
        Me.cboSubAccountOf.Location = New System.Drawing.Point(155, 287)
        Me.cboSubAccountOf.ManagingFastMouseMoving = True
        Me.cboSubAccountOf.ManagingFastMouseMovingInterval = 30
        Me.cboSubAccountOf.Name = "cboSubAccountOf"
        Me.cboSubAccountOf.SelectedItem = Nothing
        Me.cboSubAccountOf.SelectedValue = Nothing
        Me.cboSubAccountOf.Size = New System.Drawing.Size(267, 21)
        Me.cboSubAccountOf.TabIndex = 13
        '
        'lblAccountSubOf
        '
        Me.lblAccountSubOf.AutoSize = True
        Me.lblAccountSubOf.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccountSubOf.Location = New System.Drawing.Point(155, 269)
        Me.lblAccountSubOf.Name = "lblAccountSubOf"
        Me.lblAccountSubOf.Size = New System.Drawing.Size(188, 15)
        Me.lblAccountSubOf.TabIndex = 12
        Me.lblAccountSubOf.Text = "This Account is a sub-account of:"
        '
        'lblAccountStructure
        '
        Me.lblAccountStructure.AutoSize = True
        Me.lblAccountStructure.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccountStructure.ForeColor = System.Drawing.Color.MediumBlue
        Me.lblAccountStructure.Location = New System.Drawing.Point(12, 215)
        Me.lblAccountStructure.Name = "lblAccountStructure"
        Me.lblAccountStructure.Size = New System.Drawing.Size(110, 15)
        Me.lblAccountStructure.TabIndex = 8
        Me.lblAccountStructure.Text = "Account Structure:"
        '
        'chkIsActive
        '
        Me.chkIsActive.AutoSize = True
        Me.chkIsActive.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIsActive.Location = New System.Drawing.Point(155, 181)
        Me.chkIsActive.Name = "chkIsActive"
        Me.chkIsActive.Size = New System.Drawing.Size(165, 19)
        Me.chkIsActive.TabIndex = 7
        Me.chkIsActive.Text = "Is this an active account?"
        Me.chkIsActive.UseVisualStyleBackColor = True
        '
        'cboLevel
        '
        Me.cboLevel.FormattingEnabled = True
        Me.cboLevel.Items.AddRange(New Object() {"1", "2", "3", "4", "5"})
        Me.cboLevel.Location = New System.Drawing.Point(155, 233)
        Me.cboLevel.Name = "cboLevel"
        Me.cboLevel.Size = New System.Drawing.Size(46, 21)
        Me.cboLevel.TabIndex = 10
        '
        'lblLevel
        '
        Me.lblLevel.AutoSize = True
        Me.lblLevel.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLevel.Location = New System.Drawing.Point(152, 215)
        Me.lblLevel.Name = "lblLevel"
        Me.lblLevel.Size = New System.Drawing.Size(37, 15)
        Me.lblLevel.TabIndex = 9
        Me.lblLevel.Text = "Level:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(152, 60)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 15)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Name:"
        '
        'lblAccountCode
        '
        Me.lblAccountCode.AutoSize = True
        Me.lblAccountCode.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccountCode.Location = New System.Drawing.Point(152, 14)
        Me.lblAccountCode.Name = "lblAccountCode"
        Me.lblAccountCode.Size = New System.Drawing.Size(37, 15)
        Me.lblAccountCode.TabIndex = 1
        Me.lblAccountCode.Text = "Code:"
        '
        'txtAccountCode
        '
        Me.txtAccountCode.Location = New System.Drawing.Point(155, 32)
        Me.txtAccountCode.Name = "txtAccountCode"
        Me.txtAccountCode.Size = New System.Drawing.Size(188, 20)
        Me.txtAccountCode.TabIndex = 3
        '
        'txtAccountName
        '
        Me.txtAccountName.Location = New System.Drawing.Point(155, 79)
        Me.txtAccountName.Name = "txtAccountName"
        Me.txtAccountName.Size = New System.Drawing.Size(247, 20)
        Me.txtAccountName.TabIndex = 4
        '
        'lblAccount
        '
        Me.lblAccount.AutoSize = True
        Me.lblAccount.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccount.ForeColor = System.Drawing.Color.MediumBlue
        Me.lblAccount.Location = New System.Drawing.Point(12, 15)
        Me.lblAccount.Name = "lblAccount"
        Me.lblAccount.Size = New System.Drawing.Size(95, 15)
        Me.lblAccount.TabIndex = 0
        Me.lblAccount.Text = "Account Details:"
        '
        'frmChartOfAccounts_Edit_Multiple
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(489, 526)
        Me.Controls.Add(Me.PanelMain)
        Me.Controls.Add(Me.PanelAccountType)
        Me.Controls.Add(Me.PanelButtons)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(495, 514)
        Me.Name = "frmChartOfAccounts_Edit_Multiple"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Edit Account"
        Me.PanelAccountType.ResumeLayout(False)
        Me.PanelAccountType.PerformLayout()
        Me.PanelButtons.ResumeLayout(False)
        Me.PanelMain.ResumeLayout(False)
        Me.PanelMain.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelAccountType As System.Windows.Forms.Panel
    Friend WithEvents PanelButtons As System.Windows.Forms.Panel
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents lblAccountType As System.Windows.Forms.Label
    Friend WithEvents lbAccountType_Instruction As System.Windows.Forms.Label
    Friend WithEvents cboAccountTypes As MTGCComboBox
    Friend WithEvents PanelMain As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblAccountCode As System.Windows.Forms.Label
    Friend WithEvents txtAccountCode As System.Windows.Forms.TextBox
    Friend WithEvents txtAccountName As System.Windows.Forms.TextBox
    Friend WithEvents lblAccount As System.Windows.Forms.Label
    Friend WithEvents chkIsActive As System.Windows.Forms.CheckBox
    Friend WithEvents cboLevel As System.Windows.Forms.ComboBox
    Friend WithEvents lblLevel As System.Windows.Forms.Label
    Friend WithEvents lblAccountStructure As System.Windows.Forms.Label
    Friend WithEvents chkIsSubAccountOf As System.Windows.Forms.CheckBox
    Friend WithEvents cboSubAccountOf As MTGCComboBox
    Friend WithEvents lblAccountSubOf As System.Windows.Forms.Label
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents txtAccountDesc As System.Windows.Forms.TextBox
    Friend WithEvents txtAccountTypeDesc As System.Windows.Forms.TextBox
    Friend WithEvents chDebit As System.Windows.Forms.CheckBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboProject As MTGCComboBox
End Class
