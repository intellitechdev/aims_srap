﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlTypes

Public Class frmAccountSubsidiary
    Private gCon As New Clsappconfiguration()
    Dim KeyAccountID As String
    Dim KeyID As Integer
    Dim ColumnID As Integer
    Dim SubAccountKey As String

#Region "Logic"
    Private Function Msg()
        Dim str As String

        If txtColumn.Text = "" Then
            str = "Please Input Column Name."
        End If

        If SubAccountKey = "" Then
            str += "Please Select Sub-Account."
        End If

        Return str
    End Function
#End Region

#Region "Data Access Layer"

    Private Sub LoadDebitCredit()
        KeyAccountID = Nothing
        Dim myid As New Guid(gCompanyID)
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("spu_AccountAmortization_ListDebitCredit", gCon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@coid", SqlDbType.UniqueIdentifier).Value = myid
        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "Accounts")
            With cboAccount
                .ValueMember = "fxkey_AccountID"
                .DisplayMember = "AccountName"
                .DataSource = ds.Tables(0)
                '.SelectedIndex = -1
                .Text = "Select"
            End With
            gCon.sqlconn.Close()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub LoadAccountsDropdown()
        Dim myid As New Guid(gCompanyID)
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("spu_AccountAmortization_View", gCon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@coid", SqlDbType.UniqueIdentifier).Value = myid
        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "Accounts")
            With cboAccountList
                .ValueMember = "PkAccount"
                .DisplayMember = "acnt_name"
                .DataSource = ds.Tables(0)
                '.SelectedIndex = -1
                .Text = "Select"
            End With
            gCon.sqlconn.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub LoadAccounts()
        grdAccount.Rows.Clear()
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountAmortization_View",
                                                     New SqlParameter("@coid", gCompanyID))
        While rd.Read = True
            AddItem(rd.Item(0).ToString, rd.Item(1).ToString, rd.Item(2).ToString, rd.Item(3).ToString)
        End While
    End Sub

    Private Sub AddItem(ByVal PkAccount As String,
                              ByVal FkAccount As String,
                              ByVal FcAcntCode As String,
                              ByVal FcAcntTitle As String)
        Try
            Dim row As String() =
             {PkAccount, FkAccount, FcAcntCode, FcAcntTitle}

            Dim nRowIndex As Integer
            With grdAccount

                .Rows.Add(row)
                '.ClearSelection()
                nRowIndex = .Rows.Count - 1
                .FirstDisplayedScrollingRowIndex = nRowIndex
            End With
        Catch
            MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub Save()
        Dim myid As New Guid(KeyAccountID)
        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountAmortization_InsertUpdate",
                                                     New SqlParameter("@FkAccount", myid))
    End Sub

    Private Sub Delete()
        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountAmortization_Delete",
                                                     New SqlParameter("@PkAccount", KeyID))
    End Sub
    '====================================================================================================================================================
    Private Sub LoadColumns()
        grdColumns.Rows.Clear()
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountAmortizationColumn_ListByAccount",
                                                     New SqlParameter("@FkAccountID", Convert.ToInt32(cboAccountList.SelectedValue)))
        While rd.Read = True
            AddItem_Column(rd.Item(0), rd.Item(1).ToString, rd.Item(2).ToString, rd.Item(3).ToString, rd.Item(4).ToString)
        End While
    End Sub

    Private Sub AddItem_Column(ByVal PkColumnID As Integer,
                              ByVal FcColumnName As String,
                              ByVal FkSubAccountKey As String,
                              ByVal FcAcntCode As String,
                              ByVal FcAcntTitle As String)
        Try
            Dim row As String() =
             {PkColumnID, FcColumnName, FkSubAccountKey, FcAcntCode, FcAcntTitle}

            Dim nRowIndex As Integer
            With grdColumns

                .Rows.Add(row)
                '.ClearSelection()
                nRowIndex = .Rows.Count - 1
                .FirstDisplayedScrollingRowIndex = nRowIndex
            End With
        Catch
            MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub SaveColumn()
        Try
            Dim myid As New Guid(SubAccountKey)
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountAmortizationColumn_InsertUpdate",
                                                        New SqlParameter("@PkColumnID", ColumnID),
                                                        New SqlParameter("@FkAccountID", Convert.ToInt32(cboAccountList.SelectedValue)),
                                                        New SqlParameter("@FcColumnName", txtColumn.Text),
                                                        New SqlParameter("@FkSubAccountKey", myid))
            MsgBox("Saved.", MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox("Commond failed.", MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub DeleteColumn(ByVal ColumnID As Integer)
        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountAmortizationColumn_Delete",
                                                     New SqlParameter("@PkColumnID", ColumnID))
    End Sub

    Private Sub ColumnDetails()
        SubAccountKey = grdColumns.Item("PkAcntID", grdColumns.CurrentRow.Index).Value.ToString
        txtColumn.Text = grdColumns.Item("FcColumnName", grdColumns.CurrentRow.Index).Value.ToString
        txtSubAccount.Text = grdColumns.Item("FcAcntTitle", grdColumns.CurrentRow.Index).Value.ToString
    End Sub

#End Region

#Region "Event Handler"

    Private Sub frmAccountSubsidiary_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        LoadDebitCredit()
        LoadAccounts()
        LoadAccountsDropdown()
        LoadColumns()
    End Sub

    Private Sub CloseToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub AddToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AddToolStripMenuItem.Click
        If cboAccount.Text <> "Select" Then
            Save()
            LoadAccounts()
            LoadDebitCredit()
            LoadAccountsDropdown()
        Else
            MsgBox("Please Select Account", MsgBoxStyle.Exclamation, "Account Setup")
            Exit Sub
        End If
    End Sub

    Private Sub cboAccount_DropDownClosed(sender As System.Object, e As System.EventArgs) Handles cboAccount.DropDownClosed
        Try
            KeyAccountID = cboAccount.SelectedValue.ToString
        Catch ex As Exception

        End Try

    End Sub

    Private Sub grdAccount_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdAccount.CellClick
        KeyID = grdAccount.Item("PkAccountID", grdAccount.CurrentRow.Index).Value
    End Sub

    Private Sub DeleteToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles DeleteToolStripMenuItem.Click
        Delete()
        LoadAccounts()
        LoadDebitCredit()
    End Sub

    Private Sub ToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripMenuItem1.Click
        Me.Close()
    End Sub

    Private Sub ToolStripMenuItem2_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripMenuItem2.Click
        txtColumn.Clear()
        txtSubAccount.Clear()
        SubAccountKey = ""

        txtColumn.Enabled = True
        btnSearch.Enabled = True
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles btnSearch.Click
        frmSearchAccount.xModule = "Entry"
        frmSearchAccount.ShowDialog()
        If frmSearchAccount.DialogResult = DialogResult.OK Then
            txtSubAccount.Text = frmSearchAccount.grdCoAList.SelectedRows(0).Cells("acnt_name").Value.ToString()
            SubAccountKey = frmSearchAccount.grdCoAList.SelectedRows(0).Cells("acnt_id").Value.ToString()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub ToolStripMenuItem3_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripMenuItem3.Click
        If Msg() = "" Then
            SaveColumn()
            LoadColumns()

            txtColumn.Enabled = False
            btnSearch.Enabled = False

            txtColumn.Clear()
            txtSubAccount.Clear()
            SubAccountKey = ""
        Else
            MessageBox.Show(Msg)
            Exit Sub
        End If
    End Sub

    Private Sub cboAccountList_DropDownClosed(sender As System.Object, e As System.EventArgs) Handles cboAccountList.DropDownClosed
        LoadColumns()

        txtColumn.Enabled = False
        btnSearch.Enabled = False

        txtColumn.Clear()
        txtSubAccount.Clear()
        SubAccountKey = ""
    End Sub

    Private Sub ToolStripMenuItem4_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripMenuItem4.Click
        If MsgBox("Delete this row?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Account Amortization") = MsgBoxResult.Yes Then
            DeleteColumn(grdColumns.Item("PkAcntID", grdColumns.CurrentRow.Index).Value)
            LoadColumns()

            txtColumn.Enabled = False
            btnSearch.Enabled = False

            txtColumn.Clear()
            txtSubAccount.Clear()
            SubAccountKey = ""
        Else
            Exit Sub
        End If
    End Sub

    Private Sub EditToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles EditToolStripMenuItem.Click
        txtColumn.Enabled = True
    End Sub

    Private Sub CancelToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CancelToolStripMenuItem.Click
        txtColumn.Enabled = False
        btnSearch.Enabled = False
    End Sub

    Private Sub grdColumns_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdColumns.CellClick
        Try
            ColumnDetails()
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub
#End Region

End Class