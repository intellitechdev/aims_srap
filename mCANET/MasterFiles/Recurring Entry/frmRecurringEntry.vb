﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Math

Public Class frmRecurringEntry
    Private gCon As New Clsappconfiguration()
    Private flag_cell_edited As Boolean = False
    Private currentRow As Integer
    Private currentColumn As Integer

    Private Sub frmRecurringEntry_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ListMaster()
        LoadAccounts()
    End Sub

    Private Sub ListMaster()
        cboList.Items.Clear()
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_RecurringList",
                                                                            New SqlParameter("@coid", gCompanyID),
                                                                            New SqlParameter("@filter", ""))
        While rd.Read = True
            cboList.Items.Add(rd.Item("ListName"))
        End While

        rd.Close()
    End Sub

    Private Sub AddToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AddToolStripMenuItem.Click
        'Dim acntID As String
        'Dim acntCode As String
        'Dim acntName As String
        'Dim memKey As String
        'Dim memID As String
        'Dim memName As String
        'Dim LoanRef As String
        'Dim AcntRef As String
        'frmCOAFilter.xModule = "Recurring Entry"
        'frmCOAFilter.ShowDialog()
        'If frmCOAFilter.DialogResult = Windows.Forms.DialogResult.OK Then
        '    acntID = frmCOAFilter.grdCoAList.SelectedRows(0).Cells("acnt_id").Value.ToString()
        '    acntCode = frmCOAFilter.grdCoAList.SelectedRows(0).Cells("acnt_code").Value.ToString()
        '    acntName = frmCOAFilter.grdCoAList.SelectedRows(0).Cells("acnt_name").Value.ToString()
        SaveAccount()
        'End If
    End Sub

    Private Sub SaveAccount()
        Dim sSQLCmd As String = "delete from tbl_RecurringEntryMaster where ListName='" & cboList.Text & "' AND coid ='" & gCompanyID() & "'"
        SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
        Dim myID As New Guid(gCompanyID)
        Try
            For xRow As Integer = 0 To grdList.RowCount - 2
                If grdList.Item("memKey", xRow).Value.ToString <> "" Then
                    SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_RecurringEntryMaster_Insert", _
                                                    New SqlParameter("@memKey", IIf(IsDBNull(grdList.Item("memKey", xRow).Value), Nothing, grdList.Item("memKey", xRow).Value.ToString())),
                                                    New SqlParameter("@memID", IIf(IsDBNull(grdList.Item("memID", xRow).Value), Nothing, grdList.Item("memID", xRow).Value.ToString())),
                                                    New SqlParameter("@memName", IIf(IsDBNull(grdList.Item("memName", xRow).Value), Nothing, grdList.Item("memName", xRow).Value.ToString())),
                                                    New SqlParameter("@LoanRef", IIf(IsDBNull(grdList.Item("LoanRef", xRow).Value), Nothing, grdList.Item("LoanRef", xRow).Value.ToString())),
                                                    New SqlParameter("@AcntRef", IIf(IsDBNull(grdList.Item("AcntRef", xRow).Value), Nothing, grdList.Item("AcntRef", xRow).Value.ToString())),
                                                    New SqlParameter("@acntID", IIf(IsDBNull(grdList.Item("acntID", xRow).Value), Nothing, grdList.Item("acntID", xRow).Value.ToString())),
                                                    New SqlParameter("@acntCode", IIf(IsDBNull(grdList.Item("acntCode", xRow).Value), Nothing, grdList.Item("acntCode", xRow).Value.ToString())),
                                                    New SqlParameter("@acntTitle", IIf(IsDBNull(grdList.Item("acntTitle", xRow).Value), Nothing, grdList.Item("acntTitle", xRow).Value.ToString())),
                                                    New SqlParameter("@debit", IIf(IsDBNull(grdList.Item("debit", xRow).Value), Nothing, grdList.Item("debit", xRow).Value)),
                                                    New SqlParameter("@credit", IIf(IsDBNull(grdList.Item("credit", xRow).Value), Nothing, grdList.Item("credit", xRow).Value)),
                                                    New SqlParameter("@coid", myID),
                                                    New SqlParameter("@ListName", cboList.Text))
                Else
                    SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_RecurringEntryMaster_Insert1", _
                                                    New SqlParameter("@acntID", IIf(IsDBNull(grdList.Item("acntID", xRow).Value), Nothing, grdList.Item("acntID", xRow).Value.ToString())),
                                                    New SqlParameter("@acntCode", IIf(IsDBNull(grdList.Item("acntCode", xRow).Value), Nothing, grdList.Item("acntCode", xRow).Value.ToString())),
                                                    New SqlParameter("@acntTitle", IIf(IsDBNull(grdList.Item("acntTitle", xRow).Value), Nothing, grdList.Item("acntTitle", xRow).Value.ToString())),
                                                    New SqlParameter("@debit", IIf(IsDBNull(grdList.Item("debit", xRow).Value), Nothing, grdList.Item("debit", xRow).Value)),
                                                    New SqlParameter("@credit", IIf(IsDBNull(grdList.Item("credit", xRow).Value), Nothing, grdList.Item("credit", xRow).Value)),
                                                    New SqlParameter("@coid", myID),
                                                    New SqlParameter("@ListName", cboList.Text))
                End If
            Next
            MsgBox(cboList.Text & " updated successfully.", MsgBoxStyle.Information, "Recurring Entry Setup")
        Catch ex As Exception
            MsgBox("Error in saving recurring entry setup. Please notify system provider if you seen this message." & vbCr & vbCr & "Private Sub SaveAccount()", MsgBoxStyle.Critical, "Recurring Entry Setup")
        End Try
        LoadAccounts()
    End Sub

    Private Sub LoadAccounts()
        grdList.Rows.Clear()
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_RecurringEntryMaster_List",
                                                                            New SqlParameter("@ListName", cboList.Text),
                                                                            New SqlParameter("@coid", gCompanyID()))
        While rd.Read = True
            Try
                Dim row As String() =
                 {rd.Item("memKey").ToString, rd.Item("memID").ToString, rd.Item("memName").ToString, rd.Item("LoanRef").ToString, rd.Item("AcntRef").ToString, rd.Item("acntID").ToString, rd.Item("acntCode").ToString, rd.Item("acntTitle").ToString, rd.Item("debit").ToString, rd.Item("credit").ToString}

                Dim nRowIndex As Integer
                With grdList

                    .Rows.Add(row)
                    '.ClearSelection()
                    nRowIndex = .Rows.Count - 1
                    .FirstDisplayedScrollingRowIndex = nRowIndex
                    '.Rows.Add()
                End With
            Catch
                MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End Try
        End While
    End Sub

    Private Sub RemoveToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles RemoveToolStripMenuItem.Click
        'Dim cdelete As String
        'Dim i As Integer = grdList.CurrentRow.Index
        'cdelete = grdList.Item("acntID", i).Value.ToString
        'SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_RecurringEntryMaster_Delete", _
        '        New SqlParameter("@acntID", cdelete))
        Try
            grdList.Rows.Remove(grdList.CurrentRow)
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Sub CloseToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub cboList_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboList.SelectedIndexChanged
        LoadAccounts()
    End Sub

    Private Sub NewListToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles NewListToolStripMenuItem.Click
        Dim ListName As String = InputBox("Input List Name: ", "New List")
        If ListName <> "" Then
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_RecurringList",
                                                                            New SqlParameter("@coid", gCompanyID),
                                                                            New SqlParameter("@filter", ListName))
            If rd.Read = True Then
                MsgBox("List already exists", MsgBoxStyle.Exclamation, "Recurring Entry Setup")
                Exit Sub
            Else
                SaveList(ListName)
            End If

            rd.Close()
        Else
            Exit Sub
        End If
        
    End Sub

    Private Sub SaveList(ByVal Listname As String)
        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_Recurring_SaveList", _
                New SqlParameter("@coid", gCompanyID),
                New SqlParameter("@ListName", Listname))
        ListMaster()
        LoadAccounts()
    End Sub

    Private Sub DeleteListToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles DeleteListToolStripMenuItem.Click
        If MsgBox("Are you sure you want to delete this list?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Recurring Entry Setup") = MsgBoxResult.Yes Then
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_RecurringList_Delete", _
                            New SqlParameter("@coid", gCompanyID),
                            New SqlParameter("@filter", cboList.Text))
            ListMaster()
            LoadAccounts()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub grdList_CellEndEdit(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdList.CellEndEdit
        Select Case e.ColumnIndex
            Case 8
                grdList.Item("credit", e.RowIndex).Value = 0
            Case 9
                grdList.Item("debit", e.RowIndex).Value = 0
        End Select
    End Sub

    Private Sub grdList_CellValueChanged(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdList.CellValueChanged
        If e.ColumnIndex <> -1 Then
            If grdList.Columns(e.ColumnIndex).Name = "debit" Then
                If GetValuesInDataGridView(grdList, "debit", e.RowIndex) = "" Then
                    grdList.Item("debit", e.RowIndex).Value = 0
                Else
                    grdList.Item("debit", e.RowIndex).Value = Format(CDec(grdList.Item("debit", e.RowIndex).Value.ToString()), "##,##0.00")
                    'ComputeDebitAndCreditSumDetails()
                End If
            End If
            If grdList.Columns(e.ColumnIndex).Name = "credit" Then
                If GetValuesInDataGridView(grdList, "credit", e.RowIndex) = "" Then
                    grdList.Item("credit", e.RowIndex).Value = 0
                Else
                    grdList.Item("credit", e.RowIndex).Value = Format(CDec(grdList.Item("credit", e.RowIndex).Value.ToString()), "##,##0.00")
                    'ComputeDebitAndCreditSumDetails()
                End If
            End If
        End If

        'If grdList.Columns(e.ColumnIndex).Name = "cAccounts" Then
        '    Try
        '        Dim accountID As String = NormalizeValuesInDataGridView(grdList, "cAccounts", e.RowIndex)
        '        Call IsAccountValid(accountID)
        '    Catch ex As Exception
        '        MessageBox.Show(ex.Message)
        '        grdList.Item("cAccounts", e.RowIndex).Value = System.DBNull.Value
        '    End Try
        'End If

        'If grdList.Columns(e.ColumnIndex).Name = "Credit" Then
        '    Try
        '        Call ComputeDebitAndCreditSumDetails()
        '    Catch ex As Exception
        '        MessageBox.Show(ex.Message)
        '    End Try
        'End If

        'If grdList.Columns(e.ColumnIndex).Name = "Debit" Then
        '    Try
        '        Call ComputeDebitAndCreditSumDetails()
        '    Catch ex As Exception
        '        MessageBox.Show(ex.Message)
        '    End Try
        'End If
    End Sub

    Private Sub grdList_EditingControlShowing(sender As Object, e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles grdList.EditingControlShowing
        If grdList.CurrentCell.ColumnIndex = 8 Then
            AddHandler CType(e.Control, TextBox).KeyPress, AddressOf TextBox_keyPress1
        End If

        If grdList.CurrentCell.ColumnIndex = 9 Then
            AddHandler CType(e.Control, TextBox).KeyPress, AddressOf TextBox_keyPress1
        End If
    End Sub

    Private Sub TextBox_keyPress1(ByVal sender As Object, ByVal e As KeyPressEventArgs)
        If Not (Asc(e.KeyChar) = 8) Then
            If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or e.KeyChar = ".") Then e.Handled = True
        End If
    End Sub

    Private Sub grdList_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles grdList.KeyDown
        Dim xRowIndex As Integer = grdList.CurrentRow.Index
        Call ReccuringGrdColumns(xRowIndex)
        Dim xColIndex As Integer = grdList.CurrentCellAddress.X

        If cboList.Text = "" Then
            MsgBox("Please select List Name.", MsgBoxStyle.Exclamation, "Recurring Entry Setup")
            Exit Sub
        Else
            Select Case xColIndex
                Case 1
                    If e.KeyCode = Keys.Enter Then
                        If grdcolLoanRef = "" And grdcolAcntRef = "" Then
                            frmMembersFilter.ShowDialog()
                            If frmMembersFilter.DialogResult = DialogResult.OK Then
                                Dim cName As String = frmMembersFilter.grdCoAList.SelectedRows(0).Cells(2).Value.ToString()
                                Dim cID As String = frmMembersFilter.grdCoAList.SelectedRows(0).Cells(1).Value.ToString()
                                Dim cMemID As String = frmMembersFilter.grdCoAList.SelectedRows(0).Cells(0).Value.ToString()
                                Dim Debit As Decimal = 0
                                Dim Credit As Decimal = 0
                                If grdcolID = "" And grdcolName = "" And grdcolLoanRef = "" And grdcolAcntRef = "" And grdcolCode = "" And grdcolTitle = "" Then
                                    AddRow(cMemID, cID, cName, "", "", "", "", "", Format(CDec(Val(Debit)), "##,##0.00"), Format(CDec(Val(Credit)), "##,##0.00"))
                                    grdList.ClearSelection()
                                    grdList.CurrentCell = grdList.Rows(xRowIndex).Cells(8)
                                    grdList.Rows(xRowIndex).Cells("acntCode").Selected = True
                                Else
                                    grdList.Item("memID", xRowIndex).Value = cID
                                    grdList.Item("memName", xRowIndex).Value = cName
                                    grdList.Item("memKey", xRowIndex).Value = cMemID
                                    grdList.ClearSelection()
                                    grdList.CurrentCell = grdList.Rows(xRowIndex).Cells(8)
                                    grdList.Rows(xRowIndex).Cells("acntCode").Selected = True
                                End If
                            End If
                        Else
                            grdList.ClearSelection()
                            grdList.CurrentCell = grdList.Rows(xRowIndex).Cells(8)
                            grdList.Rows(xRowIndex).Cells("memName").Selected = True
                            Exit Sub
                        End If
                    End If
                Case 2
                    If e.KeyCode = Keys.Enter Then
                        If grdcolLoanRef = "" And grdcolAcntRef = "" Then
                            frmMembersFilter.ShowDialog()
                            If frmMembersFilter.DialogResult = DialogResult.OK Then
                                Dim cName As String = frmMembersFilter.grdCoAList.SelectedRows(0).Cells(2).Value.ToString()
                                Dim cID As String = frmMembersFilter.grdCoAList.SelectedRows(0).Cells(1).Value.ToString()
                                Dim cMemID As String = frmMembersFilter.grdCoAList.SelectedRows(0).Cells(0).Value.ToString()
                                Dim Debit As Decimal = 0
                                Dim Credit As Decimal = 0
                                If grdcolID = "" And grdcolName = "" And grdcolLoanRef = "" And grdcolAcntRef = "" And grdcolCode = "" And grdcolTitle = "" Then
                                    AddRow(cMemID, cID, cName, "", "", "", "", "", Format(CDec(Val(Debit)), "##,##0.00"), Format(CDec(Val(Credit)), "##,##0.00"))
                                    grdList.ClearSelection()
                                    grdList.CurrentCell = grdList.Rows(xRowIndex).Cells(8)
                                    grdList.Rows(xRowIndex).Cells("acntCode").Selected = True
                                Else
                                    grdList.Item("memID", xRowIndex).Value = cID
                                    grdList.Item("memName", xRowIndex).Value = cName
                                    grdList.Item("memKey", xRowIndex).Value = cMemID
                                    grdList.ClearSelection()
                                    grdList.CurrentCell = grdList.Rows(xRowIndex).Cells(8)
                                    grdList.Rows(xRowIndex).Cells("acntCode").Selected = True
                                End If
                            End If
                        Else
                            grdList.ClearSelection()
                            grdList.CurrentCell = grdList.Rows(xRowIndex).Cells(8)
                            grdList.Rows(xRowIndex).Cells("memName").Selected = True
                            Exit Sub
                        End If
                    End If
                Case 3
                    If e.KeyCode = Keys.Enter Then
                        frmLoanFilter.ShowDialog()
                        If frmLoanFilter.DialogResult = DialogResult.OK Then
                            Dim cID As String = frmLoanFilter.grdLoanList.SelectedRows(0).Cells(0).Value
                            Dim cName As String = frmLoanFilter.grdLoanList.SelectedRows(0).Cells(1).Value
                            Dim cAccountTitle As String = frmLoanFilter.grdLoanList.SelectedRows(0).Cells(2).Value
                            Dim cLoanRef As String = frmLoanFilter.grdLoanList.SelectedRows(0).Cells(3).Value
                            Dim cMemID As String = frmLoanFilter.grdLoanList.SelectedRows(0).Cells(6).Value.ToString
                            Dim cAccountCode As String = frmLoanFilter.grdLoanList.SelectedRows(0).Cells(7).Value
                            Dim cAccounts As String = frmLoanFilter.grdLoanList.SelectedRows(0).Cells(8).Value.ToString
                            Dim dCredit As Decimal = frmLoanFilter.grdLoanList.SelectedRows(0).Cells(5).Value
                            Dim LoanType As String = frmLoanFilter.grdLoanList.SelectedRows(0).Cells(9).Value.ToString
                            'AddDetailItem(cID, cName, cLoanRef, "", cAccounts, cAccountCode, cAccountTitle, 0, dCredit, cMemID, "")

                            If grdcolID = "" And grdcolName = "" And grdcolLoanRef = "" And grdcolAcntRef = "" And grdcolCode = "" And grdcolTitle = "" Then
                                AddRow(cMemID, cID, cName, cLoanRef, "", cAccounts, cAccountCode, cAccountTitle, Format(CDec(Val(0)), "##,##0.00"), Format(CDec(Val(dCredit)), "##,##0.00"))
                                Try
                                    Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_Loans_LoadAccountsForPayment",
                                      New SqlParameter("@pkLoanType", LoanType),
                                      New SqlParameter("@IDno", cID))

                                    Dim row As Integer
                                    While rd.Read
                                        row = grdList.RowCount - 1
                                        Dim AcntID = rd.Item("fk_Account").ToString
                                        Dim AcntCode = rd.Item("acnt_code").ToString
                                        Dim AcntTitle = rd.Item("Description").ToString
                                        Dim Amount = rd.Item("Amount").ToString
                                        Dim AcntRef = rd.Item("fcDocNumber").ToString
                                        AddRow(cMemID, cID, cName, cLoanRef, AcntRef, AcntID, AcntCode, AcntTitle, Format(CDec(Val(0)), "##,##0.00"), Format(CDec(Val(Amount)), "##,##0.00"))
                                    End While
                                    rd.Close()
                                Catch ex As Exception
                                    MsgBox(ex.Message, , "Load Loan Accounts")
                                End Try
                                grdList.ClearSelection()
                                grdList.CurrentCell = grdList.Rows(xRowIndex).Cells(8)
                                grdList.Rows(xRowIndex).Cells("Debit").Selected = True
                            ElseIf grdList.Item("acntID", xRowIndex).Value <> "" And cMemID = grdList.Item("memKey", xRowIndex).Value.ToString Then
                                grdList.Item("memID", xRowIndex).Value = cID
                                grdList.Item("memName", xRowIndex).Value = cName
                                grdList.Item("memID", xRowIndex).Value = cMemID
                                grdList.Item("LoanRef", xRowIndex).Value = cLoanRef
                                'grdList.Item("cAccounts", xRowIndex).Value = cAccounts
                                '[grdList.Item("cAccountCode", xRowIndex).Value = cAccountCode
                                'grdList.Item("cAccountTitle", xRowIndex).Value = cAccountTitle
                                grdList.ClearSelection()
                                grdList.CurrentCell = grdList.Rows(xRowIndex).Cells(8)
                                grdList.Rows(xRowIndex).Cells("Debit").Selected = True
                            End If
                        End If
                    End If
                Case 4
                    If e.KeyCode = Keys.Enter Then
                        frmSubsidiaryAccountFilter.xModule = "Entry"
                        frmSubsidiaryAccountFilter.ShowDialog()
                        If frmSubsidiaryAccountFilter.DialogResult = DialogResult.OK Then
                            Dim cAccountRef As String = frmSubsidiaryAccountFilter.grdSAList.SelectedRows(0).Cells(1).Value.ToString()
                            Dim cID As String = frmSubsidiaryAccountFilter.grdSAList.SelectedRows(0).Cells(2).Value.ToString()
                            Dim cName As String = frmSubsidiaryAccountFilter.grdSAList.SelectedRows(0).Cells(3).Value.ToString()
                            Dim cMemID As String = frmSubsidiaryAccountFilter.grdSAList.SelectedRows(0).Cells(7).Value.ToString()
                            Dim cAccountTitle As String = frmSubsidiaryAccountFilter.grdSAList.SelectedRows(0).Cells(5).Value.ToString()
                            Dim cAccount As String = frmSubsidiaryAccountFilter.grdSAList.SelectedRows(0).Cells(6).Value.ToString()
                            Dim cAccountCode As String = frmSubsidiaryAccountFilter.grdSAList.SelectedRows(0).Cells(4).Value.ToString()
                            Dim Debit As Decimal = 0
                            Dim Credit As Decimal = 0
                            If grdcolID = "" And grdcolName = "" And grdcolLoanRef = "" And grdcolAcntRef = "" And grdcolCode = "" And grdcolTitle = "" Then
                                AddRow(cMemID, cID, cName, "", cAccountRef, cAccount, cAccountCode, cAccountTitle, Format(CDec(Val(Debit)), "##,##0.00"), Format(CDec(Val(Credit)), "##,##0.00"))
                                grdList.ClearSelection()
                                grdList.CurrentCell = grdList.Rows(xRowIndex).Cells(8)
                                grdList.Rows(xRowIndex).Cells("Debit").Selected = True
                            Else
                                grdList.Item("memID", xRowIndex).Value = cID
                                grdList.Item("memName", xRowIndex).Value = cName
                                grdList.Item("memKey", xRowIndex).Value = cMemID
                                grdList.Item("acntRef", xRowIndex).Value = cAccountRef
                                grdList.Item("acntID", xRowIndex).Value = cAccount
                                grdList.Item("acntCode", xRowIndex).Value = cAccountCode
                                grdList.Item("acntTitle", xRowIndex).Value = cAccountTitle
                                grdList.ClearSelection()
                                grdList.CurrentCell = grdList.Rows(xRowIndex).Cells(8)
                                grdList.Rows(xRowIndex).Cells("Debit").Selected = True
                            End If
                        End If
                    End If
                Case 6
                    If e.KeyCode = Keys.Enter Then
                        If grdcolLoanRef = "" And grdcolAcntRef = "" Then
                            frmCOAFilter.xModule = "Entry"
                            frmCOAFilter.ShowDialog()
                            If frmCOAFilter.DialogResult = Windows.Forms.DialogResult.OK Then
                                Dim cAccountTitle As String = frmCOAFilter.grdCoAList.SelectedRows(0).Cells("acnt_name").Value.ToString()
                                Dim cAccountCode As String = frmCOAFilter.grdCoAList.SelectedRows(0).Cells("acnt_code").Value.ToString()
                                Dim cAccounts As String = frmCOAFilter.grdCoAList.SelectedRows(0).Cells("acnt_id").Value.ToString()
                                Dim Debit As Decimal = 0
                                Dim Credit As Decimal = 0
                                If grdcolID = "" And grdcolName = "" And grdcolLoanRef = "" And grdcolAcntRef = "" And grdcolCode = "" And grdcolTitle = "" Then
                                    AddRow("", "", "", "", "", cAccounts, cAccountCode, cAccountTitle, Format(CDec(Val(Debit)), "##,##0.00"), Format(CDec(Val(Credit)), "##,##0.00"))
                                    grdList.ClearSelection()
                                    grdList.CurrentCell = grdList.Rows(xRowIndex).Cells(8)
                                    grdList.Rows(xRowIndex).Cells("Debit").Selected = True
                                Else
                                    grdList.Item("acntID", xRowIndex).Value = cAccounts
                                    grdList.Item("acntCode", xRowIndex).Value = cAccountCode
                                    grdList.Item("acntTitle", xRowIndex).Value = cAccountTitle
                                    grdList.ClearSelection()
                                    grdList.CurrentCell = grdList.Rows(xRowIndex).Cells(8)
                                    grdList.Rows(xRowIndex).Cells("Debit").Selected = True
                                End If
                            End If
                            grdList.ClearSelection()
                            grdList.CurrentCell = grdList.Rows(xRowIndex).Cells(8)
                            grdList.Rows(xRowIndex).Cells("Debit").Selected = True
                            Exit Sub
                        End If
                    End If
                Case 7
                    If e.KeyCode = Keys.Enter Then
                        If grdcolLoanRef = "" And grdcolAcntRef = "" Then
                            frmCOAFilter.xModule = "Entry"
                            frmCOAFilter.ShowDialog()
                            If frmCOAFilter.DialogResult = Windows.Forms.DialogResult.OK Then
                                Dim cAccountTitle As String = frmCOAFilter.grdCoAList.SelectedRows(0).Cells("acnt_name").Value.ToString()
                                Dim cAccountCode As String = frmCOAFilter.grdCoAList.SelectedRows(0).Cells("acnt_code").Value.ToString()
                                Dim cAccounts As String = frmCOAFilter.grdCoAList.SelectedRows(0).Cells("acnt_id").Value.ToString()
                                Dim Debit As Decimal = 0
                                Dim Credit As Decimal = 0
                                If grdcolID = "" And grdcolName = "" And grdcolLoanRef = "" And grdcolAcntRef = "" And grdcolCode = "" And grdcolTitle = "" Then
                                    AddRow("", "", "", "", "", cAccounts, cAccountCode, cAccountTitle, Format(CDec(Val(Debit)), "##,##0.00"), Format(CDec(Val(Credit)), "##,##0.00"))
                                    grdList.ClearSelection()
                                    grdList.CurrentCell = grdList.Rows(xRowIndex).Cells(8)
                                    grdList.Rows(xRowIndex).Cells("Debit").Selected = True
                                Else
                                    grdList.Item("acntID", xRowIndex).Value = cAccounts
                                    grdList.Item("acntCode", xRowIndex).Value = cAccountCode
                                    grdList.Item("acntTitle", xRowIndex).Value = cAccountTitle
                                    grdList.ClearSelection()
                                    grdList.CurrentCell = grdList.Rows(xRowIndex).Cells(8)
                                    grdList.Rows(xRowIndex).Cells("Debit").Selected = True
                                End If
                            End If
                            grdList.ClearSelection()
                            grdList.CurrentCell = grdList.Rows(xRowIndex).Cells(8)
                            grdList.Rows(xRowIndex).Cells("Debit").Selected = True
                            Exit Sub
                        End If
                    End If
                Case 8
                    If e.KeyCode = Keys.Enter Then
                        e.SuppressKeyPress = True
                    End If
                Case 9
                    If e.KeyCode = Keys.Enter Then
                        e.SuppressKeyPress = True
                    End If
            End Select
        End If
    End Sub

    Private Sub AddRow(ByVal memKey As String,
                             ByVal memID As String,
                             ByVal memName As String,
                             ByVal LoanRef As String,
                             ByVal AcntRef As String,
                             ByVal acntID As String,
                             ByVal acntCode As String,
                             ByVal acntTitle As String,
                             ByVal Debit As Decimal,
                             ByVal Credit As Decimal)

        Try
            Dim row As String() =
             {memKey, memID, memName, LoanRef, AcntRef, acntID, acntCode, acntTitle, Format(CDec(Debit), "##,##0.00"), Format(CDec(Credit), "##,##0.00")}

            Dim nRowIndex As Integer
            With grdList

                .Rows.Add(row)
                '.ClearSelection()
                nRowIndex = .Rows.Count - 2
                .FirstDisplayedScrollingRowIndex = nRowIndex
                '.Rows.Add()
            End With
        Catch
            MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub grdList_SelectionChanged(sender As Object, e As System.EventArgs) Handles grdList.SelectionChanged
        If flag_cell_edited = True Then
            Select Case currentColumn
                Case 8
                    grdList.CurrentCell = grdList(currentColumn, currentRow)
                    grdList("Credit", currentRow).Value = 0
                    flag_cell_edited = False
                Case 9
                    grdList.CurrentCell = grdList(currentColumn, currentRow)
                    grdList("Debit", currentRow).Value = 0
                    flag_cell_edited = False
                Case Else
                    grdList.CurrentCell = grdList(currentColumn, currentRow)
                    flag_cell_edited = False
            End Select
            grdList.CurrentCell = grdList(currentColumn, currentRow)
            flag_cell_edited = False
        End If
    End Sub
End Class