﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCheckIssuance
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.grdCheckNoList = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtSearchCheck = New System.Windows.Forms.TextBox()
        Me.cboPreceedingZero = New System.Windows.Forms.ComboBox()
        Me.chkPreceedingZero = New System.Windows.Forms.CheckBox()
        Me.txtQuantity = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtCheckNum = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.dgvCheckList = New System.Windows.Forms.DataGridView()
        Me.btnDeleteBank = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnPreview = New System.Windows.Forms.Button()
        Me.grdBankList = New System.Windows.Forms.DataGridView()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.grdCheckNoList.SuspendLayout()
        CType(Me.dgvCheckList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.grdBankList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grdCheckNoList
        '
        Me.grdCheckNoList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdCheckNoList.BackColor = System.Drawing.Color.Transparent
        Me.grdCheckNoList.Controls.Add(Me.Label1)
        Me.grdCheckNoList.Controls.Add(Me.txtSearchCheck)
        Me.grdCheckNoList.Controls.Add(Me.cboPreceedingZero)
        Me.grdCheckNoList.Controls.Add(Me.chkPreceedingZero)
        Me.grdCheckNoList.Controls.Add(Me.txtQuantity)
        Me.grdCheckNoList.Controls.Add(Me.Label4)
        Me.grdCheckNoList.Controls.Add(Me.txtCheckNum)
        Me.grdCheckNoList.Controls.Add(Me.Label3)
        Me.grdCheckNoList.Controls.Add(Me.btnDelete)
        Me.grdCheckNoList.Controls.Add(Me.btnClose)
        Me.grdCheckNoList.Controls.Add(Me.btnAdd)
        Me.grdCheckNoList.Controls.Add(Me.dgvCheckList)
        Me.grdCheckNoList.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdCheckNoList.Location = New System.Drawing.Point(12, 187)
        Me.grdCheckNoList.Name = "grdCheckNoList"
        Me.grdCheckNoList.Size = New System.Drawing.Size(763, 287)
        Me.grdCheckNoList.TabIndex = 73
        Me.grdCheckNoList.TabStop = False
        Me.grdCheckNoList.Text = "Check Number List"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(7, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(136, 13)
        Me.Label1.TabIndex = 88
        Me.Label1.Text = "Search Check Number"
        '
        'txtSearchCheck
        '
        Me.txtSearchCheck.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchCheck.Location = New System.Drawing.Point(149, 27)
        Me.txtSearchCheck.Name = "txtSearchCheck"
        Me.txtSearchCheck.Size = New System.Drawing.Size(164, 21)
        Me.txtSearchCheck.TabIndex = 87
        '
        'cboPreceedingZero
        '
        Me.cboPreceedingZero.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPreceedingZero.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPreceedingZero.FormattingEnabled = True
        Me.cboPreceedingZero.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"})
        Me.cboPreceedingZero.Location = New System.Drawing.Point(668, 221)
        Me.cboPreceedingZero.Name = "cboPreceedingZero"
        Me.cboPreceedingZero.Size = New System.Drawing.Size(39, 21)
        Me.cboPreceedingZero.TabIndex = 86
        '
        'chkPreceedingZero
        '
        Me.chkPreceedingZero.AutoSize = True
        Me.chkPreceedingZero.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPreceedingZero.Location = New System.Drawing.Point(531, 225)
        Me.chkPreceedingZero.Name = "chkPreceedingZero"
        Me.chkPreceedingZero.Size = New System.Drawing.Size(131, 17)
        Me.chkPreceedingZero.TabIndex = 85
        Me.chkPreceedingZero.Text = "Preceeding zeroes"
        Me.chkPreceedingZero.UseVisualStyleBackColor = True
        '
        'txtQuantity
        '
        Me.txtQuantity.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtQuantity.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtQuantity.Location = New System.Drawing.Point(444, 221)
        Me.txtQuantity.MaxLength = 4
        Me.txtQuantity.Name = "txtQuantity"
        Me.txtQuantity.Size = New System.Drawing.Size(42, 21)
        Me.txtQuantity.TabIndex = 84
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(319, 224)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(119, 13)
        Me.Label4.TabIndex = 83
        Me.Label4.Text = "How many Checks?"
        '
        'txtCheckNum
        '
        Me.txtCheckNum.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtCheckNum.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCheckNum.Location = New System.Drawing.Point(110, 221)
        Me.txtCheckNum.MaxLength = 15
        Me.txtCheckNum.Name = "txtCheckNum"
        Me.txtCheckNum.Size = New System.Drawing.Size(164, 21)
        Me.txtCheckNum.TabIndex = 82
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(12, 224)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(92, 13)
        Me.Label3.TabIndex = 81
        Me.Label3.Text = "Check Number"
        '
        'btnDelete
        '
        Me.btnDelete.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Image = Global.CSAcctg.My.Resources.Resources.delete
        Me.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDelete.Location = New System.Drawing.Point(140, 250)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(122, 31)
        Me.btnDelete.TabIndex = 80
        Me.btnDelete.Text = "&Delete Check"
        Me.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Image = Global.CSAcctg.My.Resources.Resources.button_cancel
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.Location = New System.Drawing.Point(634, 250)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(123, 31)
        Me.btnClose.TabIndex = 79
        Me.btnClose.Text = "Close"
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.Image = Global.CSAcctg.My.Resources.Resources.add
        Me.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAdd.Location = New System.Drawing.Point(12, 250)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(122, 31)
        Me.btnAdd.TabIndex = 78
        Me.btnAdd.Text = "&Add Check"
        Me.btnAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'dgvCheckList
        '
        Me.dgvCheckList.AllowUserToAddRows = False
        Me.dgvCheckList.AllowUserToDeleteRows = False
        Me.dgvCheckList.AllowUserToResizeRows = False
        Me.dgvCheckList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvCheckList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvCheckList.BackgroundColor = System.Drawing.Color.White
        Me.dgvCheckList.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCheckList.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvCheckList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCheckList.Location = New System.Drawing.Point(6, 52)
        Me.dgvCheckList.Name = "dgvCheckList"
        Me.dgvCheckList.ReadOnly = True
        Me.dgvCheckList.RowHeadersVisible = False
        Me.dgvCheckList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCheckList.Size = New System.Drawing.Size(752, 161)
        Me.dgvCheckList.TabIndex = 0
        '
        'btnDeleteBank
        '
        Me.btnDeleteBank.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteBank.Image = Global.CSAcctg.My.Resources.Resources.delete
        Me.btnDeleteBank.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteBank.Location = New System.Drawing.Point(138, 129)
        Me.btnDeleteBank.Name = "btnDeleteBank"
        Me.btnDeleteBank.Size = New System.Drawing.Size(122, 31)
        Me.btnDeleteBank.TabIndex = 2
        Me.btnDeleteBank.Text = "Delete Bank"
        Me.btnDeleteBank.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.btnPreview)
        Me.GroupBox1.Controls.Add(Me.btnDeleteBank)
        Me.GroupBox1.Controls.Add(Me.grdBankList)
        Me.GroupBox1.Controls.Add(Me.btnSave)
        Me.GroupBox1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 13)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(764, 168)
        Me.GroupBox1.TabIndex = 72
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Bank List"
        '
        'btnPreview
        '
        Me.btnPreview.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.Image = Global.CSAcctg.My.Resources.Resources.Print_Preview
        Me.btnPreview.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPreview.Location = New System.Drawing.Point(558, 129)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(199, 31)
        Me.btnPreview.TabIndex = 89
        Me.btnPreview.Text = "&Preview Cancelled Checks"
        Me.btnPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'grdBankList
        '
        Me.grdBankList.AllowUserToAddRows = False
        Me.grdBankList.AllowUserToDeleteRows = False
        Me.grdBankList.AllowUserToResizeRows = False
        Me.grdBankList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdBankList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdBankList.BackgroundColor = System.Drawing.Color.White
        Me.grdBankList.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdBankList.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.grdBankList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.grdBankList.Location = New System.Drawing.Point(6, 15)
        Me.grdBankList.Name = "grdBankList"
        Me.grdBankList.ReadOnly = True
        Me.grdBankList.RowHeadersVisible = False
        Me.grdBankList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.grdBankList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdBankList.Size = New System.Drawing.Size(752, 108)
        Me.grdBankList.TabIndex = 0
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Image = Global.CSAcctg.My.Resources.Resources.add
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.Location = New System.Drawing.Point(10, 129)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(122, 31)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "Add Bank"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'frmCheckIssuance
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(788, 476)
        Me.ControlBox = False
        Me.Controls.Add(Me.grdCheckNoList)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmCheckIssuance"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Check Issuance"
        Me.grdCheckNoList.ResumeLayout(False)
        Me.grdCheckNoList.PerformLayout()
        CType(Me.dgvCheckList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.grdBankList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grdCheckNoList As System.Windows.Forms.GroupBox
    Friend WithEvents cboPreceedingZero As System.Windows.Forms.ComboBox
    Friend WithEvents chkPreceedingZero As System.Windows.Forms.CheckBox
    Friend WithEvents txtQuantity As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtCheckNum As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents dgvCheckList As System.Windows.Forms.DataGridView
    Friend WithEvents btnDeleteBank As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents grdBankList As System.Windows.Forms.DataGridView
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtSearchCheck As System.Windows.Forms.TextBox
    Friend WithEvents btnPreview As System.Windows.Forms.Button
End Class
