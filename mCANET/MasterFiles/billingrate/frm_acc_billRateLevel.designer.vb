<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_acc_billRateLevel
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_acc_billRateLevel))
        Me.grd_BillRateLev = New System.Windows.Forms.DataGridView
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripSplitButton
        Me.ts_New = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_Edit = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_Delete = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_Duplicate = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_customizeCol = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_Print = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_Resort = New System.Windows.Forms.ToolStripMenuItem
        CType(Me.grd_BillRateLev, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'grd_BillRateLev
        '
        Me.grd_BillRateLev.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grd_BillRateLev.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grd_BillRateLev.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grd_BillRateLev.Location = New System.Drawing.Point(0, 24)
        Me.grd_BillRateLev.Name = "grd_BillRateLev"
        Me.grd_BillRateLev.Size = New System.Drawing.Size(421, 204)
        Me.grd_BillRateLev.TabIndex = 0
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(421, 25)
        Me.ToolStrip1.TabIndex = 1
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_New, Me.ts_Edit, Me.ts_Delete, Me.ToolStripSeparator1, Me.ts_Duplicate, Me.ts_customizeCol, Me.ToolStripSeparator3, Me.ts_Print, Me.ts_Resort})
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(103, 22)
        Me.ToolStripButton1.Text = "&Billing Rate Level"
        '
        'ts_New
        '
        Me.ts_New.Name = "ts_New"
        Me.ts_New.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.ts_New.Size = New System.Drawing.Size(177, 22)
        Me.ts_New.Text = "New"
        '
        'ts_Edit
        '
        Me.ts_Edit.Name = "ts_Edit"
        Me.ts_Edit.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.ts_Edit.Size = New System.Drawing.Size(177, 22)
        Me.ts_Edit.Text = "Edit"
        '
        'ts_Delete
        '
        Me.ts_Delete.Name = "ts_Delete"
        Me.ts_Delete.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.ts_Delete.Size = New System.Drawing.Size(177, 22)
        Me.ts_Delete.Text = "Delete"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(174, 6)
        Me.ToolStripSeparator1.Visible = False
        '
        'ts_Duplicate
        '
        Me.ts_Duplicate.Name = "ts_Duplicate"
        Me.ts_Duplicate.Size = New System.Drawing.Size(177, 22)
        Me.ts_Duplicate.Text = "Duplicate"
        Me.ts_Duplicate.Visible = False
        '
        'ts_customizeCol
        '
        Me.ts_customizeCol.Name = "ts_customizeCol"
        Me.ts_customizeCol.Size = New System.Drawing.Size(177, 22)
        Me.ts_customizeCol.Text = "Customize Columns"
        Me.ts_customizeCol.Visible = False
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(174, 6)
        '
        'ts_Print
        '
        Me.ts_Print.Name = "ts_Print"
        Me.ts_Print.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.ts_Print.Size = New System.Drawing.Size(177, 22)
        Me.ts_Print.Text = "Print"
        '
        'ts_Resort
        '
        Me.ts_Resort.Name = "ts_Resort"
        Me.ts_Resort.Size = New System.Drawing.Size(177, 22)
        Me.ts_Resort.Text = "Re-sort"
        '
        'frm_acc_billRateLevel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Info
        Me.ClientSize = New System.Drawing.Size(421, 223)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.grd_BillRateLev)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(429, 257)
        Me.Name = "frm_acc_billRateLevel"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Billing Rate Level"
        CType(Me.grd_BillRateLev, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents grd_BillRateLev As System.Windows.Forms.DataGridView
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents ts_New As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_Edit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_Delete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_Duplicate As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_customizeCol As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_Print As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_Resort As System.Windows.Forms.ToolStripMenuItem
End Class
