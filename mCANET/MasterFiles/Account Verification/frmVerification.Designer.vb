﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVerification
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmVerification))
        Me.Label19 = New System.Windows.Forms.Label()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.btnPrintLoanLedger = New System.Windows.Forms.Button()
        Me.btnPrintStatement = New System.Windows.Forms.Button()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblClientname = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblIDno = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.tabVerification = New System.Windows.Forms.TabControl()
        Me.CurrentLoan = New System.Windows.Forms.TabPage()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtBalPrincipal = New System.Windows.Forms.TextBox()
        Me.txtBalInterest = New System.Windows.Forms.TextBox()
        Me.txtBalServicefee = New System.Windows.Forms.TextBox()
        Me.txtBalCreditAcct = New System.Windows.Forms.TextBox()
        Me.txtTotalPayment = New System.Windows.Forms.TextBox()
        Me.dgvSubsidiaryCurrent = New System.Windows.Forms.DataGridView()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dgvLoans = New System.Windows.Forms.DataGridView()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.btnFindLoan = New System.Windows.Forms.Button()
        Me.txtSearchLoan = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.LoanHistory = New System.Windows.Forms.TabPage()
        Me.dgvLoanHistorySubsidiary = New System.Windows.Forms.DataGridView()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.dgvLoanHistory = New System.Windows.Forms.DataGridView()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Debit = New System.Windows.Forms.TabPage()
        Me.TBCDebitAccounts = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.dgvDebitList = New System.Windows.Forms.DataGridView()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.dgvSoaList = New System.Windows.Forms.DataGridView()
        Me.dgvDebitDetails = New System.Windows.Forms.DataGridView()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Credit = New System.Windows.Forms.TabPage()
        Me.dgvCreditDetails = New System.Windows.Forms.DataGridView()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.dgvCredit = New System.Windows.Forms.DataGridView()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Amortization = New System.Windows.Forms.TabPage()
        Me.btnDynamicPrintAmortization = New System.Windows.Forms.Button()
        Me.txtServiceFee = New System.Windows.Forms.TextBox()
        Me.txtPrincipal = New System.Windows.Forms.TextBox()
        Me.txtInterest = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.lblLoanType = New System.Windows.Forms.Label()
        Me.lblLoanRef = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.dgvAmortizationBalances = New System.Windows.Forms.DataGridView()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.dgvAmortizationSchedule = New System.Windows.Forms.DataGridView()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnSelectedLoanPrint = New System.Windows.Forms.Button()
        Me.SOA = New System.Windows.Forms.TabPage()
        Me.CrvRpt = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.btnBrowse = New System.Windows.Forms.Button()
        Me.txtSoa = New System.Windows.Forms.TextBox()
        Me.RBHistory = New System.Windows.Forms.RadioButton()
        Me.RBBalance = New System.Windows.Forms.RadioButton()
        Me.RBPerSoa = New System.Windows.Forms.RadioButton()
        Me.RBAll = New System.Windows.Forms.RadioButton()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.dtDate = New System.Windows.Forms.DateTimePicker()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.btnPreview = New System.Windows.Forms.Button()
        Me.cboFilterSoa = New System.Windows.Forms.ComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.btnFresh = New System.Windows.Forms.Button()
        Me.btnPrintLedgers = New System.Windows.Forms.Button()
        Me.btnPrintIndividual = New System.Windows.Forms.Button()
        Me.btnFind = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.tabVerification.SuspendLayout()
        Me.CurrentLoan.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.dgvSubsidiaryCurrent, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        CType(Me.dgvLoans, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel6.SuspendLayout()
        Me.LoanHistory.SuspendLayout()
        CType(Me.dgvLoanHistorySubsidiary, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvLoanHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Debit.SuspendLayout()
        Me.TBCDebitAccounts.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.dgvDebitList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.dgvSoaList, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDebitDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Credit.SuspendLayout()
        CType(Me.dgvCreditDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvCredit, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Amortization.SuspendLayout()
        CType(Me.dgvAmortizationBalances, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvAmortizationSchedule, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SOA.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label19
        '
        Me.Label19.Location = New System.Drawing.Point(0, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(100, 23)
        Me.Label19.TabIndex = 0
        '
        'btnClose
        '
        Me.btnClose.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClose.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose.Location = New System.Drawing.Point(943, 0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 28)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSearch
        '
        Me.btnSearch.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnSearch.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSearch.Location = New System.Drawing.Point(837, 0)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(106, 28)
        Me.btnSearch.TabIndex = 2
        Me.btnSearch.Text = "Search"
        Me.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'btnPrintLoanLedger
        '
        Me.btnPrintLoanLedger.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnPrintLoanLedger.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrintLoanLedger.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPrintLoanLedger.Location = New System.Drawing.Point(0, 0)
        Me.btnPrintLoanLedger.Name = "btnPrintLoanLedger"
        Me.btnPrintLoanLedger.Size = New System.Drawing.Size(212, 28)
        Me.btnPrintLoanLedger.TabIndex = 22
        Me.btnPrintLoanLedger.Text = "Print Individual Ledger"
        Me.btnPrintLoanLedger.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPrintLoanLedger.UseVisualStyleBackColor = True
        '
        'btnPrintStatement
        '
        Me.btnPrintStatement.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnPrintStatement.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrintStatement.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPrintStatement.Location = New System.Drawing.Point(212, 0)
        Me.btnPrintStatement.Name = "btnPrintStatement"
        Me.btnPrintStatement.Size = New System.Drawing.Size(174, 28)
        Me.btnPrintStatement.TabIndex = 23
        Me.btnPrintStatement.Text = "Print All Ledgers"
        Me.btnPrintStatement.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPrintStatement.UseVisualStyleBackColor = True
        '
        'btnRefresh
        '
        Me.btnRefresh.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnRefresh.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRefresh.Location = New System.Drawing.Point(739, 0)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(98, 28)
        Me.btnRefresh.TabIndex = 24
        Me.btnRefresh.Text = "Refresh"
        Me.btnRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 22)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1066, 45)
        Me.Panel1.TabIndex = 61
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.White
        Me.GroupBox1.Controls.Add(Me.lblClientname)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.lblIDno)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1066, 42)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        '
        'lblClientname
        '
        Me.lblClientname.AutoSize = True
        Me.lblClientname.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClientname.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.lblClientname.Location = New System.Drawing.Point(433, 17)
        Me.lblClientname.Name = "lblClientname"
        Me.lblClientname.Size = New System.Drawing.Size(0, 16)
        Me.lblClientname.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(344, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(92, 14)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Client Name :"
        '
        'lblIDno
        '
        Me.lblIDno.AutoSize = True
        Me.lblIDno.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIDno.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.lblIDno.Location = New System.Drawing.Point(57, 18)
        Me.lblIDno.Name = "lblIDno"
        Me.lblIDno.Size = New System.Drawing.Size(0, 14)
        Me.lblIDno.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 14)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "ID No. :"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 67)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1066, 497)
        Me.Panel2.TabIndex = 62
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.White
        Me.Panel3.Controls.Add(Me.tabVerification)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(0, 0)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1066, 497)
        Me.Panel3.TabIndex = 59
        '
        'tabVerification
        '
        Me.tabVerification.Controls.Add(Me.CurrentLoan)
        Me.tabVerification.Controls.Add(Me.LoanHistory)
        Me.tabVerification.Controls.Add(Me.Debit)
        Me.tabVerification.Controls.Add(Me.Credit)
        Me.tabVerification.Controls.Add(Me.Amortization)
        Me.tabVerification.Controls.Add(Me.SOA)
        Me.tabVerification.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabVerification.Location = New System.Drawing.Point(0, 0)
        Me.tabVerification.Name = "tabVerification"
        Me.tabVerification.SelectedIndex = 0
        Me.tabVerification.Size = New System.Drawing.Size(1066, 497)
        Me.tabVerification.TabIndex = 0
        '
        'CurrentLoan
        '
        Me.CurrentLoan.BackColor = System.Drawing.Color.White
        Me.CurrentLoan.Controls.Add(Me.Panel5)
        Me.CurrentLoan.Controls.Add(Me.dgvSubsidiaryCurrent)
        Me.CurrentLoan.Controls.Add(Me.Panel4)
        Me.CurrentLoan.Controls.Add(Me.dgvLoans)
        Me.CurrentLoan.Controls.Add(Me.Panel6)
        Me.CurrentLoan.Location = New System.Drawing.Point(4, 23)
        Me.CurrentLoan.Name = "CurrentLoan"
        Me.CurrentLoan.Padding = New System.Windows.Forms.Padding(3)
        Me.CurrentLoan.Size = New System.Drawing.Size(1058, 470)
        Me.CurrentLoan.TabIndex = 1
        Me.CurrentLoan.Text = "Active Loan"
        '
        'Panel5
        '
        Me.Panel5.BackgroundImage = Global.CSAcctg.My.Resources.Resources.background
        Me.Panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel5.Controls.Add(Me.Label17)
        Me.Panel5.Controls.Add(Me.txtBalPrincipal)
        Me.Panel5.Controls.Add(Me.txtBalInterest)
        Me.Panel5.Controls.Add(Me.txtBalServicefee)
        Me.Panel5.Controls.Add(Me.txtBalCreditAcct)
        Me.Panel5.Controls.Add(Me.txtTotalPayment)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel5.Location = New System.Drawing.Point(3, 412)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(1052, 47)
        Me.Panel5.TabIndex = 24
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.Transparent
        Me.Label17.Dock = System.Windows.Forms.DockStyle.Right
        Me.Label17.Location = New System.Drawing.Point(480, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(72, 14)
        Me.Label17.TabIndex = 22
        Me.Label17.Text = "Balances :"
        '
        'txtBalPrincipal
        '
        Me.txtBalPrincipal.BackColor = System.Drawing.SystemColors.Info
        Me.txtBalPrincipal.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtBalPrincipal.Dock = System.Windows.Forms.DockStyle.Right
        Me.txtBalPrincipal.Location = New System.Drawing.Point(552, 0)
        Me.txtBalPrincipal.Name = "txtBalPrincipal"
        Me.txtBalPrincipal.ReadOnly = True
        Me.txtBalPrincipal.Size = New System.Drawing.Size(100, 15)
        Me.txtBalPrincipal.TabIndex = 24
        Me.txtBalPrincipal.Text = "0.00"
        Me.txtBalPrincipal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBalInterest
        '
        Me.txtBalInterest.BackColor = System.Drawing.SystemColors.Info
        Me.txtBalInterest.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtBalInterest.Dock = System.Windows.Forms.DockStyle.Right
        Me.txtBalInterest.Location = New System.Drawing.Point(652, 0)
        Me.txtBalInterest.Name = "txtBalInterest"
        Me.txtBalInterest.ReadOnly = True
        Me.txtBalInterest.Size = New System.Drawing.Size(100, 15)
        Me.txtBalInterest.TabIndex = 23
        Me.txtBalInterest.Text = "0.00"
        Me.txtBalInterest.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBalServicefee
        '
        Me.txtBalServicefee.BackColor = System.Drawing.SystemColors.Info
        Me.txtBalServicefee.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtBalServicefee.Dock = System.Windows.Forms.DockStyle.Right
        Me.txtBalServicefee.Location = New System.Drawing.Point(752, 0)
        Me.txtBalServicefee.Name = "txtBalServicefee"
        Me.txtBalServicefee.ReadOnly = True
        Me.txtBalServicefee.Size = New System.Drawing.Size(100, 15)
        Me.txtBalServicefee.TabIndex = 25
        Me.txtBalServicefee.Text = "0.00"
        Me.txtBalServicefee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBalCreditAcct
        '
        Me.txtBalCreditAcct.BackColor = System.Drawing.SystemColors.Info
        Me.txtBalCreditAcct.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtBalCreditAcct.Dock = System.Windows.Forms.DockStyle.Right
        Me.txtBalCreditAcct.Location = New System.Drawing.Point(852, 0)
        Me.txtBalCreditAcct.Name = "txtBalCreditAcct"
        Me.txtBalCreditAcct.ReadOnly = True
        Me.txtBalCreditAcct.Size = New System.Drawing.Size(100, 15)
        Me.txtBalCreditAcct.TabIndex = 27
        Me.txtBalCreditAcct.Text = "0.00"
        Me.txtBalCreditAcct.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalPayment
        '
        Me.txtTotalPayment.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotalPayment.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtTotalPayment.Dock = System.Windows.Forms.DockStyle.Right
        Me.txtTotalPayment.Location = New System.Drawing.Point(952, 0)
        Me.txtTotalPayment.Name = "txtTotalPayment"
        Me.txtTotalPayment.ReadOnly = True
        Me.txtTotalPayment.Size = New System.Drawing.Size(100, 15)
        Me.txtTotalPayment.TabIndex = 26
        Me.txtTotalPayment.Text = "0.00"
        Me.txtTotalPayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dgvSubsidiaryCurrent
        '
        Me.dgvSubsidiaryCurrent.AllowUserToAddRows = False
        Me.dgvSubsidiaryCurrent.AllowUserToDeleteRows = False
        Me.dgvSubsidiaryCurrent.AllowUserToResizeColumns = False
        Me.dgvSubsidiaryCurrent.AllowUserToResizeRows = False
        Me.dgvSubsidiaryCurrent.BackgroundColor = System.Drawing.Color.White
        Me.dgvSubsidiaryCurrent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSubsidiaryCurrent.Dock = System.Windows.Forms.DockStyle.Top
        Me.dgvSubsidiaryCurrent.Location = New System.Drawing.Point(3, 208)
        Me.dgvSubsidiaryCurrent.Name = "dgvSubsidiaryCurrent"
        Me.dgvSubsidiaryCurrent.ReadOnly = True
        Me.dgvSubsidiaryCurrent.RowHeadersVisible = False
        Me.dgvSubsidiaryCurrent.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSubsidiaryCurrent.Size = New System.Drawing.Size(1052, 204)
        Me.dgvSubsidiaryCurrent.TabIndex = 3
        '
        'Panel4
        '
        Me.Panel4.BackgroundImage = Global.CSAcctg.My.Resources.Resources.background
        Me.Panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel4.Controls.Add(Me.Label3)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel4.Location = New System.Drawing.Point(3, 171)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(1052, 37)
        Me.Panel4.TabIndex = 22
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(5, 15)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(124, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Subsidiary Ledger"
        '
        'dgvLoans
        '
        Me.dgvLoans.AllowUserToAddRows = False
        Me.dgvLoans.AllowUserToDeleteRows = False
        Me.dgvLoans.AllowUserToResizeColumns = False
        Me.dgvLoans.AllowUserToResizeRows = False
        Me.dgvLoans.BackgroundColor = System.Drawing.Color.White
        Me.dgvLoans.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLoans.Dock = System.Windows.Forms.DockStyle.Top
        Me.dgvLoans.Location = New System.Drawing.Point(3, 40)
        Me.dgvLoans.Name = "dgvLoans"
        Me.dgvLoans.ReadOnly = True
        Me.dgvLoans.RowHeadersVisible = False
        Me.dgvLoans.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLoans.Size = New System.Drawing.Size(1052, 131)
        Me.dgvLoans.TabIndex = 1
        '
        'Panel6
        '
        Me.Panel6.BackgroundImage = Global.CSAcctg.My.Resources.Resources.background
        Me.Panel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel6.Controls.Add(Me.Label20)
        Me.Panel6.Controls.Add(Me.btnFindLoan)
        Me.Panel6.Controls.Add(Me.txtSearchLoan)
        Me.Panel6.Controls.Add(Me.Label16)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel6.Location = New System.Drawing.Point(3, 3)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(1052, 37)
        Me.Panel6.TabIndex = 23
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.Location = New System.Drawing.Point(219, 15)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(59, 14)
        Me.Label20.TabIndex = 3
        Me.Label20.Text = "Search :"
        '
        'btnFindLoan
        '
        Me.btnFindLoan.Location = New System.Drawing.Point(531, 8)
        Me.btnFindLoan.Name = "btnFindLoan"
        Me.btnFindLoan.Size = New System.Drawing.Size(75, 23)
        Me.btnFindLoan.TabIndex = 2
        Me.btnFindLoan.Text = "Search"
        Me.btnFindLoan.UseVisualStyleBackColor = True
        '
        'txtSearchLoan
        '
        Me.txtSearchLoan.Location = New System.Drawing.Point(284, 9)
        Me.txtSearchLoan.Name = "txtSearchLoan"
        Me.txtSearchLoan.Size = New System.Drawing.Size(241, 22)
        Me.txtSearchLoan.TabIndex = 1
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(5, 15)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(139, 16)
        Me.Label16.TabIndex = 0
        Me.Label16.Text = "List of Active Loans"
        '
        'LoanHistory
        '
        Me.LoanHistory.BackColor = System.Drawing.Color.White
        Me.LoanHistory.Controls.Add(Me.dgvLoanHistorySubsidiary)
        Me.LoanHistory.Controls.Add(Me.Label9)
        Me.LoanHistory.Controls.Add(Me.dgvLoanHistory)
        Me.LoanHistory.Controls.Add(Me.Label12)
        Me.LoanHistory.Location = New System.Drawing.Point(4, 23)
        Me.LoanHistory.Name = "LoanHistory"
        Me.LoanHistory.Size = New System.Drawing.Size(1058, 470)
        Me.LoanHistory.TabIndex = 2
        Me.LoanHistory.Text = "Loan History"
        '
        'dgvLoanHistorySubsidiary
        '
        Me.dgvLoanHistorySubsidiary.AllowUserToAddRows = False
        Me.dgvLoanHistorySubsidiary.AllowUserToDeleteRows = False
        Me.dgvLoanHistorySubsidiary.AllowUserToResizeColumns = False
        Me.dgvLoanHistorySubsidiary.AllowUserToResizeRows = False
        Me.dgvLoanHistorySubsidiary.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvLoanHistorySubsidiary.BackgroundColor = System.Drawing.Color.White
        Me.dgvLoanHistorySubsidiary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLoanHistorySubsidiary.Location = New System.Drawing.Point(17, 172)
        Me.dgvLoanHistorySubsidiary.Name = "dgvLoanHistorySubsidiary"
        Me.dgvLoanHistorySubsidiary.ReadOnly = True
        Me.dgvLoanHistorySubsidiary.RowHeadersVisible = False
        Me.dgvLoanHistorySubsidiary.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLoanHistorySubsidiary.Size = New System.Drawing.Size(982, 220)
        Me.dgvLoanHistorySubsidiary.TabIndex = 7
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(14, 155)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(129, 14)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = "Subsidiary Ledger :"
        '
        'dgvLoanHistory
        '
        Me.dgvLoanHistory.AllowUserToAddRows = False
        Me.dgvLoanHistory.AllowUserToDeleteRows = False
        Me.dgvLoanHistory.AllowUserToResizeColumns = False
        Me.dgvLoanHistory.AllowUserToResizeRows = False
        Me.dgvLoanHistory.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvLoanHistory.BackgroundColor = System.Drawing.Color.White
        Me.dgvLoanHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLoanHistory.Location = New System.Drawing.Point(17, 28)
        Me.dgvLoanHistory.Name = "dgvLoanHistory"
        Me.dgvLoanHistory.ReadOnly = True
        Me.dgvLoanHistory.RowHeadersVisible = False
        Me.dgvLoanHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLoanHistory.Size = New System.Drawing.Size(982, 124)
        Me.dgvLoanHistory.TabIndex = 5
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(14, 11)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(127, 14)
        Me.Label12.TabIndex = 4
        Me.Label12.Text = "List of Paid Loans :"
        '
        'Debit
        '
        Me.Debit.BackColor = System.Drawing.Color.White
        Me.Debit.Controls.Add(Me.TBCDebitAccounts)
        Me.Debit.Controls.Add(Me.dgvDebitDetails)
        Me.Debit.Controls.Add(Me.Label6)
        Me.Debit.Location = New System.Drawing.Point(4, 23)
        Me.Debit.Name = "Debit"
        Me.Debit.Size = New System.Drawing.Size(1058, 470)
        Me.Debit.TabIndex = 3
        Me.Debit.Text = "Debit Accounts"
        '
        'TBCDebitAccounts
        '
        Me.TBCDebitAccounts.Controls.Add(Me.TabPage1)
        Me.TBCDebitAccounts.Controls.Add(Me.TabPage2)
        Me.TBCDebitAccounts.Location = New System.Drawing.Point(25, 3)
        Me.TBCDebitAccounts.Name = "TBCDebitAccounts"
        Me.TBCDebitAccounts.SelectedIndex = 0
        Me.TBCDebitAccounts.Size = New System.Drawing.Size(974, 241)
        Me.TBCDebitAccounts.TabIndex = 5
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.dgvDebitList)
        Me.TabPage1.Location = New System.Drawing.Point(4, 23)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(966, 214)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Debit Accounts"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 10)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(151, 14)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "List of Debit Accounts :"
        '
        'dgvDebitList
        '
        Me.dgvDebitList.AllowUserToAddRows = False
        Me.dgvDebitList.AllowUserToDeleteRows = False
        Me.dgvDebitList.AllowUserToResizeColumns = False
        Me.dgvDebitList.AllowUserToResizeRows = False
        Me.dgvDebitList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvDebitList.BackgroundColor = System.Drawing.Color.White
        Me.dgvDebitList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDebitList.Location = New System.Drawing.Point(16, 31)
        Me.dgvDebitList.Name = "dgvDebitList"
        Me.dgvDebitList.ReadOnly = True
        Me.dgvDebitList.RowHeadersVisible = False
        Me.dgvDebitList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDebitList.Size = New System.Drawing.Size(944, 170)
        Me.dgvDebitList.TabIndex = 1
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.dgvSoaList)
        Me.TabPage2.Location = New System.Drawing.Point(4, 23)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(966, 214)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "SOA Accounts"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'dgvSoaList
        '
        Me.dgvSoaList.AllowUserToAddRows = False
        Me.dgvSoaList.AllowUserToDeleteRows = False
        Me.dgvSoaList.AllowUserToResizeColumns = False
        Me.dgvSoaList.AllowUserToResizeRows = False
        Me.dgvSoaList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvSoaList.BackgroundColor = System.Drawing.Color.White
        Me.dgvSoaList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSoaList.Location = New System.Drawing.Point(16, 15)
        Me.dgvSoaList.Name = "dgvSoaList"
        Me.dgvSoaList.ReadOnly = True
        Me.dgvSoaList.RowHeadersVisible = False
        Me.dgvSoaList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSoaList.Size = New System.Drawing.Size(944, 183)
        Me.dgvSoaList.TabIndex = 2
        '
        'dgvDebitDetails
        '
        Me.dgvDebitDetails.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvDebitDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvDebitDetails.BackgroundColor = System.Drawing.Color.White
        Me.dgvDebitDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDebitDetails.Location = New System.Drawing.Point(26, 273)
        Me.dgvDebitDetails.Name = "dgvDebitDetails"
        Me.dgvDebitDetails.ReadOnly = True
        Me.dgvDebitDetails.RowHeadersVisible = False
        Me.dgvDebitDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDebitDetails.Size = New System.Drawing.Size(974, 166)
        Me.dgvDebitDetails.TabIndex = 3
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(26, 256)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(59, 14)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Details :"
        '
        'Credit
        '
        Me.Credit.BackColor = System.Drawing.Color.White
        Me.Credit.Controls.Add(Me.dgvCreditDetails)
        Me.Credit.Controls.Add(Me.Label8)
        Me.Credit.Controls.Add(Me.dgvCredit)
        Me.Credit.Controls.Add(Me.Label7)
        Me.Credit.Location = New System.Drawing.Point(4, 23)
        Me.Credit.Name = "Credit"
        Me.Credit.Size = New System.Drawing.Size(1058, 470)
        Me.Credit.TabIndex = 4
        Me.Credit.Text = "Credit Accounts"
        '
        'dgvCreditDetails
        '
        Me.dgvCreditDetails.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvCreditDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvCreditDetails.BackgroundColor = System.Drawing.Color.White
        Me.dgvCreditDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCreditDetails.Location = New System.Drawing.Point(31, 217)
        Me.dgvCreditDetails.Name = "dgvCreditDetails"
        Me.dgvCreditDetails.ReadOnly = True
        Me.dgvCreditDetails.RowHeadersVisible = False
        Me.dgvCreditDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCreditDetails.Size = New System.Drawing.Size(968, 160)
        Me.dgvCreditDetails.TabIndex = 3
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(28, 200)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(59, 14)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "Details :"
        '
        'dgvCredit
        '
        Me.dgvCredit.AllowUserToAddRows = False
        Me.dgvCredit.AllowUserToDeleteRows = False
        Me.dgvCredit.AllowUserToResizeColumns = False
        Me.dgvCredit.AllowUserToResizeRows = False
        Me.dgvCredit.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvCredit.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvCredit.BackgroundColor = System.Drawing.Color.White
        Me.dgvCredit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCredit.Location = New System.Drawing.Point(28, 26)
        Me.dgvCredit.Name = "dgvCredit"
        Me.dgvCredit.ReadOnly = True
        Me.dgvCredit.RowHeadersVisible = False
        Me.dgvCredit.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCredit.Size = New System.Drawing.Size(971, 171)
        Me.dgvCredit.TabIndex = 1
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(25, 9)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(156, 14)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "List of Credit Accounts :"
        '
        'Amortization
        '
        Me.Amortization.Controls.Add(Me.btnDynamicPrintAmortization)
        Me.Amortization.Controls.Add(Me.txtServiceFee)
        Me.Amortization.Controls.Add(Me.txtPrincipal)
        Me.Amortization.Controls.Add(Me.txtInterest)
        Me.Amortization.Controls.Add(Me.Label15)
        Me.Amortization.Controls.Add(Me.lblLoanType)
        Me.Amortization.Controls.Add(Me.lblLoanRef)
        Me.Amortization.Controls.Add(Me.Label14)
        Me.Amortization.Controls.Add(Me.Label13)
        Me.Amortization.Controls.Add(Me.dgvAmortizationBalances)
        Me.Amortization.Controls.Add(Me.Label10)
        Me.Amortization.Controls.Add(Me.dgvAmortizationSchedule)
        Me.Amortization.Controls.Add(Me.Label4)
        Me.Amortization.Controls.Add(Me.btnSelectedLoanPrint)
        Me.Amortization.Location = New System.Drawing.Point(4, 23)
        Me.Amortization.Name = "Amortization"
        Me.Amortization.Size = New System.Drawing.Size(1058, 470)
        Me.Amortization.TabIndex = 5
        Me.Amortization.Text = "Amortization"
        Me.Amortization.UseVisualStyleBackColor = True
        '
        'btnDynamicPrintAmortization
        '
        Me.btnDynamicPrintAmortization.Location = New System.Drawing.Point(246, 379)
        Me.btnDynamicPrintAmortization.Name = "btnDynamicPrintAmortization"
        Me.btnDynamicPrintAmortization.Size = New System.Drawing.Size(116, 35)
        Me.btnDynamicPrintAmortization.TabIndex = 23
        Me.btnDynamicPrintAmortization.Text = "Print Test"
        Me.btnDynamicPrintAmortization.UseVisualStyleBackColor = True
        Me.btnDynamicPrintAmortization.Visible = False
        '
        'txtServiceFee
        '
        Me.txtServiceFee.BackColor = System.Drawing.SystemColors.Info
        Me.txtServiceFee.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtServiceFee.Location = New System.Drawing.Point(641, 201)
        Me.txtServiceFee.Name = "txtServiceFee"
        Me.txtServiceFee.ReadOnly = True
        Me.txtServiceFee.Size = New System.Drawing.Size(100, 15)
        Me.txtServiceFee.TabIndex = 15
        Me.txtServiceFee.Text = "0.00"
        Me.txtServiceFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPrincipal
        '
        Me.txtPrincipal.BackColor = System.Drawing.SystemColors.Info
        Me.txtPrincipal.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtPrincipal.Location = New System.Drawing.Point(328, 202)
        Me.txtPrincipal.Name = "txtPrincipal"
        Me.txtPrincipal.ReadOnly = True
        Me.txtPrincipal.Size = New System.Drawing.Size(100, 15)
        Me.txtPrincipal.TabIndex = 14
        Me.txtPrincipal.Text = "0.00"
        Me.txtPrincipal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtInterest
        '
        Me.txtInterest.BackColor = System.Drawing.SystemColors.Info
        Me.txtInterest.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtInterest.Location = New System.Drawing.Point(492, 201)
        Me.txtInterest.Name = "txtInterest"
        Me.txtInterest.ReadOnly = True
        Me.txtInterest.Size = New System.Drawing.Size(100, 15)
        Me.txtInterest.TabIndex = 13
        Me.txtInterest.Text = "0.00"
        Me.txtInterest.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(268, 203)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(54, 14)
        Me.Label15.TabIndex = 12
        Me.Label15.Text = "Totals: "
        '
        'lblLoanType
        '
        Me.lblLoanType.AutoSize = True
        Me.lblLoanType.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanType.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.lblLoanType.Location = New System.Drawing.Point(521, 10)
        Me.lblLoanType.Name = "lblLoanType"
        Me.lblLoanType.Size = New System.Drawing.Size(13, 14)
        Me.lblLoanType.TabIndex = 11
        Me.lblLoanType.Text = "-"
        '
        'lblLoanRef
        '
        Me.lblLoanRef.AutoSize = True
        Me.lblLoanRef.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanRef.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.lblLoanRef.Location = New System.Drawing.Point(268, 10)
        Me.lblLoanRef.Name = "lblLoanRef"
        Me.lblLoanRef.Size = New System.Drawing.Size(13, 14)
        Me.lblLoanRef.TabIndex = 10
        Me.lblLoanRef.Text = "-"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(473, 10)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(42, 14)
        Me.Label14.TabIndex = 9
        Me.Label14.Text = "Title :"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(166, 10)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(96, 14)
        Me.Label13.TabIndex = 8
        Me.Label13.Text = "Reference No:"
        '
        'dgvAmortizationBalances
        '
        Me.dgvAmortizationBalances.AllowUserToAddRows = False
        Me.dgvAmortizationBalances.AllowUserToDeleteRows = False
        Me.dgvAmortizationBalances.AllowUserToResizeColumns = False
        Me.dgvAmortizationBalances.AllowUserToResizeRows = False
        Me.dgvAmortizationBalances.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvAmortizationBalances.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvAmortizationBalances.BackgroundColor = System.Drawing.Color.White
        Me.dgvAmortizationBalances.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAmortizationBalances.Location = New System.Drawing.Point(11, 238)
        Me.dgvAmortizationBalances.Name = "dgvAmortizationBalances"
        Me.dgvAmortizationBalances.ReadOnly = True
        Me.dgvAmortizationBalances.RowHeadersVisible = False
        Me.dgvAmortizationBalances.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAmortizationBalances.Size = New System.Drawing.Size(982, 139)
        Me.dgvAmortizationBalances.TabIndex = 7
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(8, 221)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(60, 14)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "Balance "
        '
        'dgvAmortizationSchedule
        '
        Me.dgvAmortizationSchedule.AllowUserToAddRows = False
        Me.dgvAmortizationSchedule.AllowUserToDeleteRows = False
        Me.dgvAmortizationSchedule.AllowUserToResizeColumns = False
        Me.dgvAmortizationSchedule.AllowUserToResizeRows = False
        Me.dgvAmortizationSchedule.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvAmortizationSchedule.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvAmortizationSchedule.BackgroundColor = System.Drawing.Color.White
        Me.dgvAmortizationSchedule.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAmortizationSchedule.Location = New System.Drawing.Point(11, 53)
        Me.dgvAmortizationSchedule.Name = "dgvAmortizationSchedule"
        Me.dgvAmortizationSchedule.ReadOnly = True
        Me.dgvAmortizationSchedule.RowHeadersVisible = False
        Me.dgvAmortizationSchedule.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAmortizationSchedule.Size = New System.Drawing.Size(982, 136)
        Me.dgvAmortizationSchedule.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 36)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(151, 14)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Amortization Schedule "
        '
        'btnSelectedLoanPrint
        '
        Me.btnSelectedLoanPrint.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnSelectedLoanPrint.Image = CType(resources.GetObject("btnSelectedLoanPrint.Image"), System.Drawing.Image)
        Me.btnSelectedLoanPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSelectedLoanPrint.Location = New System.Drawing.Point(11, 379)
        Me.btnSelectedLoanPrint.Name = "btnSelectedLoanPrint"
        Me.btnSelectedLoanPrint.Size = New System.Drawing.Size(229, 35)
        Me.btnSelectedLoanPrint.TabIndex = 22
        Me.btnSelectedLoanPrint.Text = "Preview Loan Amortization"
        Me.btnSelectedLoanPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSelectedLoanPrint.UseVisualStyleBackColor = True
        '
        'SOA
        '
        Me.SOA.Controls.Add(Me.CrvRpt)
        Me.SOA.Controls.Add(Me.Panel9)
        Me.SOA.Location = New System.Drawing.Point(4, 23)
        Me.SOA.Name = "SOA"
        Me.SOA.Size = New System.Drawing.Size(1058, 470)
        Me.SOA.TabIndex = 6
        Me.SOA.Text = "SOA"
        Me.SOA.UseVisualStyleBackColor = True
        '
        'CrvRpt
        '
        Me.CrvRpt.ActiveViewIndex = -1
        Me.CrvRpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CrvRpt.CachedPageNumberPerDoc = 10
        Me.CrvRpt.Cursor = System.Windows.Forms.Cursors.Default
        Me.CrvRpt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrvRpt.EnableDrillDown = False
        Me.CrvRpt.Location = New System.Drawing.Point(0, 33)
        Me.CrvRpt.Name = "CrvRpt"
        Me.CrvRpt.SelectionFormula = ""
        Me.CrvRpt.ShowGroupTreeButton = False
        Me.CrvRpt.ShowRefreshButton = False
        Me.CrvRpt.Size = New System.Drawing.Size(1058, 438)
        Me.CrvRpt.TabIndex = 60
        Me.CrvRpt.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        Me.CrvRpt.ViewTimeSelectionFormula = ""
        '
        'Panel9
        '
        Me.Panel9.BackColor = System.Drawing.Color.White
        Me.Panel9.Controls.Add(Me.btnBrowse)
        Me.Panel9.Controls.Add(Me.txtSoa)
        Me.Panel9.Controls.Add(Me.RBHistory)
        Me.Panel9.Controls.Add(Me.RBBalance)
        Me.Panel9.Controls.Add(Me.RBPerSoa)
        Me.Panel9.Controls.Add(Me.RBAll)
        Me.Panel9.Controls.Add(Me.Label18)
        Me.Panel9.Controls.Add(Me.dtDate)
        Me.Panel9.Controls.Add(Me.btnPrint)
        Me.Panel9.Controls.Add(Me.btnPreview)
        Me.Panel9.Controls.Add(Me.cboFilterSoa)
        Me.Panel9.Controls.Add(Me.Label11)
        Me.Panel9.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel9.Location = New System.Drawing.Point(0, 0)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(1058, 33)
        Me.Panel9.TabIndex = 0
        '
        'btnBrowse
        '
        Me.btnBrowse.Location = New System.Drawing.Point(831, 6)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(31, 24)
        Me.btnBrowse.TabIndex = 23
        Me.btnBrowse.Text = "....."
        Me.btnBrowse.UseVisualStyleBackColor = True
        Me.btnBrowse.Visible = False
        '
        'txtSoa
        '
        Me.txtSoa.Location = New System.Drawing.Point(718, 7)
        Me.txtSoa.Name = "txtSoa"
        Me.txtSoa.Size = New System.Drawing.Size(107, 22)
        Me.txtSoa.TabIndex = 22
        Me.txtSoa.Visible = False
        '
        'RBHistory
        '
        Me.RBHistory.AutoSize = True
        Me.RBHistory.Location = New System.Drawing.Point(561, 9)
        Me.RBHistory.Name = "RBHistory"
        Me.RBHistory.Size = New System.Drawing.Size(57, 17)
        Me.RBHistory.TabIndex = 21
        Me.RBHistory.TabStop = True
        Me.RBHistory.Text = "History"
        Me.RBHistory.UseVisualStyleBackColor = True
        Me.RBHistory.Visible = False
        '
        'RBBalance
        '
        Me.RBBalance.AutoSize = True
        Me.RBBalance.Location = New System.Drawing.Point(450, 8)
        Me.RBBalance.Name = "RBBalance"
        Me.RBBalance.Size = New System.Drawing.Size(82, 17)
        Me.RBBalance.TabIndex = 20
        Me.RBBalance.TabStop = True
        Me.RBBalance.Text = "Outstanding"
        Me.RBBalance.UseVisualStyleBackColor = True
        Me.RBBalance.Visible = False
        '
        'RBPerSoa
        '
        Me.RBPerSoa.AutoSize = True
        Me.RBPerSoa.Location = New System.Drawing.Point(636, 9)
        Me.RBPerSoa.Name = "RBPerSoa"
        Me.RBPerSoa.Size = New System.Drawing.Size(66, 17)
        Me.RBPerSoa.TabIndex = 19
        Me.RBPerSoa.TabStop = True
        Me.RBPerSoa.Text = "Per SOA"
        Me.RBPerSoa.UseVisualStyleBackColor = True
        Me.RBPerSoa.Visible = False
        '
        'RBAll
        '
        Me.RBAll.AutoSize = True
        Me.RBAll.Location = New System.Drawing.Point(393, 8)
        Me.RBAll.Name = "RBAll"
        Me.RBAll.Size = New System.Drawing.Size(36, 17)
        Me.RBAll.TabIndex = 18
        Me.RBAll.TabStop = True
        Me.RBAll.Text = "All"
        Me.RBAll.UseVisualStyleBackColor = True
        Me.RBAll.Visible = False
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(390, 10)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(88, 14)
        Me.Label18.TabIndex = 5
        Me.Label18.Text = "Select Date :"
        '
        'dtDate
        '
        Me.dtDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtDate.Location = New System.Drawing.Point(484, 5)
        Me.dtDate.Name = "dtDate"
        Me.dtDate.Size = New System.Drawing.Size(169, 22)
        Me.dtDate.TabIndex = 4
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.Location = New System.Drawing.Point(977, 6)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(75, 23)
        Me.btnPrint.TabIndex = 3
        Me.btnPrint.Text = "Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnPreview
        '
        Me.btnPreview.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPreview.Location = New System.Drawing.Point(896, 6)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(75, 23)
        Me.btnPreview.TabIndex = 2
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'cboFilterSoa
        '
        Me.cboFilterSoa.AutoCompleteCustomSource.AddRange(New String() {"Statement Of Accounts - Loan", "Statement Of Accounts - Detailed"})
        Me.cboFilterSoa.FormattingEnabled = True
        Me.cboFilterSoa.Items.AddRange(New Object() {"Statement Of Accounts - Loan", "Statement Of Accounts - Detailed", "Statement Of Accounts - Accounts Receivable"})
        Me.cboFilterSoa.Location = New System.Drawing.Point(86, 5)
        Me.cboFilterSoa.Name = "cboFilterSoa"
        Me.cboFilterSoa.Size = New System.Drawing.Size(296, 22)
        Me.cboFilterSoa.TabIndex = 1
        Me.cboFilterSoa.Text = "Select"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(3, 10)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(77, 14)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Filter SOA :"
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.White
        Me.Panel8.BackgroundImage = Global.CSAcctg.My.Resources.Resources.background
        Me.Panel8.Controls.Add(Me.Label22)
        Me.Panel8.Controls.Add(Me.GroupBox2)
        Me.Panel8.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel8.Location = New System.Drawing.Point(0, 0)
        Me.Panel8.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(1066, 22)
        Me.Panel8.TabIndex = 60
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.BackColor = System.Drawing.Color.Transparent
        Me.Label22.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.White
        Me.Label22.Location = New System.Drawing.Point(4, 1)
        Me.Label22.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(142, 19)
        Me.Label22.TabIndex = 15
        Me.Label22.Text = "Account Inquiry "
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label23)
        Me.GroupBox2.Controls.Add(Me.Label24)
        Me.GroupBox2.Controls.Add(Me.Label25)
        Me.GroupBox2.Controls.Add(Me.Label26)
        Me.GroupBox2.Location = New System.Drawing.Point(67, 25)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1020, 42)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label23.Location = New System.Drawing.Point(433, 17)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(0, 16)
        Me.Label23.TabIndex = 1
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(344, 18)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(92, 14)
        Me.Label24.TabIndex = 1
        Me.Label24.Text = "Client Name :"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label25.Location = New System.Drawing.Point(57, 18)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(0, 14)
        Me.Label25.TabIndex = 0
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(6, 18)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(55, 14)
        Me.Label26.TabIndex = 0
        Me.Label26.Text = "ID No. :"
        '
        'Panel7
        '
        Me.Panel7.BackgroundImage = Global.CSAcctg.My.Resources.Resources.background
        Me.Panel7.Controls.Add(Me.btnFresh)
        Me.Panel7.Controls.Add(Me.btnPrintLedgers)
        Me.Panel7.Controls.Add(Me.btnPrintIndividual)
        Me.Panel7.Controls.Add(Me.btnFind)
        Me.Panel7.Controls.Add(Me.btnCancel)
        Me.Panel7.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel7.Location = New System.Drawing.Point(0, 564)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(1066, 30)
        Me.Panel7.TabIndex = 59
        '
        'btnFresh
        '
        Me.btnFresh.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnFresh.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFresh.Image = Global.CSAcctg.My.Resources.Resources.reload1
        Me.btnFresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnFresh.Location = New System.Drawing.Point(787, 0)
        Me.btnFresh.Name = "btnFresh"
        Me.btnFresh.Size = New System.Drawing.Size(98, 30)
        Me.btnFresh.TabIndex = 29
        Me.btnFresh.Text = "Refresh"
        Me.btnFresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnFresh.UseVisualStyleBackColor = True
        '
        'btnPrintLedgers
        '
        Me.btnPrintLedgers.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnPrintLedgers.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrintLedgers.Image = CType(resources.GetObject("btnPrintLedgers.Image"), System.Drawing.Image)
        Me.btnPrintLedgers.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPrintLedgers.Location = New System.Drawing.Point(212, 0)
        Me.btnPrintLedgers.Name = "btnPrintLedgers"
        Me.btnPrintLedgers.Size = New System.Drawing.Size(174, 30)
        Me.btnPrintLedgers.TabIndex = 28
        Me.btnPrintLedgers.Text = "Print All Ledgers"
        Me.btnPrintLedgers.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPrintLedgers.UseVisualStyleBackColor = True
        '
        'btnPrintIndividual
        '
        Me.btnPrintIndividual.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnPrintIndividual.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrintIndividual.Image = CType(resources.GetObject("btnPrintIndividual.Image"), System.Drawing.Image)
        Me.btnPrintIndividual.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPrintIndividual.Location = New System.Drawing.Point(0, 0)
        Me.btnPrintIndividual.Name = "btnPrintIndividual"
        Me.btnPrintIndividual.Size = New System.Drawing.Size(212, 30)
        Me.btnPrintIndividual.TabIndex = 27
        Me.btnPrintIndividual.Text = "Print Individual Ledger"
        Me.btnPrintIndividual.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPrintIndividual.UseVisualStyleBackColor = True
        '
        'btnFind
        '
        Me.btnFind.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnFind.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFind.Image = CType(resources.GetObject("btnFind.Image"), System.Drawing.Image)
        Me.btnFind.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnFind.Location = New System.Drawing.Point(885, 0)
        Me.btnFind.Name = "btnFind"
        Me.btnFind.Size = New System.Drawing.Size(106, 30)
        Me.btnFind.TabIndex = 26
        Me.btnFind.Text = "Search"
        Me.btnFind.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnFind.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnCancel.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Image = Global.CSAcctg.My.Resources.Resources.button_cancel
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(991, 0)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 30)
        Me.btnCancel.TabIndex = 25
        Me.btnCancel.Text = "Close"
        Me.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Check"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Width = 109
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Service Fee"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Width = 109
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Interest"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Width = 108
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Principal"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Width = 109
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Credit"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Width = 109
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Debit"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 109
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Particulars"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Width = 108
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Doc No."
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Width = 109
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Date"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Width = 109
        '
        'frmVerification
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1066, 594)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel8)
        Me.Controls.Add(Me.Panel7)
        Me.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Name = "frmVerification"
        Me.Text = "..."
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.tabVerification.ResumeLayout(False)
        Me.CurrentLoan.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.dgvSubsidiaryCurrent, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.dgvLoans, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.LoanHistory.ResumeLayout(False)
        Me.LoanHistory.PerformLayout()
        CType(Me.dgvLoanHistorySubsidiary, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvLoanHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Debit.ResumeLayout(False)
        Me.Debit.PerformLayout()
        Me.TBCDebitAccounts.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.dgvDebitList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.dgvSoaList, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDebitDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Credit.ResumeLayout(False)
        Me.Credit.PerformLayout()
        CType(Me.dgvCreditDetails, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvCredit, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Amortization.ResumeLayout(False)
        Me.Amortization.PerformLayout()
        CType(Me.dgvAmortizationBalances, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvAmortizationSchedule, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SOA.ResumeLayout(False)
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents btnPrintLoanLedger As System.Windows.Forms.Button
    Friend WithEvents btnPrintStatement As System.Windows.Forms.Button
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents btnFresh As System.Windows.Forms.Button
    Friend WithEvents btnPrintLedgers As System.Windows.Forms.Button
    Friend WithEvents btnPrintIndividual As System.Windows.Forms.Button
    Friend WithEvents btnFind As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblClientname As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblIDno As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents tabVerification As System.Windows.Forms.TabControl
    Friend WithEvents CurrentLoan As System.Windows.Forms.TabPage
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtBalPrincipal As System.Windows.Forms.TextBox
    Friend WithEvents txtBalInterest As System.Windows.Forms.TextBox
    Friend WithEvents txtBalServicefee As System.Windows.Forms.TextBox
    Friend WithEvents txtBalCreditAcct As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalPayment As System.Windows.Forms.TextBox
    Friend WithEvents dgvSubsidiaryCurrent As System.Windows.Forms.DataGridView
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dgvLoans As System.Windows.Forms.DataGridView
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents btnFindLoan As System.Windows.Forms.Button
    Friend WithEvents txtSearchLoan As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents LoanHistory As System.Windows.Forms.TabPage
    Friend WithEvents dgvLoanHistorySubsidiary As System.Windows.Forms.DataGridView
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents dgvLoanHistory As System.Windows.Forms.DataGridView
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Debit As System.Windows.Forms.TabPage
    Friend WithEvents dgvDebitDetails As System.Windows.Forms.DataGridView
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Credit As System.Windows.Forms.TabPage
    Friend WithEvents dgvCreditDetails As System.Windows.Forms.DataGridView
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents dgvCredit As System.Windows.Forms.DataGridView
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Amortization As System.Windows.Forms.TabPage
    Friend WithEvents btnDynamicPrintAmortization As System.Windows.Forms.Button
    Friend WithEvents btnSelectedLoanPrint As System.Windows.Forms.Button
    Friend WithEvents txtServiceFee As System.Windows.Forms.TextBox
    Friend WithEvents txtPrincipal As System.Windows.Forms.TextBox
    Friend WithEvents txtInterest As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents lblLoanType As System.Windows.Forms.Label
    Friend WithEvents lblLoanRef As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents dgvAmortizationBalances As System.Windows.Forms.DataGridView
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents dgvAmortizationSchedule As System.Windows.Forms.DataGridView
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents SOA As System.Windows.Forms.TabPage
    Private WithEvents CrvRpt As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents dtDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents cboFilterSoa As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents txtSoa As System.Windows.Forms.TextBox
    Friend WithEvents RBHistory As System.Windows.Forms.RadioButton
    Friend WithEvents RBBalance As System.Windows.Forms.RadioButton
    Friend WithEvents RBPerSoa As System.Windows.Forms.RadioButton
    Friend WithEvents RBAll As System.Windows.Forms.RadioButton
    Friend WithEvents TBCDebitAccounts As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dgvDebitList As System.Windows.Forms.DataGridView
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents dgvSoaList As System.Windows.Forms.DataGridView
    'Friend WithEvents PanePanel5 As WindowsApplication2.PanePanel - CMS
    'Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel - CMS
End Class
