﻿Imports System
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Public Class frmVerification

    Private mycon As New Clsappconfiguration
    Private loanNo As String
    Private printAmortMode As String

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        frmVerification_Search.StartPosition = FormStartPosition.CenterScreen
        frmVerification_Search.txtSearch.Text = ""
        frmVerification_Search.ActiveControl = frmVerification_Search.txtSearch
        frmVerification_Search.ShowDialog()
    End Sub

    Private Sub lblIDno_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblIDno.TextChanged
        _Correct_Balance_Always()

        LoadLoans(txtsearch.Text)
        LoadLoanHistory()
        LoadDebitAccounts()
        LoadSoaAccounts()
        LoadCreditAccounts()

        AuditTrail_Save("AIMS - Account Inquiry", "View Ledger - ID No. :" + lblIDno.Text)
        dgvCreditDetails.Columns.Clear()
        dgvDebitDetails.Columns.Clear()
        dgvAmortizationSchedule.Columns.Clear()
        dgvSubsidiaryCurrent.Columns.Clear()
        dgvLoanHistorySubsidiary.Columns.Clear()
        dgvAmortizationBalances.Columns.Clear()
    End Sub

    Private Sub LoadLoanHistory()
        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(mycon.cnstring, "_Account_Verification_LoanHistory",
                                           New SqlParameter("@IDNo", lblIDno.Text))
            dgvLoanHistory.DataSource = ds.Tables(0)
        Catch ex As Exception
            'frmMsgBox.txtMessage.Text = "Error :" + ex.ToString
            'frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub LoadLoans(ByVal key As String)
        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(mycon.cnstring, "CMS_Select_Loaninfo_Verification",
                                           New SqlParameter("@IDNo", lblIDno.Text),
                                           New SqlParameter("@key", key))
            dgvLoans.DataSource = ds.Tables(0)
        Catch ex As Exception
            'frmMsgBox.txtMessage.Text = "Error :" + ex.ToString
            'frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub LoadAmortizationStatus()
        Try
            dgvAmortizationSchedule.DataSource = Nothing
            dgvAmortizationSchedule.Rows.Clear()
            dgvAmortizationSchedule.Columns.Clear()

            Dim ds As DataSet

            ds = SqlHelper.ExecuteDataset(mycon.cnstring, "_CIMS_LoanLedger_SelectAmortization",
                                            New SqlParameter("@LoanNo", loanNo))
            dgvAmortizationSchedule.DataSource = ds.Tables(0)

            dgvAmortizationSchedule.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvAmortizationSchedule.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvAmortizationSchedule.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvAmortizationSchedule.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            'GetTotals()

            If Not _is_Loan_Approved(loanNo) Then
                For i As Integer = 0 To dgvAmortizationSchedule.RowCount - 1
                    With dgvAmortizationSchedule.Rows(i)
                        .Cells(1).Value = "--/--/--"
                    End With
                Next
            End If
            _LoadInPaymentCharges()
        Catch ex As Exception
            'frmMsgBox.txtMessage.Text = "Error :" + ex.ToString
            'frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub GetTotals()
        Dim totPrincipal As Double
        Dim totInterest As Double
        Dim totServicefee As Double

        For i As Integer = 0 To dgvAmortizationSchedule.RowCount - 1
            With dgvAmortizationSchedule
                totPrincipal = totPrincipal + CDec(.Rows(0).Cells(2).Value)
                totInterest = totInterest + CDec(.Rows(0).Cells(3).Value)
                totServicefee = totServicefee + CDec(.Rows(0).Cells(4).Value)
            End With
        Next

        txtPrincipal.Text = Format(CDec(totPrincipal), "##,##0.00")
        txtInterest.Text = Format(CDec(totInterest), "##,##0.00")
        txtServiceFee.Text = Format(CDec(totServicefee), "##,##0.00")

    End Sub

    Private Sub Get_Balances_And_Total()
        Dim TotalPayment As Double

        For i As Integer = 0 To dgvSubsidiaryCurrent.RowCount - 1
            With dgvSubsidiaryCurrent
                TotalPayment = TotalPayment + CDec(.Rows(i).Cells(8).Value)
            End With
        Next

        txtTotalPayment.Text = Format(CDec(TotalPayment), "##,##0.00")
    End Sub

    Private Sub dgvLoans_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvLoans.Click
        Try
            With dgvLoans
                'loanNo = .SelectedRows(0).Cells(0).Value.ToString
                'lblLoanRef.Text = loanNo
                'lblLoanType.Text = .SelectedRows(0).Cells(1).Value.ToString

                'txtPrincipal.Text = .SelectedRows(0).Cells(7).Value.ToString
                'txtInterest.Text = .SelectedRows(0).Cells(8).Value.ToString
                'txtServiceFee.Text = .SelectedRows(0).Cells(9).Value.ToString

                loanNo = .CurrentRow.Cells(0).Value.ToString
                lblLoanRef.Text = loanNo
                lblLoanType.Text = .CurrentRow.Cells(1).Value.ToString

                txtPrincipal.Text = .CurrentRow.Cells(7).Value.ToString
                txtInterest.Text = .CurrentRow.Cells(8).Value.ToString
                txtServiceFee.Text = .CurrentRow.Cells(9).Value.ToString
            End With

            GetSubsidiary_Balances(loanNo)
            LoadAmortizationStatus()
            LoanSubsidiary_Current(loanNo)
            printAmortMode = "loan"
            ' Get_Balances_And_Total()
        Catch ex As Exception
            'frmMsgBox.txtMessage.Text = "Oops , No Current Loan Listed!"
            'frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Function _is_Loan_Approved(ByVal loanno As String) As Boolean
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, "_Check_LoanStatus",
                                                               New SqlParameter("@LoanNo", loanno))
            If rd.HasRows Then
                Return True 'approved
            Else
                Return False 'not approved
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

#Region "loan subsidiary"
    Private Sub Generate_Subsidiary()
        Dim sdate As New DataGridViewTextBoxColumn
        Dim docnum As New DataGridViewTextBoxColumn
        Dim particulars As New DataGridViewTextBoxColumn
        Dim debit As New DataGridViewTextBoxColumn
        Dim credit As New DataGridViewTextBoxColumn
        Dim principal As New DataGridViewTextBoxColumn
        Dim interest As New DataGridViewTextBoxColumn
        Dim servicefee As New DataGridViewTextBoxColumn
        Dim credacct As New DataGridViewTextBoxColumn
        Dim payment As New DataGridViewTextBoxColumn
        Dim unearnedinterest As New DataGridViewTextBoxColumn
        Dim check As New DataGridViewCheckBoxColumn

        With sdate
            .Name = "sDate"
            .HeaderText = "Date"
        End With
        With docnum
            .Name = "DocNum"
            .HeaderText = "Doc. No."
        End With
        With particulars
            .Name = "Particulars"
            .HeaderText = "Particulars"
            .Width = 250
        End With
        With debit
            .Name = "Debit"
            .HeaderText = "Debit"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With credit
            .Name = "Credit"
            .HeaderText = "Credit"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With principal
            .Name = "Principal"
            .HeaderText = "Principal"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With interest
            .Name = "Interest"
            .HeaderText = "Interest"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With servicefee
            .Name = "ServiceFee"
            .HeaderText = "Service Fee"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With credacct
            .Name = "CreditAcct"
            .HeaderText = "Credit Acct."
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With payment
            .Name = "Payment"
            .HeaderText = "Payment"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With unearnedinterest
            .Name = "UnearnedInterest"
            .HeaderText = "Unearned Interest"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With check
            .Name = "Check"
            .HeaderText = "Check"
            .Width = 50
        End With

        With dgvSubsidiaryCurrent
            .Columns.Add(sdate)
            .Columns.Add(docnum)
            .Columns.Add(particulars)
            .Columns.Add(debit)
            .Columns.Add(credit)
            .Columns.Add(principal)
            .Columns.Add(interest)
            .Columns.Add(servicefee)
            .Columns.Add(credacct)
            .Columns.Add(payment)
            .Columns.Add(unearnedinterest)
            .Columns.Add(check)
        End With
    End Sub
    Private Sub Generate_SubsidiaryHistory()
        Dim sdate As New DataGridViewTextBoxColumn
        Dim docnum As New DataGridViewTextBoxColumn
        Dim particulars As New DataGridViewTextBoxColumn
        Dim debit As New DataGridViewTextBoxColumn
        Dim credit As New DataGridViewTextBoxColumn
        Dim principal As New DataGridViewTextBoxColumn
        Dim interest As New DataGridViewTextBoxColumn
        Dim servicefee As New DataGridViewTextBoxColumn
        Dim credacct As New DataGridViewTextBoxColumn
        Dim payment As New DataGridViewTextBoxColumn
        Dim unearnedinterest As New DataGridViewTextBoxColumn
        Dim check As New DataGridViewCheckBoxColumn

        With sdate
            .Name = "sDate"
            .HeaderText = "Date"
        End With
        With docnum
            .Name = "DocNum"
            .HeaderText = "Doc. No."
        End With
        With particulars
            .Name = "Particulars"
            .HeaderText = "Particulars"
            .Width = 250
        End With
        With debit
            .Name = "Debit"
            .HeaderText = "Debit"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With credit
            .Name = "Credit"
            .HeaderText = "Credit"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With principal
            .Name = "Principal"
            .HeaderText = "Principal"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With interest
            .Name = "Interest"
            .HeaderText = "Interest"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With servicefee
            .Name = "ServiceFee"
            .HeaderText = "Service Fee"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With credacct
            .Name = "CreditAcct"
            .HeaderText = "Credit Acct."
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With payment
            .Name = "Payment"
            .HeaderText = "Payment"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With unearnedinterest
            .Name = "UnearnedInterest"
            .HeaderText = "Unearned Interest"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With check
            .Name = "Check"
            .HeaderText = "Check"
            .Width = 50
        End With

        With dgvLoanHistorySubsidiary
            .Columns.Add(sdate)
            .Columns.Add(docnum)
            .Columns.Add(particulars)
            .Columns.Add(debit)
            .Columns.Add(credit)
            .Columns.Add(principal)
            .Columns.Add(interest)
            .Columns.Add(servicefee)
            .Columns.Add(credacct)
            .Columns.Add(payment)
            .Columns.Add(unearnedinterest)
            .Columns.Add(check)
        End With
    End Sub
#End Region

    Private Sub LoanSubsidiary_Current(ByVal loanno As String)
        dgvSubsidiaryCurrent.Columns.Clear()
        Dim i As Integer = 0
        Try
            Generate_Subsidiary()
            Dim rd As SqlDataReader
            Dim curBal As Decimal = 0
            rd = SqlHelper.ExecuteReader(mycon.cnstring, "_View_Subsidiary_Loans_v2",
                                          New SqlParameter("@LoanNo", loanno))
            While rd.Read
                With dgvSubsidiaryCurrent
                    .Rows.Add()
                    If i = 0 Then
                        If rd(3) = 0 And rd(4) <> 0 Then 'credit
                            curBal = rd(3) - rd(4)
                            .Rows(i).Cells(0).Value = rd(0)
                            .Rows(i).Cells(1).Value = rd(1)
                            .Rows(i).Cells(2).Value = rd(2)
                            .Rows(i).Cells(3).Value = rd(3)
                            .Rows(i).Cells(4).Value = rd(4)
                            .Rows(i).Cells(5).Value = Format(curBal, "##,##0.00")
                            .Rows(i).Cells(6).Value = rd(6)
                            .Rows(i).Cells(7).Value = rd(7)
                            .Rows(i).Cells(8).Value = rd(8)
                            .Rows(i).Cells(9).Value = rd(9)
                            .Rows(i).Cells(10).Value = rd(10)
                            .Rows(i).Cells(11).Value = rd(11)
                        End If
                        If rd(4) = 0 And rd(3) <> 0 Then 'debit
                            curBal = rd(3) - rd(4)
                            .Rows(i).Cells(0).Value = rd(0)
                            .Rows(i).Cells(1).Value = rd(1)
                            .Rows(i).Cells(2).Value = rd(2)
                            .Rows(i).Cells(3).Value = rd(3)
                            .Rows(i).Cells(4).Value = rd(4)
                            .Rows(i).Cells(5).Value = Format(curBal, "##,##0.00")
                            .Rows(i).Cells(6).Value = rd(6)
                            .Rows(i).Cells(7).Value = rd(7)
                            .Rows(i).Cells(8).Value = rd(8)
                            .Rows(i).Cells(9).Value = rd(9)
                            .Rows(i).Cells(10).Value = rd(10)
                            .Rows(i).Cells(11).Value = rd(11)
                        End If
                    Else
                        If rd(3) = 0 And rd(4) <> 0 Then 'credit
                            If rd(1) <> .Rows(i - 1).Cells(1).Value Then
                                .Rows(i).Cells(5).Value = Format(CDec(curBal - rd(4)), "##,##0.00")
                                curBal = .Rows(i).Cells(5).Value
                            Else
                                .Rows(i).Cells(5).Value = Format(curBal, "##,##0.00")
                            End If
                            .Rows(i).Cells(0).Value = rd(0)
                            .Rows(i).Cells(1).Value = rd(1)
                            .Rows(i).Cells(2).Value = rd(2)
                            .Rows(i).Cells(3).Value = rd(3)
                            .Rows(i).Cells(4).Value = rd(4)
                            .Rows(i).Cells(6).Value = rd(6)
                            .Rows(i).Cells(7).Value = rd(7)
                            .Rows(i).Cells(8).Value = rd(8)
                            .Rows(i).Cells(9).Value = rd(9)
                            .Rows(i).Cells(10).Value = rd(10)
                            .Rows(i).Cells(11).Value = rd(11)
                        End If
                        If rd(4) = 0 And rd(3) <> 0 Then 'debit
                            If rd(1) <> .Rows(i - 1).Cells(1).Value Then
                                .Rows(i).Cells(5).Value = Format(CDec(rd(3) + curBal), "##,##0.00")
                                curBal = .Rows(i).Cells(5).Value
                            Else
                                .Rows(i).Cells(5).Value = Format(curBal, "##,##0.00")
                            End If
                            .Rows(i).Cells(0).Value = rd(0)
                            .Rows(i).Cells(1).Value = rd(1)
                            .Rows(i).Cells(2).Value = rd(2)
                            .Rows(i).Cells(3).Value = rd(3)
                            .Rows(i).Cells(4).Value = rd(4)
                            .Rows(i).Cells(6).Value = rd(6)
                            .Rows(i).Cells(7).Value = rd(7)
                            .Rows(i).Cells(8).Value = rd(8)
                            .Rows(i).Cells(9).Value = rd(9)
                            .Rows(i).Cells(10).Value = rd(10)
                            .Rows(i).Cells(11).Value = rd(11)
                        End If
                    End If
                    If rd(3) = 0 And rd(4) = 0 Then 'cancelled
                        .Rows(i).Cells(0).Value = rd(0)
                        .Rows(i).Cells(1).Value = rd(1)
                        .Rows(i).Cells(2).Value = rd(2)
                        .Rows(i).Cells(3).Value = rd(3)
                        .Rows(i).Cells(4).Value = rd(4)
                        .Rows(i).Cells(5).Value = Format(curBal, "##,##0.00")
                        .Rows(i).Cells(6).Value = rd(6)
                        .Rows(i).Cells(7).Value = rd(7)
                        .Rows(i).Cells(8).Value = rd(8)
                        .Rows(i).Cells(9).Value = rd(9)
                        .Rows(i).Cells(10).Value = rd(10)
                        .Rows(i).Cells(11).Value = rd(11)
                    End If
                End With
                i += 1
            End While

            txtBalPrincipal.Text = Format(CDec(dgvSubsidiaryCurrent.Rows(i - 1).Cells(5).Value.ToString), "##,##0.00")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub PrepareSubsidiaryView()
        Dim Col_Date As New DataGridViewTextBoxColumn
        Dim Col_DocNum As New DataGridViewTextBoxColumn
        Dim Col_Particulars As New DataGridViewTextBoxColumn
        Dim Col_Debit As New DataGridViewTextBoxColumn
        Dim Col_Credit As New DataGridViewTextBoxColumn
        Dim Col_Principal As New DataGridViewTextBoxColumn
        Dim Col_Interest As New DataGridViewTextBoxColumn
        Dim Col_ServiceFee As New DataGridViewTextBoxColumn
        Dim Col_Payment As New DataGridViewTextBoxColumn
        Dim Col_Check As New DataGridViewCheckBoxColumn

        With Col_Date
            .HeaderText = "Date"
            .Name = "Col_Date"
            .DataPropertyName = "Col_Date"
            '.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With Col_DocNum
            .HeaderText = "Doc No."
            .Name = "Col_DocNum"
            .DataPropertyName = "Col_DocNum"
            '.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With Col_Particulars
            .HeaderText = "Particulars"
            .Name = "Col_Particulars"
            .DataPropertyName = "Col_Particulars"
            '.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With Col_Debit
            .HeaderText = "Debit"
            .Name = "Col_Debit"
            .DataPropertyName = "Col_Debit"
            '.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With Col_Credit
            .HeaderText = "Credit"
            .Name = "Col_Credit"
            .DataPropertyName = "Col_Credit"
            '.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With Col_Principal
            .HeaderText = "Principal"
            .Name = "Col_Principal"
            .DataPropertyName = "Col_Principal"
            .ReadOnly = True
        End With
        With Col_Interest
            .HeaderText = "Interest"
            .Name = "Col_Interest"
            .DataPropertyName = "Col_Interest"
            '.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With Col_ServiceFee
            .HeaderText = "Service Fee"
            .Name = "Col_ServiceFee"
            .DataPropertyName = "Col_ServiceFee"
            '.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With Col_Payment
            .HeaderText = "Payment"
            .Name = "Col_Payment"
            .DataPropertyName = "Col_Payment"
            '.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With Col_Check
            .HeaderText = "Check"
            .Name = "Col_Check"
            .DataPropertyName = "Col_Check"
            '.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With dgvSubsidiaryCurrent
            .Columns.Clear()
            .Columns.Add(Col_Date)
            .Columns.Add(Col_DocNum)
            .Columns.Add(Col_Particulars)
            .Columns.Add(Col_Debit)
            .Columns.Add(Col_Credit)
            .Columns.Add(Col_Principal)
            .Columns.Add(Col_Interest)
            .Columns.Add(Col_ServiceFee)
            .Columns.Add(Col_Payment)
            .Columns.Add(Col_Check)
        End With
    End Sub

    Private Sub LoadDebitAccounts()
        Try
            dgvDebitList.Columns.Clear()
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(mycon.cnstring, "CMS_AccountVerification_Select_Debit_Credit",
                                          New SqlParameter("@employeeno", lblIDno.Text),
                                          New SqlParameter("@Mode", "Debit"))
            dgvDebitList.DataSource = ds.Tables(0)
            '_Gen_ViewButton_Debit()
        Catch ex As Exception
            'frmMsgBox.txtMessage.Text = "Error :" + ex.ToString
            'frmMsgBox.ShowDialog()
        End Try
    End Sub


    Private Sub LoadCreditAccounts()
        Try
            dgvCredit.Columns.Clear()
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(mycon.cnstring, "CMS_AccountVerification_Select_Debit_Credit",
                                          New SqlParameter("@employeeno", lblIDno.Text),
                                          New SqlParameter("@Mode", "Credit"))
            dgvCredit.DataSource = ds.Tables(0)
            '_Gen_ViewButton_Credit()
        Catch ex As Exception
            'frmMsgBox.txtMessage.Text = "Error :" + ex.ToString
            'frmMsgBox.ShowDialog()
        End Try
    End Sub
    Private Sub LoadSoaAccounts()
        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(mycon.cnstring, "_AccountVerification_Select_Soa_Debit_Credit",
                                          New SqlParameter("@employeeno", lblIDno.Text),
                                          New SqlParameter("@Mode", "Debit"))
            dgvSoaList.DataSource = ds.Tables(0)
        Catch ex As Exception
            'frmMsgBox.txtMessage.Text = "Error :" + ex.ToString
            'frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub dgvDebitList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvDebitList.Click
        Try
            Dim refno As String
            refno = dgvDebitList.CurrentRow.Cells(0).Value.ToString
            'frmPrintAccountAmortization.AccountUsed = dgvDebitList.CurrentRow.Cells(1).Value.ToString
            'AccountsDetail(refno, "Debit")
            DebitDetails(refno)
            frmLedgerFilter.debitref = refno

            'LoadAccount_Amortization("RegS0001-00066") 'test 7-15-15
            LoadAmortTabDetail(refno, dgvDebitList.CurrentRow.Cells(1).Value.ToString)
            printAmortMode = "account"
        Catch ex As Exception
            'frmMsgBox.txtMessage.Text = "Oops, No Debit Accounts Listed!"
            'frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub AccountsDetail(ByVal refno As String, ByVal mode As String)
        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(mycon.cnstring, "_AccountVerification_Select_DebitCredit",
                                           New SqlParameter("@EmployeeNo", lblIDno.Text),
                                           New SqlParameter("@RefNo", refno))
            Select Case mode
                Case "Debit"
                    dgvDebitDetails.DataSource = ds.Tables(0)
                Case "Credit"
                    dgvCreditDetails.DataSource = ds.Tables(0)
            End Select
        Catch ex As Exception
            'frmMsgBox.txtMessage.Text = "Error :" + ex.ToString
            'frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub dgvCredit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvCredit.Click
        Try
            Dim refno As String
            refno = dgvCredit.CurrentRow.Cells(0).Value.ToString
            'frmPrintAccountAmortization.AccountUsed = dgvCredit.CurrentRow.Cells(1).Value.ToString
            'AccountsDetail(refno, "Credit")
            CreditDetails(refno)
            frmLedgerFilter.creditRef = refno

            'LoadAccount_Amortization("RegS0001-00066") 'test 7-15-15
            LoadAmortTabDetail(refno, dgvCredit.CurrentRow.Cells(1).Value.ToString)
            printAmortMode = "account"
        Catch ex As Exception
            'frmMsgBox.txtMessage.Text = "Oops, No Credit Accounts Listed!"
            'frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub DebitDetails(ByVal refno As String)
        Try
            Audittrail_save("AIMS - Account Inquiry", "View subsidiary of Account - Ref. No :" + refno)
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(mycon.cnstring, "_DebitDetails",
                                           New SqlParameter("@EmployeeNo", lblIDno.Text),
                                           New SqlParameter("@RefNo", refno))
            dgvDebitDetails.DataSource = ds.Tables(0)
        Catch ex As Exception
            'frmMsgBox.txtMessage.Text = "Error :" + ex.ToString
            'frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub CreditDetails(ByVal refno As String)
        Try
            Audittrail_save("AIMS - Account Inquiry", "View subsidiary of Account - Ref. No :" + refno)
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(mycon.cnstring, "CreditDetails",
                                           New SqlParameter("@EmployeeNo", lblIDno.Text),
                                           New SqlParameter("@RefNo", refno))
            dgvCreditDetails.DataSource = ds.Tables(0)
        Catch ex As Exception
            'frmMsgBox.txtMessage.Text = "Error :" + ex.ToString
            'frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub dgvAmortizationSchedule_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvAmortizationSchedule.Click
        Try
            Dim loanno As String = lblLoanRef.Text
            Dim payno As String = dgvAmortizationSchedule.CurrentRow.Cells(0).Value.ToString
            View_AmortizationBalances(loanno, payno)
        Catch ex As Exception
            'frmMsgBox.txtMessage.Text = "Oops, No Amortization Schedule Listed!"
            'frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub View_AmortizationBalances(ByVal loanno As String, ByVal payno As String)
        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(mycon.cnstring, "_View_Payment_Balances",
                                           New SqlParameter("@LoanNo", loanno),
                                           New SqlParameter("@PaymentNo", payno))
            dgvAmortizationBalances.DataSource = ds.Tables(0)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnSelectedLoanPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectedLoanPrint.Click
        Try
            'frmPrint.LoadLoanAmortization(lblIDno.Text, lblLoanRef.Text)
            'frmPrint.StartPosition = FormStartPosition.CenterScreen
            'frmPrint.WindowState = FormWindowState.Maximized
            'frmPrint.ShowDialog()
            If printAmortMode = "loan" Then
                frmPrintDynamicAmortization.principal = xprincipal
                frmPrintDynamicAmortization.loantype = xloantype
                frmPrintDynamicAmortization.terms = xterms
                frmPrintDynamicAmortization.LoadAmortization(lblIDno.Text, lblLoanRef.Text)
                frmPrintDynamicAmortization.ShowDialog()
            End If
            If printAmortMode = "account" Then
                'frmPrintAccountAmortization.refNo = "RegS0001-00066"
                'frmPrintAccountAmortization.ShowDialog()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub dgvLoanHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvLoanHistory.Click
        Try
            With dgvLoanHistory
                'loanNo = .SelectedRows(0).Cells(0).Value.ToString
                'lblLoanRef.Text = loanNo
                'lblLoanType.Text = .SelectedRows(0).Cells(1).Value.ToString

                'txtPrincipal.Text = .SelectedRows(0).Cells(7).Value.ToString
                'txtInterest.Text = .SelectedRows(0).Cells(8).Value.ToString
                'txtServiceFee.Text = .SelectedRows(0).Cells(9).Value.ToString

                'xprincipal = .SelectedRows(0).Cells(7).Value.ToString
                'xloantype = .SelectedRows(0).Cells(1).Value.ToString
                'xterms = .SelectedRows(0).Cells(6).Value.ToString

                loanNo = .CurrentRow.Cells(0).Value.ToString
                lblLoanRef.Text = loanNo
                lblLoanType.Text = .CurrentRow.Cells(1).Value.ToString

                txtPrincipal.Text = .CurrentRow.Cells(7).Value.ToString
                txtInterest.Text = .CurrentRow.Cells(8).Value.ToString
                txtServiceFee.Text = .CurrentRow.Cells(9).Value.ToString

                xprincipal = .CurrentRow.Cells(7).Value.ToString
                xloantype = .CurrentRow.Cells(1).Value.ToString
                xterms = .CurrentRow.Cells(6).Value.ToString
            End With
            LoadAmortizationStatus()
            LoadLoanSubsidiary_History(loanNo)
            Get_Balances_And_Total()
            printAmortMode = "loan"
        Catch ex As Exception
            'frmMsgBox.txtMessage.Text = "Oops, No Loan History Listed!"
            'frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub LoadLoanSubsidiary_History(ByVal loanNo As String)
        dgvLoanHistorySubsidiary.Columns.Clear()
        Dim i As Integer = 0
        Try
            Generate_SubsidiaryHistory()
            Dim rd As SqlDataReader
            Dim curBal As Decimal = 0
            rd = SqlHelper.ExecuteReader(mycon.cnstring, "_View_Subsidiary_Loans_v2",
                                          New SqlParameter("@LoanNo", loanNo))
            While rd.Read
                With dgvLoanHistorySubsidiary
                    .Rows.Add()
                    If i = 0 Then
                        If rd(3) = 0 And rd(4) <> 0 Then 'credit
                            curBal = rd(3) - rd(4)
                            .Rows(i).Cells(0).Value = rd(0)
                            .Rows(i).Cells(1).Value = rd(1)
                            .Rows(i).Cells(2).Value = rd(2)
                            .Rows(i).Cells(3).Value = rd(3)
                            .Rows(i).Cells(4).Value = rd(4)
                            .Rows(i).Cells(5).Value = Format(curBal, "##,##0.00")
                            .Rows(i).Cells(6).Value = rd(6)
                            .Rows(i).Cells(7).Value = rd(7)
                            .Rows(i).Cells(8).Value = rd(8)
                            .Rows(i).Cells(9).Value = rd(9)
                            .Rows(i).Cells(10).Value = rd(10)
                            .Rows(i).Cells(11).Value = rd(11)
                        End If
                        If rd(4) = 0 And rd(3) <> 0 Then 'debit
                            curBal = rd(3) - rd(4)
                            .Rows(i).Cells(0).Value = rd(0)
                            .Rows(i).Cells(1).Value = rd(1)
                            .Rows(i).Cells(2).Value = rd(2)
                            .Rows(i).Cells(3).Value = rd(3)
                            .Rows(i).Cells(4).Value = rd(4)
                            .Rows(i).Cells(5).Value = Format(curBal, "##,##0.00")
                            .Rows(i).Cells(6).Value = rd(6)
                            .Rows(i).Cells(7).Value = rd(7)
                            .Rows(i).Cells(8).Value = rd(8)
                            .Rows(i).Cells(9).Value = rd(9)
                            .Rows(i).Cells(10).Value = rd(10)
                            .Rows(i).Cells(11).Value = rd(11)
                        End If
                    Else
                        If rd(3) = 0 And rd(4) <> 0 Then 'credit
                            If rd(1) <> .Rows(i - 1).Cells(1).Value Then
                                .Rows(i).Cells(5).Value = Format(CDec(curBal - rd(4)), "##,##0.00")
                                curBal = .Rows(i).Cells(5).Value
                            End If
                            .Rows(i).Cells(0).Value = rd(0)
                            .Rows(i).Cells(1).Value = rd(1)
                            .Rows(i).Cells(2).Value = rd(2)
                            .Rows(i).Cells(3).Value = rd(3)
                            .Rows(i).Cells(4).Value = rd(4)
                            .Rows(i).Cells(6).Value = rd(6)
                            .Rows(i).Cells(7).Value = rd(7)
                            .Rows(i).Cells(8).Value = rd(8)
                            .Rows(i).Cells(9).Value = rd(9)
                            .Rows(i).Cells(10).Value = rd(10)
                            .Rows(i).Cells(11).Value = rd(11)
                        End If
                        If rd(4) = 0 And rd(3) <> 0 Then 'debit
                            If rd(1) <> .Rows(i - 1).Cells(1).Value Then
                                .Rows(i).Cells(5).Value = Format(CDec(rd(3) + curBal), "##,##0.00")
                                curBal = .Rows(i).Cells(5).Value
                            End If
                            .Rows(i).Cells(0).Value = rd(0)
                            .Rows(i).Cells(1).Value = rd(1)
                            .Rows(i).Cells(2).Value = rd(2)
                            .Rows(i).Cells(3).Value = rd(3)
                            .Rows(i).Cells(4).Value = rd(4)
                            .Rows(i).Cells(6).Value = rd(6)
                            .Rows(i).Cells(7).Value = rd(7)
                            .Rows(i).Cells(8).Value = rd(8)
                            .Rows(i).Cells(9).Value = rd(9)
                            .Rows(i).Cells(10).Value = rd(10)
                            .Rows(i).Cells(11).Value = rd(11)
                        End If
                    End If
                    If rd(3) = 0 And rd(4) = 0 Then 'cancelled
                        .Rows(i).Cells(0).Value = rd(0)
                        .Rows(i).Cells(1).Value = rd(1)
                        .Rows(i).Cells(2).Value = rd(2)
                        .Rows(i).Cells(3).Value = rd(3)
                        .Rows(i).Cells(4).Value = rd(4)
                        .Rows(i).Cells(5).Value = rd(5)
                        .Rows(i).Cells(6).Value = rd(6)
                        .Rows(i).Cells(7).Value = rd(7)
                        .Rows(i).Cells(8).Value = rd(8)
                        .Rows(i).Cells(9).Value = rd(9)
                        .Rows(i).Cells(10).Value = rd(10)
                        .Rows(i).Cells(11).Value = rd(11)
                    End If
                End With
                i += 1
            End While

            txtBalPrincipal.Text = Format(CDec(dgvSubsidiaryCurrent.Rows(i - 1).Cells(5).Value.ToString), "##,##0.00")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnPrintLoanLedger_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintLoanLedger.Click
        'Loan ledger
        Try
            frmLedgerFilter.StartPosition = FormStartPosition.CenterScreen
            frmLedgerFilter.ShowDialog()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnPrintStatement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintStatement.Click
        Try
            Audittrail_save("AIMS - Account Inquiry", "Print Statement of Accounts - ID No :" + lblIDno.Text)
            'frmPrint.LoadREport(lblIDno.Text)
            'frmPrint.StartPosition = FormStartPosition.CenterScreen
            'frmPrint.WindowState = FormWindowState.Maximized
            'frmPrint.ShowDialog()
            frmReport_SOAChoice.ShowDialog()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetSubsidiary_Balances(ByVal loanno As String)
        Try
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(mycon.cnstring, "_LoanSubsidiary_Balances",
                                          New SqlParameter("@LoanNo", loanno))
            While rd.Read
                'txtBalPrincipal.Text = Format(CDec(rd(0)), "##,##0.00")
                txtBalInterest.Text = Format(CDec(rd(1)), "##,##0.00")
                txtBalServicefee.Text = Format(CDec(rd(2)), "##,##0.00")
                txtBalCreditAcct.Text = Format(CDec(rd(3)), "##,##0.00")
                txtTotalPayment.Text = Format(CDec(rd(4)), "##,##0.00")
            End While
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        _Correct_Balance_Always() '2/23/15

        LoadLoans(txtsearch.Text)
        LoadLoanHistory()
        LoadDebitAccounts()
        LoadCreditAccounts()

        dgvCreditDetails.Columns.Clear()
        dgvDebitDetails.Columns.Clear()
        dgvAmortizationSchedule.Columns.Clear()
        dgvSubsidiaryCurrent.Columns.Clear()
        dgvLoanHistorySubsidiary.Columns.Clear()
        dgvAmortizationBalances.Columns.Clear()
    End Sub

#Region "Added by Vince SOA - Loans"
    Private rptsummary As New ReportDocument
    Private gcon As New Clsappconfiguration
    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If

        Dim obj As ReportObject

        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        ' for each table apply connection info
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            ' check if logon was successful
            ' if TestConnectivity returns false,
            ' check logon credentials
            If (tbl.TestConnectivity()) Then
                'drop fully qualified table location
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function
    Public Sub LoadLoanLedger(ByVal empno As String)
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\LoanLedger_SummaryAmountsDue.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@EmployeeNo", empno)
            rptsummary.SetParameterValue("@dtDate", dtDate.Text)
        Catch ex As Exception
            'frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            'frmMsgBox.ShowDialog()
        End Try
        CrvRpt.ReportSource = rptsummary
    End Sub
    Public Sub LoadREport(ByVal empno As String)
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\_SOA_Loans.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@EmployeeNo", empno)
            rptsummary.SetParameterValue("@dtDate", dtDate.Text)
        Catch ex As Exception
            'frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            'frmMsgBox.ShowDialog()
        End Try
        CrvRpt.ReportSource = rptsummary
    End Sub
#End Region

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        If cboFilterSoa.Text = "Statement Of Accounts - Loan" Then
            LoadLoanLedger(lblIDno.Text)
        End If
        If cboFilterSoa.Text = "Statement Of Accounts - Detailed" Then
            LoadREport(lblIDno.Text)
        End If
        If cboFilterSoa.Text = "Statement Of Accounts - Accounts Receivable" Then
            If RBAll.Checked = True Then
                LoadREport_SoaHistory(lblIDno.Text)
            ElseIf RBPerSoa.Checked = True Then
                LoadREport_PerSoa(lblIDno.Text, txtSoa.Text)
            ElseIf RBBalance.Checked = True Then
                LoadREport_SoaBalances(lblIDno.Text)
            Else
                LoadREport_Soa(lblIDno.Text)
            End If
        End If
    End Sub
    Public Sub LoadREport_SoaHistory(ByVal empno As String)
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\_Print_SoaPerClient_History.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@EmployeeNo", empno)
        Catch ex As Exception
            'frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            'frmMsgBox.ShowDialog()
        End Try
        CrvRpt.ReportSource = rptsummary
    End Sub
    Public Sub LoadREport_PerSoa(ByVal empno As String, ByVal SoaNo As String)
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\_Print_SoaClient_PerSoa.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@EmployeeNo", empno)
            rptsummary.SetParameterValue("@SoaRef", SoaNo)
        Catch ex As Exception
            'frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            'frmMsgBox.ShowDialog()
        End Try
        CrvRpt.ReportSource = rptsummary
    End Sub
    Public Sub LoadREport_SoaBalances(ByVal empno As String)
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\_Print_Soa_Balances.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@EmployeeNo", empno)
        Catch ex As Exception
            'frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            'frmMsgBox.ShowDialog()
        End Try
        CrvRpt.ReportSource = rptsummary
    End Sub
    Public Sub LoadREport_Soa(ByVal empno As String)
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\_Print_SoaPerClient.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@EmployeeNo", empno)
            'rptsummary.SetParameterValue("@dtDate", dtDate.Text)
        Catch ex As Exception
            'frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            'frmMsgBox.ShowDialog()
        End Try
        CrvRpt.ReportSource = rptsummary
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        CrvRpt.PrintReport()
    End Sub


#Region "Printing of Individual Account Ledger 2/24/15"

    Private Sub _Gen_ViewButton_Debit()
        Dim btnView As New DataGridViewButtonColumn
        With btnView
            .Name = "btnView"
            .Text = "Print Ledger"
            .HeaderText = ""
            .UseColumnTextForButtonValue = True
            .DefaultCellStyle.ForeColor = Color.Black
        End With
        With dgvDebitList
            .Columns.Add(btnView)
        End With
    End Sub
    Private Sub _Gen_ViewButton_Credit()
        Dim btnView As New DataGridViewButtonColumn
        With btnView
            .Name = "btnView"
            .Text = "Print Ledger"
            .HeaderText = ""
            .UseColumnTextForButtonValue = True
            .DefaultCellStyle.ForeColor = Color.Black
        End With
        With dgvCredit
            .Columns.Add(btnView)
        End With
    End Sub

#End Region

    Private Sub cboFilterSoa_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFilterSoa.SelectedIndexChanged

    End Sub

    Private Sub cboFilterSoa_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFilterSoa.SelectedValueChanged
        If cboFilterSoa.Text = "Statement Of Accounts - Accounts Receivable" Then
            Label18.Visible = False
            dtDate.Visible = False
            RBPerSoa.Visible = True
            RBAll.Visible = True
            RBBalance.Visible = True
            RBHistory.Visible = True
        Else
            Label18.Visible = True
            dtDate.Visible = True
            RBAll.Visible = False
            RBPerSoa.Visible = False
            txtSoa.Visible = False
            btnBrowse.Visible = False
            RBAll.Checked = False
            RBPerSoa.Checked = False
            RBBalance.Visible = False
            RBHistory.Visible = False
        End If
    End Sub

    Private Sub RBPerSoa_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RBPerSoa.CheckedChanged
        If RBPerSoa.Checked = True Then
            txtSoa.Visible = True
            btnBrowse.Visible = True
        Else
            txtSoa.Visible = False
            btnBrowse.Visible = False
            txtSoa.Clear()
        End If
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Try
            'frmSoaFilter.ShowDialog()
            'If frmSoaFilter.DialogResult = DialogResult.Yes Then
            '    txtSoa.Text = frmSoaFilter.SoaNo
            'End If
        Catch ex As Exception
            'frmMsgBox.txtMessage.Text = "Oops, Something went wrong!"
            'frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub dgvSoaList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvSoaList.Click
        Try
            Dim SoaNo As String
            SoaNo = dgvSoaList.CurrentRow.Cells(0).Value.ToString
            SoaDetails(SoaNo)
        Catch ex As Exception
            'frmMsgBox.txtMessage.Text = "Oops, No Soa Accounts Listed!"
            'frmMsgBox.ShowDialog()
        End Try
    End Sub
    Private Sub SoaDetails(ByVal SoaNo As String)
        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(mycon.cnstring, "_SOADetails",
                                           New SqlParameter("@EmployeeNo", lblIDno.Text), _
                                           New SqlParameter("@SoaRef", SoaNo))

            dgvDebitDetails.DataSource = ds.Tables(0)
        Catch ex As Exception
            'frmMsgBox.txtMessage.Text = "Error :" + ex.ToString
            'frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub TBCDebitAccounts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles TBCDebitAccounts.Click
        Dim m_CurrentIndex As Integer
        m_CurrentIndex = TBCDebitAccounts.SelectedIndex
        'm_CurrentIndex = m_CurrentIndex + 1
        If (m_CurrentIndex = TBCDebitAccounts.TabCount) Then
            m_CurrentIndex = m_CurrentIndex + 1
            Exit Sub
        Else
            dgvDebitDetails.DataSource = Nothing
        End If
    End Sub

    Private Sub frmVerification_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnPrintDynamic_Click(sender As System.Object, e As System.EventArgs) Handles btnPrintDynamic.Click
        frmPrintDynamicAmortization.principal = xprincipal
        frmPrintDynamicAmortization.loantype = xloantype
        frmPrintDynamicAmortization.terms = xterms

        frmPrintDynamicAmortization.LoadAmortization(lblIDno.Text, lblLoanRef.Text)
        frmPrintDynamicAmortization.ShowDialog()
    End Sub

#Region "Apply InPayment Charges 5/14/15"
    'parameter
    Dim xprincipal As String
    Dim xloantype As String
    Dim xterms As String

    Private Sub _Retotal_Amortization()

    End Sub
    Private Sub _LoadInPaymentCharges()
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, "Calculate_Amortization_InPAymentCharges",
                                                  New SqlParameter("@principal", xprincipal),
                                                  New SqlParameter("@loantype", xloantype),
                                                  New SqlParameter("@terms", xterms))
            If rd.HasRows Then
                dgvAmortizationSchedule.Columns(5).Visible = False
                While rd.Read
                    dgvAmortizationSchedule.Columns.Add(rd(0).ToString.Trim, rd(1).ToString)
                    dgvAmortizationSchedule.Columns(rd(0).ToString.Trim).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    For v As Integer = 0 To dgvAmortizationSchedule.RowCount - 1
                        With dgvAmortizationSchedule.Rows(v)
                            .Cells(rd(0).ToString).Value = rd(2)
                        End With
                    Next
                End While
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

#End Region

#Region "Check loan history 2-23-15"

    Private Sub _Correct_Balance_Always() 'update loan balance
        Try
            SqlHelper.ExecuteNonQuery(gcon.cnstring, "_Inquiry_RefreshBalance",
                                      New SqlParameter("@idno", lblIDno.Text))
        Catch ex As Exception
            Throw
        End Try
    End Sub

#End Region

    '============ Load Account Amortization @vince 7-15-15 8:00pm =======================
    Private Sub LoadAccount_Amortization(ByVal refno As String)
        Try
            PrepareAccountColumns(refno)
            Dim xRow As Integer = 0
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.cnstring, "_Display_AccountDynamicAmortization",
                                                              New SqlParameter("@refNo", refno))
            If rd.HasRows Then
                While rd.Read
                    Dim xCol As Integer = 0 'starting columns 0,1 is default
                    Dim xColTotal As Decimal = 0
                    With dgvAmortizationSchedule
                        .Rows.Add() 'generate new row
                        .Rows(xRow).Cells(0).Value = rd(0)
                        .Rows(xRow).Cells(1).Value = CDate(rd(1)).ToString("MMMM dd,yyyy")
                        'rd(2)
                        Dim colval As String() = rd(3).Split(New Char() {","c})
                        Dim cv As String
                        xCol = 2 'starting index 0,1 is used by payno and date
                        For Each cv In colval
                            .Rows(xRow).Cells(xCol).Value = Format(CDec(cv), "##,##0.00")
                            xColTotal += CDec(cv)
                            xCol += 1
                        Next
                        .Rows(xRow).Cells(xCol).Value = Format(xColTotal, "##,##0.00")
                    End With
                    xRow += 1 'add row
                End While
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub PrepareAccountColumns(ByVal key As String)
        Try
            dgvAmortizationSchedule.DataSource = Nothing
            dgvAmortizationSchedule.Rows.Clear()
            dgvAmortizationSchedule.Columns.Clear()
            Dim xIndex As Integer = 0 'use as column name index
            With dgvAmortizationSchedule
                .Columns.Add("PayNo", "Pay No.")
                .Columns.Add("PayDate", "Date")
            End With
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.cnstring, "_Display_AccountDynamicColumns",
                                                              New SqlParameter("@refNo", key))
            If rd.HasRows Then
                While rd.Read
                    With dgvAmortizationSchedule
                        .Columns.Add(xIndex.ToString, rd(0))
                    End With
                    xIndex += 1
                End While
            End If
            dgvAmortizationSchedule.Columns.Add("fdTotal", "Total")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub LoadAmortTabDetail(ByVal ref As String, ByVal title As String)
        lblLoanRef.Text = ref
        lblLoanType.Text = title
    End Sub
    '====================================================================

    Private Function txtSearchLoan() As Object
        Throw New NotImplementedException
    End Function


    Private Sub btnFindLoan_Click(sender As System.Object, e As System.EventArgs)

    End Sub
End Class