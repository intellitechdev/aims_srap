Imports Microsoft.ApplicationBlocks.Data

Public Class frm_acc_terms
    Private sKeyTerms As String
    Private gCon As New Clsappconfiguration
    Private sSQLCmdQuery As String = "SELECT * FROM mTerms WHERE fbActive <> 1 "

    Public Property SQLQuery() As String
        Get
            Return sSQLCmdQuery
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Private Sub ts_New_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_New.Click
        frm_MF_termsAddEdit.Keyterms = Guid.NewGuid.ToString
        frm_MF_termsAddEdit.MdiParent = Me.MdiParent
        frm_MF_termsAddEdit.Text = "Add Terms"
        frm_MF_termsAddEdit.Show()
    End Sub

    Private Sub ts_Edit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_Edit.Click
        frm_MF_termsAddEdit.MdiParent = Me.MdiParent
        frm_MF_termsAddEdit.Keyterms = sKeyTerms
        frm_MF_termsAddEdit.Text = "Edit Terms"
        frm_MF_termsAddEdit.Show()
    End Sub

    Private Sub frm_acc_terms_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        refreshForm()
    End Sub

    Private Sub refreshForm()
        m_termsList(chkIncludeInactive.CheckState, grdTerms)
        grdSettings()
        m_chkSettings(chkIncludeInactive, sSQLCmdQuery)
        ts_showInactive.Enabled = chkIncludeInactive.Enabled
    End Sub
    Private Sub grdSettings()
        With grdTerms
            .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            .Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
            .Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .Columns(0).Visible = False
            .Columns(1).Visible = False
            .Columns.Item(1).HeaderText = "Active"
            .Columns.Item(2).HeaderText = "Terms Name"
            If chkIncludeInactive.CheckState = CheckState.Checked Then
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(1).Visible = True
            End If
        End With
    End Sub

    Private Sub chkIncludeInactive_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIncludeInactive.CheckStateChanged
        m_termsList(chkIncludeInactive.CheckState, grdTerms)
        grdSettings()
    End Sub

    Private Sub grdTerms_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdTerms.CellClick
        If Me.grdTerms.SelectedCells.Count > 0 Then
            sKeyTerms = grdTerms.CurrentRow.Cells(0).Value.ToString

            If m_isActive("mTerms", "fxKeyTerms", sKeyTerms) Then
                ts_MkInactive.Text = "Make Inactive"
            Else
                ts_MkInactive.Text = "Make Active"
            End If
        End If
    End Sub

    Private Sub grdTerms_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdTerms.DoubleClick
        If Me.grdTerms.SelectedCells.Count > 0 Then
            frm_MF_termsAddEdit.MdiParent = Me.MdiParent
            frm_MF_termsAddEdit.Keyterms = sKeyTerms
            frm_MF_termsAddEdit.Text = "Edit Terms"
            frm_MF_termsAddEdit.Show()
        End If
    End Sub

    Private Sub deleteTerms()
        If MessageBox.Show("Are you sure you want to delete this record?", "Delete", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
            Dim sSQLCmd As String = "DELETE FROM mTerms WHERE fxKeyTerms = '" & sKeyTerms & "'"
            Try
                SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
                MessageBox.Show("Record successfully deleted.")
            Catch ex As Exception
                MessageBox.Show(Err.Description)
            End Try
        End If
    End Sub

    Private Sub ts_Delete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_Delete.Click
        deleteTerms()
        refreshForm()
    End Sub

    Private Sub chkIncludeInactive_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIncludeInactive.EnabledChanged
        ts_showInactive.Enabled = chkIncludeInactive.Enabled
    End Sub

    Private Sub ts_showInactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_showInactive.Click
        chkIncludeInactive.Checked = True
    End Sub

    Private Sub ts_MkInactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_MkInactive.Click
        If ts_MkInactive.Text = "Make Active" Then
            m_mkActive("mTerms", "fxKeyTerms", sKeyTerms, True)
        Else
            m_mkActive("mTerms", "fxKeyTerms", sKeyTerms, False)
        End If
        refreshForm()
    End Sub
End Class