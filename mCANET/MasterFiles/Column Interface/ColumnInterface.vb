﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Public Class ColumnInterface
    Private gCon As New Clsappconfiguration()
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring

    Private Sub ColumnInterface_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call LoadDetails()
    End Sub

    Private Sub LoadDetails()
        grdColumns.Columns.Clear()
        grdColumns.DataSource = Nothing

        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "spu_mReportColumn_ListColumns",
                                                                        New SqlParameter("@DocType", cboDoctype.Text),
                                                                        New SqlParameter("@filter", txtsearch.Text))

        grdColumns.DataSource = ds.Tables(0)
    End Sub


End Class