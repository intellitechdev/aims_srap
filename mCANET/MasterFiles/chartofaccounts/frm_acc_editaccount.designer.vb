<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_acc_editaccount
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_acc_editaccount))
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboedit_accounttype = New System.Windows.Forms.ComboBox
        Me.grpEditAccount = New System.Windows.Forms.GroupBox
        Me.lblSubAcntCode = New System.Windows.Forms.Label
        Me.cboedit_subaccountname = New System.Windows.Forms.ComboBox
        Me.chkedit_subaccount = New System.Windows.Forms.CheckBox
        Me.txtedit_acountname = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnedit_saveaccount = New System.Windows.Forms.Button
        Me.grpOptional = New System.Windows.Forms.GroupBox
        Me.txtedit_accountnote = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtedit_accountdescription = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.chkedit_accountinactive = New System.Windows.Forms.CheckBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.lblAcntCode = New System.Windows.Forms.Label
        Me.btnOBalance = New System.Windows.Forms.Button
        Me.btnBeginningBudget = New System.Windows.Forms.Button
        Me.LstException = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.grpEditAccount.SuspendLayout()
        Me.grpOptional.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(10, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Account Type"
        '
        'cboedit_accounttype
        '
        Me.cboedit_accounttype.FormattingEnabled = True
        Me.cboedit_accounttype.Location = New System.Drawing.Point(98, 12)
        Me.cboedit_accounttype.Name = "cboedit_accounttype"
        Me.cboedit_accounttype.Size = New System.Drawing.Size(212, 21)
        Me.cboedit_accounttype.TabIndex = 0
        '
        'grpEditAccount
        '
        Me.grpEditAccount.Controls.Add(Me.lblSubAcntCode)
        Me.grpEditAccount.Controls.Add(Me.cboedit_subaccountname)
        Me.grpEditAccount.Controls.Add(Me.chkedit_subaccount)
        Me.grpEditAccount.Controls.Add(Me.txtedit_acountname)
        Me.grpEditAccount.Controls.Add(Me.Label2)
        Me.grpEditAccount.Location = New System.Drawing.Point(9, 53)
        Me.grpEditAccount.Name = "grpEditAccount"
        Me.grpEditAccount.Size = New System.Drawing.Size(544, 87)
        Me.grpEditAccount.TabIndex = 2
        Me.grpEditAccount.TabStop = False
        Me.grpEditAccount.Text = "Accounts"
        '
        'lblSubAcntCode
        '
        Me.lblSubAcntCode.AutoSize = True
        Me.lblSubAcntCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSubAcntCode.ForeColor = System.Drawing.Color.Red
        Me.lblSubAcntCode.Location = New System.Drawing.Point(254, 65)
        Me.lblSubAcntCode.Name = "lblSubAcntCode"
        Me.lblSubAcntCode.Size = New System.Drawing.Size(74, 13)
        Me.lblSubAcntCode.TabIndex = 13
        Me.lblSubAcntCode.Text = "Account Code"
        '
        'cboedit_subaccountname
        '
        Me.cboedit_subaccountname.Enabled = False
        Me.cboedit_subaccountname.FormattingEnabled = True
        Me.cboedit_subaccountname.Location = New System.Drawing.Point(250, 41)
        Me.cboedit_subaccountname.MaxDropDownItems = 25
        Me.cboedit_subaccountname.Name = "cboedit_subaccountname"
        Me.cboedit_subaccountname.Size = New System.Drawing.Size(271, 21)
        Me.cboedit_subaccountname.TabIndex = 2
        '
        'chkedit_subaccount
        '
        Me.chkedit_subaccount.AutoSize = True
        Me.chkedit_subaccount.Location = New System.Drawing.Point(136, 43)
        Me.chkedit_subaccount.Name = "chkedit_subaccount"
        Me.chkedit_subaccount.Size = New System.Drawing.Size(105, 17)
        Me.chkedit_subaccount.TabIndex = 1
        Me.chkedit_subaccount.Text = "subaccount of"
        Me.chkedit_subaccount.UseVisualStyleBackColor = True
        '
        'txtedit_acountname
        '
        Me.txtedit_acountname.Location = New System.Drawing.Point(133, 15)
        Me.txtedit_acountname.Name = "txtedit_acountname"
        Me.txtedit_acountname.Size = New System.Drawing.Size(388, 21)
        Me.txtedit_acountname.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(42, 19)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(76, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Account Name"
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(444, 324)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(108, 23)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnedit_saveaccount
        '
        Me.btnedit_saveaccount.Location = New System.Drawing.Point(332, 324)
        Me.btnedit_saveaccount.Name = "btnedit_saveaccount"
        Me.btnedit_saveaccount.Size = New System.Drawing.Size(108, 23)
        Me.btnedit_saveaccount.TabIndex = 2
        Me.btnedit_saveaccount.Text = "Save && Close"
        Me.btnedit_saveaccount.UseVisualStyleBackColor = True
        '
        'grpOptional
        '
        Me.grpOptional.Controls.Add(Me.txtedit_accountnote)
        Me.grpOptional.Controls.Add(Me.Label4)
        Me.grpOptional.Controls.Add(Me.txtedit_accountdescription)
        Me.grpOptional.Controls.Add(Me.Label3)
        Me.grpOptional.Location = New System.Drawing.Point(9, 188)
        Me.grpOptional.Name = "grpOptional"
        Me.grpOptional.Size = New System.Drawing.Size(544, 128)
        Me.grpOptional.TabIndex = 7
        Me.grpOptional.TabStop = False
        Me.grpOptional.Text = "Optional"
        '
        'txtedit_accountnote
        '
        Me.txtedit_accountnote.Location = New System.Drawing.Point(133, 90)
        Me.txtedit_accountnote.Name = "txtedit_accountnote"
        Me.txtedit_accountnote.Size = New System.Drawing.Size(388, 21)
        Me.txtedit_accountnote.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(43, 93)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(30, 13)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Note"
        '
        'txtedit_accountdescription
        '
        Me.txtedit_accountdescription.Location = New System.Drawing.Point(133, 28)
        Me.txtedit_accountdescription.Multiline = True
        Me.txtedit_accountdescription.Name = "txtedit_accountdescription"
        Me.txtedit_accountdescription.Size = New System.Drawing.Size(388, 56)
        Me.txtedit_accountdescription.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(43, 31)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(60, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Description"
        '
        'chkedit_accountinactive
        '
        Me.chkedit_accountinactive.AutoSize = True
        Me.chkedit_accountinactive.Location = New System.Drawing.Point(416, 16)
        Me.chkedit_accountinactive.Name = "chkedit_accountinactive"
        Me.chkedit_accountinactive.Size = New System.Drawing.Size(134, 17)
        Me.chkedit_accountinactive.TabIndex = 1
        Me.chkedit_accountinactive.Text = "Account is Inactive"
        Me.chkedit_accountinactive.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Red
        Me.Label5.Location = New System.Drawing.Point(12, 151)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(74, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Account Code"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Red
        Me.Label6.Location = New System.Drawing.Point(10, 164)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(75, 13)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "(numeric only)"
        '
        'lblAcntCode
        '
        Me.lblAcntCode.AutoSize = True
        Me.lblAcntCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAcntCode.ForeColor = System.Drawing.Color.Red
        Me.lblAcntCode.Location = New System.Drawing.Point(139, 38)
        Me.lblAcntCode.Name = "lblAcntCode"
        Me.lblAcntCode.Size = New System.Drawing.Size(74, 13)
        Me.lblAcntCode.TabIndex = 12
        Me.lblAcntCode.Text = "Account Code"
        '
        'btnOBalance
        '
        Me.btnOBalance.Location = New System.Drawing.Point(7, 324)
        Me.btnOBalance.Name = "btnOBalance"
        Me.btnOBalance.Size = New System.Drawing.Size(159, 23)
        Me.btnOBalance.TabIndex = 4
        Me.btnOBalance.Text = "&Enter Opening Balance"
        Me.btnOBalance.UseVisualStyleBackColor = True
        Me.btnOBalance.Visible = False
        '
        'btnBeginningBudget
        '
        Me.btnBeginningBudget.Location = New System.Drawing.Point(172, 324)
        Me.btnBeginningBudget.Name = "btnBeginningBudget"
        Me.btnBeginningBudget.Size = New System.Drawing.Size(154, 23)
        Me.btnBeginningBudget.TabIndex = 14
        Me.btnBeginningBudget.Text = "Enter Beginning Budget"
        Me.btnBeginningBudget.UseVisualStyleBackColor = True
        '
        'LstException
        '
        Me.LstException.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.LstException.Location = New System.Drawing.Point(580, 11)
        Me.LstException.Name = "LstException"
        Me.LstException.Size = New System.Drawing.Size(348, 336)
        Me.LstException.TabIndex = 15
        Me.LstException.UseCompatibleStateImageBehavior = False
        Me.LstException.View = System.Windows.Forms.View.Details
        Me.LstException.Visible = False
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Accnt_ID"
        Me.ColumnHeader1.Width = 131
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Accnt_name"
        Me.ColumnHeader2.Width = 204
        '
        'frm_acc_editaccount
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(563, 354)
        Me.Controls.Add(Me.LstException)
        Me.Controls.Add(Me.btnBeginningBudget)
        Me.Controls.Add(Me.btnOBalance)
        Me.Controls.Add(Me.lblAcntCode)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.btnedit_saveaccount)
        Me.Controls.Add(Me.chkedit_accountinactive)
        Me.Controls.Add(Me.grpOptional)
        Me.Controls.Add(Me.grpEditAccount)
        Me.Controls.Add(Me.cboedit_accounttype)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(1075, 388)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(571, 388)
        Me.Name = "frm_acc_editaccount"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Edit Account"
        Me.grpEditAccount.ResumeLayout(False)
        Me.grpEditAccount.PerformLayout()
        Me.grpOptional.ResumeLayout(False)
        Me.grpOptional.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboedit_accounttype As System.Windows.Forms.ComboBox
    Friend WithEvents grpEditAccount As System.Windows.Forms.GroupBox
    Friend WithEvents grpOptional As System.Windows.Forms.GroupBox
    Friend WithEvents txtedit_accountnote As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtedit_accountdescription As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboedit_subaccountname As System.Windows.Forms.ComboBox
    Friend WithEvents chkedit_subaccount As System.Windows.Forms.CheckBox
    Friend WithEvents txtedit_acountname As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents chkedit_accountinactive As System.Windows.Forms.CheckBox
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnedit_saveaccount As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblAcntCode As System.Windows.Forms.Label
    Friend WithEvents lblSubAcntCode As System.Windows.Forms.Label
    Friend WithEvents btnOBalance As System.Windows.Forms.Button

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
    Friend WithEvents btnBeginningBudget As System.Windows.Forms.Button
    Friend WithEvents LstException As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader



End Class
