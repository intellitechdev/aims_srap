Public Class frm_acc_newaccount

    Private gMaster As New modMasterFile
    Private gTrans As New clsTransactionFunctions

    Private Sub frm_acc_newaccount_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call gTrans.gClearGrpTxt(grpActTypeDesc)
        Call gMaster.gLoadCboAcntType(cboAcntType)
        Call checkAcntType()
    End Sub

    Private Sub cboAcntType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAcntType.SelectedIndexChanged
        txtDescription.Text = gMaster.gLoadLblAcntDescription(Me.cboAcntType.Text)
    End Sub

    Private Sub btnnew_continue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnew_continue.Click
        Try
            gAcntType = cboAcntType.SelectedItem
            frm_item_masterItemAddEdit.Text = "Add New Account"
            frm_item_masterItemAddEdit.ShowDialog()
            Me.Close()
        Catch ex As Exception
            MessageBox.Show("Please choose an account Type.", "Error!")
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Me.Close()
        frm_acc_newacnttype.ShowDialog()
    End Sub

    Private Sub btnnew_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnew_cancel.Click
        Me.Close()
    End Sub

    Private Sub checkAcntType()
        With cboAcntType
            If .Items.Count < 1 Then
                If MsgBox("Please define account type.", MsgBoxStyle.Information, Me.Text) = MsgBoxResult.Ok Then
                    Me.Close()
                    frm_acc_newacnttype.ShowDialog()
                End If
            End If
        End With
    End Sub

    
End Class