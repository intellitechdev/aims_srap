Imports System.Windows.Forms
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class dlgBeginningBudget

    Dim gcon As new Clsappconfiguration

    Private Sub SetAccountBudget()
        gBeginning_Balance = CDec(txtBeginningBudget.Text)
        gBeginningDate = dteBeginningDate.Value
    End Sub

    Private Sub loadAccountBudget()
        If gAccountID = "" Then
            txtBeginningBudget.Text = "0.00"
            dteBeginningDate.Value = gBeginningDate
        Else
            Dim LoadBudgetCommand As String = "SELECT fdAcnt_budget, "
            LoadBudgetCommand &= "fdBeginningDate "
            LoadBudgetCommand &= "FROM mAccountBudget "
            LoadBudgetCommand &= "WHERE acnt_id = '" & gAccountID & "'"

            Try

                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, LoadBudgetCommand)

                    If rd.Read Then
                        txtBeginningBudget.Text = Format(IIf(IsDBNull(rd.Item("fdAcnt_budget")), "0.00", rd.Item("fdAcnt_budget")), "##,##0.00")
                        dteBeginningDate.Value = IIf(IsDBNull(rd.Item("fdBeginningDate")), gBeginningDate, rd.Item("fdBeginningDate"))
                    Else
                        txtBeginningBudget.Text = "0.00"
                        dteBeginningDate.Value = gBeginningDate
                    End If
                End Using

            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Error!")
            End Try

        End If

    End Sub

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        SetAccountBudget()
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub BeginningBudget_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        loadAccountBudget()
    End Sub

End Class
