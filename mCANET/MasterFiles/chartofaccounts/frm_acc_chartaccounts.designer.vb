<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_acc_chartaccounts
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_acc_chartaccounts))
        Me.grdAccounts = New System.Windows.Forms.DataGridView
        Me.tsChartofAccounts = New System.Windows.Forms.ToolStrip
        Me.ToolStripDropDownButton1 = New System.Windows.Forms.ToolStripDropDownButton
        Me.msNew = New System.Windows.Forms.ToolStripMenuItem
        Me.msEdit = New System.Windows.Forms.ToolStripMenuItem
        Me.msDelete = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.msMkeInactive = New System.Windows.Forms.ToolStripMenuItem
        Me.msShowInactive = New System.Windows.Forms.ToolStripMenuItem
        Me.msCustomize = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.msImportExcel = New System.Windows.Forms.ToolStripMenuItem
        Me.msUse = New System.Windows.Forms.ToolStripMenuItem
        Me.msFindTransactions = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.msPrintList = New System.Windows.Forms.ToolStripMenuItem
        Me.msResortList = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripDropDownButton2 = New System.Windows.Forms.ToolStripDropDownButton
        Me.msWriteChecks = New System.Windows.Forms.ToolStripMenuItem
        Me.msMkeDeposits = New System.Windows.Forms.ToolStripMenuItem
        Me.msTransferFunds = New System.Windows.Forms.ToolStripMenuItem
        Me.msGenJournalEntry = New System.Windows.Forms.ToolStripMenuItem
        Me.msReconcile = New System.Windows.Forms.ToolStripMenuItem
        Me.msUseRegister = New System.Windows.Forms.ToolStripMenuItem
        Me.msWTrialBal = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripDropDownButton3 = New System.Windows.Forms.ToolStripDropDownButton
        Me.msAccountListing = New System.Windows.Forms.ToolStripMenuItem
        Me.msIncomeTax = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.msRptAllAccounts = New System.Windows.Forms.ToolStripMenuItem
        Me.mssProfit_Loss = New System.Windows.Forms.ToolStripMenuItem
        Me.msssStandard = New System.Windows.Forms.ToolStripMenuItem
        Me.msssPrevYComp = New System.Windows.Forms.ToolStripMenuItem
        Me.msssItemized = New System.Windows.Forms.ToolStripMenuItem
        Me.mssBalanceSheet = New System.Windows.Forms.ToolStripMenuItem
        Me.msssBalSta = New System.Windows.Forms.ToolStripMenuItem
        Me.msssBalComparison = New System.Windows.Forms.ToolStripMenuItem
        Me.msssBalSumm = New System.Windows.Forms.ToolStripMenuItem
        Me.msssBalItem = New System.Windows.Forms.ToolStripMenuItem
        Me.mssCashFlow = New System.Windows.Forms.ToolStripMenuItem
        Me.msssStatement = New System.Windows.Forms.ToolStripMenuItem
        Me.msssForecast = New System.Windows.Forms.ToolStripMenuItem
        Me.mssBudget = New System.Windows.Forms.ToolStripMenuItem
        Me.msssBudOverview = New System.Windows.Forms.ToolStripMenuItem
        Me.msssBudvsActual = New System.Windows.Forms.ToolStripMenuItem
        Me.msssBudComparison = New System.Windows.Forms.ToolStripMenuItem
        Me.mssOthers = New System.Windows.Forms.ToolStripMenuItem
        Me.msssOchkDetails = New System.Windows.Forms.ToolStripMenuItem
        Me.msssOmissingChecks = New System.Windows.Forms.ToolStripMenuItem
        Me.msssOdepDetials = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator
        Me.msssOtransDetailsbyAccount = New System.Windows.Forms.ToolStripMenuItem
        Me.msssOcustomTransRpt = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator
        Me.msssOadjTriBal = New System.Windows.Forms.ToolStripMenuItem
        Me.msssOGenLedger = New System.Windows.Forms.ToolStripMenuItem
        Me.msssOTrialBalance = New System.Windows.Forms.ToolStripMenuItem
        Me.msssOadjJournalEntry = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator
        Me.msssOauditTrail = New System.Windows.Forms.ToolStripMenuItem
        Me.msssOdeletedTransSumm = New System.Windows.Forms.ToolStripMenuItem
        Me.msssOdeletedTransDetails = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator
        Me.mssGraph = New System.Windows.Forms.ToolStripMenuItem
        Me.msssGincomeExpenses = New System.Windows.Forms.ToolStripMenuItem
        Me.msssGnetWorth = New System.Windows.Forms.ToolStripMenuItem
        Me.chkIncludeInactive = New System.Windows.Forms.CheckBox
        CType(Me.grdAccounts, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tsChartofAccounts.SuspendLayout()
        Me.SuspendLayout()
        '
        'grdAccounts
        '
        Me.grdAccounts.AllowUserToAddRows = False
        Me.grdAccounts.AllowUserToDeleteRows = False
        Me.grdAccounts.AllowUserToOrderColumns = True
        Me.grdAccounts.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.grdAccounts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdAccounts.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdAccounts.Location = New System.Drawing.Point(0, 0)
        Me.grdAccounts.Name = "grdAccounts"
        Me.grdAccounts.ReadOnly = True
        Me.grdAccounts.Size = New System.Drawing.Size(789, 405)
        Me.grdAccounts.TabIndex = 0
        '
        'tsChartofAccounts
        '
        Me.tsChartofAccounts.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.tsChartofAccounts.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.tsChartofAccounts.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tsChartofAccounts.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripDropDownButton1, Me.ToolStripDropDownButton2, Me.ToolStripDropDownButton3})
        Me.tsChartofAccounts.Location = New System.Drawing.Point(0, 405)
        Me.tsChartofAccounts.Name = "tsChartofAccounts"
        Me.tsChartofAccounts.Size = New System.Drawing.Size(789, 25)
        Me.tsChartofAccounts.TabIndex = 1
        Me.tsChartofAccounts.Text = "ToolStrip1"
        '
        'ToolStripDropDownButton1
        '
        Me.ToolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripDropDownButton1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.msNew, Me.msEdit, Me.msDelete, Me.ToolStripSeparator1, Me.msMkeInactive, Me.msShowInactive, Me.msCustomize, Me.ToolStripSeparator2, Me.msImportExcel, Me.msUse, Me.msFindTransactions, Me.ToolStripSeparator3, Me.msPrintList, Me.msResortList})
        Me.ToolStripDropDownButton1.Image = CType(resources.GetObject("ToolStripDropDownButton1.Image"), System.Drawing.Image)
        Me.ToolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripDropDownButton1.Name = "ToolStripDropDownButton1"
        Me.ToolStripDropDownButton1.Size = New System.Drawing.Size(71, 22)
        Me.ToolStripDropDownButton1.Text = "Accounts"
        '
        'msNew
        '
        Me.msNew.Name = "msNew"
        Me.msNew.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.msNew.Size = New System.Drawing.Size(217, 22)
        Me.msNew.Text = "New"
        Me.msNew.ToolTipText = "Create New Account"
        '
        'msEdit
        '
        Me.msEdit.Name = "msEdit"
        Me.msEdit.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.msEdit.Size = New System.Drawing.Size(217, 22)
        Me.msEdit.Text = "Edit Account"
        Me.msEdit.ToolTipText = "Edit Account"
        '
        'msDelete
        '
        Me.msDelete.Name = "msDelete"
        Me.msDelete.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.msDelete.Size = New System.Drawing.Size(217, 22)
        Me.msDelete.Text = "Delete Account"
        Me.msDelete.ToolTipText = "Delete Account"
        Me.msDelete.Visible = False
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(214, 6)
        '
        'msMkeInactive
        '
        Me.msMkeInactive.Name = "msMkeInactive"
        Me.msMkeInactive.Size = New System.Drawing.Size(217, 22)
        Me.msMkeInactive.Text = "Make Inactive"
        '
        'msShowInactive
        '
        Me.msShowInactive.Name = "msShowInactive"
        Me.msShowInactive.Size = New System.Drawing.Size(217, 22)
        Me.msShowInactive.Text = "Show Inactive Account"
        '
        'msCustomize
        '
        Me.msCustomize.Name = "msCustomize"
        Me.msCustomize.Size = New System.Drawing.Size(217, 22)
        Me.msCustomize.Text = "Customize Columns"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(214, 6)
        '
        'msImportExcel
        '
        Me.msImportExcel.Name = "msImportExcel"
        Me.msImportExcel.Size = New System.Drawing.Size(217, 22)
        Me.msImportExcel.Text = "Import from Excel"
        Me.msImportExcel.Visible = False
        '
        'msUse
        '
        Me.msUse.Name = "msUse"
        Me.msUse.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.U), System.Windows.Forms.Keys)
        Me.msUse.Size = New System.Drawing.Size(217, 22)
        Me.msUse.Text = "Use"
        Me.msUse.Visible = False
        '
        'msFindTransactions
        '
        Me.msFindTransactions.Name = "msFindTransactions"
        Me.msFindTransactions.Size = New System.Drawing.Size(217, 22)
        Me.msFindTransactions.Text = "Find Transactions In..."
        Me.msFindTransactions.Visible = False
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(214, 6)
        '
        'msPrintList
        '
        Me.msPrintList.Name = "msPrintList"
        Me.msPrintList.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.msPrintList.Size = New System.Drawing.Size(217, 22)
        Me.msPrintList.Text = "Print List"
        Me.msPrintList.ToolTipText = "Print List of Accounts"
        Me.msPrintList.Visible = False
        '
        'msResortList
        '
        Me.msResortList.Name = "msResortList"
        Me.msResortList.Size = New System.Drawing.Size(217, 22)
        Me.msResortList.Text = "Re-sort List"
        Me.msResortList.ToolTipText = "Re-sort list"
        Me.msResortList.Visible = False
        '
        'ToolStripDropDownButton2
        '
        Me.ToolStripDropDownButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripDropDownButton2.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.msWriteChecks, Me.msMkeDeposits, Me.msTransferFunds, Me.msGenJournalEntry, Me.msReconcile, Me.msUseRegister, Me.msWTrialBal})
        Me.ToolStripDropDownButton2.Image = CType(resources.GetObject("ToolStripDropDownButton2.Image"), System.Drawing.Image)
        Me.ToolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripDropDownButton2.Name = "ToolStripDropDownButton2"
        Me.ToolStripDropDownButton2.Size = New System.Drawing.Size(71, 22)
        Me.ToolStripDropDownButton2.Text = "Activities"
        '
        'msWriteChecks
        '
        Me.msWriteChecks.Name = "msWriteChecks"
        Me.msWriteChecks.Size = New System.Drawing.Size(252, 22)
        Me.msWriteChecks.Text = "Write Checks"
        '
        'msMkeDeposits
        '
        Me.msMkeDeposits.Name = "msMkeDeposits"
        Me.msMkeDeposits.Size = New System.Drawing.Size(252, 22)
        Me.msMkeDeposits.Text = "Make Deposits"
        '
        'msTransferFunds
        '
        Me.msTransferFunds.Name = "msTransferFunds"
        Me.msTransferFunds.Size = New System.Drawing.Size(252, 22)
        Me.msTransferFunds.Text = "Transfer Funds"
        '
        'msGenJournalEntry
        '
        Me.msGenJournalEntry.Name = "msGenJournalEntry"
        Me.msGenJournalEntry.Size = New System.Drawing.Size(252, 22)
        Me.msGenJournalEntry.Text = "Make General Journal Entries"
        '
        'msReconcile
        '
        Me.msReconcile.Name = "msReconcile"
        Me.msReconcile.Size = New System.Drawing.Size(252, 22)
        Me.msReconcile.Text = "Reconcile"
        '
        'msUseRegister
        '
        Me.msUseRegister.Name = "msUseRegister"
        Me.msUseRegister.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.R), System.Windows.Forms.Keys)
        Me.msUseRegister.Size = New System.Drawing.Size(252, 22)
        Me.msUseRegister.Text = "Use Register"
        Me.msUseRegister.Visible = False
        '
        'msWTrialBal
        '
        Me.msWTrialBal.Name = "msWTrialBal"
        Me.msWTrialBal.Size = New System.Drawing.Size(252, 22)
        Me.msWTrialBal.Text = "Working Trial Balance"
        '
        'ToolStripDropDownButton3
        '
        Me.ToolStripDropDownButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripDropDownButton3.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.msAccountListing, Me.msIncomeTax, Me.ToolStripSeparator4, Me.msRptAllAccounts})
        Me.ToolStripDropDownButton3.Image = CType(resources.GetObject("ToolStripDropDownButton3.Image"), System.Drawing.Image)
        Me.ToolStripDropDownButton3.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripDropDownButton3.Name = "ToolStripDropDownButton3"
        Me.ToolStripDropDownButton3.Size = New System.Drawing.Size(64, 22)
        Me.ToolStripDropDownButton3.Text = "Reports"
        Me.ToolStripDropDownButton3.Visible = False
        '
        'msAccountListing
        '
        Me.msAccountListing.Name = "msAccountListing"
        Me.msAccountListing.Size = New System.Drawing.Size(229, 22)
        Me.msAccountListing.Text = "Account Listing"
        '
        'msIncomeTax
        '
        Me.msIncomeTax.Name = "msIncomeTax"
        Me.msIncomeTax.Size = New System.Drawing.Size(229, 22)
        Me.msIncomeTax.Text = "Income Tax Preparations"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(226, 6)
        '
        'msRptAllAccounts
        '
        Me.msRptAllAccounts.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mssProfit_Loss, Me.mssBalanceSheet, Me.mssCashFlow, Me.mssBudget, Me.mssOthers, Me.ToolStripSeparator5, Me.mssGraph})
        Me.msRptAllAccounts.Name = "msRptAllAccounts"
        Me.msRptAllAccounts.Size = New System.Drawing.Size(229, 22)
        Me.msRptAllAccounts.Text = "Reports on All Accounts"
        '
        'mssProfit_Loss
        '
        Me.mssProfit_Loss.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.msssStandard, Me.msssPrevYComp, Me.msssItemized})
        Me.mssProfit_Loss.Name = "mssProfit_Loss"
        Me.mssProfit_Loss.Size = New System.Drawing.Size(167, 22)
        Me.mssProfit_Loss.Text = "Profit & Loss"
        '
        'msssStandard
        '
        Me.msssStandard.Name = "msssStandard"
        Me.msssStandard.Size = New System.Drawing.Size(214, 22)
        Me.msssStandard.Text = "Standard"
        '
        'msssPrevYComp
        '
        Me.msssPrevYComp.Name = "msssPrevYComp"
        Me.msssPrevYComp.Size = New System.Drawing.Size(214, 22)
        Me.msssPrevYComp.Text = "Prev Year Comparison"
        '
        'msssItemized
        '
        Me.msssItemized.Name = "msssItemized"
        Me.msssItemized.Size = New System.Drawing.Size(214, 22)
        Me.msssItemized.Text = "Itemized"
        '
        'mssBalanceSheet
        '
        Me.mssBalanceSheet.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.msssBalSta, Me.msssBalComparison, Me.msssBalSumm, Me.msssBalItem})
        Me.mssBalanceSheet.Name = "mssBalanceSheet"
        Me.mssBalanceSheet.Size = New System.Drawing.Size(167, 22)
        Me.mssBalanceSheet.Text = "Balance Sheet"
        '
        'msssBalSta
        '
        Me.msssBalSta.Name = "msssBalSta"
        Me.msssBalSta.Size = New System.Drawing.Size(154, 22)
        Me.msssBalSta.Text = "Standard"
        '
        'msssBalComparison
        '
        Me.msssBalComparison.Name = "msssBalComparison"
        Me.msssBalComparison.Size = New System.Drawing.Size(154, 22)
        Me.msssBalComparison.Text = "Comparison"
        '
        'msssBalSumm
        '
        Me.msssBalSumm.Name = "msssBalSumm"
        Me.msssBalSumm.Size = New System.Drawing.Size(154, 22)
        Me.msssBalSumm.Text = "Summary"
        '
        'msssBalItem
        '
        Me.msssBalItem.Name = "msssBalItem"
        Me.msssBalItem.Size = New System.Drawing.Size(154, 22)
        Me.msssBalItem.Text = "Itemized"
        '
        'mssCashFlow
        '
        Me.mssCashFlow.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.msssStatement, Me.msssForecast})
        Me.mssCashFlow.Name = "mssCashFlow"
        Me.mssCashFlow.Size = New System.Drawing.Size(167, 22)
        Me.mssCashFlow.Text = "Cash Flow"
        '
        'msssStatement
        '
        Me.msssStatement.Name = "msssStatement"
        Me.msssStatement.Size = New System.Drawing.Size(227, 22)
        Me.msssStatement.Text = "Statement of Cash Flows"
        '
        'msssForecast
        '
        Me.msssForecast.Name = "msssForecast"
        Me.msssForecast.Size = New System.Drawing.Size(227, 22)
        Me.msssForecast.Text = "Forecast"
        '
        'mssBudget
        '
        Me.mssBudget.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.msssBudOverview, Me.msssBudvsActual, Me.msssBudComparison})
        Me.mssBudget.Name = "mssBudget"
        Me.mssBudget.Size = New System.Drawing.Size(167, 22)
        Me.mssBudget.Text = "Budget"
        '
        'msssBudOverview
        '
        Me.msssBudOverview.Name = "msssBudOverview"
        Me.msssBudOverview.Size = New System.Drawing.Size(231, 22)
        Me.msssBudOverview.Text = "Budget Overview"
        '
        'msssBudvsActual
        '
        Me.msssBudvsActual.Name = "msssBudvsActual"
        Me.msssBudvsActual.Size = New System.Drawing.Size(231, 22)
        Me.msssBudvsActual.Text = "Budget vs. Actual"
        '
        'msssBudComparison
        '
        Me.msssBudComparison.Name = "msssBudComparison"
        Me.msssBudComparison.Size = New System.Drawing.Size(231, 22)
        Me.msssBudComparison.Text = "P && L Budget Comparison"
        '
        'mssOthers
        '
        Me.mssOthers.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.msssOchkDetails, Me.msssOmissingChecks, Me.msssOdepDetials, Me.ToolStripSeparator6, Me.msssOtransDetailsbyAccount, Me.msssOcustomTransRpt, Me.ToolStripSeparator7, Me.msssOadjTriBal, Me.msssOGenLedger, Me.msssOTrialBalance, Me.msssOadjJournalEntry, Me.ToolStripSeparator8, Me.msssOauditTrail, Me.msssOdeletedTransSumm, Me.msssOdeletedTransDetails})
        Me.mssOthers.Name = "mssOthers"
        Me.mssOthers.Size = New System.Drawing.Size(167, 22)
        Me.mssOthers.Text = "Other"
        '
        'msssOchkDetails
        '
        Me.msssOchkDetails.Name = "msssOchkDetails"
        Me.msssOchkDetails.Size = New System.Drawing.Size(309, 22)
        Me.msssOchkDetails.Text = "Check Details"
        '
        'msssOmissingChecks
        '
        Me.msssOmissingChecks.Name = "msssOmissingChecks"
        Me.msssOmissingChecks.Size = New System.Drawing.Size(309, 22)
        Me.msssOmissingChecks.Text = "Missing Checks"
        '
        'msssOdepDetials
        '
        Me.msssOdepDetials.Name = "msssOdepDetials"
        Me.msssOdepDetials.Size = New System.Drawing.Size(309, 22)
        Me.msssOdepDetials.Text = "Deposit Details"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(306, 6)
        '
        'msssOtransDetailsbyAccount
        '
        Me.msssOtransDetailsbyAccount.Name = "msssOtransDetailsbyAccount"
        Me.msssOtransDetailsbyAccount.Size = New System.Drawing.Size(309, 22)
        Me.msssOtransDetailsbyAccount.Text = "Transaction Details by Account"
        '
        'msssOcustomTransRpt
        '
        Me.msssOcustomTransRpt.Name = "msssOcustomTransRpt"
        Me.msssOcustomTransRpt.Size = New System.Drawing.Size(309, 22)
        Me.msssOcustomTransRpt.Text = "Custom Transaction Details Report"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(306, 6)
        '
        'msssOadjTriBal
        '
        Me.msssOadjTriBal.Name = "msssOadjTriBal"
        Me.msssOadjTriBal.Size = New System.Drawing.Size(309, 22)
        Me.msssOadjTriBal.Text = "Adjusted Trial Balance"
        '
        'msssOGenLedger
        '
        Me.msssOGenLedger.Name = "msssOGenLedger"
        Me.msssOGenLedger.Size = New System.Drawing.Size(309, 22)
        Me.msssOGenLedger.Text = "General Ledger"
        '
        'msssOTrialBalance
        '
        Me.msssOTrialBalance.Name = "msssOTrialBalance"
        Me.msssOTrialBalance.Size = New System.Drawing.Size(309, 22)
        Me.msssOTrialBalance.Text = "Trial Balance"
        '
        'msssOadjJournalEntry
        '
        Me.msssOadjJournalEntry.Name = "msssOadjJournalEntry"
        Me.msssOadjJournalEntry.Size = New System.Drawing.Size(309, 22)
        Me.msssOadjJournalEntry.Text = "Adjusting Journal Entries"
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(306, 6)
        '
        'msssOauditTrail
        '
        Me.msssOauditTrail.Name = "msssOauditTrail"
        Me.msssOauditTrail.Size = New System.Drawing.Size(309, 22)
        Me.msssOauditTrail.Text = "Audit Trail"
        '
        'msssOdeletedTransSumm
        '
        Me.msssOdeletedTransSumm.Name = "msssOdeletedTransSumm"
        Me.msssOdeletedTransSumm.Size = New System.Drawing.Size(309, 22)
        Me.msssOdeletedTransSumm.Text = "Voided/Deleted Transactions Summary"
        '
        'msssOdeletedTransDetails
        '
        Me.msssOdeletedTransDetails.Name = "msssOdeletedTransDetails"
        Me.msssOdeletedTransDetails.Size = New System.Drawing.Size(309, 22)
        Me.msssOdeletedTransDetails.Text = "Voided/Deleted Transactions Details"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(164, 6)
        '
        'mssGraph
        '
        Me.mssGraph.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.msssGincomeExpenses, Me.msssGnetWorth})
        Me.mssGraph.Name = "mssGraph"
        Me.mssGraph.Size = New System.Drawing.Size(167, 22)
        Me.mssGraph.Text = "Graphs"
        '
        'msssGincomeExpenses
        '
        Me.msssGincomeExpenses.Name = "msssGincomeExpenses"
        Me.msssGincomeExpenses.Size = New System.Drawing.Size(198, 22)
        Me.msssGincomeExpenses.Text = "Income && Expenses"
        '
        'msssGnetWorth
        '
        Me.msssGnetWorth.Name = "msssGnetWorth"
        Me.msssGnetWorth.Size = New System.Drawing.Size(198, 22)
        Me.msssGnetWorth.Text = "Net Worth"
        '
        'chkIncludeInactive
        '
        Me.chkIncludeInactive.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkIncludeInactive.AutoSize = True
        Me.chkIncludeInactive.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.chkIncludeInactive.Location = New System.Drawing.Point(668, 409)
        Me.chkIncludeInactive.Name = "chkIncludeInactive"
        Me.chkIncludeInactive.Size = New System.Drawing.Size(116, 17)
        Me.chkIncludeInactive.TabIndex = 3
        Me.chkIncludeInactive.Text = "Include in&active"
        Me.chkIncludeInactive.UseVisualStyleBackColor = False
        '
        'frm_acc_chartaccounts
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(789, 430)
        Me.Controls.Add(Me.grdAccounts)
        Me.Controls.Add(Me.chkIncludeInactive)
        Me.Controls.Add(Me.tsChartofAccounts)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(785, 454)
        Me.Name = "frm_acc_chartaccounts"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Chart of Accounts"
        Me.TopMost = True
        CType(Me.grdAccounts, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tsChartofAccounts.ResumeLayout(False)
        Me.tsChartofAccounts.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents grdAccounts As System.Windows.Forms.DataGridView
    Friend WithEvents tsChartofAccounts As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripDropDownButton1 As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents msNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msDelete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents msMkeInactive As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msShowInactive As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msCustomize As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents msImportExcel As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msUse As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msFindTransactions As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents msPrintList As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msResortList As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripDropDownButton2 As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents msWriteChecks As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msMkeDeposits As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msGenJournalEntry As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msReconcile As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msWTrialBal As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripDropDownButton3 As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents msIncomeTax As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents msAccountListing As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msRptAllAccounts As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mssProfit_Loss As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mssBalanceSheet As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mssCashFlow As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mssBudget As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mssOthers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mssGraph As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msssStandard As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msssPrevYComp As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msssItemized As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msssBalSta As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msssBalComparison As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msssBalSumm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msssBalItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msssStatement As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msssForecast As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msssBudOverview As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msssBudvsActual As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msssBudComparison As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msssOchkDetails As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msssOmissingChecks As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msssOdepDetials As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents msssOtransDetailsbyAccount As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msssOcustomTransRpt As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents msssOadjTriBal As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msssOGenLedger As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msssOTrialBalance As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msssOadjJournalEntry As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents msssOauditTrail As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msssOdeletedTransSumm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msssOdeletedTransDetails As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msssGincomeExpenses As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msssGnetWorth As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msTransferFunds As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msUseRegister As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkIncludeInactive As System.Windows.Forms.CheckBox

End Class
