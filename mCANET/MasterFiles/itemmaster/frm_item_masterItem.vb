Imports Microsoft.ApplicationBlocks.Data

Public Class frm_item_masterItem
    Private gCon As New Clsappconfiguration
    Private sSQLCmdQuery As String = "SELECT * FROM mItem00Master WHERE fbActive <> 1 "
    Private sKeyItem As String

    Public Property SQLQuery() As String
        Get
            Return sSQLCmdQuery
        End Get
        Set(ByVal value As String)

        End Set
    End Property
    Private Sub frm_acc_itemmaster_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        refreshForm()
    End Sub
    Public Sub refreshForm()

        m_ItemList(chkIncludeInactive.CheckState, grdItems)
        m_chkSettings(chkIncludeInactive, sSQLCmdQuery)
        ts_item_showinactive.Enabled = chkIncludeInactive.Enabled
        Arrange_GridItems(grdItems)
    End Sub
    Private Sub Arrange_GridItems(ByVal gridItems As DataGridView)
        Const GroupName As Integer = 3
        Const Brand As Integer = 4
        Const OnHand As Integer = 6
        Const Price As Integer = 7

        With gridItems
            .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(GroupName).Width = 120
            .Columns(OnHand).Width = 100
            .Columns(Brand).Width = 150
            .Columns(OnHand).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(Brand).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(Price).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(Price).DefaultCellStyle.Format = "##,##0.00"
        End With

    End Sub
    Private Sub Filter()
        m_ItemList(chkIncludeInactive.CheckState, grdItems, txtFilter.Text)
        m_chkSettings(chkIncludeInactive, sSQLCmdQuery)
        ts_item_showinactive.Enabled = chkIncludeInactive.Enabled
        Arrange_GridItems(grdItems)
        txtFilter.SelectAll()
    End Sub
    'Private Sub load_items_listofitems()
    '    With lst_item_listofitems
    '        .Clear()
    '        .View = View.Details
    '        .Columns.Add("Name", 120, HorizontalAlignment.Left)
    '        .Columns.Add("Description", 130, HorizontalAlignment.Left)
    '        .Columns.Add("Type", 70, HorizontalAlignment.Left)
    '        .Columns.Add("Account", 110, HorizontalAlignment.Left)
    '        .Columns.Add("On Hand", 70, HorizontalAlignment.Left)
    '        .Columns.Add("On Sales Order", 90, HorizontalAlignment.Left)
    '        .Columns.Add("Price", 100, HorizontalAlignment.Left)
    '    End With
    'End Sub

    Private Sub ts_item_new_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_item_new.Click
        'frm_item_masterItemAddEdit.Text = "New Item"
        'frm_item_masterItemAddEdit.IsNew = True
        'frm_item_masterItemAddEdit.Refresh()
        'frm_item_masterItemAddEdit.KeyItem = Guid.NewGuid.ToString
        'frm_item_masterItemAddEdit.ShowDialog()

        Dim x As New frm_item_masterItemAddEdit
        gItemMasterMode = "New Item"
        x.ShowDialog()
    End Sub
    Private Sub ts_item_edit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_item_edit.Click

        'frm_item_masterItemAddEdit.IsNew = False
        frm_item_masterItemAddEdit.Refresh()
        'frm_item_masterItemAddEdit.KeyItem = sKeyItem
        gItemMasterMode = "Edit Item"
        frm_item_masterItemAddEdit.ShowDialog()
    End Sub
    Private Sub ts_item_createinvoice_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_item_createinvoice.Click
        frm_cust_CreateInvoice.ShowDialog()
    End Sub
    Private Sub ts_item_salereceipt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_item_salereceipt.Click
        frm_cust_CreateSalesReceipt.ShowDialog()
    End Sub
    Private Sub ts_item_itemprices_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_item_itemprices.Click
        frm_item_changeprice.ShowDialog()
    End Sub
    Private Sub ts_item_buildassenblies_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_item_buildassenblies.Click
        frm_item_buildassembies.ShowDialog()
    End Sub
    Private Sub ts_item_availability_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_item_availability.Click
        frm_item_availability.KeyItem = sKeyItem
        frm_item_availability.ShowDialog()
    End Sub
    Private Sub ts_item_purchaseorder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_item_purchaseorder.Click
        frm_vend_CreatePurchaseOrder.ShowDialog()
    End Sub
    Private Sub ts_item_recEnterbill_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_item_recEnterbill.Click
        frm_vend_EnterBills.ShowDialog()
    End Sub
    Private Sub ts_item_recItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_item_recItem.Click
        frm_vend_EnterBills.ShowDialog()
    End Sub
    Private Sub ts_item_billrecItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_item_billrecItem.Click
        frm_vend_EnterBillsForItems.ShowDialog()
    End Sub
    Private Sub ts_item_quantity_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_item_quantity.Click
        Dim x As New frm_item_adjQty
        x.ShowDialog()
    End Sub
    Private Sub ts_item_showinactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_item_showinactive.Click
        chkIncludeInactive.Checked = True
    End Sub
    Private Sub chkIncludeInactive_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIncludeInactive.CheckStateChanged
        m_ItemList(chkIncludeInactive.CheckState, grdItems)
    End Sub
    Private Sub grdItems_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdItems.CellClick
        If Me.grdItems.SelectedCells.Count > 0 AndAlso _
                          Not Me.grdItems.CurrentRow.Index = _
                          Me.grdItems.Rows.Count - 1 Then
            sKeyItem = grdItems.CurrentRow.Cells(0).Value.ToString
            gItemsKeys = grdItems.CurrentRow.Cells(0).Value.ToString
            If m_isActive("mItemMaster", "fxKeyItem", sKeyItem) Then
                ts_item_makeinactive.Text = "Make Inactive"
            Else
                ts_item_makeinactive.Text = "Make Active"
            End If
        End If
    End Sub
    Private Sub chkIncludeInactive_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIncludeInactive.EnabledChanged
        ts_item_showinactive.Enabled = chkIncludeInactive.Enabled
    End Sub

    'Private Sub ts_showInactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_showInactive.Click
    '    chkIncludeInactive.Checked = True
    'End Sub

    Private Sub ts_item_makeinactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_item_makeinactive.Click
        If ts_item_makeinactive.Text = "Make Active" Then
            m_mkActive("mItem00Master", "fxKeyItem", sKeyItem, True)
        Else
            m_mkActive("mItem00Master", "fxKeyItem", sKeyItem, False)
        End If
        refreshForm()
    End Sub
    Private Sub grdItems_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdItems.DoubleClick
        'If Me.grdItems.SelectedCells.Count > 0 AndAlso _
        '               Not Me.grdItems.CurrentRow.Index = _
        '               Me.grdItems.Rows.Count - 1 Then
        sKeyItem = grdItems.CurrentRow.Cells(0).Value.ToString
        gItemsKeys = grdItems.CurrentRow.Cells(0).Value.ToString
        gItemMasterMode = "Edit Item"
        'frm_item_masterItemAddEdit.IsNew = False
        'frm_item_masterItemAddEdit.KeyItem = sKeyItem
        frm_item_masterItemAddEdit.ShowDialog()
        'End If
    End Sub
    Private Sub ts_item_delete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_item_delete.Click
        deleteItems()
        refreshForm()
    End Sub
    Private Sub deleteItems()
        If MessageBox.Show("Are you sure you want to delete this record?", "Delete", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
            Dim sSQLCmd As String = "DELETE FROM mItem00Master WHERE fxKeyItem = '" & sKeyItem & "'"
            Try
                SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
                MessageBox.Show("Record successfully deleted.")
            Catch ex As Exception
                MessageBox.Show(Err.Description)
            End Try
        End If
    End Sub
    Private Sub ts_item_excImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_item_excImport.Click
        frmImportItem.ShowDialog()
    End Sub

    Private Sub BtnFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnFilter.Click
        Filter()
    End Sub

    Private Sub txtFilter_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtFilter.KeyPress
        If Asc(e.KeyChar) = Keys.Enter Then
            Filter()
            e.Handled = True
        End If
    End Sub

End Class