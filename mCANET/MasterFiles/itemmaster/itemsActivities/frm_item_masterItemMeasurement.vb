Imports Microsoft.ApplicationBlocks.Data

Public Class frm_item_masterItemMeasurement
    Public gcon As New Clsappconfiguration
    Private sKeyType As String
    Private sKeyUnit As String
    Private sKeyUnitUsed As String

    Public Property KeyUnitUsed() As String
        Get
            Return sKeyUnitUsed
        End Get
        Set(ByVal value As String)
            sKeyUnitUsed = value
        End Set
    End Property

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub frm_item_masterItemMeasurement_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sKeyUnitUsed = "" Or sKeyUnitUsed = Nothing Then
            sKeyUnitUsed = Guid.NewGuid.ToString
        End If
        m_loadUnitType(cboUnitTypeName, cboUnitTypeID)
        LoadUnitUsed()
        m_selectedCboValue(cboUnitTypeName, cboUnitTypeID, sKeyType)
        m_selectedCboValue(cboUnitName, cboUnitID, sKeyUnit)
    End Sub

    Private Sub LoadUnitUsed()
        Try
            Dim dtUnit As DataSet = m_getMeasurementUsed(sKeyUnitUsed)

            Using rd As DataTableReader = dtUnit.CreateDataReader
                If rd.Read Then
                    sKeyUnit = rd.Item("fxKeyUnit").ToString
                    sKeyUnitUsed = rd.Item("fxKeyUnitUsed").ToString
                    sKeyType = rd.Item("fxKeyUnitType").ToString
                Else
                    sKeyUnit = ""
                    sKeyUnitUsed = ""
                    sKeyType = ""
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Unit Used")
        End Try
    End Sub

    Private Sub cboUnitTypeName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboUnitTypeName.SelectedIndexChanged
        cboUnitTypeID.SelectedIndex = cboUnitTypeName.SelectedIndex
        sKeyType = cboUnitTypeID.SelectedItem.ToString

        If cboUnitTypeName.SelectedIndex = 1 Then
            MessageBox.Show("In Progress")
        End If

        If cboUnitTypeID.SelectedIndex <> 0 And _
        cboUnitTypeID.SelectedIndex <> 1 Then
            m_loadUnit(cboUnitName, cboUnitID, sKeyType)
            loadType()
        End If
    End Sub

    Private Sub loadType()
        Dim dtType As DataTable = m_getMeasurementType(sKeyType).Tables(0)
        txtDescription.Text = dtType.Rows(0)("fcUnitTypeDesc")
    End Sub

    Private Sub cboUnitName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboUnitName.SelectedIndexChanged
        cboUnitID.SelectedIndex = cboUnitName.SelectedIndex
        sKeyUnit = cboUnitID.SelectedItem.ToString

        If cboUnitName.SelectedIndex = 1 Then
            MessageBox.Show("In Progress")
        End If

        If cboUnitID.SelectedIndex <> 0 And _
        cboUnitID.SelectedIndex <> 1 Then
            loadUnit()
        End If
    End Sub

    Private Sub loadUnit()
        Dim dtUnit As DataTable = m_getMeasurement(sKeyUnit).Tables(0)
        lblAbbrevUnit.Text = dtUnit.Rows(0)("fcUnitAbbreviation")
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        updateMeasurementUsed()
    End Sub

    Private Sub updateMeasurementUsed()
        Dim sSQLCmd As String = "usp_m_measurementUsed_update "
        sSQLCmd &= "@fxKeyUnitUsed ='" & sKeyUnitUsed & "'"
        sSQLCmd &= ",@fxKeyUnit ='" & sKeyUnit & "'"
        sSQLCmd &= ",@fcUnitName ='" & cboUnitName.SelectedItem & "'"
        sSQLCmd &= ",@fcUnitAbbreviation ='" & lblAbbrevUnit.Text & "'"
        sSQLCmd &= ",@fxKeyCompany ='" & gCompanyID() & "'"
        Try
            SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSQLCmd)
            MessageBox.Show("Record successfully updated", "Add Unit of Measurement")
            'frm_item_masterItemAddEdit.unitSettings()
            'm_loadUnitUsed(frm_item_masterItemAddEdit.cboUnitName, frm_item_masterItemAddEdit.cboUnitID)
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Add Unit of Measurement")
        End Try
    End Sub
End Class