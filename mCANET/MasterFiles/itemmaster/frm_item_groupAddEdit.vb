Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_item_groupAddEdit
    Private gCon As New Clsappconfiguration
    Private sKeyGroup As String

    Public Property KeyGroup() As String
        Get
            Return sKeyGroup
        End Get
        Set(ByVal value As String)
            sKeyGroup = value
        End Set
    End Property

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub addEditItemGroup()
        Dim sSQLCmd As String = "usp_m_itemGroup_update "
        sSQLCmd &= "@fxKeyItemGroup = '" & sKeyGroup & "'"
        sSQLCmd &= ",@fcItemGroupName = '" & txtGroupName.Text & "'"
        sSQLCmd &= ",@fcItemGroupDescription = '" & txtGroupDesc.Text & "'"
        sSQLCmd &= ",@fbActive =" & IIf(chkInactive.Checked = True, 0, 1)
        sSQLCmd &= ",@fxKeyCompany = '" & gCompanyID() & "'"
        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            MsgBox("Record successfully updated", MsgBoxStyle.Information, "Add/Edit Item Group")
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Add/Edit Item Group")
        End Try

    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        addEditItemGroup()
        m_itemGroupList(frm_item_group.chkIncludeInactive.CheckState, frm_item_group.grdItemGroup)
        'm_loadItemGroup(frm_item_masterItemAddEdit.cbo_item_itemGroupName, frm_item_masterItemAddEdit.cbo_item_itemGroupID)
        m_chkSettings(frm_item_group.chkIncludeInactive, frm_item_group.SQLQuery)
        Me.Close()
    End Sub

    'Private Sub loadItemGroup()

    '    Dim sSQLCmd As String = "SELECT * FROM mItem01Group WHERE fxKeyItemGroup='" & sKeyGroup & "'"

    '    Try
    '        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
    '            If rd.Read Then
    '                txtGroupName.Text = rd.Item("fcItemGroupName")
    '                txtGroupDesc.Text = rd.Item("fcItemGroupDescription")
    '                chkInactive.Checked = IIf(rd.Item("fbActive") = True, False, True)
    '            End If
    '        End Using
    '    Catch ex As Exception
    '        MsgBox(Err.Description, MsgBoxStyle.Exclamation, "Load Item Group")
    '    End Try
    'End Sub

    Private Sub frm_MF_termsAddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'rbtnStandard.Checked = True
        'ctrlSettings()
        If sKeyGroup = "" Or sKeyGroup = Nothing Then
            sKeyGroup = Guid.NewGuid.ToString
        End If
        'loadItemGroup()
    End Sub

    'Private Sub ctrlSettings()
    '    If rbtnStandard.Checked = True Then
    '        txtStdDaysDiscount.Enabled = True
    '        txtStdDaysDue.Enabled = True
    '        txtStdPercentage.Enabled = True

    '        txtDtDaysDiscount.Enabled = False
    '        txtDtDaysDue.Enabled = False
    '        txtDtPercentage.Enabled = False
    '        txtDtDaysNxDue.Enabled = False
    '    Else
    '        txtStdDaysDiscount.Enabled = False
    '        txtStdDaysDue.Enabled = False
    '        txtStdPercentage.Enabled = False

    '        txtDtDaysDiscount.Enabled = True
    '        txtDtDaysDue.Enabled = True
    '        txtDtPercentage.Enabled = True
    '        txtDtDaysNxDue.Enabled = True
    '    End If
    'End Sub

    'Private Sub rbtnStandard_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    ctrlSettings()
    'End Sub
End Class