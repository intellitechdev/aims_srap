Imports Microsoft.ApplicationBlocks.Data

Public Class frm_MF_paymentMethod
    Private sKeyPaymentMethod As String
    Private gCon As New Clsappconfiguration
    Private sSQLCmdQuery As String = "SELECT * FROM mCustomer03PaymentMethod WHERE fbActive <> 1 "

    Public Property SQLQuery() As String
        Get
            Return sSQLCmdQuery
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Private Sub ts_New_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_New.Click
        frm_MF_paymentMethodAddEdit.KeyPayMethod = Guid.NewGuid.ToString
        frm_MF_paymentMethodAddEdit.MdiParent = Me.MdiParent
        frm_MF_paymentMethodAddEdit.Text = "Add Payment Method"
        frm_MF_paymentMethodAddEdit.Show()
    End Sub

    Private Sub ts_Edit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_Edit.Click
        frm_MF_paymentMethodAddEdit.MdiParent = Me.MdiParent
        frm_MF_paymentMethodAddEdit.KeyPayMethod = sKeyPaymentMethod
        frm_MF_paymentMethodAddEdit.Text = "Edit Payment Method"
        frm_MF_paymentMethodAddEdit.Show()
    End Sub

    Private Sub frm_MF_paymentMethod_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        refreshForm()
    End Sub

    Private Sub refreshForm()
        m_paymentMethodList(chkIncludeInactive.CheckState, grdPayment)
        ' grdSettings()
        m_chkSettings(chkIncludeInactive, sSQLCmdQuery)
        ts_showInactive.Enabled = chkIncludeInactive.Enabled
    End Sub
    'Private Sub grdSettings()
    '    With grdPayment
    '        .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
    '        .Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
    '        .Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
    '        .Columns(0).Visible = False
    '        .Columns(1).Visible = False
    '        .Columns.Item(1).HeaderText = "Active"
    '        .Columns.Item(2).HeaderText = "Terms Name"
    '        If chkIncludeInactive.CheckState = CheckState.Checked Then
    '            .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
    '            .Columns(1).Visible = True
    '        End If
    '    End With
    'End Sub

    Private Sub chkIncludeInactive_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIncludeInactive.CheckStateChanged
        m_paymentMethodList(chkIncludeInactive.CheckState, grdPayment)
        ' grdSettings()
    End Sub

    Private Sub grdPayment_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdPayment.CellClick
        If Me.grdPayment.SelectedCells.Count > 0 Then
            sKeyPaymentMethod = grdPayment.CurrentRow.Cells(0).Value.ToString

            If m_isActive("mCustomer03PaymentMethod", "fxKeyPaymentMethod", sKeyPaymentMethod) Then
                ts_MkInactive.Text = "Make Inactive"
            Else
                ts_MkInactive.Text = "Make Active"
            End If
        End If
    End Sub

    Private Sub grdPayment_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdPayment.DoubleClick
        If Me.grdPayment.SelectedCells.Count > 0 Then
            frm_MF_paymentMethodAddEdit.MdiParent = Me.MdiParent
            frm_MF_paymentMethodAddEdit.KeyPayMethod = sKeyPaymentMethod
            frm_MF_paymentMethodAddEdit.Text = "Edit Payment Method"
            frm_MF_paymentMethodAddEdit.Show()
        End If
    End Sub

    Private Sub deletePayment()
        If MessageBox.Show("Are you sure you want to delete this record?", "Delete", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
            Dim sSQLCmd As String = "DELETE FROM mCustomer03PaymentMethod "
            sSQLCmd &= "WHERE fxKeyPaymentMethod = '" & sKeyPaymentMethod & "'"
            Try
                SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
                MessageBox.Show("Record successfully deleted.")
            Catch ex As Exception
                MessageBox.Show(Err.Description)
            End Try
        End If
    End Sub

    Private Sub ts_Delete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_Delete.Click
        deletePayment()
        refreshForm()
    End Sub

    Private Sub chkIncludeInactive_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIncludeInactive.EnabledChanged
        ts_showInactive.Enabled = chkIncludeInactive.Enabled
    End Sub

    Private Sub ts_showInactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_showInactive.Click
        chkIncludeInactive.Checked = True
    End Sub

    Private Sub ts_MkInactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_MkInactive.Click
        If ts_MkInactive.Text = "Make Active" Then
            m_mkActive("mCustomer03PaymentMethod", "fxKeyPaymentMethod", sKeyPaymentMethod, True)
        Else
            m_mkActive("mCustomer03PaymentMethod", "fxKeyPaymentMethod", sKeyPaymentMethod, False)
        End If
        refreshForm()
    End Sub
End Class