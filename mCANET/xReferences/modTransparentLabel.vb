

'                   ########################################################
'                   ### Created by: Charl Magne "Ionflux" Onod San Pedro ###
'                   ### Date Created: July 02, 2010                      ###
'                   ### Website: http://www.ionflux.site50.net           ###
'                   ########################################################

Public Class modTransparentLabel

    Inherits Label
    Private _rotationAngle As Integer
    Private _quadrantMode As Boolean

    Friend WithEvents Label1 As System.Windows.Forms.Label
    Public Sub New()
        Me.SetStyle(ControlStyles.Opaque, True)
        Me.SetStyle(ControlStyles.OptimizedDoubleBuffer, False)
    End Sub

    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            cp.ExStyle = cp.ExStyle Or &H20
            Return cp
        End Get
    End Property

    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(0, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 23)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Label1"
        Me.ResumeLayout(False)

    End Sub
    '################################


    'Properties
    Public Property rotationAngle() As Integer
        Get
            Return _rotationAngle
        End Get
        Set(ByVal Value As Integer)
            _rotationAngle = Value
            MyBase.Refresh()
        End Set
    End Property

    Public Property quadrantMode() As Boolean
        Get
            Return _quadrantMode
        End Get
        Set(ByVal Value As Boolean)
            _quadrantMode = Value
            MyBase.Refresh()
        End Set
    End Property
    Protected Overrides Sub OnPaint(ByVal e As System.Windows.Forms.PaintEventArgs)

        Dim width As Double = e.Graphics.MeasureString(Text, Me.Font).Width
        Dim height As Double = e.Graphics.MeasureString(Text, Me.Font).Height

        Dim angleRadian As Double = ((_rotationAngle Mod 360) / 180) * Math.PI

        Dim myBrush As Brush = New SolidBrush(Me.ForeColor)

        If _quadrantMode = True Then
            'Quad I
            If (_rotationAngle >= 0 AndAlso _rotationAngle < 90) Or (_rotationAngle < -270 AndAlso _rotationAngle >= -360) Then
                e.Graphics.TranslateTransform(CInt(Math.Sin(angleRadian) * height), 0)
                'Quad II
            ElseIf (_rotationAngle >= 90 AndAlso _rotationAngle < 180) Or (_rotationAngle < -180 AndAlso _rotationAngle >= -270) Then
                e.Graphics.TranslateTransform(ClientRectangle.Width, CInt(height - (Math.Sin(angleRadian) * height)))
                'Quad III
            ElseIf (_rotationAngle >= 180 AndAlso _rotationAngle < 270) Or (_rotationAngle < -90 AndAlso _rotationAngle >= -180) Then
                e.Graphics.TranslateTransform(ClientRectangle.Width + CInt(Math.Sin(angleRadian) * height), ClientRectangle.Height)
            Else 'Quad IV
                e.Graphics.TranslateTransform(0, ClientRectangle.Height - CInt(Math.Cos(angleRadian) * height))
            End If
        Else 'Center Mode
            e.Graphics.TranslateTransform(CInt((ClientRectangle.Width + (height * Math.Sin(angleRadian)) - (width * Math.Cos(angleRadian))) / 2), CInt((ClientRectangle.Height - (height * Math.Cos(angleRadian)) - (width * Math.Sin(angleRadian))) / 2))
        End If
        'Make the actual rotation,and draw the string
        e.Graphics.RotateTransform(CInt(_rotationAngle))
        e.Graphics.DrawString(Me.Text, Me.Font, myBrush, 0, 0)
        e.Graphics.ResetTransform()
    End Sub
End Class
