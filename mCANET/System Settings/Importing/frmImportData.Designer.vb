<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportData
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportData))
        Me.grdImport = New System.Windows.Forms.DataGridView
        Me.MenuStripUploads = New System.Windows.Forms.MenuStrip
        Me.btnChrtAcnts = New System.Windows.Forms.ToolStripMenuItem
        Me.ImportCOF = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuAccountTypeBrowse = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuUploadAccount_type = New System.Windows.Forms.ToolStripMenuItem
        Me.BrowseCOF = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuBrowseAccounts = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuUploadAccounts = New System.Windows.Forms.ToolStripMenuItem
        Me.btnCustMas = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuBrowseCustomer = New System.Windows.Forms.ToolStripMenuItem
        Me.ImportCust = New System.Windows.Forms.ToolStripMenuItem
        Me.CustomerMasterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AddressToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AdditionalInfoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.CustomerTypeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.TermsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SendMethodToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.PaymentMethodToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.TaxCodeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SendMethodToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.btnItemMas = New System.Windows.Forms.ToolStripMenuItem
        Me.BulkUpdateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuSRPupdates = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuBrowseSRPUpdates = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuUploadSRPUpdates = New System.Windows.Forms.ToolStripMenuItem
        Me.ItemMasterUploadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.HavaianasItemsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuBrowseItem = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuUploadItem = New System.Windows.Forms.ToolStripMenuItem
        Me.StoreItemsUploadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.BrowseStoreItems = New System.Windows.Forms.ToolStripMenuItem
        Me.UploadStoreItems = New System.Windows.Forms.ToolStripMenuItem
        Me.btnSupMas = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuBrowseSupplier = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuUploadSupplier = New System.Windows.Forms.ToolStripMenuItem
        Me.openfile = New System.Windows.Forms.OpenFileDialog
        Me.pbUpload = New System.Windows.Forms.ProgressBar
        Me.bgwMainUploader = New System.ComponentModel.BackgroundWorker
        Me.bgwUpdate = New System.ComponentModel.BackgroundWorker
        Me.bgwStoreUploader = New System.ComponentModel.BackgroundWorker
        CType(Me.grdImport, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStripUploads.SuspendLayout()
        Me.SuspendLayout()
        '
        'grdImport
        '
        Me.grdImport.AllowUserToAddRows = False
        Me.grdImport.AllowUserToDeleteRows = False
        Me.grdImport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdImport.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdImport.Location = New System.Drawing.Point(0, 0)
        Me.grdImport.Name = "grdImport"
        Me.grdImport.Size = New System.Drawing.Size(656, 385)
        Me.grdImport.TabIndex = 0
        '
        'MenuStripUploads
        '
        Me.MenuStripUploads.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MenuStripUploads.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnChrtAcnts, Me.btnCustMas, Me.btnItemMas, Me.btnSupMas})
        Me.MenuStripUploads.Location = New System.Drawing.Point(0, 385)
        Me.MenuStripUploads.Name = "MenuStripUploads"
        Me.MenuStripUploads.Size = New System.Drawing.Size(656, 24)
        Me.MenuStripUploads.TabIndex = 1
        Me.MenuStripUploads.Text = "MenuStrip1"
        '
        'btnChrtAcnts
        '
        Me.btnChrtAcnts.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ImportCOF, Me.BrowseCOF})
        Me.btnChrtAcnts.Name = "btnChrtAcnts"
        Me.btnChrtAcnts.Size = New System.Drawing.Size(106, 20)
        Me.btnChrtAcnts.Text = "Chart of Accounts"
        '
        'ImportCOF
        '
        Me.ImportCOF.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuAccountTypeBrowse, Me.MenuUploadAccount_type})
        Me.ImportCOF.Name = "ImportCOF"
        Me.ImportCOF.Size = New System.Drawing.Size(151, 22)
        Me.ImportCOF.Text = "Account &Type"
        '
        'MenuAccountTypeBrowse
        '
        Me.MenuAccountTypeBrowse.Name = "MenuAccountTypeBrowse"
        Me.MenuAccountTypeBrowse.Size = New System.Drawing.Size(120, 22)
        Me.MenuAccountTypeBrowse.Text = "&Browse"
        '
        'MenuUploadAccount_type
        '
        Me.MenuUploadAccount_type.Enabled = False
        Me.MenuUploadAccount_type.Name = "MenuUploadAccount_type"
        Me.MenuUploadAccount_type.Size = New System.Drawing.Size(120, 22)
        Me.MenuUploadAccount_type.Text = "&Upload"
        '
        'BrowseCOF
        '
        Me.BrowseCOF.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuBrowseAccounts, Me.MenuUploadAccounts})
        Me.BrowseCOF.Name = "BrowseCOF"
        Me.BrowseCOF.Size = New System.Drawing.Size(151, 22)
        Me.BrowseCOF.Text = "&Accounts"
        '
        'MenuBrowseAccounts
        '
        Me.MenuBrowseAccounts.Name = "MenuBrowseAccounts"
        Me.MenuBrowseAccounts.Size = New System.Drawing.Size(120, 22)
        Me.MenuBrowseAccounts.Text = "Browse"
        '
        'MenuUploadAccounts
        '
        Me.MenuUploadAccounts.Enabled = False
        Me.MenuUploadAccounts.Name = "MenuUploadAccounts"
        Me.MenuUploadAccounts.Size = New System.Drawing.Size(120, 22)
        Me.MenuUploadAccounts.Text = "Upload"
        '
        'btnCustMas
        '
        Me.btnCustMas.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuBrowseCustomer, Me.ImportCust})
        Me.btnCustMas.Name = "btnCustMas"
        Me.btnCustMas.Size = New System.Drawing.Size(101, 20)
        Me.btnCustMas.Text = "Customer Master"
        '
        'MenuBrowseCustomer
        '
        Me.MenuBrowseCustomer.Name = "MenuBrowseCustomer"
        Me.MenuBrowseCustomer.Size = New System.Drawing.Size(120, 22)
        Me.MenuBrowseCustomer.Text = "&Browse"
        '
        'ImportCust
        '
        Me.ImportCust.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CustomerMasterToolStripMenuItem, Me.AddressToolStripMenuItem, Me.AdditionalInfoToolStripMenuItem, Me.CustomerTypeToolStripMenuItem, Me.TermsToolStripMenuItem, Me.SendMethodToolStripMenuItem, Me.PaymentMethodToolStripMenuItem, Me.TaxCodeToolStripMenuItem, Me.SendMethodToolStripMenuItem1})
        Me.ImportCust.Name = "ImportCust"
        Me.ImportCust.Size = New System.Drawing.Size(120, 22)
        Me.ImportCust.Text = "&Import"
        '
        'CustomerMasterToolStripMenuItem
        '
        Me.CustomerMasterToolStripMenuItem.Name = "CustomerMasterToolStripMenuItem"
        Me.CustomerMasterToolStripMenuItem.Size = New System.Drawing.Size(167, 22)
        Me.CustomerMasterToolStripMenuItem.Text = "Customer Master"
        '
        'AddressToolStripMenuItem
        '
        Me.AddressToolStripMenuItem.Name = "AddressToolStripMenuItem"
        Me.AddressToolStripMenuItem.Size = New System.Drawing.Size(167, 22)
        Me.AddressToolStripMenuItem.Text = "Address Details"
        '
        'AdditionalInfoToolStripMenuItem
        '
        Me.AdditionalInfoToolStripMenuItem.Name = "AdditionalInfoToolStripMenuItem"
        Me.AdditionalInfoToolStripMenuItem.Size = New System.Drawing.Size(167, 22)
        Me.AdditionalInfoToolStripMenuItem.Text = "Additional Info"
        '
        'CustomerTypeToolStripMenuItem
        '
        Me.CustomerTypeToolStripMenuItem.Name = "CustomerTypeToolStripMenuItem"
        Me.CustomerTypeToolStripMenuItem.Size = New System.Drawing.Size(167, 22)
        Me.CustomerTypeToolStripMenuItem.Text = "Customer Type"
        '
        'TermsToolStripMenuItem
        '
        Me.TermsToolStripMenuItem.Name = "TermsToolStripMenuItem"
        Me.TermsToolStripMenuItem.Size = New System.Drawing.Size(167, 22)
        Me.TermsToolStripMenuItem.Text = "Terms"
        '
        'SendMethodToolStripMenuItem
        '
        Me.SendMethodToolStripMenuItem.Name = "SendMethodToolStripMenuItem"
        Me.SendMethodToolStripMenuItem.Size = New System.Drawing.Size(167, 22)
        Me.SendMethodToolStripMenuItem.Text = "Send Method"
        '
        'PaymentMethodToolStripMenuItem
        '
        Me.PaymentMethodToolStripMenuItem.Name = "PaymentMethodToolStripMenuItem"
        Me.PaymentMethodToolStripMenuItem.Size = New System.Drawing.Size(167, 22)
        Me.PaymentMethodToolStripMenuItem.Text = "Payment Method"
        '
        'TaxCodeToolStripMenuItem
        '
        Me.TaxCodeToolStripMenuItem.Name = "TaxCodeToolStripMenuItem"
        Me.TaxCodeToolStripMenuItem.Size = New System.Drawing.Size(167, 22)
        Me.TaxCodeToolStripMenuItem.Text = "Tax Code"
        '
        'SendMethodToolStripMenuItem1
        '
        Me.SendMethodToolStripMenuItem1.Name = "SendMethodToolStripMenuItem1"
        Me.SendMethodToolStripMenuItem1.Size = New System.Drawing.Size(167, 22)
        Me.SendMethodToolStripMenuItem1.Text = "Send Method"
        '
        'btnItemMas
        '
        Me.btnItemMas.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BulkUpdateToolStripMenuItem, Me.ItemMasterUploadToolStripMenuItem})
        Me.btnItemMas.Name = "btnItemMas"
        Me.btnItemMas.Size = New System.Drawing.Size(77, 20)
        Me.btnItemMas.Text = "Item Master"
        '
        'BulkUpdateToolStripMenuItem
        '
        Me.BulkUpdateToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuSRPupdates})
        Me.BulkUpdateToolStripMenuItem.Name = "BulkUpdateToolStripMenuItem"
        Me.BulkUpdateToolStripMenuItem.Size = New System.Drawing.Size(120, 22)
        Me.BulkUpdateToolStripMenuItem.Text = "Up&date"
        '
        'MenuSRPupdates
        '
        Me.MenuSRPupdates.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuBrowseSRPUpdates, Me.MenuUploadSRPUpdates})
        Me.MenuSRPupdates.Name = "MenuSRPupdates"
        Me.MenuSRPupdates.Size = New System.Drawing.Size(142, 22)
        Me.MenuSRPupdates.Text = "&SRP Update"
        '
        'MenuBrowseSRPUpdates
        '
        Me.MenuBrowseSRPUpdates.Name = "MenuBrowseSRPUpdates"
        Me.MenuBrowseSRPUpdates.Size = New System.Drawing.Size(120, 22)
        Me.MenuBrowseSRPUpdates.Text = "&Browse"
        '
        'MenuUploadSRPUpdates
        '
        Me.MenuUploadSRPUpdates.Enabled = False
        Me.MenuUploadSRPUpdates.Name = "MenuUploadSRPUpdates"
        Me.MenuUploadSRPUpdates.Size = New System.Drawing.Size(120, 22)
        Me.MenuUploadSRPUpdates.Text = "&Upload"
        '
        'ItemMasterUploadToolStripMenuItem
        '
        Me.ItemMasterUploadToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HavaianasItemsToolStripMenuItem, Me.StoreItemsUploadToolStripMenuItem})
        Me.ItemMasterUploadToolStripMenuItem.Name = "ItemMasterUploadToolStripMenuItem"
        Me.ItemMasterUploadToolStripMenuItem.Size = New System.Drawing.Size(120, 22)
        Me.ItemMasterUploadToolStripMenuItem.Text = "Up&load"
        '
        'HavaianasItemsToolStripMenuItem
        '
        Me.HavaianasItemsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuBrowseItem, Me.MenuUploadItem})
        Me.HavaianasItemsToolStripMenuItem.Name = "HavaianasItemsToolStripMenuItem"
        Me.HavaianasItemsToolStripMenuItem.Size = New System.Drawing.Size(187, 22)
        Me.HavaianasItemsToolStripMenuItem.Text = "Main Uploader"
        '
        'MenuBrowseItem
        '
        Me.MenuBrowseItem.Name = "MenuBrowseItem"
        Me.MenuBrowseItem.Size = New System.Drawing.Size(120, 22)
        Me.MenuBrowseItem.Text = "Browse"
        '
        'MenuUploadItem
        '
        Me.MenuUploadItem.Enabled = False
        Me.MenuUploadItem.Name = "MenuUploadItem"
        Me.MenuUploadItem.Size = New System.Drawing.Size(120, 22)
        Me.MenuUploadItem.Text = "Upload"
        '
        'StoreItemsUploadToolStripMenuItem
        '
        Me.StoreItemsUploadToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BrowseStoreItems, Me.UploadStoreItems})
        Me.StoreItemsUploadToolStripMenuItem.Name = "StoreItemsUploadToolStripMenuItem"
        Me.StoreItemsUploadToolStripMenuItem.Size = New System.Drawing.Size(187, 22)
        Me.StoreItemsUploadToolStripMenuItem.Text = "Store Items Uploader"
        '
        'BrowseStoreItems
        '
        Me.BrowseStoreItems.Name = "BrowseStoreItems"
        Me.BrowseStoreItems.Size = New System.Drawing.Size(120, 22)
        Me.BrowseStoreItems.Text = "Browse"
        '
        'UploadStoreItems
        '
        Me.UploadStoreItems.Enabled = False
        Me.UploadStoreItems.Name = "UploadStoreItems"
        Me.UploadStoreItems.Size = New System.Drawing.Size(120, 22)
        Me.UploadStoreItems.Text = "Upload"
        '
        'btnSupMas
        '
        Me.btnSupMas.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuBrowseSupplier, Me.MenuUploadSupplier})
        Me.btnSupMas.Name = "btnSupMas"
        Me.btnSupMas.Size = New System.Drawing.Size(93, 20)
        Me.btnSupMas.Text = "Supplier Master"
        '
        'MenuBrowseSupplier
        '
        Me.MenuBrowseSupplier.Name = "MenuBrowseSupplier"
        Me.MenuBrowseSupplier.ShowShortcutKeys = False
        Me.MenuBrowseSupplier.Size = New System.Drawing.Size(118, 22)
        Me.MenuBrowseSupplier.Text = "&Browse"
        '
        'MenuUploadSupplier
        '
        Me.MenuUploadSupplier.Enabled = False
        Me.MenuUploadSupplier.Name = "MenuUploadSupplier"
        Me.MenuUploadSupplier.Size = New System.Drawing.Size(118, 22)
        Me.MenuUploadSupplier.Text = "&Upload"
        '
        'openfile
        '
        Me.openfile.DefaultExt = "csv"
        Me.openfile.Title = "Open File"
        '
        'pbUpload
        '
        Me.pbUpload.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pbUpload.Location = New System.Drawing.Point(516, 391)
        Me.pbUpload.MarqueeAnimationSpeed = 20
        Me.pbUpload.Name = "pbUpload"
        Me.pbUpload.Size = New System.Drawing.Size(135, 15)
        Me.pbUpload.Style = System.Windows.Forms.ProgressBarStyle.Marquee
        Me.pbUpload.TabIndex = 2
        Me.pbUpload.Visible = False
        '
        'bgwMainUploader
        '
        '
        'bgwUpdate
        '
        '
        'bgwStoreUploader
        '
        '
        'frmImportData
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(656, 409)
        Me.Controls.Add(Me.pbUpload)
        Me.Controls.Add(Me.grdImport)
        Me.Controls.Add(Me.MenuStripUploads)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStripUploads
        Me.MinimizeBox = False
        Me.Name = "frmImportData"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Import Master"
        CType(Me.grdImport, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStripUploads.ResumeLayout(False)
        Me.MenuStripUploads.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents grdImport As System.Windows.Forms.DataGridView
    Friend WithEvents MenuStripUploads As System.Windows.Forms.MenuStrip
    Friend WithEvents btnChrtAcnts As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnSupMas As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnCustMas As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents openfile As System.Windows.Forms.OpenFileDialog
    Friend WithEvents BrowseCOF As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuBrowseCustomer As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuBrowseSupplier As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImportCOF As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImportCust As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuUploadSupplier As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CustomerMasterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddressToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AdditionalInfoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CustomerTypeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TermsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SendMethodToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PaymentMethodToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TaxCodeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SendMethodToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents pbUpload As System.Windows.Forms.ProgressBar
    Friend WithEvents bgwMainUploader As System.ComponentModel.BackgroundWorker
    Friend WithEvents MenuBrowseAccounts As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuUploadAccounts As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuAccountTypeBrowse As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuUploadAccount_type As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bgwUpdate As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgwStoreUploader As System.ComponentModel.BackgroundWorker
    Friend WithEvents btnItemMas As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BulkUpdateToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuSRPupdates As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuBrowseSRPUpdates As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuUploadSRPUpdates As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ItemMasterUploadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HavaianasItemsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuBrowseItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuUploadItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StoreItemsUploadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BrowseStoreItems As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UploadStoreItems As System.Windows.Forms.ToolStripMenuItem
End Class
