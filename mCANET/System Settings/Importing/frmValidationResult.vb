Imports System.IO

Public Class frmValidationResult
    Public xParent As frmItemUploadProcess
    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub

    Private Sub frmValidationResult_ResizeBegin(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeBegin
        Me.Opacity = 0.5
    End Sub

    Private Sub frmValidationResult_ResizeEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeEnd
        Me.Opacity = 1
    End Sub

    Private Sub BtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Dim filePath As String = "C:\" & Format(Now.Date, "MM-dd-yyyy") & " Validation result.txt"
        GenerateListToTextFile(filePath)
        OpenTextFile(filePath)
    End Sub

    Private Sub GenerateListToTextFile(ByVal filePath As String)
        Dim TextFile As New StreamWriter(filePath)
        TextFile.Flush()

        For Each accountType As ListViewItem In LstAccount.Items
            TextFile.WriteLine(accountType.Text)
        Next

        TextFile.Close()
    End Sub

    Private Sub OpenTextFile(ByVal filePath As String)
        If (System.IO.File.Exists(filePath)) Then
            System.Diagnostics.Process.Start(filePath)
        End If
    End Sub

    Private Sub SaveFileDialog1_FileOk(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles SaveFileDialog1.FileOk
      
    End Sub

   
End Class