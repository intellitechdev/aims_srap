<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAccesibility
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.chChngePwrd = New System.Windows.Forms.CheckBox()
        Me.chUserOptn = New System.Windows.Forms.CheckBox()
        Me.chImport = New System.Windows.Forms.CheckBox()
        Me.chPeriodRestrict = New System.Windows.Forms.CheckBox()
        Me.chPref = New System.Windows.Forms.CheckBox()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.chBills = New System.Windows.Forms.CheckBox()
        Me.chSupMaster = New System.Windows.Forms.CheckBox()
        Me.chEnterBills4RcveItem = New System.Windows.Forms.CheckBox()
        Me.chPayBills = New System.Windows.Forms.CheckBox()
        Me.chRecievedItem = New System.Windows.Forms.CheckBox()
        Me.chBills4Apprvl = New System.Windows.Forms.CheckBox()
        Me.chPO = New System.Windows.Forms.CheckBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.chBankRecon = New System.Windows.Forms.CheckBox()
        Me.chWriteChecks = New System.Windows.Forms.CheckBox()
        Me.chMakeDeposit = New System.Windows.Forms.CheckBox()
        Me.chTransFunds = New System.Windows.Forms.CheckBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.chSO = New System.Windows.Forms.CheckBox()
        Me.chCustMaster = New System.Windows.Forms.CheckBox()
        Me.chDebit = New System.Windows.Forms.CheckBox()
        Me.chSR = New System.Windows.Forms.CheckBox()
        Me.chCredit = New System.Windows.Forms.CheckBox()
        Me.chInv = New System.Windows.Forms.CheckBox()
        Me.chRecievePaymnt = New System.Windows.Forms.CheckBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.chWorkngTrlBal = New System.Windows.Forms.CheckBox()
        Me.chAccntntRecon = New System.Windows.Forms.CheckBox()
        Me.chGenJournEnt = New System.Windows.Forms.CheckBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.chCustType = New System.Windows.Forms.CheckBox()
        Me.chSalesRep = New System.Windows.Forms.CheckBox()
        Me.chSalesTax = New System.Windows.Forms.CheckBox()
        Me.ChClass = New System.Windows.Forms.CheckBox()
        Me.chTerm = New System.Windows.Forms.CheckBox()
        Me.chItemMaster = New System.Windows.Forms.CheckBox()
        Me.chSupType = New System.Windows.Forms.CheckBox()
        Me.chChartofAccounts = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chItemMasterInventry = New System.Windows.Forms.CheckBox()
        Me.chGLListing = New System.Windows.Forms.CheckBox()
        Me.chBllsPymntAcctRpt = New System.Windows.Forms.CheckBox()
        Me.chCustList = New System.Windows.Forms.CheckBox()
        Me.chFinanceCharge = New System.Windows.Forms.CheckBox()
        Me.ChCustTrnsctnRpt = New System.Windows.Forms.CheckBox()
        Me.chUndepostFunds = New System.Windows.Forms.CheckBox()
        Me.chAPSummary = New System.Windows.Forms.CheckBox()
        Me.chReimburseExpnse = New System.Windows.Forms.CheckBox()
        Me.chJVSummryperAccnt = New System.Windows.Forms.CheckBox()
        Me.chIncmeStatement = New System.Windows.Forms.CheckBox()
        Me.chBudgetMonitor = New System.Windows.Forms.CheckBox()
        Me.chTaxRpt = New System.Windows.Forms.CheckBox()
        Me.chCollectionRpt = New System.Windows.Forms.CheckBox()
        Me.chSupMasterRpt = New System.Windows.Forms.CheckBox()
        Me.chAging = New System.Windows.Forms.CheckBox()
        Me.chGLByAccnt = New System.Windows.Forms.CheckBox()
        Me.chSalesRpt = New System.Windows.Forms.CheckBox()
        Me.chCVHistory = New System.Windows.Forms.CheckBox()
        Me.chJVSummary = New System.Windows.Forms.CheckBox()
        Me.chAPCVList = New System.Windows.Forms.CheckBox()
        Me.chBalShit = New System.Windows.Forms.CheckBox()
        Me.chStatementofAccnt = New System.Windows.Forms.CheckBox()
        Me.BtnOK = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.GroupBox8)
        Me.GroupBox2.Controls.Add(Me.GroupBox7)
        Me.GroupBox2.Controls.Add(Me.GroupBox6)
        Me.GroupBox2.Controls.Add(Me.GroupBox5)
        Me.GroupBox2.Controls.Add(Me.GroupBox4)
        Me.GroupBox2.Controls.Add(Me.GroupBox3)
        Me.GroupBox2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(2, 27)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(347, 437)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Module Accesibility"
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.chChngePwrd)
        Me.GroupBox8.Controls.Add(Me.chUserOptn)
        Me.GroupBox8.Controls.Add(Me.chImport)
        Me.GroupBox8.Controls.Add(Me.chPeriodRestrict)
        Me.GroupBox8.Controls.Add(Me.chPref)
        Me.GroupBox8.Location = New System.Drawing.Point(6, 292)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(335, 82)
        Me.GroupBox8.TabIndex = 5
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "System Settings"
        '
        'chChngePwrd
        '
        Me.chChngePwrd.AutoSize = True
        Me.chChngePwrd.Checked = True
        Me.chChngePwrd.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chChngePwrd.Location = New System.Drawing.Point(6, 33)
        Me.chChngePwrd.Name = "chChngePwrd"
        Me.chChngePwrd.Size = New System.Drawing.Size(141, 20)
        Me.chChngePwrd.TabIndex = 1
        Me.chChngePwrd.Text = "Change Password"
        Me.chChngePwrd.UseVisualStyleBackColor = True
        '
        'chUserOptn
        '
        Me.chUserOptn.AutoSize = True
        Me.chUserOptn.BackColor = System.Drawing.Color.Transparent
        Me.chUserOptn.Checked = True
        Me.chUserOptn.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chUserOptn.Location = New System.Drawing.Point(6, 15)
        Me.chUserOptn.Name = "chUserOptn"
        Me.chUserOptn.Size = New System.Drawing.Size(107, 20)
        Me.chUserOptn.TabIndex = 0
        Me.chUserOptn.Text = "User Options"
        Me.chUserOptn.UseVisualStyleBackColor = False
        '
        'chImport
        '
        Me.chImport.AutoSize = True
        Me.chImport.Checked = True
        Me.chImport.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chImport.Location = New System.Drawing.Point(195, 33)
        Me.chImport.Name = "chImport"
        Me.chImport.Size = New System.Drawing.Size(94, 20)
        Me.chImport.TabIndex = 4
        Me.chImport.Text = "Importings"
        Me.chImport.UseVisualStyleBackColor = True
        '
        'chPeriodRestrict
        '
        Me.chPeriodRestrict.AutoSize = True
        Me.chPeriodRestrict.BackColor = System.Drawing.Color.Transparent
        Me.chPeriodRestrict.Checked = True
        Me.chPeriodRestrict.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chPeriodRestrict.Location = New System.Drawing.Point(195, 15)
        Me.chPeriodRestrict.Name = "chPeriodRestrict"
        Me.chPeriodRestrict.Size = New System.Drawing.Size(140, 20)
        Me.chPeriodRestrict.TabIndex = 3
        Me.chPeriodRestrict.Text = "Period Restriction"
        Me.chPeriodRestrict.UseVisualStyleBackColor = False
        '
        'chPref
        '
        Me.chPref.AutoSize = True
        Me.chPref.Checked = True
        Me.chPref.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chPref.Location = New System.Drawing.Point(6, 52)
        Me.chPref.Name = "chPref"
        Me.chPref.Size = New System.Drawing.Size(103, 20)
        Me.chPref.TabIndex = 2
        Me.chPref.Text = "Preferences"
        Me.chPref.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.chBills)
        Me.GroupBox7.Controls.Add(Me.chSupMaster)
        Me.GroupBox7.Controls.Add(Me.chEnterBills4RcveItem)
        Me.GroupBox7.Controls.Add(Me.chPayBills)
        Me.GroupBox7.Controls.Add(Me.chRecievedItem)
        Me.GroupBox7.Controls.Add(Me.chBills4Apprvl)
        Me.GroupBox7.Controls.Add(Me.chPO)
        Me.GroupBox7.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox7.Location = New System.Drawing.Point(195, 17)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(146, 175)
        Me.GroupBox7.TabIndex = 4
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Supplier"
        '
        'chBills
        '
        Me.chBills.AutoSize = True
        Me.chBills.Checked = True
        Me.chBills.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chBills.Location = New System.Drawing.Point(6, 39)
        Me.chBills.Name = "chBills"
        Me.chBills.Size = New System.Drawing.Size(54, 20)
        Me.chBills.TabIndex = 1
        Me.chBills.Text = "Bills"
        Me.chBills.UseVisualStyleBackColor = True
        '
        'chSupMaster
        '
        Me.chSupMaster.AutoSize = True
        Me.chSupMaster.Checked = True
        Me.chSupMaster.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chSupMaster.Location = New System.Drawing.Point(6, 21)
        Me.chSupMaster.Name = "chSupMaster"
        Me.chSupMaster.Size = New System.Drawing.Size(127, 20)
        Me.chSupMaster.TabIndex = 0
        Me.chSupMaster.Text = "Supplier Master"
        Me.chSupMaster.UseVisualStyleBackColor = True
        '
        'chEnterBills4RcveItem
        '
        Me.chEnterBills4RcveItem.BackColor = System.Drawing.Color.Transparent
        Me.chEnterBills4RcveItem.Checked = True
        Me.chEnterBills4RcveItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chEnterBills4RcveItem.Location = New System.Drawing.Point(6, 130)
        Me.chEnterBills4RcveItem.Name = "chEnterBills4RcveItem"
        Me.chEnterBills4RcveItem.Size = New System.Drawing.Size(118, 40)
        Me.chEnterBills4RcveItem.TabIndex = 6
        Me.chEnterBills4RcveItem.Text = "Enter Bills for Recieve Item"
        Me.chEnterBills4RcveItem.UseVisualStyleBackColor = False
        '
        'chPayBills
        '
        Me.chPayBills.AutoSize = True
        Me.chPayBills.Checked = True
        Me.chPayBills.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chPayBills.Location = New System.Drawing.Point(6, 75)
        Me.chPayBills.Name = "chPayBills"
        Me.chPayBills.Size = New System.Drawing.Size(82, 20)
        Me.chPayBills.TabIndex = 3
        Me.chPayBills.Text = "Pay Bills"
        Me.chPayBills.UseVisualStyleBackColor = True
        '
        'chRecievedItem
        '
        Me.chRecievedItem.AutoSize = True
        Me.chRecievedItem.Checked = True
        Me.chRecievedItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chRecievedItem.Location = New System.Drawing.Point(6, 112)
        Me.chRecievedItem.Name = "chRecievedItem"
        Me.chRecievedItem.Size = New System.Drawing.Size(118, 20)
        Me.chRecievedItem.TabIndex = 5
        Me.chRecievedItem.Text = "Recieved Item"
        Me.chRecievedItem.UseVisualStyleBackColor = True
        '
        'chBills4Apprvl
        '
        Me.chBills4Apprvl.AutoSize = True
        Me.chBills4Apprvl.BackColor = System.Drawing.Color.Transparent
        Me.chBills4Apprvl.Checked = True
        Me.chBills4Apprvl.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chBills4Apprvl.Location = New System.Drawing.Point(6, 57)
        Me.chBills4Apprvl.Name = "chBills4Apprvl"
        Me.chBills4Apprvl.Size = New System.Drawing.Size(136, 20)
        Me.chBills4Apprvl.TabIndex = 2
        Me.chBills4Apprvl.Text = "Bills for Approval"
        Me.chBills4Apprvl.UseVisualStyleBackColor = False
        '
        'chPO
        '
        Me.chPO.AutoSize = True
        Me.chPO.BackColor = System.Drawing.Color.Transparent
        Me.chPO.Checked = True
        Me.chPO.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chPO.Location = New System.Drawing.Point(6, 94)
        Me.chPO.Name = "chPO"
        Me.chPO.Size = New System.Drawing.Size(132, 20)
        Me.chPO.TabIndex = 4
        Me.chPO.Text = "Purchase Orders"
        Me.chPO.UseVisualStyleBackColor = False
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.chBankRecon)
        Me.GroupBox6.Controls.Add(Me.chWriteChecks)
        Me.GroupBox6.Controls.Add(Me.chMakeDeposit)
        Me.GroupBox6.Controls.Add(Me.chTransFunds)
        Me.GroupBox6.Location = New System.Drawing.Point(195, 197)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(133, 89)
        Me.GroupBox6.TabIndex = 2
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Banking"
        '
        'chBankRecon
        '
        Me.chBankRecon.AutoSize = True
        Me.chBankRecon.Checked = True
        Me.chBankRecon.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chBankRecon.Location = New System.Drawing.Point(6, 31)
        Me.chBankRecon.Name = "chBankRecon"
        Me.chBankRecon.Size = New System.Drawing.Size(90, 20)
        Me.chBankRecon.TabIndex = 1
        Me.chBankRecon.Text = "Reconcile"
        Me.chBankRecon.UseVisualStyleBackColor = True
        '
        'chWriteChecks
        '
        Me.chWriteChecks.AutoSize = True
        Me.chWriteChecks.BackColor = System.Drawing.Color.Transparent
        Me.chWriteChecks.Checked = True
        Me.chWriteChecks.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chWriteChecks.Location = New System.Drawing.Point(6, 13)
        Me.chWriteChecks.Name = "chWriteChecks"
        Me.chWriteChecks.Size = New System.Drawing.Size(110, 20)
        Me.chWriteChecks.TabIndex = 0
        Me.chWriteChecks.Text = "Write Checks"
        Me.chWriteChecks.UseVisualStyleBackColor = False
        '
        'chMakeDeposit
        '
        Me.chMakeDeposit.AutoSize = True
        Me.chMakeDeposit.Checked = True
        Me.chMakeDeposit.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chMakeDeposit.Location = New System.Drawing.Point(6, 67)
        Me.chMakeDeposit.Name = "chMakeDeposit"
        Me.chMakeDeposit.Size = New System.Drawing.Size(112, 20)
        Me.chMakeDeposit.TabIndex = 3
        Me.chMakeDeposit.Text = "Make Deposit"
        Me.chMakeDeposit.UseVisualStyleBackColor = True
        '
        'chTransFunds
        '
        Me.chTransFunds.AutoSize = True
        Me.chTransFunds.Checked = True
        Me.chTransFunds.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chTransFunds.Location = New System.Drawing.Point(6, 49)
        Me.chTransFunds.Name = "chTransFunds"
        Me.chTransFunds.Size = New System.Drawing.Size(120, 20)
        Me.chTransFunds.TabIndex = 2
        Me.chTransFunds.Text = "Transfer Funds"
        Me.chTransFunds.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.chSO)
        Me.GroupBox5.Controls.Add(Me.chCustMaster)
        Me.GroupBox5.Controls.Add(Me.chDebit)
        Me.GroupBox5.Controls.Add(Me.chSR)
        Me.GroupBox5.Controls.Add(Me.chCredit)
        Me.GroupBox5.Controls.Add(Me.chInv)
        Me.GroupBox5.Controls.Add(Me.chRecievePaymnt)
        Me.GroupBox5.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.Location = New System.Drawing.Point(7, 380)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(146, 150)
        Me.GroupBox5.TabIndex = 3
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Customer"
        Me.GroupBox5.Visible = False
        '
        'chSO
        '
        Me.chSO.AutoSize = True
        Me.chSO.Checked = True
        Me.chSO.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chSO.Location = New System.Drawing.Point(6, 39)
        Me.chSO.Name = "chSO"
        Me.chSO.Size = New System.Drawing.Size(102, 20)
        Me.chSO.TabIndex = 1
        Me.chSO.Text = "Sales Order"
        Me.chSO.UseVisualStyleBackColor = True
        '
        'chCustMaster
        '
        Me.chCustMaster.AutoSize = True
        Me.chCustMaster.Checked = True
        Me.chCustMaster.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chCustMaster.Location = New System.Drawing.Point(6, 21)
        Me.chCustMaster.Name = "chCustMaster"
        Me.chCustMaster.Size = New System.Drawing.Size(133, 20)
        Me.chCustMaster.TabIndex = 0
        Me.chCustMaster.Text = "Customer Master"
        Me.chCustMaster.UseVisualStyleBackColor = True
        '
        'chDebit
        '
        Me.chDebit.AutoSize = True
        Me.chDebit.BackColor = System.Drawing.Color.Transparent
        Me.chDebit.Checked = True
        Me.chDebit.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chDebit.Location = New System.Drawing.Point(6, 130)
        Me.chDebit.Name = "chDebit"
        Me.chDebit.Size = New System.Drawing.Size(103, 20)
        Me.chDebit.TabIndex = 6
        Me.chDebit.Text = "Debit Memo"
        Me.chDebit.UseVisualStyleBackColor = False
        '
        'chSR
        '
        Me.chSR.AutoSize = True
        Me.chSR.Checked = True
        Me.chSR.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chSR.Location = New System.Drawing.Point(6, 75)
        Me.chSR.Name = "chSR"
        Me.chSR.Size = New System.Drawing.Size(114, 20)
        Me.chSR.TabIndex = 3
        Me.chSR.Text = "Sales Reciept"
        Me.chSR.UseVisualStyleBackColor = True
        '
        'chCredit
        '
        Me.chCredit.AutoSize = True
        Me.chCredit.Checked = True
        Me.chCredit.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chCredit.Location = New System.Drawing.Point(6, 112)
        Me.chCredit.Name = "chCredit"
        Me.chCredit.Size = New System.Drawing.Size(108, 20)
        Me.chCredit.TabIndex = 5
        Me.chCredit.Text = "Credit Memo"
        Me.chCredit.UseVisualStyleBackColor = True
        '
        'chInv
        '
        Me.chInv.AutoSize = True
        Me.chInv.Checked = True
        Me.chInv.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chInv.Location = New System.Drawing.Point(6, 57)
        Me.chInv.Name = "chInv"
        Me.chInv.Size = New System.Drawing.Size(73, 20)
        Me.chInv.TabIndex = 2
        Me.chInv.Text = "Invoice"
        Me.chInv.UseVisualStyleBackColor = True
        '
        'chRecievePaymnt
        '
        Me.chRecievePaymnt.AutoSize = True
        Me.chRecievePaymnt.BackColor = System.Drawing.Color.Transparent
        Me.chRecievePaymnt.Checked = True
        Me.chRecievePaymnt.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chRecievePaymnt.Location = New System.Drawing.Point(6, 94)
        Me.chRecievePaymnt.Name = "chRecievePaymnt"
        Me.chRecievePaymnt.Size = New System.Drawing.Size(138, 20)
        Me.chRecievePaymnt.TabIndex = 4
        Me.chRecievePaymnt.Text = "Recieve Payment"
        Me.chRecievePaymnt.UseVisualStyleBackColor = False
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.chWorkngTrlBal)
        Me.GroupBox4.Controls.Add(Me.chAccntntRecon)
        Me.GroupBox4.Controls.Add(Me.chGenJournEnt)
        Me.GroupBox4.Location = New System.Drawing.Point(6, 197)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(183, 89)
        Me.GroupBox4.TabIndex = 1
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Accountant"
        '
        'chWorkngTrlBal
        '
        Me.chWorkngTrlBal.AutoSize = True
        Me.chWorkngTrlBal.BackColor = System.Drawing.Color.Transparent
        Me.chWorkngTrlBal.Checked = True
        Me.chWorkngTrlBal.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chWorkngTrlBal.Location = New System.Drawing.Point(6, 59)
        Me.chWorkngTrlBal.Name = "chWorkngTrlBal"
        Me.chWorkngTrlBal.Size = New System.Drawing.Size(168, 20)
        Me.chWorkngTrlBal.TabIndex = 2
        Me.chWorkngTrlBal.Text = "Working Trial Balance"
        Me.chWorkngTrlBal.UseVisualStyleBackColor = False
        '
        'chAccntntRecon
        '
        Me.chAccntntRecon.AutoSize = True
        Me.chAccntntRecon.Checked = True
        Me.chAccntntRecon.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chAccntntRecon.Location = New System.Drawing.Point(6, 40)
        Me.chAccntntRecon.Name = "chAccntntRecon"
        Me.chAccntntRecon.Size = New System.Drawing.Size(90, 20)
        Me.chAccntntRecon.TabIndex = 1
        Me.chAccntntRecon.Text = "Reconcile"
        Me.chAccntntRecon.UseVisualStyleBackColor = True
        '
        'chGenJournEnt
        '
        Me.chGenJournEnt.AutoSize = True
        Me.chGenJournEnt.BackColor = System.Drawing.Color.Transparent
        Me.chGenJournEnt.Checked = True
        Me.chGenJournEnt.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chGenJournEnt.Location = New System.Drawing.Point(6, 21)
        Me.chGenJournEnt.Name = "chGenJournEnt"
        Me.chGenJournEnt.Size = New System.Drawing.Size(177, 20)
        Me.chGenJournEnt.TabIndex = 0
        Me.chGenJournEnt.Text = "General Journal Entries"
        Me.chGenJournEnt.UseVisualStyleBackColor = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.chCustType)
        Me.GroupBox3.Controls.Add(Me.chSalesRep)
        Me.GroupBox3.Controls.Add(Me.chSalesTax)
        Me.GroupBox3.Controls.Add(Me.ChClass)
        Me.GroupBox3.Controls.Add(Me.chTerm)
        Me.GroupBox3.Controls.Add(Me.chItemMaster)
        Me.GroupBox3.Controls.Add(Me.chSupType)
        Me.GroupBox3.Controls.Add(Me.chChartofAccounts)
        Me.GroupBox3.Location = New System.Drawing.Point(6, 17)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(183, 175)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Master Files"
        '
        'chCustType
        '
        Me.chCustType.AutoSize = True
        Me.chCustType.Checked = True
        Me.chCustType.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chCustType.Location = New System.Drawing.Point(9, 96)
        Me.chCustType.Name = "chCustType"
        Me.chCustType.Size = New System.Drawing.Size(121, 20)
        Me.chCustType.TabIndex = 4
        Me.chCustType.Text = "Customer Type"
        Me.chCustType.UseVisualStyleBackColor = True
        '
        'chSalesRep
        '
        Me.chSalesRep.AutoSize = True
        Me.chSalesRep.Checked = True
        Me.chSalesRep.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chSalesRep.Location = New System.Drawing.Point(9, 77)
        Me.chSalesRep.Name = "chSalesRep"
        Me.chSalesRep.Size = New System.Drawing.Size(91, 20)
        Me.chSalesRep.TabIndex = 3
        Me.chSalesRep.Text = "Sales Rep"
        Me.chSalesRep.UseVisualStyleBackColor = True
        '
        'chSalesTax
        '
        Me.chSalesTax.AutoSize = True
        Me.chSalesTax.Checked = True
        Me.chSalesTax.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chSalesTax.Location = New System.Drawing.Point(9, 59)
        Me.chSalesTax.Name = "chSalesTax"
        Me.chSalesTax.Size = New System.Drawing.Size(89, 20)
        Me.chSalesTax.TabIndex = 2
        Me.chSalesTax.Text = "Sales Tax"
        Me.chSalesTax.UseVisualStyleBackColor = True
        '
        'ChClass
        '
        Me.ChClass.AutoSize = True
        Me.ChClass.BackColor = System.Drawing.Color.Transparent
        Me.ChClass.Checked = True
        Me.ChClass.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChClass.Location = New System.Drawing.Point(9, 150)
        Me.ChClass.Name = "ChClass"
        Me.ChClass.Size = New System.Drawing.Size(80, 20)
        Me.ChClass.TabIndex = 7
        Me.ChClass.Text = "Classing"
        Me.ChClass.UseVisualStyleBackColor = False
        '
        'chTerm
        '
        Me.chTerm.AutoSize = True
        Me.chTerm.BackColor = System.Drawing.Color.Transparent
        Me.chTerm.Checked = True
        Me.chTerm.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chTerm.Location = New System.Drawing.Point(9, 132)
        Me.chTerm.Name = "chTerm"
        Me.chTerm.Size = New System.Drawing.Size(59, 20)
        Me.chTerm.TabIndex = 6
        Me.chTerm.Text = "Term"
        Me.chTerm.UseVisualStyleBackColor = False
        '
        'chItemMaster
        '
        Me.chItemMaster.AutoSize = True
        Me.chItemMaster.Checked = True
        Me.chItemMaster.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chItemMaster.Location = New System.Drawing.Point(9, 41)
        Me.chItemMaster.Name = "chItemMaster"
        Me.chItemMaster.Size = New System.Drawing.Size(101, 20)
        Me.chItemMaster.TabIndex = 1
        Me.chItemMaster.Text = "Item Master"
        Me.chItemMaster.UseVisualStyleBackColor = True
        '
        'chSupType
        '
        Me.chSupType.AutoSize = True
        Me.chSupType.Checked = True
        Me.chSupType.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chSupType.Location = New System.Drawing.Point(9, 114)
        Me.chSupType.Name = "chSupType"
        Me.chSupType.Size = New System.Drawing.Size(115, 20)
        Me.chSupType.TabIndex = 5
        Me.chSupType.Text = "Supplier Type"
        Me.chSupType.UseVisualStyleBackColor = True
        '
        'chChartofAccounts
        '
        Me.chChartofAccounts.AutoSize = True
        Me.chChartofAccounts.Checked = True
        Me.chChartofAccounts.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chChartofAccounts.Location = New System.Drawing.Point(9, 23)
        Me.chChartofAccounts.Name = "chChartofAccounts"
        Me.chChartofAccounts.Size = New System.Drawing.Size(138, 20)
        Me.chChartofAccounts.TabIndex = 0
        Me.chChartofAccounts.Text = "Chart of Accounts"
        Me.chChartofAccounts.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chItemMasterInventry)
        Me.GroupBox1.Controls.Add(Me.chGLListing)
        Me.GroupBox1.Controls.Add(Me.chBllsPymntAcctRpt)
        Me.GroupBox1.Controls.Add(Me.chCustList)
        Me.GroupBox1.Controls.Add(Me.chFinanceCharge)
        Me.GroupBox1.Controls.Add(Me.ChCustTrnsctnRpt)
        Me.GroupBox1.Controls.Add(Me.chUndepostFunds)
        Me.GroupBox1.Controls.Add(Me.chAPSummary)
        Me.GroupBox1.Controls.Add(Me.chReimburseExpnse)
        Me.GroupBox1.Controls.Add(Me.chJVSummryperAccnt)
        Me.GroupBox1.Controls.Add(Me.chIncmeStatement)
        Me.GroupBox1.Controls.Add(Me.chBudgetMonitor)
        Me.GroupBox1.Controls.Add(Me.chTaxRpt)
        Me.GroupBox1.Controls.Add(Me.chCollectionRpt)
        Me.GroupBox1.Controls.Add(Me.chSupMasterRpt)
        Me.GroupBox1.Controls.Add(Me.chAging)
        Me.GroupBox1.Controls.Add(Me.chGLByAccnt)
        Me.GroupBox1.Controls.Add(Me.chSalesRpt)
        Me.GroupBox1.Controls.Add(Me.chCVHistory)
        Me.GroupBox1.Controls.Add(Me.chJVSummary)
        Me.GroupBox1.Controls.Add(Me.chAPCVList)
        Me.GroupBox1.Controls.Add(Me.chBalShit)
        Me.GroupBox1.Controls.Add(Me.chStatementofAccnt)
        Me.GroupBox1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(355, 27)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(284, 437)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Reports"
        '
        'chItemMasterInventry
        '
        Me.chItemMasterInventry.AutoSize = True
        Me.chItemMasterInventry.Checked = True
        Me.chItemMasterInventry.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chItemMasterInventry.Location = New System.Drawing.Point(6, 271)
        Me.chItemMasterInventry.Name = "chItemMasterInventry"
        Me.chItemMasterInventry.Size = New System.Drawing.Size(164, 20)
        Me.chItemMasterInventry.TabIndex = 14
        Me.chItemMasterInventry.Text = "Item Master/Inventory"
        Me.chItemMasterInventry.UseVisualStyleBackColor = True
        '
        'chGLListing
        '
        Me.chGLListing.AutoSize = True
        Me.chGLListing.Checked = True
        Me.chGLListing.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chGLListing.Location = New System.Drawing.Point(6, 162)
        Me.chGLListing.Name = "chGLListing"
        Me.chGLListing.Size = New System.Drawing.Size(172, 20)
        Me.chGLListing.TabIndex = 8
        Me.chGLListing.Text = "G/L Transaction Listing"
        Me.chGLListing.UseVisualStyleBackColor = True
        '
        'chBllsPymntAcctRpt
        '
        Me.chBllsPymntAcctRpt.AutoSize = True
        Me.chBllsPymntAcctRpt.Checked = True
        Me.chBllsPymntAcctRpt.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chBllsPymntAcctRpt.Location = New System.Drawing.Point(6, 35)
        Me.chBllsPymntAcctRpt.Name = "chBllsPymntAcctRpt"
        Me.chBllsPymntAcctRpt.Size = New System.Drawing.Size(221, 20)
        Me.chBllsPymntAcctRpt.TabIndex = 1
        Me.chBllsPymntAcctRpt.Text = "Bills Payment Accounts Report"
        Me.chBllsPymntAcctRpt.UseVisualStyleBackColor = True
        '
        'chCustList
        '
        Me.chCustList.AutoSize = True
        Me.chCustList.Checked = True
        Me.chCustList.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chCustList.Location = New System.Drawing.Point(6, 253)
        Me.chCustList.Name = "chCustList"
        Me.chCustList.Size = New System.Drawing.Size(113, 20)
        Me.chCustList.TabIndex = 13
        Me.chCustList.Text = "Customer List"
        Me.chCustList.UseVisualStyleBackColor = True
        '
        'chFinanceCharge
        '
        Me.chFinanceCharge.AutoSize = True
        Me.chFinanceCharge.Checked = True
        Me.chFinanceCharge.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chFinanceCharge.Location = New System.Drawing.Point(6, 144)
        Me.chFinanceCharge.Name = "chFinanceCharge"
        Me.chFinanceCharge.Size = New System.Drawing.Size(128, 20)
        Me.chFinanceCharge.TabIndex = 7
        Me.chFinanceCharge.Text = "Finance Charge"
        Me.chFinanceCharge.UseVisualStyleBackColor = True
        '
        'ChCustTrnsctnRpt
        '
        Me.ChCustTrnsctnRpt.AutoSize = True
        Me.ChCustTrnsctnRpt.BackColor = System.Drawing.Color.Transparent
        Me.ChCustTrnsctnRpt.Checked = True
        Me.ChCustTrnsctnRpt.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChCustTrnsctnRpt.Location = New System.Drawing.Point(6, 414)
        Me.ChCustTrnsctnRpt.Name = "ChCustTrnsctnRpt"
        Me.ChCustTrnsctnRpt.Size = New System.Drawing.Size(220, 20)
        Me.ChCustTrnsctnRpt.TabIndex = 22
        Me.ChCustTrnsctnRpt.Text = "Customer Transaction  Reports"
        Me.ChCustTrnsctnRpt.UseVisualStyleBackColor = False
        '
        'chUndepostFunds
        '
        Me.chUndepostFunds.AutoSize = True
        Me.chUndepostFunds.BackColor = System.Drawing.Color.Transparent
        Me.chUndepostFunds.Checked = True
        Me.chUndepostFunds.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chUndepostFunds.Location = New System.Drawing.Point(6, 378)
        Me.chUndepostFunds.Name = "chUndepostFunds"
        Me.chUndepostFunds.Size = New System.Drawing.Size(148, 20)
        Me.chUndepostFunds.TabIndex = 20
        Me.chUndepostFunds.Text = "Undeposited Funds"
        Me.chUndepostFunds.UseVisualStyleBackColor = False
        '
        'chAPSummary
        '
        Me.chAPSummary.AutoSize = True
        Me.chAPSummary.Checked = True
        Me.chAPSummary.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chAPSummary.Location = New System.Drawing.Point(6, 17)
        Me.chAPSummary.Name = "chAPSummary"
        Me.chAPSummary.Size = New System.Drawing.Size(205, 20)
        Me.chAPSummary.TabIndex = 0
        Me.chAPSummary.Text = "Accounts Payable Summary"
        Me.chAPSummary.UseVisualStyleBackColor = True
        '
        'chReimburseExpnse
        '
        Me.chReimburseExpnse.AutoSize = True
        Me.chReimburseExpnse.Checked = True
        Me.chReimburseExpnse.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chReimburseExpnse.Location = New System.Drawing.Point(6, 307)
        Me.chReimburseExpnse.Name = "chReimburseExpnse"
        Me.chReimburseExpnse.Size = New System.Drawing.Size(179, 20)
        Me.chReimburseExpnse.TabIndex = 16
        Me.chReimburseExpnse.Text = "Reimbursable Expenses"
        Me.chReimburseExpnse.UseVisualStyleBackColor = True
        '
        'chJVSummryperAccnt
        '
        Me.chJVSummryperAccnt.AutoSize = True
        Me.chJVSummryperAccnt.BackColor = System.Drawing.Color.Transparent
        Me.chJVSummryperAccnt.Checked = True
        Me.chJVSummryperAccnt.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chJVSummryperAccnt.Location = New System.Drawing.Point(6, 235)
        Me.chJVSummryperAccnt.Name = "chJVSummryperAccnt"
        Me.chJVSummryperAccnt.Size = New System.Drawing.Size(276, 20)
        Me.chJVSummryperAccnt.TabIndex = 12
        Me.chJVSummryperAccnt.Text = "Journal Voucher Summary per Account"
        Me.chJVSummryperAccnt.UseVisualStyleBackColor = False
        '
        'chIncmeStatement
        '
        Me.chIncmeStatement.AutoSize = True
        Me.chIncmeStatement.Checked = True
        Me.chIncmeStatement.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chIncmeStatement.Location = New System.Drawing.Point(6, 198)
        Me.chIncmeStatement.Name = "chIncmeStatement"
        Me.chIncmeStatement.Size = New System.Drawing.Size(143, 20)
        Me.chIncmeStatement.TabIndex = 10
        Me.chIncmeStatement.Text = "Income Statement"
        Me.chIncmeStatement.UseVisualStyleBackColor = True
        '
        'chBudgetMonitor
        '
        Me.chBudgetMonitor.AutoSize = True
        Me.chBudgetMonitor.Checked = True
        Me.chBudgetMonitor.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chBudgetMonitor.Location = New System.Drawing.Point(6, 396)
        Me.chBudgetMonitor.Name = "chBudgetMonitor"
        Me.chBudgetMonitor.Size = New System.Drawing.Size(190, 20)
        Me.chBudgetMonitor.TabIndex = 21
        Me.chBudgetMonitor.Text = "Budget Monitoring Report"
        Me.chBudgetMonitor.UseVisualStyleBackColor = True
        '
        'chTaxRpt
        '
        Me.chTaxRpt.AutoSize = True
        Me.chTaxRpt.Checked = True
        Me.chTaxRpt.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chTaxRpt.Location = New System.Drawing.Point(6, 360)
        Me.chTaxRpt.Name = "chTaxRpt"
        Me.chTaxRpt.Size = New System.Drawing.Size(96, 20)
        Me.chTaxRpt.TabIndex = 19
        Me.chTaxRpt.Text = "Tax Report"
        Me.chTaxRpt.UseVisualStyleBackColor = True
        '
        'chCollectionRpt
        '
        Me.chCollectionRpt.AutoSize = True
        Me.chCollectionRpt.BackColor = System.Drawing.Color.Transparent
        Me.chCollectionRpt.Checked = True
        Me.chCollectionRpt.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chCollectionRpt.Location = New System.Drawing.Point(6, 126)
        Me.chCollectionRpt.Name = "chCollectionRpt"
        Me.chCollectionRpt.Size = New System.Drawing.Size(137, 20)
        Me.chCollectionRpt.TabIndex = 6
        Me.chCollectionRpt.Text = "Collection Report"
        Me.chCollectionRpt.UseVisualStyleBackColor = False
        '
        'chSupMasterRpt
        '
        Me.chSupMasterRpt.AutoSize = True
        Me.chSupMasterRpt.BackColor = System.Drawing.Color.Transparent
        Me.chSupMasterRpt.Checked = True
        Me.chSupMasterRpt.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chSupMasterRpt.Location = New System.Drawing.Point(6, 289)
        Me.chSupMasterRpt.Name = "chSupMasterRpt"
        Me.chSupMasterRpt.Size = New System.Drawing.Size(127, 20)
        Me.chSupMasterRpt.TabIndex = 15
        Me.chSupMasterRpt.Text = "Supplier Master"
        Me.chSupMasterRpt.UseVisualStyleBackColor = False
        '
        'chAging
        '
        Me.chAging.AutoSize = True
        Me.chAging.Checked = True
        Me.chAging.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chAging.Location = New System.Drawing.Point(6, 71)
        Me.chAging.Name = "chAging"
        Me.chAging.Size = New System.Drawing.Size(110, 20)
        Me.chAging.TabIndex = 3
        Me.chAging.Text = "Aging Report"
        Me.chAging.UseVisualStyleBackColor = True
        '
        'chGLByAccnt
        '
        Me.chGLByAccnt.AutoSize = True
        Me.chGLByAccnt.BackColor = System.Drawing.Color.Transparent
        Me.chGLByAccnt.Checked = True
        Me.chGLByAccnt.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chGLByAccnt.Location = New System.Drawing.Point(6, 180)
        Me.chGLByAccnt.Name = "chGLByAccnt"
        Me.chGLByAccnt.Size = New System.Drawing.Size(124, 20)
        Me.chGLByAccnt.TabIndex = 9
        Me.chGLByAccnt.Text = "G/L By Account"
        Me.chGLByAccnt.UseVisualStyleBackColor = False
        '
        'chSalesRpt
        '
        Me.chSalesRpt.AutoSize = True
        Me.chSalesRpt.BackColor = System.Drawing.Color.Transparent
        Me.chSalesRpt.Checked = True
        Me.chSalesRpt.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chSalesRpt.Location = New System.Drawing.Point(6, 326)
        Me.chSalesRpt.Name = "chSalesRpt"
        Me.chSalesRpt.Size = New System.Drawing.Size(108, 20)
        Me.chSalesRpt.TabIndex = 17
        Me.chSalesRpt.Text = "Sales Report"
        Me.chSalesRpt.UseVisualStyleBackColor = False
        '
        'chCVHistory
        '
        Me.chCVHistory.AutoSize = True
        Me.chCVHistory.Checked = True
        Me.chCVHistory.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chCVHistory.Location = New System.Drawing.Point(6, 108)
        Me.chCVHistory.Name = "chCVHistory"
        Me.chCVHistory.Size = New System.Drawing.Size(169, 20)
        Me.chCVHistory.TabIndex = 5
        Me.chCVHistory.Text = "Check Voucher History"
        Me.chCVHistory.UseVisualStyleBackColor = True
        '
        'chJVSummary
        '
        Me.chJVSummary.AutoSize = True
        Me.chJVSummary.BackColor = System.Drawing.Color.Transparent
        Me.chJVSummary.Checked = True
        Me.chJVSummary.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chJVSummary.Location = New System.Drawing.Point(6, 217)
        Me.chJVSummary.Name = "chJVSummary"
        Me.chJVSummary.Size = New System.Drawing.Size(196, 20)
        Me.chJVSummary.TabIndex = 11
        Me.chJVSummary.Text = "Journal Voucher Summary"
        Me.chJVSummary.UseVisualStyleBackColor = False
        '
        'chAPCVList
        '
        Me.chAPCVList.AutoSize = True
        Me.chAPCVList.BackColor = System.Drawing.Color.Transparent
        Me.chAPCVList.Checked = True
        Me.chAPCVList.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chAPCVList.Location = New System.Drawing.Point(6, 53)
        Me.chAPCVList.Name = "chAPCVList"
        Me.chAPCVList.Size = New System.Drawing.Size(106, 20)
        Me.chAPCVList.TabIndex = 2
        Me.chAPCVList.Text = "AP && CV List"
        Me.chAPCVList.UseVisualStyleBackColor = False
        '
        'chBalShit
        '
        Me.chBalShit.AutoSize = True
        Me.chBalShit.BackColor = System.Drawing.Color.Transparent
        Me.chBalShit.Checked = True
        Me.chBalShit.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chBalShit.Location = New System.Drawing.Point(6, 90)
        Me.chBalShit.Name = "chBalShit"
        Me.chBalShit.Size = New System.Drawing.Size(120, 20)
        Me.chBalShit.TabIndex = 4
        Me.chBalShit.Text = "Balance Sheet"
        Me.chBalShit.UseVisualStyleBackColor = False
        '
        'chStatementofAccnt
        '
        Me.chStatementofAccnt.AutoSize = True
        Me.chStatementofAccnt.BackColor = System.Drawing.Color.Transparent
        Me.chStatementofAccnt.Checked = True
        Me.chStatementofAccnt.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chStatementofAccnt.Location = New System.Drawing.Point(6, 343)
        Me.chStatementofAccnt.Name = "chStatementofAccnt"
        Me.chStatementofAccnt.Size = New System.Drawing.Size(169, 20)
        Me.chStatementofAccnt.TabIndex = 18
        Me.chStatementofAccnt.Text = "Statement of Accounts"
        Me.chStatementofAccnt.UseVisualStyleBackColor = False
        '
        'BtnOK
        '
        Me.BtnOK.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnOK.Image = Global.CSAcctg.My.Resources.Resources.apply
        Me.BtnOK.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnOK.Location = New System.Drawing.Point(558, 470)
        Me.BtnOK.Name = "BtnOK"
        Me.BtnOK.Size = New System.Drawing.Size(81, 28)
        Me.BtnOK.TabIndex = 3
        Me.BtnOK.Text = "OK"
        Me.BtnOK.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BtnOK.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(82, 1)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(438, 23)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Modules and Reports Accessibility Control Panel"
        '
        'frmAccesibility
        '
        Me.AcceptButton = Me.BtnOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(644, 501)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.BtnOK)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmAccesibility"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Accesibility"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents chChngePwrd As System.Windows.Forms.CheckBox
    Friend WithEvents chUserOptn As System.Windows.Forms.CheckBox
    Friend WithEvents chImport As System.Windows.Forms.CheckBox
    Friend WithEvents chPeriodRestrict As System.Windows.Forms.CheckBox
    Friend WithEvents chPref As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents chBills As System.Windows.Forms.CheckBox
    Friend WithEvents chSupMaster As System.Windows.Forms.CheckBox
    Friend WithEvents chEnterBills4RcveItem As System.Windows.Forms.CheckBox
    Friend WithEvents chPayBills As System.Windows.Forms.CheckBox
    Friend WithEvents chRecievedItem As System.Windows.Forms.CheckBox
    Friend WithEvents chBills4Apprvl As System.Windows.Forms.CheckBox
    Friend WithEvents chPO As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents chBankRecon As System.Windows.Forms.CheckBox
    Friend WithEvents chWriteChecks As System.Windows.Forms.CheckBox
    Friend WithEvents chMakeDeposit As System.Windows.Forms.CheckBox
    Friend WithEvents chTransFunds As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents chSO As System.Windows.Forms.CheckBox
    Friend WithEvents chCustMaster As System.Windows.Forms.CheckBox
    Friend WithEvents chDebit As System.Windows.Forms.CheckBox
    Friend WithEvents chSR As System.Windows.Forms.CheckBox
    Friend WithEvents chCredit As System.Windows.Forms.CheckBox
    Friend WithEvents chInv As System.Windows.Forms.CheckBox
    Friend WithEvents chRecievePaymnt As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents chWorkngTrlBal As System.Windows.Forms.CheckBox
    Friend WithEvents chAccntntRecon As System.Windows.Forms.CheckBox
    Friend WithEvents chGenJournEnt As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents chCustType As System.Windows.Forms.CheckBox
    Friend WithEvents chSalesRep As System.Windows.Forms.CheckBox
    Friend WithEvents chSalesTax As System.Windows.Forms.CheckBox
    Friend WithEvents chTerm As System.Windows.Forms.CheckBox
    Friend WithEvents chItemMaster As System.Windows.Forms.CheckBox
    Friend WithEvents chSupType As System.Windows.Forms.CheckBox
    Friend WithEvents chChartofAccounts As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chBllsPymntAcctRpt As System.Windows.Forms.CheckBox
    Friend WithEvents chAPSummary As System.Windows.Forms.CheckBox
    Friend WithEvents chCollectionRpt As System.Windows.Forms.CheckBox
    Friend WithEvents chAging As System.Windows.Forms.CheckBox
    Friend WithEvents chCVHistory As System.Windows.Forms.CheckBox
    Friend WithEvents chAPCVList As System.Windows.Forms.CheckBox
    Friend WithEvents chBalShit As System.Windows.Forms.CheckBox
    Friend WithEvents chGLListing As System.Windows.Forms.CheckBox
    Friend WithEvents chFinanceCharge As System.Windows.Forms.CheckBox
    Friend WithEvents chJVSummryperAccnt As System.Windows.Forms.CheckBox
    Friend WithEvents chIncmeStatement As System.Windows.Forms.CheckBox
    Friend WithEvents chGLByAccnt As System.Windows.Forms.CheckBox
    Friend WithEvents chJVSummary As System.Windows.Forms.CheckBox
    Friend WithEvents chItemMasterInventry As System.Windows.Forms.CheckBox
    Friend WithEvents chCustList As System.Windows.Forms.CheckBox
    Friend WithEvents chUndepostFunds As System.Windows.Forms.CheckBox
    Friend WithEvents chReimburseExpnse As System.Windows.Forms.CheckBox
    Friend WithEvents chTaxRpt As System.Windows.Forms.CheckBox
    Friend WithEvents chSupMasterRpt As System.Windows.Forms.CheckBox
    Friend WithEvents chSalesRpt As System.Windows.Forms.CheckBox
    Friend WithEvents ChCustTrnsctnRpt As System.Windows.Forms.CheckBox
    Friend WithEvents chBudgetMonitor As System.Windows.Forms.CheckBox
    Friend WithEvents ChClass As System.Windows.Forms.CheckBox
    Friend WithEvents BtnOK As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chStatementofAccnt As System.Windows.Forms.CheckBox
End Class
