﻿Imports System.Data.SqlClient
Imports Microsoft.VisualBasic.FileSystem
Imports System.Data
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.OleDb
Public Class frmAccountRegisterUploader

    Dim filenym As String
    Dim fpath As String
    Private UploaderType As String
    Private xFilePath As String = ""

    Dim con As New Clsappconfiguration

    Private Sub btnbrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnbrowse.Click
        OpenFileDialog1.Filter = "Microsoft Excel (*.xlsx)|*.xlsx|Microsoft Excel 97-2003 (*.xls)|*.xls"
        OpenFileDialog1.ShowDialog()
        filenym = OpenFileDialog1.FileName
        fpath = Path.GetFullPath(filenym)
        txtpath.Text = fpath
        xFilePath = txtpath.Text
        UploaderType = IO.Path.GetExtension(xFilePath)
        If DialogResult.OK Then
            Import()
        End If
    End Sub

    Private Sub Import()
        dgvUploader.Rows.Clear()
        Dim MyConnection As System.Data.OleDb.OleDbConnection
        MyConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + txtpath.Text.ToString + "';Extended Properties=Excel 8.0;")
        MyConnection.Open()
        Dim dbCommand As New OleDbCommand("select * from [Sheet1$]", MyConnection)
        Dim rd As OleDbDataReader
        rd = dbCommand.ExecuteReader

        Try
            While rd.Read = True
                Dim row As String() = New String() {rd.Item(0).ToString, rd.Item(1).ToString, rd.Item(2).ToString, rd.Item(3).ToString, Format(CDec(rd.Item(4).ToString), "##,##0.00")}
                dgvUploader.Rows.Add(row)
            End While
            rd.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Try
            For xCtr As Integer = 0 To dgvUploader.RowCount - 1
                With dgvUploader
                    Dim empno As String = .Item(1, xCtr).Value.ToString
                    Dim DocNumber As String = .Item(0, xCtr).Value.ToString
                    Dim AcctTitle As String = .Item(3, xCtr).Value.ToString
                    Dim Schedule As String = .Item(4, xCtr).Value
                    Dim myguid As Guid = New Guid(gCompanyID)

                    SqlHelper.ExecuteNonQuery(con.cnstring, "Masterfile_AccountRegister_Upload",
                                             New SqlParameter("@AccountTitle", AcctTitle),
                                             New SqlParameter("@fcDocNumber", DocNumber),
                                             New SqlParameter("@fcEmployeeNo", empno),
                                             New SqlParameter("@fnSchedule", Schedule),
                                             New SqlParameter("@fbClosed", False),
                                             New SqlParameter("@coid", myguid))
                End With
            Next

            AuditTrail_Save("DATABASE", "Data Migration Tools > Account Register > Upload Account Register")
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        MsgBox("Record Succesfully Added!", vbInformation)
    End Sub

End Class