Public Class frmMyClickFlashscreen
    Inherits System.Windows.Forms.Form
    Dim ctr As Integer = 0
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If Me.Opacity < 1 Then
            Me.Opacity = Me.Opacity + 0.02
            'Me.Refresh()
        Else
            Timer2.Enabled = True
            Timer1.Enabled = False
        End If
    End Sub

    Private Sub frmMyClickFlashscreen_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Opacity = 0.1
    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        If ctr < 1 Then
            ctr += 1
        Else
            Timer2.Interval = 5
            If Me.Opacity <> 0 Then
                Me.Opacity = Me.Opacity - 0.02
                Me.Refresh()
            Else

                Timer2.Enabled = False
                frmSys_UserLogin.Show()
                'frmSys_UserLogin.Opacity = 1.0
                frmSys_UserLogin.Username.Focus()
                Me.Hide()
            End If
        End If
    End Sub
End Class