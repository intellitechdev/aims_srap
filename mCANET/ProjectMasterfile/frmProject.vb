﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmProject
    '-------------------------------------
    '-- Author:		 JOVELYN ANCHETA
    '-- Create date: 5/13/15
    '-------------------------------------

    Private gCon As New Clsappconfiguration()
    Private ds As New DataSet

    Dim ProjectID As String

    Private Sub Insert_Project()
        Dim compID As String = gCompanyID()

        If txtProjectName.Text <> "" Then
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "Insert_Project",
                                      New SqlParameter("@fcProject", txtProjectName.Text),
                                      New SqlParameter("@co_id", compID))
            MessageBox.Show("Project successfully added", "Add Project", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            MessageBox.Show("Nothing to Save", "Add Project", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub Update_Project()
        Dim compID As String = gCompanyID()
        Dim ID As New Guid(ProjectID)

        If txtProjectName.Text <> "" Then
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "Update_Project",
                                      New SqlParameter("@pkProject", ID),
                                      New SqlParameter("@fcProject", txtProjectName.Text),
                                      New SqlParameter("@co_id", compID))
            MessageBox.Show("Project successfully Updated", "Update Project", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub frmProject_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Call LoadProjectToGrid()
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        Try
            If btnSave.Text = "&Add" Then
                btnSave.Text = "&Save"
                btnSave.Image = My.Resources.floppy
                txtProjectName.Enabled = True
                txtProjectName.Focus()
            ElseIf btnSave.Text = "&Save" Then
                Call Insert_Project()
                btnSave.Text = "&Add"
                btnSave.Image = My.Resources._new
                txtProjectName.Text = ""
                txtProjectName.Enabled = False
                Call LoadProjectToGrid()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        Try
            If btnSave.Text = "&Save" Then
                btnSave.Text = "&Add"
                btnSave.Image = My.Resources._new
                txtProjectName.Enabled = False
            ElseIf btnUpdate.Text = "&Save" Or btnUpdate.Visible = True Then
                btnUpdate.Text = "&Update"
                btnUpdate.Image = My.Resources.edit
                btnUpdate.Visible = False
                txtProjectName.Text = ""
                txtProjectName.Enabled = False
            ElseIf btnSave.Text = "&Add" Then
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Function GetProject() As DataSet
        Dim storedProcedure As String = "Select_Project"
        Dim companyID As String = gCompanyID()
        Dim ds As New DataSet()

        Try
            ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, storedProcedure, _
                    New SqlParameter("@co_id", companyID))
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Return ds
    End Function

    Public Sub LoadProjectToGrid()
        With dtgvProjectList
            ds = GetProject()
            .DataSource = ds.Tables(0)

            .Columns("pkProject").Visible = False
            .Columns("fkCo_id").Visible = False


            .Columns("fcProject").Width = 395
            .Columns("fcProject").HeaderText = "Project Name"
        End With
    End Sub

    Private Sub Delete_Project()
        Dim ID As New Guid(ProjectID)

        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "Delete_Project",
                                  New SqlParameter("@pkProject", ID))
        MessageBox.Show("Project Successfully Deleted", "Delete Project", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub btnDelete_Click(sender As System.Object, e As System.EventArgs) Handles btnDelete.Click
        If MsgBox("Are you sure you want to DELETE this TRANSACTION?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "TRANSACTION ENTRY") = MsgBoxResult.Yes Then
            Call Delete_Project()
            Call LoadProjectToGrid()
            txtProjectName.Text = ""
        Else
            Exit Sub
        End If
    End Sub

    Private Sub dtgvProjectList_Click(sender As Object, e As System.EventArgs) Handles dtgvProjectList.Click
        Dim i As Integer = dtgvProjectList.CurrentRow.Index
        ProjectID = dtgvProjectList.Item("pkProject", i).Value.ToString
        txtProjectName.Text = dtgvProjectList.Item("fcProject", i).Value.ToString
        btnUpdate.Visible = True
    End Sub

    Private Sub btnUpdate_Click(sender As System.Object, e As System.EventArgs) Handles btnUpdate.Click
        Try
            If btnUpdate.Text = "&Update" Then
                btnUpdate.Text = "&Save"
                btnUpdate.Image = My.Resources.floppy
                txtProjectName.Enabled = True
                txtProjectName.Focus()
            ElseIf btnUpdate.Text = "&Save" Then
                Call Update_Project()
                btnUpdate.Text = "&Update"
                btnUpdate.Image = My.Resources.edit
                txtProjectName.Text = ""
                txtProjectName.Enabled = False
                Call LoadProjectToGrid()
                btnUpdate.Visible = False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class