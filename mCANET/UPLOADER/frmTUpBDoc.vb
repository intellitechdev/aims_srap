﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmTUpBDoc

    Private con As New Clsappconfiguration
    Public doctype As String = ""
    Dim ds As DataSet
    Public cellPos As Integer

    Private Sub frmTUpBDoc_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If isLoaded(doctype, "") Then
            txtSearch.Text = ""
            ActiveControl = txtSearch
        End If
    End Sub

    Private Function isLoaded(ByVal doctype As String, ByVal key As String) As Boolean
        Try
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_Collections_Integrator_SelectDocNumber",
                                           New SqlParameter("@doctype", doctype),
                                           New SqlParameter("@search", key))
            dgvList.DataSource = ds.Tables(0)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        doctype = ""
        dgvList.Columns.Clear()
        dgvList.DataSource = Nothing
        Me.Close()
    End Sub

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        If e.KeyCode = Keys.Enter Then
            If isSelected() Then
                Me.Close()
            End If
        End If
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        If isLoaded(doctype, txtSearch.Text) Then

        End If
    End Sub

    Private Function isSelected() As Boolean
        Try
            ' frmCollections_Integrator.dgvList.CurrentRow.Cells(cellPos).Value = dgvList.SelectedRows(0).Cells(0).Value.ToString
            frmTUp.txtORNo.Text = dgvList.SelectedRows(0).Cells(0).Value.ToString
            'frmEntry001.txtDocNumber.Text = dgvList.SelectedRows(0).Cells(0).Value.ToString
            Return (True)
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub dgvList_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvList.DoubleClick
        If isSelected() Then
            Me.Close()
        End If
    End Sub

    Private Sub dgvList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvList.KeyDown
        If e.KeyCode = Keys.Enter Then
            If isSelected() Then
                Me.Close()
            End If
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If isSelected() Then
            Me.Close()
        End If
    End Sub

End Class