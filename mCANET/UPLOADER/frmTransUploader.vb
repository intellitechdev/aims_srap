﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmTransUploader

    Private con As New Clsappconfiguration

    Private Sub frmTransUploader_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _BootLoad()
    End Sub

#Region "Prep Col"
    Private Sub _BootLoad()
        dgvList.DataSource = Nothing
        dgvList.Columns.Clear()
        dgvList.Rows.Clear()

        Dim idno As New DataGridViewTextBoxColumn
        Dim clientname As New DataGridViewTextBoxColumn
        Dim loanref As New DataGridViewTextBoxColumn
        Dim acctref As New DataGridViewTextBoxColumn
        Dim acctcode As New DataGridViewTextBoxColumn
        Dim accttitle As New DataGridViewTextBoxColumn
        Dim debit As New DataGridViewTextBoxColumn
        Dim credit As New DataGridViewTextBoxColumn
        Dim fxkey As New DataGridViewTextBoxColumn
        With idno
            .Name = "idno"
            .HeaderText = "ID No."
            .Width = 100
        End With
        With clientname
            .Name = "clientname"
            .HeaderText = "Name"
            .Width = 250
        End With
        With loanref
            .Name = "loanref"
            .HeaderText = "Loan Ref."
            .Width = 100
        End With
        With acctref
            .Name = "acctref"
            .HeaderText = "Account Ref."
            .Width = 100
        End With
        With acctcode
            .Name = "acctcode"
            .HeaderText = "Code"
            .Width = 100
        End With
        With accttitle
            .Name = "accttitle"
            .HeaderText = "Account Title"
            .Width = 250
        End With
        With debit
            .Name = "debit"
            .HeaderText = "Debit"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Width = 150
        End With
        With credit
            .Name = "credit"
            .HeaderText = "Credit"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Width = 150
        End With
        With fxkey
            .Name = "fxkey"
            .Visible = False
        End With
        With dgvList
            .Columns.Add(idno)
            .Columns.Add(clientname)
            .Columns.Add(loanref)
            .Columns.Add(acctref)
            .Columns.Add(acctcode)
            .Columns.Add(accttitle)
            .Columns.Add(debit)
            .Columns.Add(credit)
            .Columns.Add(fxkey)
        End With
    End Sub
#End Region

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click

    End Sub

End Class