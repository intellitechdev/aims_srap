﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Public Class frmGeneralJournalReport
    Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument
    Dim MonthValue As String

    Private Sub frmGeneralJournalReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        rptsummary.Close()
    End Sub
    Private Sub frmGeneralJournalReport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Combo()
    End Sub
   
    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function
    Private Sub LoadReport()

        rptsummary.Load(Application.StartupPath & "\Accounting Reports\GJM.rpt")
        Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))

        rptsummary.Refresh()

        rptsummary.SetParameterValue("@coid", gCompanyID())
        rptsummary.SetParameterValue("@fdTransDate", MonthValue)
        rptsummary.SetParameterValue("@Year", dtpListing.Text)
        rptsummary.SetParameterValue("@coid", gCompanyID(), "GJMDRCR")
        rptsummary.SetParameterValue("@fdTransDate", MonthValue, "GJMDRCR")
        rptsummary.SetParameterValue("@Year", dtpListing.Text, "GJMDRCR")

        rptsummary.SetParameterValue("@coid", gCompanyID(), "Subreport_CancelledEntry_Monthly.rpt")
        rptsummary.SetParameterValue("@fdTransDate", GetDateMonthly(), "Subreport_CancelledEntry_Monthly.rpt")
        rptsummary.SetParameterValue("@Doctype", "General Journal", "Subreport_CancelledEntry_Monthly.rpt")
    End Sub

    Private Sub LoadReportDaily()

        rptsummary.Load(Application.StartupPath & "\Accounting Reports\GeneralJournalDaily.rpt")
        Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
        rptsummary.Refresh()
        rptsummary.SetParameterValue("@coid", gCompanyID())
        rptsummary.SetParameterValue("@fdTransDate", DateTimePicker1.Text)
        'rptsummary.SetParameterValue("@Year", dtpListing.Text)
        rptsummary.SetParameterValue("@coid", gCompanyID(), "SubReportGeneralJournalDaily.rpt")
        rptsummary.SetParameterValue("@fdTransDate", DateTimePicker1.Text, "SubReportGeneralJournalDaily.rpt")
        'rptsummary.SetParameterValue("@Year", dtpListing.Text, "SubReportGeneralJournalMonthly.rpt")

        rptsummary.SetParameterValue("@coid", gCompanyID(), "Subreport_CancelledEntry.rpt")
        rptsummary.SetParameterValue("@fdTransDate", DateTimePicker1.Text, "Subreport_CancelledEntry.rpt")
        rptsummary.SetParameterValue("@Doctype", "General Journal", "Subreport_CancelledEntry.rpt")
    End Sub

    Private Function GetDateMonthly()
        Return CDate(MonthValue & " " & dtpListing.Text)
    End Function

    Private Sub bgwProcessReport_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwProcessReport.DoWork
        If Me.rbDaily.Checked Then
            LoadReportDaily()
        ElseIf Me.rbMonthly.Checked Then
            LoadReport()
        End If
    End Sub

    Private Sub bgwProcessReport_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwProcessReport.RunWorkerCompleted
        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary

        picLoading.Visible = False
    End Sub

    Private Sub cbomonth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbomonth.SelectedIndexChanged
        MonthValue = cbomonth.SelectedValue
    End Sub
    Friend Class IntervalItem
        Public Property Text As String
        Public Property Value As Integer
    End Class
    Private Sub Combo()
        Dim intervals As New List(Of IntervalItem)

        intervals.Add(New IntervalItem With {.Text = "", .Value = 0})
        intervals.Add(New IntervalItem With {.Text = "January", .Value = 1})
        intervals.Add(New IntervalItem With {.Text = "February", .Value = 2})
        intervals.Add(New IntervalItem With {.Text = "March", .Value = 3})
        intervals.Add(New IntervalItem With {.Text = "April", .Value = 4})
        intervals.Add(New IntervalItem With {.Text = "May", .Value = 5})
        intervals.Add(New IntervalItem With {.Text = "June", .Value = 6})
        intervals.Add(New IntervalItem With {.Text = "July", .Value = 7})
        intervals.Add(New IntervalItem With {.Text = "August", .Value = 8})
        intervals.Add(New IntervalItem With {.Text = "September", .Value = 9})
        intervals.Add(New IntervalItem With {.Text = "October", .Value = 10})
        intervals.Add(New IntervalItem With {.Text = "November", .Value = 11})
        intervals.Add(New IntervalItem With {.Text = "December", .Value = 12})

        With Me.cbomonth
            .DisplayMember = "Text"
            .ValueMember = "Value"
            .DataSource = intervals
        End With
    End Sub

    Private Sub CloseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub PreviewToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PreviewToolStripMenuItem.Click
        If rbDaily.Checked Then
            picLoading.Visible = True
            AuditTrail_Save("REPORTS", "View General Journal > GJ - Daily > " & dtpListing.Text)
            If bgwProcessReport.IsBusy = False Then
                bgwProcessReport.RunWorkerAsync()
            End If
        ElseIf rbMonthly.Checked Then
            picLoading.Visible = True
            AuditTrail_Save("REPORTS", "View General Journal > GJ - Monthly (Summarized) > " & cbomonth.Text & " " & dtpListing.Text)
            If bgwProcessReport.IsBusy = False Then
                bgwProcessReport.RunWorkerAsync()
            End If
        End If
    End Sub

    Private Sub rbDaily_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rbDaily.CheckedChanged
        If Me.rbDaily.Checked Then
            DateTimePicker1.Enabled = True
            cbomonth.Enabled = False
            dtpListing.Enabled = False
        End If
    End Sub

    Private Sub rbMonthly_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rbMonthly.CheckedChanged
        If Me.rbMonthly.Checked Then
            DateTimePicker1.Enabled = False
            cbomonth.Enabled = True
            dtpListing.Enabled = True
        End If
    End Sub

    Private Sub rbDaily_Click(sender As System.Object, e As System.EventArgs) Handles rbDaily.Click
        If Me.rbDaily.Checked Then
            DateTimePicker1.Enabled = True
            cbomonth.Enabled = False
            dtpListing.Enabled = False
        End If
    End Sub

    Private Sub rbMonthly_Click(sender As System.Object, e As System.EventArgs) Handles rbMonthly.Click
        If Me.rbMonthly.Checked Then
            DateTimePicker1.Enabled = False
            cbomonth.Enabled = True
            dtpListing.Enabled = True
        End If
    End Sub
End Class