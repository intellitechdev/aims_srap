﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmPrintDynamicAmortization

    Private con As New Clsappconfiguration
    Private maxRows As Integer
    Public v_loanno As String
    Private Sub frmPrintDynamicAmortization_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        totinterest = 0
        totprincipal = 0
        totservicefee = 0
        TotalAmortization = 0
        totPayment = 0
        maxRows = 0
    End Sub

    Public Sub LoadAmortization(ByVal idno As String, ByVal loanno As String)
        Dim strwriter As String = ""
        Try

            'Load CSS
            strwriter += "<head><style type='text/css'>b{ font-size:11px; } small{ font-size:11px; } </style></head>"

            'Load Header
            strwriter += "<body style='font-family:verdana;padding:3px;'><center><h3 style='color:#2983a6'>Loan Amortization Schedule</h3></center><br><br><hr>"


            'Load Loan and Borrower Info
            Dim rd1 As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_Print_LoanAmortization_2",
                                                              New SqlParameter("@EmployeeNo", idno),
                                                              New SqlParameter("@LoanNo", loanno))
            If rd1.HasRows Then
                strwriter += "<table>"
                While rd1.Read
                    strwriter += "<tr><td><small><b>ID No.    :</b> " + idno + "</small></td></tr>"
                    strwriter += "<tr><td><small><b>Name      :</b> " + rd1(0).ToString + "</small></td></tr>"
                    strwriter += "<tr><td><small><b>Loan No.  :</b> " + loanno + "</small></td></tr>"
                    strwriter += "<tr><td><small><b>Loan Type :</b> " + rd1(1).ToString + "</small></td></tr></table>"
                End While
            End If

            'Get Database Values ---------------------------------------------------------------------
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_Print_LoanAmortization",
                                                              New SqlParameter("@EmployeeNo", idno),
                                                              New SqlParameter("@LoanNo", loanno))
            '-----------------------------------------------------------------------------------------
            'Load Divider 
            strwriter += "<table><tr>" '=================================
            'Load default column headers
            strwriter += "<td><div><table border='1'><tr>" + SetColumn("Pay No.") + SetColumn("Date") + SetColumn("Principal") + SetColumn("Interest") + SetColumn("Service Fee") + LoadAdditionalColumns() + SetColumn("Total") + "</tr>"
            'Load Default column values in amortization
            If rd.HasRows Then
                While rd.Read
                    strwriter += "<tr>" + SetColumnValue(rd("No.").ToString, "") + SetColumnValue(rd("Date").ToString, "") + SetColumnValue(rd("Principal").ToString, "p") + SetColumnValue(rd("Interest").ToString, "i") + SetColumnValue(rd("Service Fee").ToString, "sf") + LoadAdditionalValues() + GetTotalPayment(CDec(rd("Total").ToString)) + "</tr>"
                    maxRows += 1
                End While
                strwriter += LoadTotals()
                strwriter += "</table></div></td>"
                'call / loan amortization balance
                strwriter += LoadBalancesHeader()
                strwriter += LoadBalancesContent()
                strwriter += LoadBalancesTotals()
                strwriter += LoadBalancesEnd()
            Else
                strwriter = "<b>No results found!</b>"
            End If
            'exit divider
            strwriter += "</tr></table></body>" '=========================
        Catch ex As Exception
            strwriter = "Error :" + ex.Message
        End Try
        webWriter.DocumentText = strwriter
    End Sub

    'total payments per row 
    Dim TotalAmortization As Decimal = 0
    Dim totPayment As Decimal = 0
    Private Function GetTotalPayment(ByVal amort As Decimal) As String
        TotalAmortization += (totPayment + amort)
        Return "<td><small>" + Format((totPayment + amort), "##,##0.00") + "</small></td>"
    End Function
    Private Function GetTotalAmortization() As String
        Return "<td><small><b>" + Format(TotalAmortization, "##,##0.00") + "</b></small></td>"
    End Function

    Private Function SetColumn(ByVal colname As String) As String
        Try
            Return "<td style='background-color:#2983a6;color:white;'><small><b>" + colname + "</b></small></td>"
        Catch ex As Exception
            Return "error."
        End Try
    End Function

    Private Function SetColumnValue(ByVal cval As String, ByVal varname As String) As String
        Try
            Select Case varname
                Case "p"
                    totprincipal = totprincipal + CDec(cval)
                Case "i"
                    totinterest = totinterest + CDec(cval)
                Case "sf"
                    totservicefee = totservicefee + CDec(cval)
            End Select
            Return "<td><small>" + cval + "</small></td>"
        Catch ex As Exception
            Return "error."
        End Try
    End Function

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        webWriter.ShowPrintDialog()
    End Sub

    Public principal As String
    Public loantype As String
    Public terms As String

    Private Function LoadAdditionalColumns() As String
        Dim str As String = ""
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "Calculate_Amortization_InPAymentCharges",
                                                          New SqlParameter("@principal", principal),
                                                          New SqlParameter("@loantype", loantype),
                                                          New SqlParameter("@terms", terms))
        If rd.HasRows Then
            While rd.Read
                str += "<td style='background-color:#2983a6;color:white;'><small><b>" + rd(1).ToString + "</b></small></td>"
            End While
        End If
        Return str
    End Function
    Private Function LoadAdditionalValues() As String
        totPayment = 0
        Dim str As String = ""
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "Calculate_Amortization_InPAymentCharges",
                                                          New SqlParameter("@principal", principal),
                                                          New SqlParameter("@loantype", loantype),
                                                          New SqlParameter("@terms", terms))
        If rd.HasRows Then
            While rd.Read
                str += "<td><small>" + rd(2).ToString + "</small></td>"
                totPayment += CDec(rd(2))
            End While
        End If
        Return str
    End Function

    Private totprincipal As Decimal
    Private totinterest As Decimal
    Private totservicefee As Decimal

    Private Function LoadTotals() As String
        Return "<tr><td><small><b>Totals :</b></small></td><td>---</td><td><small><b>" + Format(totprincipal, "##,##0.00") + "</b></small></td><td><small><b>" + Format(totinterest, "##,##0.00") + "</b></small></td><td><small><b>" + Format(totservicefee, "##,##0.00") + "</b></small></td>" + LoadAdditionalColumnsTotal() + GetTotalAmortization()
    End Function
    Private Function LoadAdditionalColumnsTotal() As String
        Dim str As String = ""
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "Calculate_Amortization_InPAymentCharges",
                                                          New SqlParameter("@principal", principal),
                                                          New SqlParameter("@loantype", loantype),
                                                          New SqlParameter("@terms", terms))
        If rd.HasRows Then
            While rd.Read
                str += "<td><small><b>" + Format(CDec(rd(3).ToString), "##,##0.00") + "</b></small></td>"
            End While
        End If
        Return str
    End Function

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        webWriter.ShowPrintPreviewDialog()
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Function LoadBalancesHeader() As String
        Return "<td><div><table border='1'><tr>" + SetColumn("Pay No.") + SetColumn("Doc. Number") + SetColumn("Date") + SetColumn("Principal") + SetColumn("Interest") + SetColumn("Service Fee") + LoadAdditionalColumns() + "</tr>"
    End Function
    Private Function LoadBalancesContent() As String
        Dim strcontent As String = ""
        For i As Integer = 0 To maxRows - 1
            strcontent += "<tr><td><small>" + (i + 1).ToString + "</small></td></tr>"
        Next
        Return strcontent
    End Function
    Private Function LoadBalancesTotals() As String
        Return "<tr><td><small><b>Totals :</b></small></td></tr>"
    End Function
    Private Function LoadBalancesEnd() As String
        Return "</table></div></td>"
    End Function

End Class