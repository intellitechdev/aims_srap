﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCDJDaily
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblDate = New System.Windows.Forms.Label()
        Me.dtpYear = New System.Windows.Forms.DateTimePicker()
        Me.lblTransaction = New System.Windows.Forms.Label()
        Me.cbotransaction = New System.Windows.Forms.ComboBox()
        Me.cbodate = New System.Windows.Forms.ComboBox()
        Me.RBMonthly = New System.Windows.Forms.RadioButton()
        Me.dtpListing = New System.Windows.Forms.DateTimePicker()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.CloseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PreviewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.picLoading = New System.Windows.Forms.PictureBox()
        Me.bgwProcessReport = New System.ComponentModel.BackgroundWorker()
        Me.Panel1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.BackgroundImage = Global.CSAcctg.My.Resources.Resources.background
        Me.Panel1.Controls.Add(Me.lblDate)
        Me.Panel1.Controls.Add(Me.dtpYear)
        Me.Panel1.Controls.Add(Me.lblTransaction)
        Me.Panel1.Controls.Add(Me.cbotransaction)
        Me.Panel1.Controls.Add(Me.cbodate)
        Me.Panel1.Controls.Add(Me.RBMonthly)
        Me.Panel1.Controls.Add(Me.dtpListing)
        Me.Panel1.Controls.Add(Me.MenuStrip1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(972, 45)
        Me.Panel1.TabIndex = 0
        '
        'lblDate
        '
        Me.lblDate.AutoSize = True
        Me.lblDate.BackColor = System.Drawing.Color.Transparent
        Me.lblDate.Location = New System.Drawing.Point(18, 17)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(42, 15)
        Me.lblDate.TabIndex = 25
        Me.lblDate.Text = "Date:"
        '
        'dtpYear
        '
        Me.dtpYear.CustomFormat = "yyyy"
        Me.dtpYear.Enabled = False
        Me.dtpYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpYear.Location = New System.Drawing.Point(334, 10)
        Me.dtpYear.Name = "dtpYear"
        Me.dtpYear.Size = New System.Drawing.Size(72, 23)
        Me.dtpYear.TabIndex = 24
        Me.dtpYear.Visible = False
        '
        'lblTransaction
        '
        Me.lblTransaction.AutoSize = True
        Me.lblTransaction.Enabled = False
        Me.lblTransaction.Location = New System.Drawing.Point(402, 48)
        Me.lblTransaction.Name = "lblTransaction"
        Me.lblTransaction.Size = New System.Drawing.Size(84, 15)
        Me.lblTransaction.TabIndex = 23
        Me.lblTransaction.Text = "Transaction"
        Me.lblTransaction.Visible = False
        '
        'cbotransaction
        '
        Me.cbotransaction.Enabled = False
        Me.cbotransaction.FormattingEnabled = True
        Me.cbotransaction.Items.AddRange(New Object() {"", "All", "Receipts", "Disbursements", "Adjustments"})
        Me.cbotransaction.Location = New System.Drawing.Point(514, 45)
        Me.cbotransaction.Name = "cbotransaction"
        Me.cbotransaction.Size = New System.Drawing.Size(184, 23)
        Me.cbotransaction.TabIndex = 22
        Me.cbotransaction.Visible = False
        '
        'cbodate
        '
        Me.cbodate.Enabled = False
        Me.cbodate.FormattingEnabled = True
        Me.cbodate.Location = New System.Drawing.Point(472, 10)
        Me.cbodate.Name = "cbodate"
        Me.cbodate.Size = New System.Drawing.Size(96, 23)
        Me.cbodate.TabIndex = 21
        Me.cbodate.Visible = False
        '
        'RBMonthly
        '
        Me.RBMonthly.AutoSize = True
        Me.RBMonthly.Enabled = False
        Me.RBMonthly.Location = New System.Drawing.Point(402, 14)
        Me.RBMonthly.Name = "RBMonthly"
        Me.RBMonthly.Size = New System.Drawing.Size(74, 19)
        Me.RBMonthly.TabIndex = 20
        Me.RBMonthly.Text = "Monthly"
        Me.RBMonthly.UseVisualStyleBackColor = True
        Me.RBMonthly.Visible = False
        '
        'dtpListing
        '
        Me.dtpListing.CustomFormat = "MMMM dd, yyyy"
        Me.dtpListing.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpListing.Location = New System.Drawing.Point(64, 13)
        Me.dtpListing.Name = "dtpListing"
        Me.dtpListing.Size = New System.Drawing.Size(184, 23)
        Me.dtpListing.TabIndex = 18
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.Transparent
        Me.MenuStrip1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MenuStrip1.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CloseToolStripMenuItem, Me.PreviewToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(972, 45)
        Me.MenuStrip1.TabIndex = 27
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'CloseToolStripMenuItem
        '
        Me.CloseToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.CloseToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.CloseToolStripMenuItem.Name = "CloseToolStripMenuItem"
        Me.CloseToolStripMenuItem.Size = New System.Drawing.Size(70, 41)
        Me.CloseToolStripMenuItem.Text = "Close"
        '
        'PreviewToolStripMenuItem
        '
        Me.PreviewToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.PreviewToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.Print_Preview
        Me.PreviewToolStripMenuItem.Name = "PreviewToolStripMenuItem"
        Me.PreviewToolStripMenuItem.Size = New System.Drawing.Size(84, 41)
        Me.PreviewToolStripMenuItem.Text = "Preview"
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CrystalReportViewer1.CachedPageNumberPerDoc = 10
        Me.CrystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default
        Me.CrystalReportViewer1.DisplayStatusBar = False
        Me.CrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrystalReportViewer1.EnableDrillDown = False
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(0, 45)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.ShowGroupTreeButton = False
        Me.CrystalReportViewer1.ShowParameterPanelButton = False
        Me.CrystalReportViewer1.ShowRefreshButton = False
        Me.CrystalReportViewer1.ShowTextSearchButton = False
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(972, 568)
        Me.CrystalReportViewer1.TabIndex = 1
        Me.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        '
        'picLoading
        '
        Me.picLoading.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.picLoading.Image = Global.CSAcctg.My.Resources.Resources.LoadingData
        Me.picLoading.Location = New System.Drawing.Point(414, 285)
        Me.picLoading.Name = "picLoading"
        Me.picLoading.Size = New System.Drawing.Size(145, 110)
        Me.picLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picLoading.TabIndex = 22
        Me.picLoading.TabStop = False
        Me.picLoading.Visible = False
        '
        'bgwProcessReport
        '
        '
        'frmCDJDaily
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(972, 613)
        Me.Controls.Add(Me.picLoading)
        Me.Controls.Add(Me.CrystalReportViewer1)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmCDJDaily"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Cash Disbursements - Daily"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents dtpYear As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblTransaction As System.Windows.Forms.Label
    Friend WithEvents cbotransaction As System.Windows.Forms.ComboBox
    Friend WithEvents cbodate As System.Windows.Forms.ComboBox
    Friend WithEvents RBMonthly As System.Windows.Forms.RadioButton
    Friend WithEvents dtpListing As System.Windows.Forms.DateTimePicker
    Friend WithEvents picLoading As System.Windows.Forms.PictureBox
    Friend WithEvents bgwProcessReport As System.ComponentModel.BackgroundWorker
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents CloseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PreviewToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
