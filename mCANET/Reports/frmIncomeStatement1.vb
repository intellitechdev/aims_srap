Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmIncomeStatement

    Private gcon As New Clsappconfiguration
    Private rptsummary As New ReportDocument


    Private level As String
    Public Property GetLevel() As String
        Get
            Return level
        End Get
        Set(ByVal value As String)
            level = value
        End Set
    End Property


    Private dteFrom As Date
    Public Property GetDateFrom() As Date
        Get
            Return dteFrom
        End Get
        Set(ByVal value As Date)
            dteFrom = value
        End Set
    End Property

    Private dteTo As Date
    Public Property GetDateTo() As Date
        Get
            Return dteTo
        End Get
        Set(ByVal value As Date)
            dteTo = value
        End Set
    End Property

    Private Sub LoadReport(ByVal dateFrom As Date, ByVal dateTo As Date)
        Dim pxSalesAccount As String = ""
        ' rptsummary.Refresh()
        rptsummary.Load(Application.StartupPath & "\Accounting Reports\incomestatement.rpt")

        Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, Clsappconfiguration.GetDecryptedData(gcon.Password))
        rptsummary.Refresh()
        rptsummary.SetParameterValue("@balanceSheetType", GetLevel())
        rptsummary.SetParameterValue("@dateFrom", dateFrom)
        rptsummary.SetParameterValue("@dateTo", dateTo)
        rptsummary.SetParameterValue("@compID", gCompanyID())

    End Sub

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        picLoading.Visible = True
        bgwLoadReport.RunWorkerAsync()
    End Sub

    Private Sub frmIncomeStatement_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        rptsummary.Close()
    End Sub

    Private Sub frmIncomeStatement_LoadExtracted()
        Dim spu_loadLevels As String = "CAS_m_ChartOfAccounts_LoadLevels"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, spu_loadLevels, _
             New SqlParameter("companyID", gCompanyID()))
                While rd.Read()
                    Me.cboLoadLevel.Items.Add(rd.Item("level").ToString)
                End While
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Private Sub frmIncomeStatement_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.cboLoadLevel.SelectedIndex = 0
        GetDateFrom() = dateFrom.Value.Date
        GetDateTo() = dateTo.Value.Date
    End Sub

    Private Sub bgwLoadReport_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwLoadReport.DoWork
        Call LoadReport(GetDateFrom(), GetDateTo())
    End Sub

    Private Sub bgwLoadReport_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwLoadReport.RunWorkerCompleted
        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary

        picLoading.Visible = False
    End Sub

    Private Sub cboLoadLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLoadLevel.SelectedIndexChanged
        GetLevel() = cboLoadLevel.SelectedItem
    End Sub

    Private Sub dateFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dateFrom.ValueChanged
        GetDateFrom() = dateFrom.Value.Date
    End Sub

    Private Sub dateTo_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dateTo.ValueChanged
        GetDateTo() = dateTo.Value.Date
    End Sub
End Class