﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared

Public Class frmTrialBalanceDailySingleForm
    Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument
    Private Sub frmTrialBalanceDailySingleForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub PreviewToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles PreviewToolStripMenuItem.Click
        picLoading.Visible = True

        If bgwrReportProcess.IsBusy = False Then
            bgwrReportProcess.RunWorkerAsync()
        End If
    End Sub

    Private Sub bgwProcessReport_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwrReportProcess.DoWork
        LoadReport()
    End Sub

    Private Sub bgwProcessReport_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwrReportProcess.RunWorkerCompleted
        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary

        picLoading.Visible = False
    End Sub

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function
    Private Sub LoadReport()
        If rbtDaily.Checked = True Then
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\trialbalanceSingleFormDaily.rpt")
            Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@coName", gCompanyName)
            rptsummary.SetParameterValue("@transDate", GetDate)
        Else
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\trialbalanceSingleFormMonthly.rpt")
            Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@coName", gCompanyName)
            rptsummary.SetParameterValue("@fdTransDate", GetLastDayOfMonth)
        End If
    End Sub

    Public Function GetLastDayOfMonth() As Date
        GetLastDayOfMonth = DateSerial(Year(dtpDate.Value), Month(dtpDate.Value) + 1, 0)
    End Function

    Private Function GetDate() As Date
        GetDate = dtpDate.Value
    End Function

    Private Sub CloseToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub rbtDaily_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rbtDaily.CheckedChanged
        If rbtDaily.Checked = True Then
            rbtMonthly.Checked = False
            dtpDate.CustomFormat = "MMMM dd, yyyy"
        End If
    End Sub

    Private Sub rbtMonthly_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rbtMonthly.CheckedChanged
        If rbtMonthly.Checked = True Then
            rbtDaily.Checked = False
            dtpDate.CustomFormat = "MMMM yyyy"
        End If
    End Sub
End Class