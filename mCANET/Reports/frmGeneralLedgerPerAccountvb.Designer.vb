<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGeneralLedgerPerAccountvb
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGeneralLedgerPerAccountvb))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.picLoading = New System.Windows.Forms.PictureBox
        Me.btnGenerate = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.dteTo = New System.Windows.Forms.DateTimePicker
        Me.dteFrom = New System.Windows.Forms.DateTimePicker
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.mtcboAccounts = New MTGCComboBox
        Me.crvGenLedgerPerAccnt = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.bgwGenLedPerAcnt = New System.ComponentModel.BackgroundWorker
        Me.Panel1.SuspendLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.picLoading)
        Me.Panel1.Controls.Add(Me.btnGenerate)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.dteTo)
        Me.Panel1.Controls.Add(Me.dteFrom)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.mtcboAccounts)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(811, 78)
        Me.Panel1.TabIndex = 0
        '
        'picLoading
        '
        Me.picLoading.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picLoading.Image = Global.CSAcctg.My.Resources.Resources.loading_gif_animation
        Me.picLoading.Location = New System.Drawing.Point(623, 39)
        Me.picLoading.Name = "picLoading"
        Me.picLoading.Size = New System.Drawing.Size(28, 31)
        Me.picLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picLoading.TabIndex = 2
        Me.picLoading.TabStop = False
        Me.picLoading.Visible = False
        '
        'btnGenerate
        '
        Me.btnGenerate.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerate.Location = New System.Drawing.Point(511, 40)
        Me.btnGenerate.Name = "btnGenerate"
        Me.btnGenerate.Size = New System.Drawing.Size(106, 30)
        Me.btnGenerate.TabIndex = 6
        Me.btnGenerate.Text = "Generate"
        Me.btnGenerate.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(222, 14)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(26, 19)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "To"
        '
        'dteTo
        '
        Me.dteTo.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dteTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteTo.Location = New System.Drawing.Point(254, 11)
        Me.dteTo.Name = "dteTo"
        Me.dteTo.Size = New System.Drawing.Size(115, 26)
        Me.dteTo.TabIndex = 4
        '
        'dteFrom
        '
        Me.dteFrom.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dteFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteFrom.Location = New System.Drawing.Point(95, 11)
        Me.dteFrom.Name = "dteFrom"
        Me.dteFrom.Size = New System.Drawing.Size(115, 26)
        Me.dteFrom.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 14)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 19)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Period:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 45)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(76, 19)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Accounts:"
        '
        'mtcboAccounts
        '
        Me.mtcboAccounts.ArrowBoxColor = System.Drawing.SystemColors.Control
        Me.mtcboAccounts.ArrowColor = System.Drawing.Color.Black
        Me.mtcboAccounts.BindedControl = CType(resources.GetObject("mtcboAccounts.BindedControl"), MTGCComboBox.ControlloAssociato)
        Me.mtcboAccounts.BorderStyle = MTGCComboBox.TipiBordi.Fixed3D
        Me.mtcboAccounts.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.mtcboAccounts.ColumnNum = 3
        Me.mtcboAccounts.ColumnWidth = "350; 50; 0"
        Me.mtcboAccounts.DisabledArrowBoxColor = System.Drawing.SystemColors.Control
        Me.mtcboAccounts.DisabledArrowColor = System.Drawing.Color.LightGray
        Me.mtcboAccounts.DisabledBackColor = System.Drawing.SystemColors.Control
        Me.mtcboAccounts.DisabledBorderColor = System.Drawing.SystemColors.InactiveBorder
        Me.mtcboAccounts.DisabledForeColor = System.Drawing.SystemColors.GrayText
        Me.mtcboAccounts.DisplayMember = "Text"
        Me.mtcboAccounts.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.mtcboAccounts.DropDownBackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.mtcboAccounts.DropDownForeColor = System.Drawing.Color.Black
        Me.mtcboAccounts.DropDownStyle = MTGCComboBox.CustomDropDownStyle.DropDownList
        Me.mtcboAccounts.DropDownWidth = 400
        Me.mtcboAccounts.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mtcboAccounts.GridLineColor = System.Drawing.Color.LightGray
        Me.mtcboAccounts.GridLineHorizontal = False
        Me.mtcboAccounts.GridLineVertical = False
        Me.mtcboAccounts.LoadingType = MTGCComboBox.CaricamentoCombo.ComboBoxItem
        Me.mtcboAccounts.Location = New System.Drawing.Point(95, 43)
        Me.mtcboAccounts.ManagingFastMouseMoving = True
        Me.mtcboAccounts.ManagingFastMouseMovingInterval = 30
        Me.mtcboAccounts.MaxDropDownItems = 15
        Me.mtcboAccounts.Name = "mtcboAccounts"
        Me.mtcboAccounts.SelectedItem = Nothing
        Me.mtcboAccounts.SelectedValue = Nothing
        Me.mtcboAccounts.Size = New System.Drawing.Size(400, 24)
        Me.mtcboAccounts.TabIndex = 0
        '
        'crvGenLedgerPerAccnt
        '
        Me.crvGenLedgerPerAccnt.ActiveViewIndex = -1
        Me.crvGenLedgerPerAccnt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvGenLedgerPerAccnt.DisplayGroupTree = False
        Me.crvGenLedgerPerAccnt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvGenLedgerPerAccnt.EnableDrillDown = False
        Me.crvGenLedgerPerAccnt.Location = New System.Drawing.Point(0, 78)
        Me.crvGenLedgerPerAccnt.Name = "crvGenLedgerPerAccnt"
        Me.crvGenLedgerPerAccnt.SelectionFormula = ""
        Me.crvGenLedgerPerAccnt.ShowGotoPageButton = False
        Me.crvGenLedgerPerAccnt.ShowGroupTreeButton = False
        Me.crvGenLedgerPerAccnt.ShowRefreshButton = False
        Me.crvGenLedgerPerAccnt.Size = New System.Drawing.Size(811, 462)
        Me.crvGenLedgerPerAccnt.TabIndex = 1
        Me.crvGenLedgerPerAccnt.ViewTimeSelectionFormula = ""
        '
        'bgwGenLedPerAcnt
        '
        '
        'frmGeneralLedgerPerAccountvb
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(811, 540)
        Me.Controls.Add(Me.crvGenLedgerPerAccnt)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmGeneralLedgerPerAccountvb"
        Me.Text = "General Ledger Per Account"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents crvGenLedgerPerAccnt As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents mtcboAccounts As MTGCComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dteTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dteFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnGenerate As System.Windows.Forms.Button
    Friend WithEvents bgwGenLedPerAcnt As System.ComponentModel.BackgroundWorker
    Friend WithEvents picLoading As System.Windows.Forms.PictureBox
End Class
