Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmTrialBalanceSFM

    Private gcon As New Clsappconfiguration
    Private rptsummary As New ReportDocument

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Private Sub FrmTrialBalance_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        rptsummary.Close()
    End Sub

    Private Sub FrmTrialBalance_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub LoadReport()
        bgwLoadReport.RunWorkerAsync()
    End Sub

    Private Sub bgwLoadReport_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwLoadReport.DoWork
        Try
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\TrialBalance.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, Clsappconfiguration.GetDecryptedData(gcon.Password))
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@dateFrom", dteFrom.Value.Date)
            rptsummary.SetParameterValue("@dateTo", dteTo.Value.Date)
            rptsummary.SetParameterValue("@companyID", gCompanyID())
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error: Trial Balance", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub bgwLoadReport_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwLoadReport.RunWorkerCompleted
        Me.crvTrialBalance.Visible = True
        Me.crvTrialBalance.ReportSource = rptsummary

        picLoading.Visible = False
    End Sub

    Private Sub btnGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        picLoading.Visible = True

        LoadReport()
    End Sub
End Class