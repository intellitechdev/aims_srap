Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmDeposit_Voucher
    Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument

    Private employeeNo As String
    Public Property GetEmployeeNo() As String
        Get
            Return employeeNo
        End Get
        Set(ByVal value As String)
            employeeNo = value
        End Set
    End Property
    Private preparedDate As Date
    Public Property GetPrepared() As Date
        Get
            Return preparedDate
        End Get
        Set(ByVal value As Date)
            preparedDate = value
        End Set
    End Property
    Private reviewedby As String
    Public Property GetReviewedby() As String
        Get
            Return reviewedby
        End Get
        Set(ByVal value As String)
            reviewedby = value
        End Set
    End Property
    Private approvedBy As Integer
    Public Property GetApprovedBy() As Integer
        Get
            Return approvedBy
        End Get
        Set(ByVal value As Integer)
            approvedBy = value
        End Set
    End Property

    Private pkJournalID As String
    Public Property GetJournalID() As String
        Get
            Return pkJournalID
        End Get
        Set(ByVal value As String)
            pkJournalID = value
        End Set
    End Property


    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        picLoading.Visible = True
        If bgwProcessReport.IsBusy = False Then
            bgwProcessReport.RunWorkerAsync()
        End If
    End Sub

    Private Sub LoadReport()
        rptsummary.Load(Application.StartupPath & "\Accounting Reports\deposit_voucher.rpt")
        Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
        rptsummary.Refresh()
        rptsummary.SetParameterValue("@pk_JVEntry", GetJournalID())
        rptsummary.SetParameterValue("@fxKeyCompany", gCompanyID())
        rptsummary.SetParameterValue("@preparedby", get_preparedby())
        rptsummary.SetParameterValue("@reviewedby", GetReviewedby())
        rptsummary.SetParameterValue("@approvedby", GetApprovedBy())
        rptsummary.SetParameterValue("@employeeNo", GetEmployeeNo())
    End Sub

    Private Sub frmJournalRpt_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        crvRpt.Refresh()

        LoadReportValues()
        LoadComboBoxWithMembers(cboEmployeeNo)

        Panel1.Visible = False
        btnHidePanel.Top = 3

        picLoading.Visible = True
        If bgwProcessReport.IsBusy = False Then
            bgwProcessReport.RunWorkerAsync()
        End If
    End Sub

    Private Sub frmJournalRpt_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        gfiEntryNo = ""
        rptsummary.Close()
        Me.Dispose()
    End Sub

    Private Sub txtReviewed_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtReviewed.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            bgwProcessReport.RunWorkerAsync()
        End If
    End Sub

    Private Sub bgwProcessReport_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwProcessReport.DoWork
        LoadReport()
    End Sub

    Private Sub bgwProcessReport_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwProcessReport.RunWorkerCompleted
        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary

        picLoading.Visible = False
    End Sub

    Private Sub cboEmployeeNo_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployeeNo.SelectedIndexChanged
        If cboEmployeeNo.Text <> Nothing Then
            GetEmployeeNo() = cboEmployeeNo.SelectedItem.Col2.ToString()
        Else
            GetEmployeeNo() = Nothing
        End If
    End Sub

    Private Sub LoadReportValues()
        GetEmployeeNo() = cboEmployeeNo.Text
        GetReviewedby() = txtReviewed.Text
    End Sub

    Private Sub LoadComboBoxWithMembers(ByVal MultiColumnCbo As MTGCComboBox)

        Dim members As New DataTable("Members")
        Dim dataRowAccounts As DataRow
        Dim storedProc As String = "usp_rpt_JournalVoucher_Members"

        members.Columns.Add("fcEmployeeNo", System.Type.GetType("System.String"))
        members.Columns.Add("FullName", System.Type.GetType("System.String"))

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, CommandType.StoredProcedure, storedProc, _
                        New SqlParameter("@fiEntryNo", gfiEntryNo), _
                        New SqlParameter("@fxKeyCompany", gCompanyID()))
                While rd.Read
                    dataRowAccounts = members.NewRow
                    dataRowAccounts("fcEmployeeNo") = rd.Item(0).ToString
                    dataRowAccounts("FullName") = rd.Item(1).ToString
                    members.Rows.Add(dataRowAccounts)
                End While
            End Using

        Catch ex As Exception
            Throw ex
        End Try

        MultiColumnCbo.Items.Clear()
        MultiColumnCbo.LoadingType = MTGCComboBox.CaricamentoCombo.DataTable
        MultiColumnCbo.SourceDataString = New String(1) {"FullName", "fcEmployeeNo"}
        MultiColumnCbo.SourceDataTable = members
    End Sub


    Private Sub btnHidePanel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHidePanel.Click
        If btnHidePanel.Text = "Hide" Then
            btnHidePanel.Text = "Show"
            Panel1.Visible = False
            btnHidePanel.Top = 3
        Else
            btnHidePanel.Top = 73
            btnHidePanel.Text = "Hide"
            Panel1.Visible = True
        End If
    End Sub


End Class