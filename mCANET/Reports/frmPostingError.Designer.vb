﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPostingError
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.grdErrorList = New System.Windows.Forms.DataGridView()
        Me.cLine = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LoanRef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AcntRef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.grdErrorList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grdErrorList
        '
        Me.grdErrorList.AllowUserToAddRows = False
        Me.grdErrorList.AllowUserToDeleteRows = False
        Me.grdErrorList.BackgroundColor = System.Drawing.Color.White
        Me.grdErrorList.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdErrorList.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.grdErrorList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdErrorList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cLine, Me.cID, Me.cName, Me.LoanRef, Me.AcntRef})
        Me.grdErrorList.Location = New System.Drawing.Point(12, 12)
        Me.grdErrorList.Name = "grdErrorList"
        Me.grdErrorList.ReadOnly = True
        Me.grdErrorList.RowHeadersVisible = False
        Me.grdErrorList.Size = New System.Drawing.Size(617, 206)
        Me.grdErrorList.TabIndex = 0
        '
        'cLine
        '
        Me.cLine.HeaderText = "Line No."
        Me.cLine.Name = "cLine"
        Me.cLine.ReadOnly = True
        Me.cLine.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.cLine.Width = 60
        '
        'cID
        '
        Me.cID.HeaderText = "ID"
        Me.cID.Name = "cID"
        Me.cID.ReadOnly = True
        Me.cID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'cName
        '
        Me.cName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.cName.HeaderText = "Name"
        Me.cName.Name = "cName"
        Me.cName.ReadOnly = True
        Me.cName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'LoanRef
        '
        Me.LoanRef.HeaderText = "Loan Ref."
        Me.LoanRef.Name = "LoanRef"
        Me.LoanRef.ReadOnly = True
        Me.LoanRef.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'AcntRef
        '
        Me.AcntRef.HeaderText = "Account Ref."
        Me.AcntRef.Name = "AcntRef"
        Me.AcntRef.ReadOnly = True
        Me.AcntRef.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'frmPostingError
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(641, 228)
        Me.Controls.Add(Me.grdErrorList)
        Me.Name = "frmPostingError"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.grdErrorList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grdErrorList As System.Windows.Forms.DataGridView
    Friend WithEvents cLine As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LoanRef As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AcntRef As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
