Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared


Public Class frmSalesInvoiceSummaryRpt

    Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument

    Private Sub frmSalesInvoiceSummaryRpt_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        rptsummary.Close()
    End Sub


    Private Sub frmSalesInvoiceSummaryRpt_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'rptsummary.Refresh()
        rptsummary.Load(Application.StartupPath & "\Accounting Reports\salesinvoice_summary_revised.rpt")

        Logon(rptsummary, con.Server, con.Database, con.Username, "sa")
        rptsummary.Refresh()
        rptsummary.SetParameterValue("@fxKeyCompany", gCompanyID())
        rptsummary.SetParameterValue("@fxKeyInvoice", gInvoiceKey)
        rptsummary.SetParameterValue("@deliveryno", gDeliveryNumber)
        rptsummary.SetParameterValue("@fcAppliedVAT", gAppliedVAT)
        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary
    End Sub

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If

        Dim obj As ReportObject

        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        ' for each table apply connection info
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            ' check if logon was successful
            ' if TestConnectivity returns false,
            ' check logon credentials
            If (tbl.TestConnectivity()) Then
                'drop fully qualified table location
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next

        Return True
    End Function

End Class