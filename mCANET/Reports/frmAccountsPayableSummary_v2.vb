Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports CrystalDecisions.Shared


Public Class frmAccountsPayableSummary_v2

    Private gCon As New Clsappconfiguration()
    Private rptSummary As New ReportDocument


    Private dateFrom As Date
    Public Property GetDateFrom() As Date
        Get
            Return dateFrom
        End Get
        Set(ByVal value As Date)
            dateFrom = value
        End Set
    End Property


    Private dateTo As Date
    Public Property GetDateTo() As Date
        Get
            Return dateTo
        End Get
        Set(ByVal value As Date)
            dateTo = value
        End Set
    End Property

    Private subsidiary As String
    Public Property GetSubsidiary() As String
        Get
            Return subsidiary
        End Get
        Set(ByVal value As String)
            subsidiary = value
        End Set
    End Property



    Private Sub frmAccountsPayableSummary_v2_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        rptSummary.Close()
    End Sub

    Private Function GetSupplierList() As DataSet
        Dim ds As New DataSet

        ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "usp_m_msupplier_getname", _
                New SqlParameter("@coid", gCompanyID()))

        Return ds
    End Function

    Private Sub LoadReport()
        Dim reportPath As String = Application.StartupPath & "\Accounting Reports\AccountsPayable_Summary.rpt"
        rptSummary.Load(reportPath)
        Logon(rptSummary, gCon.Server, gCon.Database, gCon.Username, Clsappconfiguration.GetDecryptedData(gCon.Password))
        rptSummary.Refresh()

        rptSummary.SetParameterValue("@companyID", gCompanyID())
        rptSummary.SetParameterValue("@from", dteFrom.Value.Date)
        rptSummary.SetParameterValue("@to", dteTo.Value.Date)
        'rptSummary.SetParameterValue("@subsidiary", IIf(GetSubsidiary() = "", Nothing, GetSubsidiary()))

    End Sub

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Private Sub bgwLoadReport_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwLoadReport.DoWork
        Call LoadReport()
    End Sub

    Private Sub bgwLoadReport_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwLoadReport.RunWorkerCompleted
        picLoading.Visible = False
        crvRpt.ReportSource = rptSummary
    End Sub

    Private Sub btnLoadReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoadReport.Click
        If bgwLoadReport.IsBusy = False Then
            picLoading.Visible = True
            bgwLoadReport.RunWorkerAsync()
        End If
    End Sub

End Class