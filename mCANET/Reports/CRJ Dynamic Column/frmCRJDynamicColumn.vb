﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmCRJDynamicColumn
    Private gCon As New Clsappconfiguration()
    Dim c1 As String
    Dim c2 As String
    Dim c3 As String
    Dim c4 As String
    Private Sub frmCRJDynamicColumn_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        LoadColumns()
    End Sub

#Region "Data Layer"

    Private Sub LoadColumns()
        txtColumn1.Clear()
        txtColumn2.Clear()
        txtColumn3.Clear()
        txtColumn4.Clear()

        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_CRJDynamicColumn_ListByID",
                                                                            New SqlParameter("@FxColumn", 1))

        If rd.Read = True Then
            c1 = rd.Item(0).ToString
            txtColumn1.Text = rd.Item(1).ToString
        End If

        Dim rd2 As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_CRJDynamicColumn_ListByID",
                                                                            New SqlParameter("@FxColumn", 2))
        If rd2.Read = True Then
            c2 = rd2.Item(0).ToString
            txtColumn2.Text = rd2.Item(1).ToString
        End If

        Dim rd3 As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_CRJDynamicColumn_ListByID",
                                                                            New SqlParameter("@FxColumn", 3))
        If rd3.Read = True Then
            c3 = rd3.Item(0).ToString
            txtColumn3.Text = rd3.Item(1).ToString
        End If

        Dim rd4 As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_CRJDynamicColumn_ListByID",
                                                                            New SqlParameter("@FxColumn", 4))
        If rd4.Read = True Then
            c4 = rd4.Item(0).ToString
            txtColumn4.Text = rd4.Item(1).ToString
        End If

    End Sub

    Private Sub SaveColumn(ByVal ColumnID As Integer, ByVal acntKey As String)
        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_CRJDynamicColumn_Update",
                                                                            New SqlParameter("@FxColumn", ColumnID),
                                                                            New SqlParameter("@FkAccountKey", acntKey))
        MsgBox("Column " & ColumnID & " updated.", MsgBoxStyle.Information, "Dynamic Column")
        LoadColumns()
    End Sub

    Private Sub DeleteColumn(ByVal ColumnID As Integer)
        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_CRJDynamicColumn_Delete",
                                                                            New SqlParameter("@FxColumn", ColumnID))
        MsgBox("Column " & ColumnID & " account deleted.", MsgBoxStyle.Information, "Dynamic Column")
        LoadColumns()
    End Sub

#End Region

#Region "Events"

    Private Sub btnSave1_Click(sender As System.Object, e As System.EventArgs) Handles btnSave1.Click

        Try
            SaveColumn(1, c1)
        Catch ex As Exception
            MsgBox("Please select account for dynamic column 1.", MsgBoxStyle.Exclamation, "Dynamic Column")
        End Try
    End Sub

    Private Sub btnSave2_Click(sender As System.Object, e As System.EventArgs) Handles btnSave2.Click
        Try
            SaveColumn(2, c2)
        Catch ex As Exception
            MsgBox("Please select account for dynamic column 2.", MsgBoxStyle.Exclamation, "Dynamic Column")
        End Try
    End Sub

    Private Sub btnSave3_Click(sender As System.Object, e As System.EventArgs) Handles btnSave3.Click
        Try
            SaveColumn(3, c3)
        Catch ex As Exception
            MsgBox("Please select account for dynamic column 3.", MsgBoxStyle.Exclamation, "Dynamic Column")
        End Try
    End Sub

    Private Sub btnSave4_Click(sender As System.Object, e As System.EventArgs) Handles btnSave4.Click
        Try
            SaveColumn(4, c4)
        Catch ex As Exception
            MsgBox("Please select account for dynamic column .", MsgBoxStyle.Exclamation, "Dynamic Column")
        End Try
    End Sub
    
    Private Sub btnSearch1_Click(sender As System.Object, e As System.EventArgs) Handles btnSearch1.Click
        Try
            frmCOAFilter.xModule = "Entry"
            frmCOAFilter.ShowDialog()
            If frmCOAFilter.DialogResult = Windows.Forms.DialogResult.OK Then
                txtColumn1.Text = frmCOAFilter.grdCoAList.SelectedRows(0).Cells("acnt_code").Value.ToString() + " | " + frmCOAFilter.grdCoAList.SelectedRows(0).Cells("acnt_name").Value.ToString()
                c1 = frmCOAFilter.grdCoAList.SelectedRows(0).Cells("acnt_id").Value.ToString()
            End If
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Sub btnSearch2_Click(sender As System.Object, e As System.EventArgs) Handles btnSearch2.Click
        Try
            frmCOAFilter.xModule = "Entry"
            frmCOAFilter.ShowDialog()
            If frmCOAFilter.DialogResult = Windows.Forms.DialogResult.OK Then
                txtColumn2.Text = frmCOAFilter.grdCoAList.SelectedRows(0).Cells("acnt_code").Value.ToString() + " | " + frmCOAFilter.grdCoAList.SelectedRows(0).Cells("acnt_name").Value.ToString()
                c2 = frmCOAFilter.grdCoAList.SelectedRows(0).Cells("acnt_id").Value.ToString()
            End If
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Sub btnSearch3_Click(sender As System.Object, e As System.EventArgs) Handles btnSearch3.Click
        Try
            frmCOAFilter.xModule = "Entry"
            frmCOAFilter.ShowDialog()
            If frmCOAFilter.DialogResult = Windows.Forms.DialogResult.OK Then
                txtColumn3.Text = frmCOAFilter.grdCoAList.SelectedRows(0).Cells("acnt_code").Value.ToString() + " | " + frmCOAFilter.grdCoAList.SelectedRows(0).Cells("acnt_name").Value.ToString()
                c3 = frmCOAFilter.grdCoAList.SelectedRows(0).Cells("acnt_id").Value.ToString()
            End If
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Sub btnSearch4_Click(sender As System.Object, e As System.EventArgs) Handles btnSearch4.Click
        Try
            frmCOAFilter.xModule = "Entry"
            frmCOAFilter.ShowDialog()
            If frmCOAFilter.DialogResult = Windows.Forms.DialogResult.OK Then
                txtColumn4.Text = frmCOAFilter.grdCoAList.SelectedRows(0).Cells("acnt_code").Value.ToString() + " | " + frmCOAFilter.grdCoAList.SelectedRows(0).Cells("acnt_name").Value.ToString()
                c4 = frmCOAFilter.grdCoAList.SelectedRows(0).Cells("acnt_id").Value.ToString()
            End If
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Sub btnDelete1_Click(sender As System.Object, e As System.EventArgs) Handles btnDelete1.Click
        DeleteColumn(1)
    End Sub

    Private Sub btnDelete2_Click(sender As System.Object, e As System.EventArgs) Handles btnDelete2.Click
        DeleteColumn(2)
    End Sub

    Private Sub btnDelete3_Click(sender As System.Object, e As System.EventArgs) Handles btnDelete3.Click
        DeleteColumn(3)
    End Sub

    Private Sub btnDelete4_Click(sender As System.Object, e As System.EventArgs) Handles btnDelete4.Click
        DeleteColumn(4)
    End Sub

#End Region

End Class