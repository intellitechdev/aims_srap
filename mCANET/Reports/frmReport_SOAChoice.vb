﻿Public Class frmReport_SOAChoice

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub frmReport_SOAChoice_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        chkHorizontal.Checked = False
        chkVertical.Checked = False
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        If chkHorizontal.Checked = False And chkVertical.Checked = False Then
            MsgBox("Select Type of Application Form to Continue Printing.", vbInformation, "Oops.")
            Exit Sub
        End If
        If chkHorizontal.Checked = True Then
            frmPrint.LoadReport2(frmVerification.lblIDno.Text)
            frmPrint.WindowState = FormWindowState.Maximized
            frmPrint.ShowDialog()
        End If
        If chkVertical.Checked = True Then
            frmPrint.LoadREport(frmVerification.lblIDno.Text)
            frmPrint.WindowState = FormWindowState.Maximized
            frmPrint.ShowDialog()
        End If
    End Sub

    Private Sub chkHorizontal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkHorizontal.CheckedChanged
        If chkHorizontal.Checked = True Then
            chkVertical.Checked = False
        End If
    End Sub

    Private Sub chkVertical_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkVertical.CheckedChanged
        If chkVertical.Checked = True Then
            chkHorizontal.Checked = False
        End If
    End Sub
End Class