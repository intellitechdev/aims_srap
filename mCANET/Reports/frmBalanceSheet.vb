Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmBalanceSheet

    Private gcon As New Clsappconfiguration
    Private rptsummary As New ReportDocument
    Dim sclick As Integer = 0


    Private level As String
    Public Property GetLevel() As String
        Get
            Return level
        End Get
        Set(ByVal value As String)
            level = value
        End Set
    End Property

    Private dteFrom As Date
    Public Property GetdateFrom() As Date
        Get
            Return dteFrom
        End Get
        Set(ByVal value As Date)
            dteFrom = value
        End Set
    End Property

    Private dteTo As Date
    Public Property GetDateTo() As Date
        Get
            Return dteTo
        End Get
        Set(ByVal value As Date)
            dteTo = value
        End Set
    End Property


    Private Sub btnGO_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGO.Click
        picLoading.Visible = True
        bgwLoadReport.RunWorkerAsync()
    End Sub

    Private Sub LoadReport()
        Try
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\balancesheet.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, Clsappconfiguration.GetDecryptedData(gcon.Password))
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@balanceSheetType", GetLevel())
            rptsummary.SetParameterValue("@dateFrom", GetdateFrom())
            rptsummary.SetParameterValue("@dateTo", GetDateTo())
            rptsummary.SetParameterValue("@co_id", gCompanyID())

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Private Sub frmBalanceSheet_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        rptsummary.Close()
    End Sub

    Private Sub frmBalanceSheet_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.cboLoadLevels.SelectedIndex = 0
        GetDateTo() = dateTo.Value.Date
        GetdateFrom() = dateFrom.Value.Date
    End Sub

    Private Sub frmBalanceSheet_LoadExtracted()
        Dim spu_loadLevels As String = "CAS_m_ChartOfAccounts_LoadLevels"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, spu_loadLevels, _
             New SqlParameter("companyID", gCompanyID()))
                While rd.Read()
                    Me.cboLoadLevels.Items.Add(rd.Item("level").ToString)
                End While
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Private Function Get_CompanyDateStart()
        Dim dateStart As Date = #1/1/1900#
        Dim spu_LoadDate As String = "CAS_t_Company_DateStart_Get"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, spu_LoadDate, _
            New SqlParameter("@co_id", gCompanyID()))
                While rd.Read()
                    dateStart = rd.Item("co_companyStart")
                End While
            End Using
        Catch ex As Exception

        End Try

        Return dateStart
    End Function

    Private Sub bgwLoadReport_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwLoadReport.DoWork
        LoadReport()
    End Sub

    Private Sub bgwLoadReport_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwLoadReport.RunWorkerCompleted
        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary

        picLoading.Visible = False
    End Sub

    Private Sub cboLoadLevels_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLoadLevels.SelectedIndexChanged
        GetLevel() = cboLoadLevels.SelectedItem
    End Sub

    Private Sub dateTo_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dateTo.ValueChanged
        GetDateTo() = dateTo.Value.Date
    End Sub

    Private Sub dateFrom_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dateFrom.ValueChanged
        GetdateFrom() = dateFrom.Value.Date
    End Sub
End Class