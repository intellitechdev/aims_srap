﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGeneralJournal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.crvRpt = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.RBMonthly = New System.Windows.Forms.RadioButton()
        Me.bgwProcessReport = New System.ComponentModel.BackgroundWorker()
        Me.RBDaily = New System.Windows.Forms.RadioButton()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.dtpListing = New System.Windows.Forms.DateTimePicker()
        Me.btnPreview = New System.Windows.Forms.Button()
        Me.picLoading = New System.Windows.Forms.PictureBox()
        Me.dtpMonth = New System.Windows.Forms.DateTimePicker()
        Me.Panel1.SuspendLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'crvRpt
        '
        Me.crvRpt.ActiveViewIndex = -1
        Me.crvRpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvRpt.Cursor = System.Windows.Forms.Cursors.Default
        Me.crvRpt.DisplayStatusBar = False
        Me.crvRpt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvRpt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.crvRpt.Location = New System.Drawing.Point(0, 75)
        Me.crvRpt.Name = "crvRpt"
        Me.crvRpt.SelectionFormula = ""
        Me.crvRpt.ShowGotoPageButton = False
        Me.crvRpt.ShowGroupTreeButton = False
        Me.crvRpt.ShowParameterPanelButton = False
        Me.crvRpt.ShowRefreshButton = False
        Me.crvRpt.Size = New System.Drawing.Size(843, 450)
        Me.crvRpt.TabIndex = 20
        Me.crvRpt.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        Me.crvRpt.ViewTimeSelectionFormula = ""
        '
        'RBMonthly
        '
        Me.RBMonthly.AutoSize = True
        Me.RBMonthly.Location = New System.Drawing.Point(12, 45)
        Me.RBMonthly.Name = "RBMonthly"
        Me.RBMonthly.Size = New System.Drawing.Size(62, 17)
        Me.RBMonthly.TabIndex = 11
        Me.RBMonthly.Text = "Monthly"
        Me.RBMonthly.UseVisualStyleBackColor = True
        '
        'bgwProcessReport
        '
        '
        'RBDaily
        '
        Me.RBDaily.AutoSize = True
        Me.RBDaily.Location = New System.Drawing.Point(12, 16)
        Me.RBDaily.Name = "RBDaily"
        Me.RBDaily.Size = New System.Drawing.Size(48, 17)
        Me.RBDaily.TabIndex = 10
        Me.RBDaily.Text = "Daily"
        Me.RBDaily.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.dtpMonth)
        Me.Panel1.Controls.Add(Me.RBMonthly)
        Me.Panel1.Controls.Add(Me.RBDaily)
        Me.Panel1.Controls.Add(Me.dtpListing)
        Me.Panel1.Controls.Add(Me.btnPreview)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(843, 75)
        Me.Panel1.TabIndex = 19
        '
        'dtpListing
        '
        Me.dtpListing.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpListing.Location = New System.Drawing.Point(108, 16)
        Me.dtpListing.Name = "dtpListing"
        Me.dtpListing.Size = New System.Drawing.Size(128, 20)
        Me.dtpListing.TabIndex = 8
        Me.dtpListing.Visible = False
        '
        'btnPreview
        '
        Me.btnPreview.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.Image = Global.CSAcctg.My.Resources.Resources.Print_Preview
        Me.btnPreview.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPreview.Location = New System.Drawing.Point(251, 36)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(96, 31)
        Me.btnPreview.TabIndex = 4
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'picLoading
        '
        Me.picLoading.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.picLoading.Image = Global.CSAcctg.My.Resources.Resources.LoadingData
        Me.picLoading.Location = New System.Drawing.Point(357, 240)
        Me.picLoading.Name = "picLoading"
        Me.picLoading.Size = New System.Drawing.Size(124, 95)
        Me.picLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picLoading.TabIndex = 21
        Me.picLoading.TabStop = False
        Me.picLoading.Visible = False
        '
        'dtpMonth
        '
        Me.dtpMonth.CustomFormat = "MMMM, yyyy"
        Me.dtpMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpMonth.Location = New System.Drawing.Point(108, 45)
        Me.dtpMonth.Name = "dtpMonth"
        Me.dtpMonth.Size = New System.Drawing.Size(128, 20)
        Me.dtpMonth.TabIndex = 12
        Me.dtpMonth.Visible = False
        '
        'frmGeneralJournal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(843, 525)
        Me.Controls.Add(Me.crvRpt)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.picLoading)
        Me.Name = "frmGeneralJournal"
        Me.Text = "frmGeneralJournal"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents crvRpt As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents RBMonthly As System.Windows.Forms.RadioButton
    Friend WithEvents bgwProcessReport As System.ComponentModel.BackgroundWorker
    Friend WithEvents RBDaily As System.Windows.Forms.RadioButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dtpListing As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents picLoading As System.Windows.Forms.PictureBox
    Friend WithEvents dtpMonth As System.Windows.Forms.DateTimePicker
End Class
