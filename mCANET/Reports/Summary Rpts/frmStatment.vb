Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmStatment
    Private gCon As New Clsappconfiguration
    Private Sub frmStatment_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call m_loadCustomer(cboCustomerName, cboCustomerID)
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub cboCustomerName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCustomerName.SelectedIndexChanged
        cboCustomerID.SelectedIndex = cboCustomerName.SelectedIndex
        gKeyCustomer = cboCustomerID.SelectedItem
        Call loadbillslist()
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Call checkvalidity()
        cboCustomerID.SelectedIndex = cboCustomerName.SelectedIndex
        gKeyCustomer = cboCustomerID.SelectedItem
        gKeyStatmentNumber = txtSA.Text
        gRvwd = "Ms Weng Santos"
        gApprvd = "Mr Jon Syjuco/Ms Anne Gonzalez"
        frmStatementAcnt.Show()
    End Sub

    Private Sub checkvalidity()
        With Me.DataGridView1
            Dim xRow As Integer
            Dim sVal As Integer = 0
            For xRow = 0 To .Rows.Count - 1
                If Not .Rows(xRow).Cells(2).Value.ToString Is Nothing Then
                    If .Rows(xRow).Cells(1).Value = True Then
                        sVal = 1
                    Else
                        sVal = 0
                    End If
                    Call updateinvoice(.Rows(xRow).Cells(0).Value.ToString, sVal)
                End If
            Next
        End With
    End Sub

    Private Sub updateinvoice(ByVal psKeyInvoice As String, ByVal psSelected As Integer)
        Try
            Dim sSQLCmd As String = "UPDATE tInvoice SET fbCreateStatement ='" & psSelected & "' where fxKeyInvoice='" & psKeyInvoice & "' "
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub loadbillslist()
        DataGridView1.Columns.Clear()
        Dim ds As DataSet = m_GetInvoiceTransactions(gKeyCustomer)
        If ds IsNot Nothing Then
            With DataGridView1
                Dim dtBill As DataTable = ds.Tables(0)
                .DataSource = dtBill.DefaultView
                .Columns(0).Visible = False
                .Columns(1).Width = 60
                .Columns(2).Width = 100
                .Columns(3).Width = 100
                .Columns(4).Width = 150
            End With
        End If
    End Sub

End Class