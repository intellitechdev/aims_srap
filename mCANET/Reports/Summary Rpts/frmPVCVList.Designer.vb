<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPVCVList
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPVCVList))
        Me.grdList = New System.Windows.Forms.DataGridView
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.dteEnd = New System.Windows.Forms.DateTimePicker
        Me.Label2 = New System.Windows.Forms.Label
        Me.dtefrom = New System.Windows.Forms.DateTimePicker
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnGenList = New System.Windows.Forms.Button
        Me.btnPrint = New System.Windows.Forms.Button
        Me.chkAP = New System.Windows.Forms.RadioButton
        Me.chkCV = New System.Windows.Forms.RadioButton
        CType(Me.grdList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'grdList
        '
        Me.grdList.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdList.Location = New System.Drawing.Point(0, 79)
        Me.grdList.Name = "grdList"
        Me.grdList.Size = New System.Drawing.Size(703, 394)
        Me.grdList.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dteEnd)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.dtefrom)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.btnGenList)
        Me.GroupBox1.Location = New System.Drawing.Point(220, 7)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(473, 66)
        Me.GroupBox1.TabIndex = 7
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Transaction Date"
        '
        'dteEnd
        '
        Me.dteEnd.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteEnd.Location = New System.Drawing.Point(245, 28)
        Me.dteEnd.Name = "dteEnd"
        Me.dteEnd.Size = New System.Drawing.Size(95, 21)
        Me.dteEnd.TabIndex = 10
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(184, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "End Date"
        '
        'dtefrom
        '
        Me.dtefrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtefrom.Location = New System.Drawing.Point(83, 28)
        Me.dtefrom.Name = "dtefrom"
        Me.dtefrom.Size = New System.Drawing.Size(95, 21)
        Me.dtefrom.TabIndex = 8
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(15, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(66, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Start Date"
        '
        'btnGenList
        '
        Me.btnGenList.Location = New System.Drawing.Point(348, 27)
        Me.btnGenList.Name = "btnGenList"
        Me.btnGenList.Size = New System.Drawing.Size(109, 23)
        Me.btnGenList.TabIndex = 0
        Me.btnGenList.Text = "Generate List"
        Me.btnGenList.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.Location = New System.Drawing.Point(568, 477)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(109, 23)
        Me.btnPrint.TabIndex = 11
        Me.btnPrint.Text = "Print List"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'chkAP
        '
        Me.chkAP.AutoSize = True
        Me.chkAP.Location = New System.Drawing.Point(12, 18)
        Me.chkAP.Name = "chkAP"
        Me.chkAP.Size = New System.Drawing.Size(176, 17)
        Me.chkAP.TabIndex = 12
        Me.chkAP.TabStop = True
        Me.chkAP.Text = "Accounts Payable Voucher"
        Me.chkAP.UseVisualStyleBackColor = True
        '
        'chkCV
        '
        Me.chkCV.AutoSize = True
        Me.chkCV.Location = New System.Drawing.Point(12, 40)
        Me.chkCV.Name = "chkCV"
        Me.chkCV.Size = New System.Drawing.Size(112, 17)
        Me.chkCV.TabIndex = 13
        Me.chkCV.TabStop = True
        Me.chkCV.Text = "Check Voucher"
        Me.chkCV.UseVisualStyleBackColor = True
        '
        'frmPVCVList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(700, 502)
        Me.Controls.Add(Me.chkCV)
        Me.Controls.Add(Me.chkAP)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.grdList)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmPVCVList"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "List"
        CType(Me.grdList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents grdList As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnGenList As System.Windows.Forms.Button
    Friend WithEvents dteEnd As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dtefrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents chkAP As System.Windows.Forms.RadioButton
    Friend WithEvents chkCV As System.Windows.Forms.RadioButton
End Class
