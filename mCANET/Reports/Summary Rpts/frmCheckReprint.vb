Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmCheckReprint
    Private gMoneyConverter As New WPMs
    Dim tmpChkAmt As Decimal = 0.0
    Private Sub frmCheckReprint_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call loadgrid()
    End Sub
    Private Function getAmountinWords(ByVal AmountinNumbers As Integer)

    End Function
    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        If getUsersGroup(gUserName) = 3 Then
            If UCase(gCompanyID()) = "87D534F7-C698-4713-B433-FDB51F9A72E3" Then
                gRvwd = "Ms Weng Santos"
                gApprvd = "Mr Jon Syjuco/Ms Anne Gonzalez"
            End If
            
            Call eventpreview()
            frmCheckVoucher_Individual.ShowDialog()
        Else
            MsgBox("you don't have enought rights to preview the details, pls contact your system administrator to access this module...", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub eventpreview()
        With Me.grdCVPrint
            If .Rows.Count > 0 Then
                Try
                    Dim xRow As Integer = .CurrentRow.Index
                    '  Dim xRow As Integer = .CurrentRow.Index
                    Dim tmpDate As Date = CDate("01/01/1900")

                    Dim tmpChkwrd As String = ""
                    If mkDefaultValues(xRow, 3) <> Nothing Then
                        tmpDate = Microsoft.VisualBasic.FormatDateTime(.CurrentRow.Cells(3).Value, DateFormat.ShortDate)
                    End If
                    If mkDefaultValues(xRow, 5) <> Nothing Then
                        tmpChkAmt = .Rows(xRow).Cells(5).Value
                    End If
                    If mkDefaultValues(xRow, 6) <> Nothing Then
                        tmpChkwrd = .Rows(xRow).Cells(6).Value
                    End If
                    gItemsKeys = .CurrentRow.Cells(0).Value.ToString
                    gPayeeID = m_getname(.CurrentRow.Cells(2).Value, "Supplier")
                    gCheckNumber = .CurrentRow.Cells(4).Value
                    gCheckDate = tmpDate
                    gCheckAmt = tmpChkAmt
                    gCVno = .Rows(xRow).Cells(1).Value
                    gPreparedBy = get_preparedby(.CurrentRow.Cells(8).Value)
                    gCheckAmtwords = tmpChkwrd

                    frmCheckVoucher_Individual.OriginfrmAmount = tmpChkAmt
                    gCheckAmtwords = gMoneyConverter.gGenerateCurrencyInWords(tmpChkAmt)
                Catch ex As Exception
                End Try
            End If
        End With
    End Sub

    Private Function mkDefaultValues(ByVal irow As Integer, ByVal iCol As Integer)
        Return IIf(IsDBNull(grdCVPrint.Rows(irow).Cells(iCol).Value) = True, _
                        Nothing, grdCVPrint.Rows(irow).Cells(iCol).Value)
    End Function


    Private Sub loadgrid()
        With Me.grdCVPrint
            .DataSource = m_GetCheckVoucherHistory().Tables(0)

            .Columns("fcRefNo").HeaderText = "CV #"
            .Columns("fcPayee").HeaderText = "Payee"
            .Columns("fcChkDate").HeaderText = "Check Date"
            .Columns("fcChkNo").HeaderText = "Check #"
            .Columns("fcChkAmt").HeaderText = "Check Amount"
            .Columns("fcMemo").HeaderText = "Memo"
            .Columns("fuCreatedBy").HeaderText = "Prepared By"

            .Columns("fxKey").Visible = False
            .Columns("fcRefNo").ReadOnly = True
            .Columns("fcPayee").ReadOnly = True
            .Columns("fcChkDate").ReadOnly = True
            .Columns("fcChkNo").ReadOnly = True
            .Columns("fcChkAmt").ReadOnly = True
            .Columns("fcAmtWrds").Visible = False
            .Columns("fcMemo").ReadOnly = True
            .Columns("fuCreatedBy").Visible = False

            .Columns("fcRefNo").Width = 100
            .Columns("fcPayee").Width = 300
            .Columns("fcChkDate").Width = 100
            .Columns("fcChkNo").Width = 100
            .Columns("fcChkAmt").Width = 150
            .Columns("fcMemo").Width = 300

            .Columns("fcRefNo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopCenter

            .Columns("fcChkDate").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopCenter
            .Columns("fcChkDate").DefaultCellStyle.Format = "MM/dd/yyyy"

            .Columns("fcChkAmt").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            .Columns("fcChkAmt").DefaultCellStyle.Format = "##,##0.00"
        End With
    End Sub

    Private Sub grdCVPrint_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdCVPrint.CellContentClick

    End Sub
End Class