﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Public Class frmPOSEntry
    Private gCon As New Clsappconfiguration()
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring
    Dim coid As String
    Dim dv As New DataView
    Dim ds As New DataSet
    Private Sub frmPOSEntry_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ViewEntryHeader()
    End Sub
    Private Sub ViewEntryHeader()
        Dim co_id As New Guid(gCompanyID)
        ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "sp_ViewPOSEntryDate",
                                      New SqlParameter("@co_id", co_id))
        dv.Table = ds.Tables(0)

        dv = New DataView(ds.Tables(0))
        grdDetails.DataSource = dv

        With grdDetails
            .Columns("pkID").Visible = False
            .Columns("co_id").Visible = False
            .Columns("fcDescription").HeaderText = "Description"
            .Columns("fcDescription").Width = 200
            .Columns("dtDate").HeaderText = "Date"
            .Columns("dtDate").Width = 100
            .Columns("fbIsProcessed").HeaderText = ""
            .Columns("fbIsProcessed").ReadOnly = True
            .Columns("fbIsProcessed").Width = 50
            .Font = New Font("Verdana", 9, FontStyle.Regular)
        End With
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If grdDetails.SelectedRows(0).Cells(4).Value = True Then
            MsgBox("Entry is Already Posted!!!", MsgBoxStyle.Information)
        Else
            Try
                Dim Desc As String = grdDetails.SelectedRows(0).Cells(2).Value.ToString

                frmGeneralJournalEntries.keyID = grdDetails.SelectedRows(0).Cells(0).Value.ToString
                frmGeneralJournalEntries.dtpDatePrepared.Value = grdDetails.SelectedRows(0).Cells(3).Value.ToString
                frmGeneralJournalEntries.txtMemo.Text = Desc

                Me.Close()
            Catch ex As Exception

            End Try
            Me.DialogResult = Windows.Forms.DialogResult.OK
        End If

    End Sub

    Private Sub grdDetails_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdDetails.DoubleClick
        If grdDetails.SelectedRows(0).Cells(4).Value = True Then
            MsgBox("Entry is Already Posted!!!", MsgBoxStyle.Information)
        Else
            Try
                Dim Desc As String = grdDetails.SelectedRows(0).Cells(2).Value.ToString

                frmGeneralJournalEntries.keyID = grdDetails.SelectedRows(0).Cells(0).Value.ToString
                frmGeneralJournalEntries.dtpDatePrepared.Value = grdDetails.SelectedRows(0).Cells(3).Value.ToString
                frmGeneralJournalEntries.txtMemo.Text = Desc

                Me.Close()
            Catch ex As Exception

            End Try
            Me.DialogResult = Windows.Forms.DialogResult.OK
        End If

    End Sub

    Private Sub grdDetails_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdDetails.CellContentClick

    End Sub

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.Click

    End Sub
End Class