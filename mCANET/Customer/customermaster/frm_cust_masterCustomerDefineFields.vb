Public Class frm_cust_masterCustomerDefineFields

    Private Sub frm_vend_masterVendorDefinedFields_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Settings("")
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Settings("Add")
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Settings("Edit")
    End Sub

    Private Sub btnDeleteSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteSave.Click
        Settings("Delete")
    End Sub

    Private Sub Settings(ByVal sMode As String)
        Select Case sMode
            Case "Add"
                pnlGrid.Visible = False
                btnDeleteSave.Text = "Save"

                btnAdd.Enabled = False
                btnEdit.Enabled = False
            Case "Edit"
                pnlGrid.Visible = False
                btnDeleteSave.Text = "Save"

                btnAdd.Enabled = False
                btnEdit.Enabled = False
            Case "Delete"
                pnlGrid.Visible = True
                btnAdd.Enabled = True
                btnEdit.Enabled = True
                btnDeleteSave.Text = "Delete"
            Case Else
                pnlGrid.Visible = True
                btnAdd.Enabled = True
                btnEdit.Enabled = True
                btnDeleteSave.Text = "Delete"
        End Select

    End Sub


    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub
End Class