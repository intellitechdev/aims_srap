Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_cust_CreateCreditMemos
    Private gcon As New Clsappconfiguration
    Private cls As New clsTransactionFunctions

    Private Const cKeyCreditItem = 0
    Private Const cItem = 1
    Private Const cDescription = 2
    Private Const cQuantity = 3
    Private Const cUnit = 4
    Private Const cPrice = 5
    Private Const cAmount = 6
    Private Const cTax = 7

    Private sCoAddress As String
    Private sKeyCreditMemo As String
    Private sKeyCustomer As String
    Private sKeySalesTaxCode As String
    Private sKeyTax As String
    Private sBillAdd As String

    Private bRefund As Boolean
    Private dPercentage As Long = 0
    Private sKeyToDel() As String = {""}
    Private iDel As Integer = 0

    Private sKeySalesTaxCodeInitial As String
    Private sBillAddInitial As String
    Private dTotalInitial As Decimal
    Private dTaxAmntInitial As Decimal

    Private ClassRefNo As String = ""

    Dim checkDate As New clsRestrictedDate
    Private Sub finalizedForm()
        cls.gGridClear(Me.grdCreditMemo)
        cls.gClearFormTxt(Me)
        'grdCreditMemo.RowCount = 1
        'cls.gClearGrpTxt(Me)
        sKeyCreditMemo = ""
        gKeyPayAccount = ""
        gKeyTaxAccount = ""
        gKeySalesAccount = ""
        gKeyCOSAccount = ""
        gTaxPercent = "0.00"
        gKeyOutputTax = ""
        gOutputTaxPercent = "0.00"
        gKeyMerchAcnt = ""
        gKeyDiscount = ""



        ' Call xclose()
        'Me.refreshForm()

        'Me.Close()
        Me.Dispose()
    End Sub

    Public Property KeyCreditMemo() As String
        Get
            Return sKeyCreditMemo
        End Get
        Set(ByVal value As String)
            sKeyCreditMemo = value
        End Set
    End Property

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If validateAccountability("CM") = "Kulang" Then
            frmCreditAccnt.ShowDialog()
            Exit Sub
        Else
            finalizedForm()
        End If
    End Sub

    Private Sub xclose()
        cboSD.SelectedIndex = 0
        cboTaxName.SelectedIndex = 0
        cboTaxName.Text = ""
        cboRD.SelectedIndex = 0
        lblSDpercent.Text = "0.0"
        lblRDPercent.Text = "0.0"
        lblSaleDiscount.Text = "0.00"
        lblRetDiscount.Text = "0.00"
        lblBalance.Text = "0.00"
        lblPercentage.Text = "0.0"
    End Sub

  
    Private Sub frm_cust_CreateCreditMemos_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        'cls.gGridClear(Me.grdCreditMemo)
        'cls.gClearFormTxt(Me)
        ''cls.gClearGrpTxt(Me)
        'sKeyCreditMemo = ""
        'gKeyCredit = ""
        'Call xclose()
        finalizedForm()
    End Sub

    Private Function getjventryid() As String
        Dim stemp As String = ""
        Dim xCnt As Integer = 0
        Dim sstmp() As String
        Dim sSQLCmd As String

        Try
            sSQLCmd = "select fcCreditNo from tCreditMemo order by fcCreditNo desc"
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    If Not rd.Item(0).ToString Is Nothing Then
                        stemp = rd.Item(0).ToString
                        sstmp = Split(stemp, "-")
                        ' MsgBox(CStr(Format(CDec(Now.Month), "00")) + "     " + sstmp(1), MsgBoxStyle.Information)
                        stemp = "TSA" + "-" + "CM" + "-" + CStr(Format(CDec((CDec(sstmp(2)) + 1)), "000000"))
                    End If

                Else
                    stemp = "TSA" + "-" + "CM" + "-" + "000001"
                End If
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return stemp
    End Function


    Private Sub frm_cust_CreateCreditMemos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Refresh()
        Call loaddiscounts()
        If sKeyCreditMemo = "" Or sKeyCreditMemo = Nothing Then
            txtCreditNo.Text = getjventryid()
            sKeyCreditMemo = Guid.NewGuid.ToString
            refreshForm()
            createColumn()
            IsDateRestricted()
        Else
            refreshForm()
            createColumn()
            loadCreditMemo()
            IsDateRestricted()
        End If

        If frmMain.LblClassActivator.Visible = True Then
            Label2.Visible = True
            TxtClassName.Visible = True
            TxtClassName.Enabled = True
            BtnBrowse.Enabled = True
            BtnBrowse.Visible = True
        Else
            Label2.Visible = False
            TxtClassName.Visible = False
            TxtClassName.Enabled = False
            BtnBrowse.Enabled = False
            BtnBrowse.Visible = False
        End If
        If validateAccountability("CM") = "Kulang" Then
            frmCreditAccnt.ShowDialog()
        End If
    End Sub

    Private Sub refreshForm()
        Dim dtComp As DataRow = gCompanyInfo().Tables(0).Rows(0)
        sCoAddress = dtComp("co_name").ToString + _
                         vbCrLf + dtComp("co_street").ToString + _
                        " " + dtComp("co_city").ToString + _
                        vbCrLf + dtComp("co_zip").ToString

        m_loadCustomer(cboCustomerName, cboCustomerID)
        m_loadSalesTaxCode(cboTaxCodeName, cboTaxCodeID)
        m_loadTax(cboTaxName, cboTaxID)
        'loadCreditMemo()
        m_selectedCboValue(cboCustomerName, cboCustomerID, sKeyCustomer)
        m_selectedCboValue(cboTaxCodeName, cboTaxCodeID, sKeySalesTaxCodeInitial)
        m_selectedCboValue(cboTaxName, cboTaxID, sKeyTax)

        txtBillingAdd.Text = sBillAddInitial

        lblTotal.Text = Format(dTotalInitial, "#,##0.00")
        lblTaxAmt.Text = Format(dTaxAmntInitial, "#,##0.00")

        If bRefund = True Then
            lblRefund.Visible = True
        Else
            lblRefund.Visible = False
        End If

    End Sub

    Private Sub loaddiscounts()
        Call m_GetSalesDiscount(cboSD, Me)
        Call m_GetRetailerDiscount(cboRD, Me)
    End Sub
    
    Private Sub loadCreditMemo()
        Dim dtPO As DataSet = m_GetCreditMemo(sKeyCreditMemo)
        Using rd As DataTableReader = dtPO.CreateDataReader
            'Try
            If rd.Read Then
                sBillAddInitial = rd.Item("fcBillTo")
                txtCreditNo.Text = rd.Item("fcInvoiceNo")
                txtPONo.Text = rd.Item("fcPoNo")
                dteCreditMemo.Value = rd.Item("fdDateTransact")

                sKeyCustomer = rd.Item("fxKeyCustomer").ToString
                cboCustomerID.SelectedItem = sKeyCustomer
                cboCustomerName.SelectedIndex = cboCustomerID.SelectedIndex
                'sKeyTax = rd.Item("fxKeyTax").ToString
                'cboTaxName.SelectedText = rd.Item("fcTaxCode")
                ' sKeySalesTaxCodeInitial = rd.Item("fxKeySalesTaxCode").ToString
                'lblBalance1.Text = rd.Item("fdBalance")
                dTotalInitial = rd.Item("fdTotalAmt")
                dTaxAmntInitial = rd.Item("fdTaxAmt")
                cboTaxName.SelectedItem = rd.Item("fcTaxCode").ToString
                lblPercentage.Text = rd.Item("fdTaxPrcnt")
                lblTaxAmt.Text = rd.Item("fdTaxAmt")
                cboSD.SelectedItem = rd.Item("fcSDCode").ToString
                lblSDpercent.Text = rd.Item("fdSDPrcnt")
                lblSaleDiscount.Text = rd.Item("fdSDAmt")
                cboRD.SelectedItem = rd.Item("fcRDCode").ToString
                lblRDPercent.Text = rd.Item("fcRDPrcnt")
                lblRetDiscount.Text = rd.Item("fdRDAmt")
                lblBalance.Text = rd.Item("fdTotCredit")
                txtCustomerMsg.Text = rd.Item("fcMessage")
                txtMemo.Text = rd.Item("fcMemo")
                txtClassRefno.Text = rd.Item("fxClassRefno").ToString
                ClassRefNo = rd.Item("fxClassRefno").ToString
                TxtClassName.Text = GetClassName(ClassRefNo)
            Else
                sBillAddInitial = ""
                txtCreditNo.Text = defaultCreditNo()
                txtPONo.Text = ""
                dteCreditMemo.Value = Now

                sKeyCustomer = ""
                sKeyTax = ""
                sKeySalesTaxCodeInitial = ""
                lblBalance1.Text = "0.00"

                dTotalInitial = "0.00"
                dTaxAmntInitial = "0.00"
                bRefund = False
                txtCustomerMsg.Text = ""
                txtMemo.Text = ""
                TxtClassName.Text = "No Class Selected"
            End If
        End Using
        Call loadCreditItem(sKeyCreditMemo)
        'Catch ex As Exception
        '    MessageBox.Show(Err.Description, "Load Credit Memo")
        'End Try
    End Sub

    Private Sub createColumn()

        Me.grdCreditMemo.Columns.Clear()

        Dim colKeySRItem As New DataGridViewTextBoxColumn
        Dim colItem As New DataGridViewComboBoxColumn
        Dim colDescription As New DataGridViewTextBoxColumn
        Dim colQuantity As New DataGridViewTextBoxColumn
        Dim colUnit As New DataGridViewTextBoxColumn
        Dim colRate As New DataGridViewTextBoxColumn
        Dim colAmount As New DataGridViewTextBoxColumn
        Dim colSelect As New DataGridViewCheckBoxColumn
        Dim dtItem As DataTable = m_GetItem.Tables(0)
        'Dim dtSR As DataTable = loadCreditItem(sKeyCreditMemo).Tables(0)

        'Try

        colKeySRItem.Name = "cKeyItem"

        With colItem
            .Name = "cItem"
            .HeaderText = "Item"
            .DataPropertyName = "fcItemName"
            .DataSource = dtItem.DefaultView
            .DisplayMember = "fcItemName"
            .ValueMember = "fcItemName"
            .DropDownWidth = 250
            .DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
        End With

        colDescription.HeaderText = "Description"
        colDescription.Name = "cDescription"


        colQuantity.HeaderText = "Returned"
        colQuantity.Name = "cQty"




        colRate.HeaderText = "Price"
        colRate.Name = "cPrice"


        colAmount.HeaderText = "Amount"
        colAmount.Name = "cAmount"

        With colSelect
            .HeaderText = "Select"
            .Name = "cSelect"
        End With


        With Me.grdCreditMemo
            .Columns.Clear()

            .Columns.Add(colKeySRItem)
            .Columns.Add(colItem)
            .Columns.Add(colDescription)
            .Columns.Add(colQuantity)
            '.Columns.Add(colUnit)
            .Columns.Add(colRate)
            .Columns.Add(colAmount)
            .Columns.Add(colSelect)


            .Columns("cQty").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopCenter
            .Columns("cPrice").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            .Columns("cAmount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight

            .Columns("cPrice").DefaultCellStyle.BackColor = Color.Gainsboro
            .Columns("cPrice").DefaultCellStyle.Format = "##,##0.0000"

            .Columns("cAmount").DefaultCellStyle.BackColor = Color.Gainsboro
            .Columns("cAmount").DefaultCellStyle.Format = "##,##0.0000"

            .Columns("cKeyItem").Visible = False
            .Columns(cUnit).ReadOnly = True
            .Columns("cSelect").Width = 50

            .Columns("cItem").Width = 285
            .Columns("cDescription").Width = 150
            .Columns("cQty").Width = 80
            .Columns("cPrice").Width = 120
            .Columns("cAmount").Width = 120
            '  .Columns("cSelect").Width = 50
        End With

        'Catch ex As Exception
        '    MessageBox.Show(Err.Description, "Load Credit Memos Item")
        'End Try

    End Sub

    Private Sub loadCreditItem(ByVal sKey As String)
        Dim sSQLCmd As String = "usp_t_creditMemo_items "
        sSQLCmd &= " @fxKeyCredit='" & sKey & "' "
        With Me.grdCreditMemo
            .Rows.Clear()
            'Try
            Dim dtsTable As DataSet = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSQLCmd)
            Dim xRow As Integer = 0
            For xRow = 0 To dtsTable.Tables(0).Rows.Count - 1
                .Rows.Add()
                .Item("cKeyItem", xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("fxKeyCreditItem").ToString
                .Item("cQty", xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("fdQty")
                .Item("cDescription", xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("fcDescription")
                .Item("cItem", xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("cItem").ToString

                .Item("cPrice", xRow).Value = Format(CDec(dtsTable.Tables(0).Rows(xRow).Item("fdPrice")), "##,##0.00##")
                .Item("cAmount", xRow).Value = Format(CDec(dtsTable.Tables(0).Rows(xRow).Item("fdAmnt")), "##,##0.00##")
            Next
            'MsgBox(.Rows(0).Cells("cItem").Value.ToString, MsgBoxStyle.Information)
            'Catch ex As Exception
            '    MessageBox.Show(Err.Description, "Load Credit Items")
            'End Try
        End With
    End Sub


    Private Function defaultCreditNo() As String
        Dim sDefault As String = "1"
        Dim sPONoFetch As String = ""
        Dim sPONo() As String = {""}
        Dim sPONosTemp As String = ""
        Dim i As Integer = 0
        Dim sSCLCmd As String

        Try
            sSCLCmd = "SELECT fcInvoiceNo FROM tInvoice ORDER BY fcInvoiceNo"
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSCLCmd)
                While rd.Read
                    If IsNumeric(rd.Item("fcInvoiceNo")) Then
                        sPONoFetch = rd.Item("fcInvoiceNo")
                        sPONosTemp &= sPONoFetch & " "
                    End If
                End While
            End Using

            sPONo = sPONosTemp.Split
            If UBound(sPONo) > 0 Then
                For i = 0 To UBound(sPONo)
                    If CStr(i + 1) <> sPONo(i) Then
                        If i > 0 Then
                            sDefault = CStr(CInt(sPONo(i - 1) + 1))
                        End If
                        Exit For
                    End If
                Next

            End If
            Return sDefault

        Catch ex As Exception
            MessageBox.Show(Err.Description, "Credit No.")
            Return sDefault
        End Try
    End Function

    Private Sub cboCustomerName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCustomerName.SelectedIndexChanged
        cboCustomerID.SelectedIndex = cboCustomerName.SelectedIndex
        sKeyCustomer = cboCustomerID.SelectedItem

        If cboCustomerName.SelectedIndex = 1 Then
            frm_cust_masterCustomerAddEdit.MdiParent = Me.MdiParent
            frm_cust_masterCustomerAddEdit.KeyCustomer = Guid.NewGuid.ToString
            frm_cust_masterCustomerAddEdit.KeyAddress = Guid.NewGuid.ToString
            frm_cust_masterCustomerAddEdit.IsNew = True
            frm_cust_masterCustomerAddEdit.Text = "Add Customer"
            frm_cust_masterCustomerAddEdit.Show()
        End If

        If sKeyCustomer <> "" Then
            Dim dtCustomer As DataTable = m_GetCustomer(sKeyCustomer).Tables(0)
            txtBillingAdd.Text = dtCustomer.Rows(0)("fcBillingAddress").ToString

            cboTaxCodeID.SelectedItem = dtCustomer.Rows(0)("fxKeySalesTaxCode").ToString
            cboTaxCodeName.SelectedIndex = cboTaxCodeID.SelectedIndex
        End If
    End Sub

    Private Sub cboTaxCodeName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTaxCodeName.SelectedIndexChanged
        cboTaxCodeID.SelectedIndex = cboTaxCodeName.SelectedIndex
        sKeySalesTaxCode = cboTaxCodeID.SelectedItem

        Call total()

        If cboTaxCodeName.SelectedIndex = 1 Then
            frm_vend_SalesTaxAdd.Text = "New Sales Tax Code"
            frm_vend_SalesTaxAdd.KeySalesTaxCode = Guid.NewGuid.ToString
            frm_vend_SalesTaxAdd.Show()
        End If
    End Sub

    Private Function isTaxable(ByVal bCustomer As Boolean, _
    Optional ByVal iRow As Integer = 0) As Boolean
        Dim bTaxable As Boolean
        If bCustomer Then
            If sKeySalesTaxCode <> "" Then
                bTaxable = m_isTaxable(sKeySalesTaxCode)
            Else
                bTaxable = False
            End If
        Else
            Dim sKey As String
            Dim sName As String
            sName = grdCreditMemo.Item(cTax, iRow).Value
            sKey = m_getSalesTaxCodeByName(sName).ToString
            bTaxable = m_isTaxable(sKey)
        End If
        Return bTaxable
    End Function

    Private Sub cboTaxName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTaxName.SelectedIndexChanged
        cboTaxID.SelectedIndex = cboTaxName.SelectedIndex
        sKeyTax = cboTaxID.SelectedItem.ToString

        If sKeyTax <> "" Then
            Dim dtTax As DataSet = m_GetTax(sKeyTax)
            dPercentage = dtTax.Tables(0).Rows(0)("fdTaxRate")
        Else
            dPercentage = 0
        End If
        lblPercentage.Text = Format(CDec(dPercentage), "0.00")

        '  Try
        Call total()
        'Catch ex As Exception
        'End Try

        If cboTaxName.SelectedIndex = 1 Then
            frm_MF_taxAddEdit.Text = "New Sales Tax"
            frm_MF_taxAddEdit.KeyTax = Guid.NewGuid.ToString
            frm_MF_taxAddEdit.Show()
        End If
        LblSDAcntAmt.Text = ComputeSalesDiscountAmountForGL(Format(CDec(m_GetRetailersDiscountPercentage(cboRD.SelectedItem, Me)), "##0.0#"), Format(CDec(dPercentage), "0.00"), lblSaleDiscount.Text)
    End Sub

    'Private Sub grdCreditMemo_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles grdCreditMemo.CellPainting
    '    Dim sf As New StringFormat
    '    sf.Alignment = StringAlignment.Center
    '    If e.ColumnIndex < 0 AndAlso e.RowIndex >= 0 AndAlso e.RowIndex < grdCreditMemo.Rows.Count Then
    '        e.PaintBackground(e.ClipBounds, True)
    '        e.Graphics.DrawString((e.RowIndex + 1).ToString, Me.Font, Brushes.Black, e.CellBounds, sf)
    '        e.Handled = True
    '    End If
    'End Sub

    Private Sub grdCreditMemo_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdCreditMemo.CellValueChanged
        If gEditEvent <> 1 Then
            If grdCreditMemo.RowCount > 1 Then
                If e.RowIndex >= 0 Then
                    Try
                        If grdCreditMemo.CurrentRow.Cells("cItem").Value IsNot Nothing Then
                            Dim sItemTemp As String = grdCreditMemo.CurrentRow.Cells("cItem").Value.ToString
                            Dim sKeyItemTemp As String = m_GetItemByName(sItemTemp)
                            Dim dtItemTemp As DataTable = m_GetItem(sKeyItemTemp).Tables(0)
                            Dim dQuantity As Long = 0
                            Dim dAmount As String
                            Dim dRate As Decimal = 0
                            If mkDefaultValues(grdCreditMemo.CurrentRow.Index, 3) <> Nothing Then
                                dQuantity = grdCreditMemo.CurrentRow.Cells("cQty").Value
                            End If
                            If mkDefaultValues(grdCreditMemo.CurrentRow.Index, 5) <> Nothing Then
                                dRate = grdCreditMemo.CurrentRow.Cells("cPrice").Value
                            End If

                            If e.ColumnIndex = cItem Then
                                If sItemTemp <> "" Then
                                    Try
                                        grdCreditMemo.CurrentRow.Cells("cDescription").Value = dtItemTemp.Rows(0)("fcItemDescription")
                                        grdCreditMemo.CurrentRow.Cells("cPrice").Value = dtItemTemp.Rows(0)("fdPrice")
                                    Catch
                                        MessageBox.Show(Err.Description, "Credit Item")
                                    End Try
                                End If
                            End If
                            If e.ColumnIndex = cQuantity Then
                                Try
                                    dAmount = CDec(grdCreditMemo.CurrentRow.Cells("cQty").Value) * CDec(grdCreditMemo.CurrentRow.Cells("cPrice").Value)
                                    grdCreditMemo.CurrentRow.Cells("cAmount").Value = CDec(dAmount.ToString)
                                Catch
                                    MessageBox.Show(Err.Description, "Data Validation")
                                End Try

                            End If

                            Call total()
                        End If
                    Catch ex As Exception
                        MessageBox.Show(Err.Description, "Credit Items")
                    End Try
                End If
            End If
        End If
    End Sub

    Private Function mkDefaultValues(ByVal irow As Integer, ByVal iCol As Integer)
        Return IIf(IsDBNull(grdCreditMemo.Rows(irow).Cells(iCol).Value) = True, _
                        Nothing, grdCreditMemo.Rows(irow).Cells(iCol).Value)

    End Function

    Private Sub grdCreditMemo_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles grdCreditMemo.EditingControlShowing
        Dim comboBoxItem As DataGridViewComboBoxColumn = grdCreditMemo.Columns("cItem")
        If (grdCreditMemo.CurrentCellAddress.X = comboBoxItem.DisplayIndex) Then
            Dim cb As ComboBox = e.Control
            If (cb IsNot Nothing) Then
                cb.DropDownStyle = ComboBoxStyle.DropDown
                cb.AutoCompleteMode = AutoCompleteMode.Suggest
                cb.AutoCompleteSource = AutoCompleteSource.ListItems
            End If
        End If
    End Sub

    Private Sub grdCreditMemo_UserDeletedRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowEventArgs) Handles grdCreditMemo.UserDeletedRow
        Call total()
    End Sub

    Private Sub grdCreditMemo_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles grdCreditMemo.UserDeletingRow
        If grdCreditMemo.Rows.Count > 0 Then
            If MessageBox.Show("Are you sure you want to delete this record?", "Validation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                ReDim Preserve sKeyToDel(iDel)
                sKeyToDel(iDel) = e.Row.Cells(cKeyCreditItem).Value.ToString
                deleteCMItem_revised(sKeyToDel(iDel))
                iDel += 1
            Else
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub btnAccountability_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccountability.Click
        Call frmCreditAccnt.ShowDialog()
        'sKeyCreditMemo = ""
        'Me.Refresh()
        'iDel = 0
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If validateAccountability("CM") = "Kulang" Then
            frmCreditAccnt.ShowDialog()
            Exit Sub
        Else
            If cboCustomerID.SelectedItem = "" Then
                MessageBox.Show("Customer field cannot be empty!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            ElseIf grdCreditMemo.Rows.Count <= 1 Then
                MessageBox.Show("Item field cannot be empty!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Else
                Call updateCreditMemo()
                If updateCreditItem() = True Then
                    MsgBox("saving successful...", MsgBoxStyle.Information, Me.Text)
                End If
            End If
        End If
    End Sub

    Private Sub updateCreditMemo()
        'Try
        Dim sSQLCmd As String = "usp_t_creditmemo_save "
        sSQLCmd &= " @fxKeyCredit ='" & sKeyCreditMemo & "' "
        sSQLCmd &= ",@fcCreditNo ='" & txtCreditNo.Text & "' "
        sSQLCmd &= ",@fxKeyCustomer ='" & sKeyCustomer & "' "
        sSQLCmd &= ",@fdTransDate ='" & Microsoft.VisualBasic.FormatDateTime(dteCreditMemo.Value, DateFormat.ShortDate) & "' "
        sSQLCmd &= ",@fcPONo ='" & txtPONo.Text & "' "
        sSQLCmd &= ",@fcMessage ='" & txtCustomerMsg.Text & "' "
        sSQLCmd &= ",@fcMemo ='" & txtMemo.Text & "' "
        sSQLCmd &= ",@fcTaxCode ='" & cboTaxName.SelectedItem & "' "
        sSQLCmd &= ",@fdTaxPrcnt='" & CDec(lblPercentage.Text) & "' "
        sSQLCmd &= ",@fdTaxAmt ='" & CDec(lblTaxAmt.Text) & "' "
        sSQLCmd &= ",@fdTotalAmt ='" & CDec(lblTotal.Text) & "' "
        sSQLCmd &= ",@fcSDCode='" & cboSD.SelectedItem & "' "
        sSQLCmd &= ",@fdSDPrcnt='" & CDec(lblSDpercent.Text) & "' "
        sSQLCmd &= ",@fdSDAmt='" & CDec(lblSaleDiscount.Text) & "' "
        sSQLCmd &= ",@fcRDCode='" & cboRD.SelectedItem & "' "
        sSQLCmd &= ",@fdRDPrcnt='" & CDec(lblRDPercent.Text) & "' "
        sSQLCmd &= ",@fdRDAmt='" & CDec(lblRetDiscount.Text) & "' "
        sSQLCmd &= ",@fdTotCredit ='" & lblBalance.Text & "' "
        sSQLCmd &= ",@fxKeyCompany = '" & gCompanyID() & "' "
        sSQLCmd &= ",@fuCreatedBy='" & gUserName & "' "
        sSQLCmd &= ",@fxKeyPayAcnt =" & IIf(gKeyPayAccount = "", "NULL", "'" & gKeyPayAccount & "'") & " "
        sSQLCmd &= ",@fxKeyTaxCrdAcnt =" & IIf(gKeyTaxAccount = "", "NULL", "'" & gKeyTaxAccount & "'") & " "
        sSQLCmd &= ",@fxKeySalesAcnt =" & IIf(gKeySalesAccount = "", "NULL", "'" & gKeySalesAccount & "'") & " "
        sSQLCmd &= ",@fdTaxCrdPrcnt ='" & gTaxPercent & "' "
        sSQLCmd &= ",@fxKeyOutputTax =" & IIf(gKeyOutputTax = "", "NULL", "'" & gKeyOutputTax & "'") & " "
        sSQLCmd &= ",@fdOutputTaxPrcnt ='" & gOutputTaxPercent & "' "
        sSQLCmd &= ",@fxKeyCOSAcnt=" & IIf(gKeyCOSAccount = "", "NULL", "'" & gKeyCOSAccount & "'") & " "
        sSQLCmd &= ",@fxKeyMerAcnt=" & IIf(gKeyMerchAcnt = "", "NULL", "'" & gKeyMerchAcnt & "'") & " "
        sSQLCmd &= ",@fxKeySalesDiscount=" & IIf(gKeyDiscount = "", "NULL", "'" & gKeyDiscount & "'") & " "
        sSQLCmd &= ",@fdSDAcntAmt='" & CDec(LblSDAcntAmt.Text) & "' "
        sSQLCmd &= ",@fxClassRefno ='" & ClassRefNo & "' "
        SqlHelper.ExecuteScalar(gcon.cnstring, CommandType.Text, sSQLCmd)
        'Catch ex As Exception
        '    MessageBox.Show(Err.Description, "Add/Edit Memo")
        '    ' Me.Close()
        'End Try
    End Sub

    Private Function updateCreditItem() As Boolean
        Dim iRow As Integer
        Dim sKeySRItem As String
        Dim sItem As String = ""
        Dim sDescription As String = ""
        Dim dQuantity As Long = 0
        Dim dRate As Decimal = 0.0
        Dim dAmount As Decimal = 0.0
        Dim sKeyTaxCode As String = ""
        'Try
        If Me.grdCreditMemo.Rows.Count >= 1 Then
            For iRow = 0 To (grdCreditMemo.RowCount - 2)
                If mkDefaultValues(iRow, 0) <> Nothing Then
                    sKeySRItem = grdCreditMemo.Rows(iRow).Cells("cKeyItem").Value.ToString
                Else
                    sKeySRItem = Guid.NewGuid.ToString
                    'this will prevent double items in credit memo
                    grdCreditMemo.Rows(iRow).Cells("cKeyItem").Value = sKeySRItem
                End If
                If mkDefaultValues(iRow, 1) <> Nothing Then
                    sItem = grdCreditMemo.Rows(iRow).Cells("cItem").Value.ToString
                    sItem = m_GetItemByName(sItem)
                End If
                If mkDefaultValues(iRow, 2) <> Nothing Then
                    sDescription = grdCreditMemo.Rows(iRow).Cells("cDescription").Value
                End If
                If mkDefaultValues(iRow, 3) <> Nothing Then
                    dQuantity = grdCreditMemo.Rows(iRow).Cells("cQty").Value
                Else
                    dQuantity = 0
                End If
                If mkDefaultValues(iRow, 4) <> Nothing Then
                    dRate = grdCreditMemo.Rows(iRow).Cells("cPrice").Value
                Else
                    dRate = 0
                End If
                If mkDefaultValues(iRow, 5) <> Nothing Then
                    dAmount = grdCreditMemo.Rows(iRow).Cells("cAmount").Value
                Else
                    dAmount = 0
                End If

                Dim sSQLCmd As String = "usp_t_creditmemoitems_save "
                sSQLCmd &= " @fxKeyCompany='" & gCompanyID() & "' "
                sSQLCmd &= ",@fxKeyCredit='" & sKeyCreditMemo & "' "
                sSQLCmd &= ",@fxKeyCreditItem='" & sKeySRItem & "' "
                sSQLCmd &= ",@fxKeyItem='" & sItem & "' "
                sSQLCmd &= ",@fcDescription='" & sDescription & "' "
                sSQLCmd &= ",@fdQty='" & dQuantity & "' "
                sSQLCmd &= ",@fdPrice='" & dRate & "' "
                sSQLCmd &= ",@fdAmt='" & dAmount & "' "
                sSQLCmd &= ",@fuCreatedBy='" & gUserName & "' "
                SqlHelper.ExecuteScalar(gcon.cnstring, CommandType.Text, sSQLCmd)
            Next
            Return True
        End If
        'Catch ex As Exception
        '    ' MessageBox.Show(Err.Description, "Add/Edit Credit Items")
        '    Return False
        'End Try
    End Function

    Private Sub updateBillAddress()
        Dim sSQLCmd As String = "UPDATE mAddress SET fcAddress = '" & txtBillingAdd.Text & "' "
        sSQLCmd &= "WHERE fxKeyID = '" & sKeyCustomer & "' AND "
        sSQLCmd &= "fbCustomer = 1 AND fcAddressName = ''"
        Try
            SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Update Billing Address")
        End Try
    End Sub

    Private Sub lblTotal_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblTotal.TextChanged
        'Try
        If IsNumeric(lblBalance1.Text) And IsNumeric(lblTotal.Text) Then ' And IsNumeric(lblPayments.Text) Then
            calculatedueamount()
            lblBalance1.Text = FormatNumber(lblBalance1.Text, 2, , , TriState.False)
        End If
        'Catch ex As Exception
        'End Try
    End Sub

    Private Sub cboSD_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSD.SelectedIndexChanged
        If cboSD.SelectedIndex = 1 Then
            frmSalesDiscount.ShowDialog()
        ElseIf cboSD.SelectedIndex > 2 Then
            lblSDpercent.Text = Format(CDec(m_GetSalesDiscountPercentage(cboSD.SelectedItem, Me)), "##0.0#")
            Call computesalesdiscount()
        ElseIf cboSD.SelectedIndex = 0 Then
            lblSaleDiscount.Text = "0.00"
            lblSDpercent.Text = "0.0"
            Call calculatedueamount()
        End If
        LblSDAcntAmt.Text = ComputeSalesDiscountAmountForGL(Format(CDec(m_GetRetailersDiscountPercentage(cboRD.SelectedItem, Me)), "##0.0#"), Format(CDec(dPercentage), "0.00"), lblSaleDiscount.Text)
    End Sub

    Private Sub computesalesdiscount()
        If lblTotal.Text <> "0.00" Then
            lblSaleDiscount.Text = Format(CDec(lblTotal.Text * (CDec(m_GetSalesDiscountPercentage(cboSD.SelectedItem, Me)) / 100)), "##,##0.00")
        Else
            lblSaleDiscount.Text = "0.00"
        End If
    End Sub

    Private Sub calculatedueamount()
        'lblBalance.Text = CDec(lblTotal.Text) - (CDec(IIf(lblSaleDiscount.Text = "", 0, lblSaleDiscount.Text)) + CDec(lblRetDiscount.Text)) '+ CDec(lblPayments.Text))
        lblBalance.Text = CDec(IIf(lblTotal.Text = "", 0, lblTotal.Text)) - (CDec(IIf(lblSaleDiscount.Text = "", 0, lblSaleDiscount.Text)) + CDec(IIf(lblRetDiscount.Text = "", 0, lblRetDiscount.Text))) '+ CDec(lblPayments.Text))
    End Sub

    Private Sub cboRD_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboRD.SelectedIndexChanged
        If cboRD.SelectedIndex = 1 Then
            frmRetailersDiscount.ShowDialog()
        ElseIf cboRD.SelectedIndex > 2 Then
            lblRDPercent.Text = Format(CDec(m_GetRetailersDiscountPercentage(cboRD.SelectedItem, Me)), "##0.0#")
            Call computeretailersdiscount()
        ElseIf cboRD.SelectedIndex = 0 Then
            lblRetDiscount.Text = "0.00"
            lblRDPercent.Text = "0.0"
            Call calculatedueamount()
        End If
        LblSDAcntAmt.Text = ComputeSalesDiscountAmountForGL(Format(CDec(m_GetRetailersDiscountPercentage(cboRD.SelectedItem, Me)), "##0.0#"), Format(CDec(dPercentage), "0.00"), lblSaleDiscount.Text)
    End Sub

    Private Sub computeretailersdiscount()
        If lblTotal.Text <> "0.00" Then
            lblRetDiscount.Text = Format(CDec(CDec(lblTotal.Text) - CDec(lblSaleDiscount.Text)) * (CDec(m_GetRetailersDiscountPercentage(cboRD.SelectedItem, Me)) / 100), "##,##0.00")
        Else
            lblRetDiscount.Text = "0.00"
        End If
    End Sub

    'Private Sub lblTaxAmt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblTaxAmt.TextChanged
    '    'Try
    '    Call total()
    '    ''Catch ex As Exception
    '    ''End Try
    'End Sub

    Private Sub lblSaleDiscount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblSaleDiscount.TextChanged
        'Try
        Call calculatedueamount()
        'Catch ex As Exception
        'End Try
    End Sub

    Private Sub lblRetDiscount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblRetDiscount.TextChanged
        'Try
        Call calculatedueamount()
        'Catch ex As Exception
        'End Try
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        frmcreditmemo.ShowDialog()
    End Sub

    Private Sub ts_Preview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_Preview.Click
        frmcreditmemo.ShowDialog()
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Call deleteCMItem()
        Call loadCreditItem(sKeyCreditMemo)
        Call total()
    End Sub

    Private Sub deleteCMItem()
        Dim xCnt As Integer
        If MsgBox("Are you sure to delete the selected item?", MsgBoxStyle.OkCancel, Me.Text) = MsgBoxResult.Ok Then
            With Me.grdCreditMemo
                ' Try
                If .RowCount > 1 Then
                    For xCnt = 0 To .RowCount - 1
                        If .Rows(xCnt).Cells("cSelect").Value = True Then
                            If mkDefaultValues(xCnt, 1) <> Nothing Then
                                If sKeyCreditMemo <> "" Then
                                    'If .Rows(xCnt).Cells("cKeyItem").Value IsNot Nothing Then
                                    Call m_deleteCreditItem(.Rows(xCnt).Cells("cKeyItem").Value.ToString, Me)
                                    ' .Rows.Remove(.Rows(xCnt))
                                    'End If
                                End If
                            End If
                        End If
                    Next
                End If

                ' Call total()

                ' Catch ex As Exception
                '    ' MessageBox.Show(Err.Description, "Delete Credit Memo")
                ' End Try

            End With
        End If
    End Sub
    Private Sub deleteCMItem_revised(ByVal fxKeyCreditItem As String)
        Dim commandString As String = "DELETE FROM tCreditMemo_Item WHERE fxKeyCreditItem = '" & fxKeyCreditItem & "'"
        Try
            SqlHelper.ExecuteScalar(gcon.cnstring, CommandType.Text, commandString)
            MessageBox.Show("Record Successfully Deleted!", "Delete Credit Memo Item")
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Delete CM Item")
        End Try
    End Sub

    Private Sub total()
        'Try
        'Call computeTaxAmt()

        If grdCreditMemo.RowCount > 1 Then
            With Me.grdCreditMemo
                Dim xCnt As Integer = 0
                Dim xTotal As Decimal = 0
                For xCnt = 0 To .RowCount - 1
                    If .Item("cDescription", xCnt).Value IsNot Nothing Then
                        xTotal += CDec(.Item("cAmount", xCnt).Value)
                    End If
                Next
                lblTaxAmt.Text = Format(CDec(xTotal * (CDec(lblPercentage.Text) / 100)), "#,##0.00")
                lblTotal.Text = Format(CDec(xTotal + CDec(lblTaxAmt.Text)), "#,##0.00")

            End With
        Else
            lblTaxAmt.Text = Format(0, "#,##0.00")
            lblTotal.Text = Format(0, "#,##0.00")
        End If
        Call computeretailersdiscount()

        'Catch ex As Exception
        'End Try
    End Sub

    'Private Sub computeTaxAmt()
    '    If grdCreditMemo.RowCount > 1 Then
    '        With Me.grdCreditMemo
    '            Dim xCnt As Integer = 0
    '            Dim xTotal As Decimal = 0
    '            For xCnt = 0 To .RowCount - 1
    '                If .Item("cDescription", xCnt).Value IsNot Nothing Then

    '                    xTotal += CDec(.Item("cAmount", xCnt).Value)
    '                End If
    '            Next
    '            lblTaxAmt.Text = Format(CDec(xTotal * (CDec(lblPercentage.Text) / 100)), "##,##0.00")
    '        End With
    '    End If
    'End Sub

    Private Sub IsDateRestricted()
        'Validate Date
        If checkDate.checkIfRestricted(dteCreditMemo.Value) Then
            btnSave.Enabled = False 'true if date is NOT within range of period restricted
            btnAccountability.Enabled = False
        Else
            btnSave.Enabled = True 'false if date is within range of period restricted
            btnAccountability.Enabled = True
        End If
    End Sub

    Private Sub dteCreditMemo_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteCreditMemo.ValueChanged
        IsDateRestricted()
    End Sub

    Private Sub BtnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnBrowse.Click
        FrmBrowseClass.Mode = "BROWSE MODE"
        FrmBrowseClass.ShowDialog()
        txtClassRefno.Text = FrmBrowseClass.Data2
        TxtClassName.Text = FrmBrowseClass.Data1
        ClassRefNo = txtClassRefno.Text
    End Sub
End Class