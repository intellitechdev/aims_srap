<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAccountability
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAccountability))
        Me.Label24 = New System.Windows.Forms.Label
        Me.txtPercent = New System.Windows.Forms.TextBox
        Me.cboTaxAcnt = New System.Windows.Forms.ComboBox
        Me.Label23 = New System.Windows.Forms.Label
        Me.cboPayAcnt = New System.Windows.Forms.ComboBox
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboSalesAcnt = New System.Windows.Forms.ComboBox
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtPer1 = New System.Windows.Forms.TextBox
        Me.cboOutputTax = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.cboCOS = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.cboMerch = New System.Windows.Forms.ComboBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cboDiscount = New System.Windows.Forms.ComboBox
        Me.lblDiscount = New System.Windows.Forms.Label
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(413, 79)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(19, 13)
        Me.Label24.TabIndex = 7
        Me.Label24.Text = "%"
        Me.Label24.Visible = False
        '
        'txtPercent
        '
        Me.txtPercent.Location = New System.Drawing.Point(371, 76)
        Me.txtPercent.Name = "txtPercent"
        Me.txtPercent.Size = New System.Drawing.Size(40, 21)
        Me.txtPercent.TabIndex = 6
        Me.txtPercent.Text = "0.00"
        Me.txtPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPercent.WordWrap = False
        '
        'cboTaxAcnt
        '
        Me.cboTaxAcnt.DropDownWidth = 500
        Me.cboTaxAcnt.FormattingEnabled = True
        Me.cboTaxAcnt.Location = New System.Drawing.Point(171, 76)
        Me.cboTaxAcnt.MaxDropDownItems = 25
        Me.cboTaxAcnt.Name = "cboTaxAcnt"
        Me.cboTaxAcnt.Size = New System.Drawing.Size(199, 21)
        Me.cboTaxAcnt.TabIndex = 5
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(13, 79)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(116, 13)
        Me.Label23.TabIndex = 4
        Me.Label23.Text = "Tax Credit Account"
        '
        'cboPayAcnt
        '
        Me.cboPayAcnt.DropDownWidth = 500
        Me.cboPayAcnt.FormattingEnabled = True
        Me.cboPayAcnt.Location = New System.Drawing.Point(171, 51)
        Me.cboPayAcnt.MaxDropDownItems = 25
        Me.cboPayAcnt.Name = "cboPayAcnt"
        Me.cboPayAcnt.Size = New System.Drawing.Size(199, 21)
        Me.cboPayAcnt.TabIndex = 3
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(13, 54)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(152, 13)
        Me.Label20.TabIndex = 2
        Me.Label20.Text = "Debit Account Receivable"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(114, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Credit Gross Sales"
        '
        'cboSalesAcnt
        '
        Me.cboSalesAcnt.DropDownWidth = 500
        Me.cboSalesAcnt.FormattingEnabled = True
        Me.cboSalesAcnt.Location = New System.Drawing.Point(171, 24)
        Me.cboSalesAcnt.MaxDropDownItems = 25
        Me.cboSalesAcnt.Name = "cboSalesAcnt"
        Me.cboSalesAcnt.Size = New System.Drawing.Size(199, 21)
        Me.cboSalesAcnt.TabIndex = 1
        '
        'btnClose
        '
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.Location = New System.Drawing.Point(283, 209)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(87, 25)
        Me.btnClose.TabIndex = 19
        Me.btnClose.Text = "Close"
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Image = Global.CSAcctg.My.Resources.Resources.floppy
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.Location = New System.Drawing.Point(187, 209)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(90, 25)
        Me.btnSave.TabIndex = 18
        Me.btnSave.Text = "S&ave "
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(413, 106)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(19, 13)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "%"
        '
        'txtPer1
        '
        Me.txtPer1.Location = New System.Drawing.Point(371, 103)
        Me.txtPer1.Name = "txtPer1"
        Me.txtPer1.Size = New System.Drawing.Size(40, 21)
        Me.txtPer1.TabIndex = 10
        Me.txtPer1.Text = "0.00"
        Me.txtPer1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboOutputTax
        '
        Me.cboOutputTax.DropDownWidth = 500
        Me.cboOutputTax.FormattingEnabled = True
        Me.cboOutputTax.Location = New System.Drawing.Point(171, 102)
        Me.cboOutputTax.MaxDropDownItems = 25
        Me.cboOutputTax.Name = "cboOutputTax"
        Me.cboOutputTax.Size = New System.Drawing.Size(199, 21)
        Me.cboOutputTax.TabIndex = 9
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(13, 106)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(109, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Credit Output Tax"
        '
        'cboCOS
        '
        Me.cboCOS.DropDownWidth = 500
        Me.cboCOS.FormattingEnabled = True
        Me.cboCOS.Location = New System.Drawing.Point(171, 128)
        Me.cboCOS.MaxDropDownItems = 25
        Me.cboCOS.Name = "cboCOS"
        Me.cboCOS.Size = New System.Drawing.Size(199, 21)
        Me.cboCOS.TabIndex = 13
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(13, 132)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(67, 13)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "Debit COS"
        '
        'cboMerch
        '
        Me.cboMerch.DropDownWidth = 500
        Me.cboMerch.FormattingEnabled = True
        Me.cboMerch.Location = New System.Drawing.Point(171, 155)
        Me.cboMerch.MaxDropDownItems = 25
        Me.cboMerch.Name = "cboMerch"
        Me.cboMerch.Size = New System.Drawing.Size(199, 21)
        Me.cboMerch.TabIndex = 15
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(13, 158)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(117, 13)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "Credit Merchandise"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cboSalesAcnt)
        Me.GroupBox1.Controls.Add(Me.cboPayAcnt)
        Me.GroupBox1.Controls.Add(Me.cboTaxAcnt)
        Me.GroupBox1.Controls.Add(Me.cboOutputTax)
        Me.GroupBox1.Controls.Add(Me.cboCOS)
        Me.GroupBox1.Controls.Add(Me.cboMerch)
        Me.GroupBox1.Controls.Add(Me.cboDiscount)
        Me.GroupBox1.Controls.Add(Me.Label24)
        Me.GroupBox1.Controls.Add(Me.lblDiscount)
        Me.GroupBox1.Controls.Add(Me.Label23)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtPer1)
        Me.GroupBox1.Controls.Add(Me.txtPercent)
        Me.GroupBox1.Controls.Add(Me.btnClose)
        Me.GroupBox1.Controls.Add(Me.btnSave)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(437, 244)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Accounts"
        '
        'cboDiscount
        '
        Me.cboDiscount.DropDownWidth = 500
        Me.cboDiscount.FormattingEnabled = True
        Me.cboDiscount.Location = New System.Drawing.Point(171, 182)
        Me.cboDiscount.MaxDropDownItems = 25
        Me.cboDiscount.Name = "cboDiscount"
        Me.cboDiscount.Size = New System.Drawing.Size(199, 21)
        Me.cboDiscount.TabIndex = 17
        '
        'lblDiscount
        '
        Me.lblDiscount.AutoSize = True
        Me.lblDiscount.Location = New System.Drawing.Point(13, 185)
        Me.lblDiscount.Name = "lblDiscount"
        Me.lblDiscount.Size = New System.Drawing.Size(125, 13)
        Me.lblDiscount.TabIndex = 16
        Me.lblDiscount.Text = "Debit Sales Discount"
        '
        'frmAccountability
        '
        Me.AcceptButton = Me.btnSave
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(445, 250)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAccountability"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Invoice Accountability"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtPercent As System.Windows.Forms.TextBox
    Friend WithEvents cboTaxAcnt As System.Windows.Forms.ComboBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents cboPayAcnt As System.Windows.Forms.ComboBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboSalesAcnt As System.Windows.Forms.ComboBox
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtPer1 As System.Windows.Forms.TextBox
    Friend WithEvents cboOutputTax As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboCOS As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cboMerch As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cboDiscount As System.Windows.Forms.ComboBox
    Friend WithEvents lblDiscount As System.Windows.Forms.Label
End Class
