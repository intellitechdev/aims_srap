Public Class frmCoCenters

    Private Sub frmCoCenters_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call loadgrid()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If m_savecocenter(txtDiscCode.Text, txtDescription.Text, Me) = "save successful.." Then
            MsgBox("save successful..", MsgBoxStyle.Information, Me.Text)
            Call loadgrid()
            txtDiscCode.Text = ""
            txtDescription.Text = ""
        Else
            MsgBox("saving co-center encounters an error...", MsgBoxStyle.Information, Me.Text)
        End If
    End Sub

    Private Sub grdCoCenters_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdCoCenters.CellClick
        With grdCoCenters
            txtDiscCode.Text = .CurrentRow.Cells(1).Value.ToString
            txtDescription.Text = .CurrentRow.Cells(2).Value.ToString
        End With
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub loadgrid()
        With grdCoCenters
            .Columns.Clear()
            Dim dtBill As DataTable = m_getLoadCoCenters().Tables(0)
            .DataSource = dtBill.DefaultView
            .Columns(0).Visible = False
            .Columns(1).Width = 100
            .Columns(2).Width = 250
        End With
    End Sub

 

End Class