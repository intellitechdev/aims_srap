Imports Microsoft.ApplicationBlocks.Data
Imports System.data.SqlClient

Public Class frmPostingVerification

    Public Mode As String
    Private gCon As New Clsappconfiguration
    Public xParent As frmBillsListforApproval

    Private Sub ApproveBills()
        With xParent
            Dim isReviewed As Boolean = True

            For Each xRow As DataGridViewRow In .grdBillsApproval.Rows
                If .grdBillsApproval.Item(1, xRow.Index).Value = True Then
                    If .grdBillsApproval.Item(6, xRow.Index).Value = False Then
                        isReviewed = False
                    End If
                End If
            Next

            If isReviewed = True Then
                For Each xRow As DataGridViewRow In .grdBillsApproval.Rows
                    If .grdBillsApproval.Item(1, xRow.Index).Value = True Then
                        If .grdBillsApproval.Item(6, xRow.Index).Value = True Then
                            g_postbills(.grdBillsApproval.Item(0, xRow.Index).Value.ToString, strSysCurrentFullName)
                        End If
                    End If
                Next
            Else
                MessageBox.Show("User Error! You have to review first the bill before posting.", "User Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

        End With

    End Sub

    Private Sub PostAsPettyCash_bills(ByVal psBillKey As String, ByVal User As String)
        Dim sSQLCmd As String = "usp_t_tBills_PostPettyCash "
        sSQLCmd &= " @fxKeyCompany ='" & gCompanyID() & "' "
        sSQLCmd &= ",@fxKeyBill ='" & psBillKey & "' "
        sSQLCmd &= ",@fbPosted='1' "
        sSQLCmd &= ",@User ='" & User & "'"
        Try
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            ShowErrorMessage("PostPettyCash", "ModDefaultVaue", ex.Message)
        End Try
    End Sub


    Private Sub PostAsPettyCash()
        With xParent
            For Each xrow As DataGridViewRow In .grdBillsApproval.Rows
                If .grdBillsApproval.Item(1, xrow.Index).Value = True Then
                    PostAsPettyCash_bills(xParent.grdBillsApproval.Item(0, xrow.Index).Value.ToString, strSysCurrentFullName)
                End If
            Next
        End With
    End Sub

    Private Sub ReviewedBills()
        With xParent
            For Each xrow As DataGridViewRow In .grdBillsApproval.Rows
                If .grdBillsApproval.Item(1, xrow.Index).Value = True Then
                    g_reviewedbills(xParent.grdBillsApproval.Item(0, xrow.Index).Value.ToString, strSysCurrentFullName)
                End If
            Next
        End With
    End Sub

    Private Function VerifyPassword() As Boolean
        Dim sqlCmdText As String = "SYS_GetUser"
        sqlCmdText &= "  @LogName=" & gUserName
        sqlCmdText &= ", @Password='" & Clsappconfiguration.GetEncryptedData(txtPassword.Text) & "'"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sqlCmdText)
                If rd.Read Then
                    Return True
                Else
                    MsgBox("Invalid password!", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Access denied")
                    Return False
                End If
            End Using
        Catch ex As Exception
            ShowErrorMessage("VerifyPassword", Me.Name, ex.Message)
            Return False
        End Try
    End Function


    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If VerifyPassword() = True Then
            Select Case Mode
                Case "Review"
                    ReviewedBills()
                    Close()
                Case "Approving"
                    ApproveBills()
                    Close()
                Case "Petty Cash"
                    PostAsPettyCash()
                    Close()
            End Select
        Else
            txtPassword.Text = ""
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        txtPassword.Text = ""
        Me.Close()
    End Sub


    Private Sub frmPostingVerification_ResizeBegin(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeBegin
        Me.Opacity = 0.5
    End Sub

    Private Sub frmPostingVerification_ResizeEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeEnd
        Me.Opacity = 1
    End Sub
End Class