﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Public Class frmSearchBills
    Private gCon As New Clsappconfiguration
    Private search As String
    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Close()
    End Sub

    Private Sub frmSearchBills_ResizeBegin(sender As System.Object, e As System.EventArgs) Handles MyBase.ResizeBegin
        Opacity = 0.75
    End Sub

    Private Sub frmSearchBills_ResizeEnd(sender As Object, e As System.EventArgs) Handles Me.ResizeEnd
        Opacity = 1
    End Sub

     
    Private Function SearchBills(ByVal find As String, ByVal company As String) As DataSet

        Dim sSQLCmd As String = "usp_t_bills_find "
        sSQLCmd &= "@fxKeyCompany = '" & gCompanyID() & "'"
        sSQLCmd &= ",@find = '" & find & "'"

        'sSQLCmd &= ",@TransactionType='" & sType & "'"

        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Bills Information")
            Return Nothing
        End Try
    End Function
    Private Sub LoadGridSupplier2()

        ' Dim dtTableTransaction As DataTable

        Dim col1 As New DataGridViewTextBoxColumn
        Dim col2 As New DataGridViewTextBoxColumn
        Dim col3 As New DataGridViewTextBoxColumn
        Dim col4 As New DataGridViewTextBoxColumn
        Dim col5 As New DataGridViewTextBoxColumn
        Dim col6 As New DataGridViewTextBoxColumn
        Dim col7 As New DataGridViewTextBoxColumn
        Dim col8 As New DataGridViewCheckBoxColumn
        Dim col9 As New DataGridViewCheckBoxColumn
        Dim GridSupplier2_bindingsource As New BindingSource
        GridSupplier2_bindingsource.DataSource = SearchBills(search, gCompanyID)
        GridSupplier2_bindingsource.DataMember = SearchBills(search, gCompanyID).Tables(0).TableName

        frm_vend_masterVendor.grdSupplier2.DataSource = GridSupplier2_bindingsource

        frm_vend_masterVendor.DisplayBills_to_Grid(frm_vend_masterVendor.grdSupplier2)
        frm_vend_masterVendor.Arrange_Bills_on_GridSupplier2()

    End Sub

    Private Sub btnSearch_Click(sender As System.Object, e As System.EventArgs) Handles btnSearch.Click
        search = TextBox1.Text
        LoadGridSupplier2()
    End Sub
End Class