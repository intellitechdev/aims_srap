<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_vend_EnterBills
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_vend_EnterBills))
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.tb_Items = New System.Windows.Forms.TabPage()
        Me.btnShowPO = New System.Windows.Forms.Button()
        Me.btnRcvAll = New System.Windows.Forms.Button()
        Me.btnSelectPO = New System.Windows.Forms.Button()
        Me.grdItems = New System.Windows.Forms.DataGridView()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.toolStripViewPaymentHistory = New System.Windows.Forms.ToolStripButton()
        Me.rbtnBill = New System.Windows.Forms.RadioButton()
        Me.rbtnCredit = New System.Windows.Forms.RadioButton()
        Me.chkBillRcv = New System.Windows.Forms.CheckBox()
        Me.PanelBill = New System.Windows.Forms.Panel()
        Me.btnDocNo = New System.Windows.Forms.Button()
        Me.cboMember = New System.Windows.Forms.ComboBox()
        Me.LblTransStatus = New CSAcctg.modTransparentLabel()
        Me.MtcboAPAcnt = New MTGCComboBox()
        Me.txtClassName = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.BtnBrowse = New System.Windows.Forms.Button()
        Me.txtLoanNo = New System.Windows.Forms.TextBox()
        Me.lblLoanNo = New System.Windows.Forms.Label()
        Me.txtRefNo = New System.Windows.Forms.MaskedTextBox()
        Me.lblDateDisc = New System.Windows.Forms.Label()
        Me.lblDiscDate = New System.Windows.Forms.Label()
        Me.dteBillDue = New System.Windows.Forms.DateTimePicker()
        Me.lblBilldue = New System.Windows.Forms.Label()
        Me.txtAmtDue = New System.Windows.Forms.TextBox()
        Me.lblamtdue = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.dteTransDate = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.txtMemo = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lblTerms = New System.Windows.Forms.Label()
        Me.lbladd = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblbill = New System.Windows.Forms.Label()
        Me.lblReceiptItem = New System.Windows.Forms.Label()
        Me.txtClassRefNo = New System.Windows.Forms.TextBox()
        Me.cboTerms_Multicombo = New MTGCComboBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tb_Expenses = New System.Windows.Forms.TabPage()
        Me.ListDelete = New System.Windows.Forms.ListView()
        Me.fxkeybillsexpensesdelete = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.grdExpenses = New System.Windows.Forms.DataGridView()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtdebittotal = New System.Windows.Forms.TextBox()
        Me.txtCredittotal = New System.Windows.Forms.TextBox()
        Me.chkRecurring = New System.Windows.Forms.CheckBox()
        Me.BtnDelete = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnPreview = New System.Windows.Forms.Button()
        Me.btnSaveOnly = New System.Windows.Forms.Button()
        Me.btnAddPO = New System.Windows.Forms.Button()
        Me.tb_Items.SuspendLayout()
        CType(Me.grdItems, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.PanelBill.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.tb_Expenses.SuspendLayout()
        CType(Me.grdExpenses, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tb_Items
        '
        Me.tb_Items.BackColor = System.Drawing.Color.White
        Me.tb_Items.Controls.Add(Me.btnShowPO)
        Me.tb_Items.Controls.Add(Me.btnRcvAll)
        Me.tb_Items.Controls.Add(Me.btnSelectPO)
        Me.tb_Items.Controls.Add(Me.grdItems)
        Me.tb_Items.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_Items.Location = New System.Drawing.Point(4, 24)
        Me.tb_Items.Name = "tb_Items"
        Me.tb_Items.Padding = New System.Windows.Forms.Padding(3)
        Me.tb_Items.Size = New System.Drawing.Size(1008, 184)
        Me.tb_Items.TabIndex = 1
        Me.tb_Items.Text = "Items Php 0.00"
        '
        'btnShowPO
        '
        Me.btnShowPO.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnShowPO.Enabled = False
        Me.btnShowPO.Location = New System.Drawing.Point(185, 227)
        Me.btnShowPO.Name = "btnShowPO"
        Me.btnShowPO.Size = New System.Drawing.Size(87, 27)
        Me.btnShowPO.TabIndex = 4
        Me.btnShowPO.Text = "Show PO"
        Me.btnShowPO.UseVisualStyleBackColor = True
        '
        'btnRcvAll
        '
        Me.btnRcvAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRcvAll.Enabled = False
        Me.btnRcvAll.Location = New System.Drawing.Point(94, 227)
        Me.btnRcvAll.Name = "btnRcvAll"
        Me.btnRcvAll.Size = New System.Drawing.Size(87, 27)
        Me.btnRcvAll.TabIndex = 3
        Me.btnRcvAll.Text = "Receive All"
        Me.btnRcvAll.UseVisualStyleBackColor = True
        '
        'btnSelectPO
        '
        Me.btnSelectPO.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSelectPO.Enabled = False
        Me.btnSelectPO.Location = New System.Drawing.Point(3, 227)
        Me.btnSelectPO.Name = "btnSelectPO"
        Me.btnSelectPO.Size = New System.Drawing.Size(87, 27)
        Me.btnSelectPO.TabIndex = 2
        Me.btnSelectPO.Text = "Select PO"
        Me.btnSelectPO.UseVisualStyleBackColor = True
        '
        'grdItems
        '
        Me.grdItems.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdItems.BackgroundColor = System.Drawing.Color.White
        Me.grdItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdItems.GridColor = System.Drawing.Color.Black
        Me.grdItems.Location = New System.Drawing.Point(0, 0)
        Me.grdItems.Name = "grdItems"
        Me.grdItems.Size = New System.Drawing.Size(983, 220)
        Me.grdItems.TabIndex = 1
        '
        'ToolStrip1
        '
        Me.ToolStrip1.BackColor = System.Drawing.Color.White
        Me.ToolStrip1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.toolStripViewPaymentHistory})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1026, 25)
        Me.ToolStrip1.TabIndex = 0
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'toolStripViewPaymentHistory
        '
        Me.toolStripViewPaymentHistory.Image = CType(resources.GetObject("toolStripViewPaymentHistory.Image"), System.Drawing.Image)
        Me.toolStripViewPaymentHistory.Name = "toolStripViewPaymentHistory"
        Me.toolStripViewPaymentHistory.Size = New System.Drawing.Size(149, 22)
        Me.toolStripViewPaymentHistory.Text = "View Payment History"
        '
        'rbtnBill
        '
        Me.rbtnBill.AutoSize = True
        Me.rbtnBill.Location = New System.Drawing.Point(2, 27)
        Me.rbtnBill.Name = "rbtnBill"
        Me.rbtnBill.Size = New System.Drawing.Size(58, 19)
        Me.rbtnBill.TabIndex = 1
        Me.rbtnBill.TabStop = True
        Me.rbtnBill.Text = "Bill(s)"
        Me.rbtnBill.UseVisualStyleBackColor = True
        Me.rbtnBill.Visible = False
        '
        'rbtnCredit
        '
        Me.rbtnCredit.AutoSize = True
        Me.rbtnCredit.Location = New System.Drawing.Point(62, 27)
        Me.rbtnCredit.Name = "rbtnCredit"
        Me.rbtnCredit.Size = New System.Drawing.Size(58, 19)
        Me.rbtnCredit.TabIndex = 2
        Me.rbtnCredit.TabStop = True
        Me.rbtnCredit.Text = "Credit"
        Me.rbtnCredit.UseVisualStyleBackColor = True
        Me.rbtnCredit.Visible = False
        '
        'chkBillRcv
        '
        Me.chkBillRcv.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkBillRcv.AutoSize = True
        Me.chkBillRcv.Location = New System.Drawing.Point(918, 27)
        Me.chkBillRcv.Name = "chkBillRcv"
        Me.chkBillRcv.Size = New System.Drawing.Size(96, 19)
        Me.chkBillRcv.TabIndex = 3
        Me.chkBillRcv.Text = "Bill Received"
        Me.chkBillRcv.UseVisualStyleBackColor = True
        '
        'PanelBill
        '
        Me.PanelBill.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelBill.BackColor = System.Drawing.Color.White
        Me.PanelBill.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanelBill.Controls.Add(Me.btnDocNo)
        Me.PanelBill.Controls.Add(Me.cboMember)
        Me.PanelBill.Controls.Add(Me.LblTransStatus)
        Me.PanelBill.Controls.Add(Me.MtcboAPAcnt)
        Me.PanelBill.Controls.Add(Me.txtClassName)
        Me.PanelBill.Controls.Add(Me.Label9)
        Me.PanelBill.Controls.Add(Me.BtnBrowse)
        Me.PanelBill.Controls.Add(Me.txtLoanNo)
        Me.PanelBill.Controls.Add(Me.lblLoanNo)
        Me.PanelBill.Controls.Add(Me.txtRefNo)
        Me.PanelBill.Controls.Add(Me.lblDateDisc)
        Me.PanelBill.Controls.Add(Me.lblDiscDate)
        Me.PanelBill.Controls.Add(Me.dteBillDue)
        Me.PanelBill.Controls.Add(Me.lblBilldue)
        Me.PanelBill.Controls.Add(Me.txtAmtDue)
        Me.PanelBill.Controls.Add(Me.lblamtdue)
        Me.PanelBill.Controls.Add(Me.Label8)
        Me.PanelBill.Controls.Add(Me.dteTransDate)
        Me.PanelBill.Controls.Add(Me.Label4)
        Me.PanelBill.Controls.Add(Me.txtAddress)
        Me.PanelBill.Controls.Add(Me.txtMemo)
        Me.PanelBill.Controls.Add(Me.Label6)
        Me.PanelBill.Controls.Add(Me.lblTerms)
        Me.PanelBill.Controls.Add(Me.lbladd)
        Me.PanelBill.Controls.Add(Me.Label10)
        Me.PanelBill.Controls.Add(Me.Label2)
        Me.PanelBill.Controls.Add(Me.lblbill)
        Me.PanelBill.Controls.Add(Me.lblReceiptItem)
        Me.PanelBill.Controls.Add(Me.txtClassRefNo)
        Me.PanelBill.Controls.Add(Me.cboTerms_Multicombo)
        Me.PanelBill.Location = New System.Drawing.Point(5, 48)
        Me.PanelBill.Name = "PanelBill"
        Me.PanelBill.Size = New System.Drawing.Size(1016, 244)
        Me.PanelBill.TabIndex = 4
        '
        'btnDocNo
        '
        Me.btnDocNo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDocNo.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDocNo.Location = New System.Drawing.Point(954, 92)
        Me.btnDocNo.Name = "btnDocNo"
        Me.btnDocNo.Size = New System.Drawing.Size(45, 23)
        Me.btnDocNo.TabIndex = 40
        Me.btnDocNo.Text = "....."
        Me.btnDocNo.UseVisualStyleBackColor = True
        '
        'cboMember
        '
        Me.cboMember.FormattingEnabled = True
        Me.cboMember.Location = New System.Drawing.Point(120, 34)
        Me.cboMember.Name = "cboMember"
        Me.cboMember.Size = New System.Drawing.Size(362, 23)
        Me.cboMember.TabIndex = 39
        '
        'LblTransStatus
        '
        Me.LblTransStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblTransStatus.Font = New System.Drawing.Font("Calibri", 72.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTransStatus.ForeColor = System.Drawing.Color.Silver
        Me.LblTransStatus.Location = New System.Drawing.Point(3, -1)
        Me.LblTransStatus.Name = "LblTransStatus"
        Me.LblTransStatus.quadrantMode = False
        Me.LblTransStatus.rotationAngle = -25
        Me.LblTransStatus.Size = New System.Drawing.Size(808, 156)
        Me.LblTransStatus.TabIndex = 36
        Me.LblTransStatus.Text = "Status"
        Me.LblTransStatus.Visible = False
        '
        'MtcboAPAcnt
        '
        Me.MtcboAPAcnt.ArrowBoxColor = System.Drawing.SystemColors.Control
        Me.MtcboAPAcnt.ArrowColor = System.Drawing.Color.Black
        Me.MtcboAPAcnt.BindedControl = CType(resources.GetObject("MtcboAPAcnt.BindedControl"), MTGCComboBox.ControlloAssociato)
        Me.MtcboAPAcnt.BorderStyle = MTGCComboBox.TipiBordi.Fixed3D
        Me.MtcboAPAcnt.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.MtcboAPAcnt.ColumnNum = 2
        Me.MtcboAPAcnt.ColumnWidth = "200;0"
        Me.MtcboAPAcnt.DisabledArrowBoxColor = System.Drawing.SystemColors.Control
        Me.MtcboAPAcnt.DisabledArrowColor = System.Drawing.Color.LightGray
        Me.MtcboAPAcnt.DisabledBackColor = System.Drawing.SystemColors.Control
        Me.MtcboAPAcnt.DisabledBorderColor = System.Drawing.SystemColors.InactiveBorder
        Me.MtcboAPAcnt.DisabledForeColor = System.Drawing.SystemColors.GrayText
        Me.MtcboAPAcnt.DisplayMember = "Text"
        Me.MtcboAPAcnt.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.MtcboAPAcnt.DropDownBackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.MtcboAPAcnt.DropDownForeColor = System.Drawing.Color.Black
        Me.MtcboAPAcnt.DropDownStyle = MTGCComboBox.CustomDropDownStyle.DropDown
        Me.MtcboAPAcnt.DropDownWidth = 220
        Me.MtcboAPAcnt.GridLineColor = System.Drawing.Color.LightGray
        Me.MtcboAPAcnt.GridLineHorizontal = False
        Me.MtcboAPAcnt.GridLineVertical = False
        Me.MtcboAPAcnt.LoadingType = MTGCComboBox.CaricamentoCombo.ComboBoxItem
        Me.MtcboAPAcnt.Location = New System.Drawing.Point(120, 67)
        Me.MtcboAPAcnt.ManagingFastMouseMoving = True
        Me.MtcboAPAcnt.ManagingFastMouseMovingInterval = 30
        Me.MtcboAPAcnt.MaxDropDownItems = 20
        Me.MtcboAPAcnt.Name = "MtcboAPAcnt"
        Me.MtcboAPAcnt.SelectedItem = Nothing
        Me.MtcboAPAcnt.SelectedValue = Nothing
        Me.MtcboAPAcnt.Size = New System.Drawing.Size(165, 24)
        Me.MtcboAPAcnt.TabIndex = 35
        '
        'txtClassName
        '
        Me.txtClassName.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtClassName.BackColor = System.Drawing.Color.White
        Me.txtClassName.Location = New System.Drawing.Point(820, 34)
        Me.txtClassName.Name = "txtClassName"
        Me.txtClassName.ReadOnly = True
        Me.txtClassName.Size = New System.Drawing.Size(112, 23)
        Me.txtClassName.TabIndex = 31
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(649, 37)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(40, 15)
        Me.Label9.TabIndex = 30
        Me.Label9.Text = "Class:"
        '
        'BtnBrowse
        '
        Me.BtnBrowse.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnBrowse.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnBrowse.Location = New System.Drawing.Point(938, 33)
        Me.BtnBrowse.Name = "BtnBrowse"
        Me.BtnBrowse.Size = New System.Drawing.Size(70, 27)
        Me.BtnBrowse.TabIndex = 29
        Me.BtnBrowse.Text = "Browse"
        Me.BtnBrowse.UseVisualStyleBackColor = True
        '
        'txtLoanNo
        '
        Me.txtLoanNo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtLoanNo.Location = New System.Drawing.Point(819, 179)
        Me.txtLoanNo.Name = "txtLoanNo"
        Me.txtLoanNo.Size = New System.Drawing.Size(186, 23)
        Me.txtLoanNo.TabIndex = 28
        '
        'lblLoanNo
        '
        Me.lblLoanNo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblLoanNo.AutoSize = True
        Me.lblLoanNo.Location = New System.Drawing.Point(646, 182)
        Me.lblLoanNo.Name = "lblLoanNo"
        Me.lblLoanNo.Size = New System.Drawing.Size(57, 15)
        Me.lblLoanNo.TabIndex = 27
        Me.lblLoanNo.Text = "Loan No.:"
        '
        'txtRefNo
        '
        Me.txtRefNo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtRefNo.BackColor = System.Drawing.Color.White
        Me.txtRefNo.Location = New System.Drawing.Point(819, 92)
        Me.txtRefNo.Name = "txtRefNo"
        Me.txtRefNo.ReadOnly = True
        Me.txtRefNo.Size = New System.Drawing.Size(129, 23)
        Me.txtRefNo.TabIndex = 26
        '
        'lblDateDisc
        '
        Me.lblDateDisc.AutoSize = True
        Me.lblDateDisc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateDisc.Location = New System.Drawing.Point(386, 179)
        Me.lblDateDisc.Name = "lblDateDisc"
        Me.lblDateDisc.Size = New System.Drawing.Size(83, 13)
        Me.lblDateDisc.TabIndex = 25
        Me.lblDateDisc.Text = "mm/dd/yyyy"
        Me.lblDateDisc.Visible = False
        '
        'lblDiscDate
        '
        Me.lblDiscDate.AutoSize = True
        Me.lblDiscDate.Location = New System.Drawing.Point(293, 179)
        Me.lblDiscDate.Name = "lblDiscDate"
        Me.lblDiscDate.Size = New System.Drawing.Size(84, 15)
        Me.lblDiscDate.TabIndex = 24
        Me.lblDiscDate.Text = "Discount Date"
        Me.lblDiscDate.Visible = False
        '
        'dteBillDue
        '
        Me.dteBillDue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dteBillDue.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteBillDue.Location = New System.Drawing.Point(819, 150)
        Me.dteBillDue.Name = "dteBillDue"
        Me.dteBillDue.Size = New System.Drawing.Size(186, 23)
        Me.dteBillDue.TabIndex = 20
        '
        'lblBilldue
        '
        Me.lblBilldue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblBilldue.AutoSize = True
        Me.lblBilldue.Location = New System.Drawing.Point(646, 155)
        Me.lblBilldue.Name = "lblBilldue"
        Me.lblBilldue.Size = New System.Drawing.Size(59, 15)
        Me.lblBilldue.TabIndex = 19
        Me.lblBilldue.Text = "Due Date:"
        '
        'txtAmtDue
        '
        Me.txtAmtDue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAmtDue.BackColor = System.Drawing.Color.White
        Me.txtAmtDue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAmtDue.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmtDue.Location = New System.Drawing.Point(819, 121)
        Me.txtAmtDue.Name = "txtAmtDue"
        Me.txtAmtDue.ReadOnly = True
        Me.txtAmtDue.Size = New System.Drawing.Size(186, 27)
        Me.txtAmtDue.TabIndex = 18
        Me.txtAmtDue.Text = "0.00"
        Me.txtAmtDue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblamtdue
        '
        Me.lblamtdue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblamtdue.AutoSize = True
        Me.lblamtdue.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblamtdue.Location = New System.Drawing.Point(645, 123)
        Me.lblamtdue.Name = "lblamtdue"
        Me.lblamtdue.Size = New System.Drawing.Size(69, 19)
        Me.lblamtdue.TabIndex = 17
        Me.lblamtdue.Text = "Amt. Due"
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(645, 96)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(105, 15)
        Me.Label8.TabIndex = 15
        Me.Label8.Text = "Document Ref.No.:"
        '
        'dteTransDate
        '
        Me.dteTransDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dteTransDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteTransDate.Location = New System.Drawing.Point(819, 63)
        Me.dteTransDate.Name = "dteTransDate"
        Me.dteTransDate.Size = New System.Drawing.Size(186, 23)
        Me.dteTransDate.TabIndex = 14
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(646, 68)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 15)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Date:"
        '
        'txtAddress
        '
        Me.txtAddress.BackColor = System.Drawing.Color.White
        Me.txtAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.Location = New System.Drawing.Point(120, 93)
        Me.txtAddress.Multiline = True
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(362, 69)
        Me.txtAddress.TabIndex = 12
        '
        'txtMemo
        '
        Me.txtMemo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMemo.BackColor = System.Drawing.Color.White
        Me.txtMemo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtMemo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMemo.Location = New System.Drawing.Point(120, 205)
        Me.txtMemo.Name = "txtMemo"
        Me.txtMemo.Size = New System.Drawing.Size(885, 21)
        Me.txtMemo.TabIndex = 11
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(9, 207)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(68, 15)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Particulars"
        '
        'lblTerms
        '
        Me.lblTerms.AutoSize = True
        Me.lblTerms.Location = New System.Drawing.Point(35, 168)
        Me.lblTerms.Name = "lblTerms"
        Me.lblTerms.Size = New System.Drawing.Size(42, 15)
        Me.lblTerms.TabIndex = 7
        Me.lblTerms.Text = "Terms:"
        Me.lblTerms.Visible = False
        '
        'lbladd
        '
        Me.lbladd.AutoSize = True
        Me.lbladd.Location = New System.Drawing.Point(9, 102)
        Me.lbladd.Name = "lbladd"
        Me.lbladd.Size = New System.Drawing.Size(54, 15)
        Me.lbladd.TabIndex = 3
        Me.lbladd.Text = "Address:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(9, 73)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(107, 15)
        Me.Label10.TabIndex = 1
        Me.Label10.Text = "Accounts Payable:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 39)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(55, 15)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Member:"
        '
        'lblbill
        '
        Me.lblbill.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblbill.BackColor = System.Drawing.Color.DarkBlue
        Me.lblbill.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblbill.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lblbill.Location = New System.Drawing.Point(3, 3)
        Me.lblbill.Name = "lblbill"
        Me.lblbill.Size = New System.Drawing.Size(1007, 21)
        Me.lblbill.TabIndex = 0
        Me.lblbill.Text = "Bill(s)"
        Me.lblbill.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblReceiptItem
        '
        Me.lblReceiptItem.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReceiptItem.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblReceiptItem.Location = New System.Drawing.Point(116, 99)
        Me.lblReceiptItem.Name = "lblReceiptItem"
        Me.lblReceiptItem.Size = New System.Drawing.Size(135, 69)
        Me.lblReceiptItem.TabIndex = 23
        Me.lblReceiptItem.Text = "Item Receipt Only"
        '
        'txtClassRefNo
        '
        Me.txtClassRefNo.BackColor = System.Drawing.Color.White
        Me.txtClassRefNo.Enabled = False
        Me.txtClassRefNo.Location = New System.Drawing.Point(735, 33)
        Me.txtClassRefNo.Name = "txtClassRefNo"
        Me.txtClassRefNo.Size = New System.Drawing.Size(144, 23)
        Me.txtClassRefNo.TabIndex = 31
        Me.txtClassRefNo.Visible = False
        '
        'cboTerms_Multicombo
        '
        Me.cboTerms_Multicombo.ArrowBoxColor = System.Drawing.SystemColors.Control
        Me.cboTerms_Multicombo.ArrowColor = System.Drawing.Color.Black
        Me.cboTerms_Multicombo.BindedControl = CType(resources.GetObject("cboTerms_Multicombo.BindedControl"), MTGCComboBox.ControlloAssociato)
        Me.cboTerms_Multicombo.BorderStyle = MTGCComboBox.TipiBordi.Fixed3D
        Me.cboTerms_Multicombo.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.cboTerms_Multicombo.ColumnNum = 2
        Me.cboTerms_Multicombo.ColumnWidth = "90;0"
        Me.cboTerms_Multicombo.DisabledArrowBoxColor = System.Drawing.SystemColors.Control
        Me.cboTerms_Multicombo.DisabledArrowColor = System.Drawing.Color.LightGray
        Me.cboTerms_Multicombo.DisabledBackColor = System.Drawing.SystemColors.Control
        Me.cboTerms_Multicombo.DisabledBorderColor = System.Drawing.SystemColors.InactiveBorder
        Me.cboTerms_Multicombo.DisabledForeColor = System.Drawing.SystemColors.GrayText
        Me.cboTerms_Multicombo.DisplayMember = "Text"
        Me.cboTerms_Multicombo.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cboTerms_Multicombo.DropDownBackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.cboTerms_Multicombo.DropDownForeColor = System.Drawing.Color.Black
        Me.cboTerms_Multicombo.DropDownStyle = MTGCComboBox.CustomDropDownStyle.DropDownList
        Me.cboTerms_Multicombo.GridLineColor = System.Drawing.Color.LightGray
        Me.cboTerms_Multicombo.GridLineHorizontal = False
        Me.cboTerms_Multicombo.GridLineVertical = False
        Me.cboTerms_Multicombo.LoadingType = MTGCComboBox.CaricamentoCombo.ComboBoxItem
        Me.cboTerms_Multicombo.Location = New System.Drawing.Point(120, 168)
        Me.cboTerms_Multicombo.ManagingFastMouseMoving = True
        Me.cboTerms_Multicombo.ManagingFastMouseMovingInterval = 30
        Me.cboTerms_Multicombo.Name = "cboTerms_Multicombo"
        Me.cboTerms_Multicombo.SelectedItem = Nothing
        Me.cboTerms_Multicombo.SelectedValue = Nothing
        Me.cboTerms_Multicombo.Size = New System.Drawing.Size(165, 24)
        Me.cboTerms_Multicombo.TabIndex = 38
        Me.cboTerms_Multicombo.Visible = False
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.tb_Expenses)
        Me.TabControl1.Controls.Add(Me.tb_Items)
        Me.TabControl1.Location = New System.Drawing.Point(5, 300)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1016, 212)
        Me.TabControl1.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight
        Me.TabControl1.TabIndex = 5
        '
        'tb_Expenses
        '
        Me.tb_Expenses.Controls.Add(Me.ListDelete)
        Me.tb_Expenses.Controls.Add(Me.grdExpenses)
        Me.tb_Expenses.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_Expenses.Location = New System.Drawing.Point(4, 24)
        Me.tb_Expenses.Name = "tb_Expenses"
        Me.tb_Expenses.Padding = New System.Windows.Forms.Padding(3)
        Me.tb_Expenses.Size = New System.Drawing.Size(1008, 184)
        Me.tb_Expenses.TabIndex = 0
        Me.tb_Expenses.Text = "Expenses Php 0.00"
        Me.tb_Expenses.UseVisualStyleBackColor = True
        '
        'ListDelete
        '
        Me.ListDelete.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ListDelete.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.fxkeybillsexpensesdelete})
        Me.ListDelete.Location = New System.Drawing.Point(783, 17)
        Me.ListDelete.Name = "ListDelete"
        Me.ListDelete.Size = New System.Drawing.Size(219, 151)
        Me.ListDelete.TabIndex = 1
        Me.ListDelete.UseCompatibleStateImageBehavior = False
        Me.ListDelete.View = System.Windows.Forms.View.Details
        Me.ListDelete.Visible = False
        '
        'fxkeybillsexpensesdelete
        '
        Me.fxkeybillsexpensesdelete.Text = "fxkeybillsexpensesdelete"
        Me.fxkeybillsexpensesdelete.Width = 191
        '
        'grdExpenses
        '
        Me.grdExpenses.AllowUserToDeleteRows = False
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.SkyBlue
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black
        Me.grdExpenses.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle5
        Me.grdExpenses.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdExpenses.BackgroundColor = System.Drawing.Color.White
        Me.grdExpenses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdExpenses.GridColor = System.Drawing.Color.Black
        Me.grdExpenses.Location = New System.Drawing.Point(6, 8)
        Me.grdExpenses.Name = "grdExpenses"
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.SkyBlue
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black
        Me.grdExpenses.RowsDefaultCellStyle = DataGridViewCellStyle6
        Me.grdExpenses.Size = New System.Drawing.Size(1007, 180)
        Me.grdExpenses.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(222, 515)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(33, 15)
        Me.Label5.TabIndex = 35
        Me.Label5.Text = "Total"
        '
        'txtdebittotal
        '
        Me.txtdebittotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtdebittotal.BackColor = System.Drawing.Color.White
        Me.txtdebittotal.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtdebittotal.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtdebittotal.Location = New System.Drawing.Point(258, 515)
        Me.txtdebittotal.Name = "txtdebittotal"
        Me.txtdebittotal.ReadOnly = True
        Me.txtdebittotal.Size = New System.Drawing.Size(126, 16)
        Me.txtdebittotal.TabIndex = 27
        Me.txtdebittotal.Text = "0.00"
        Me.txtdebittotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCredittotal
        '
        Me.txtCredittotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtCredittotal.BackColor = System.Drawing.Color.White
        Me.txtCredittotal.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCredittotal.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCredittotal.Location = New System.Drawing.Point(386, 515)
        Me.txtCredittotal.Name = "txtCredittotal"
        Me.txtCredittotal.ReadOnly = True
        Me.txtCredittotal.Size = New System.Drawing.Size(126, 16)
        Me.txtCredittotal.TabIndex = 36
        Me.txtCredittotal.Text = "0.00"
        Me.txtCredittotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'chkRecurring
        '
        Me.chkRecurring.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkRecurring.AutoSize = True
        Me.chkRecurring.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRecurring.Location = New System.Drawing.Point(842, 299)
        Me.chkRecurring.Name = "chkRecurring"
        Me.chkRecurring.Size = New System.Drawing.Size(173, 17)
        Me.chkRecurring.TabIndex = 38
        Me.chkRecurring.Text = "Recurring Entries Settings"
        Me.chkRecurring.UseVisualStyleBackColor = True
        Me.chkRecurring.Visible = False
        '
        'BtnDelete
        '
        Me.BtnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnDelete.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnDelete.Image = Global.CSAcctg.My.Resources.Resources.deletebig
        Me.BtnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnDelete.Location = New System.Drawing.Point(655, 513)
        Me.BtnDelete.Name = "BtnDelete"
        Me.BtnDelete.Size = New System.Drawing.Size(88, 36)
        Me.BtnDelete.TabIndex = 39
        Me.BtnDelete.Text = "Delete"
        Me.BtnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BtnDelete.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.Location = New System.Drawing.Point(944, 513)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(77, 36)
        Me.btnClose.TabIndex = 30
        Me.btnClose.Text = "Close"
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Image = CType(resources.GetObject("btnSave.Image"), System.Drawing.Image)
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.Location = New System.Drawing.Point(819, 513)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(123, 36)
        Me.btnSave.TabIndex = 29
        Me.btnSave.Text = "S&ave && New"
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnPreview
        '
        Me.btnPreview.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPreview.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.Image = Global.CSAcctg.My.Resources.Resources.Print_Preview
        Me.btnPreview.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPreview.Location = New System.Drawing.Point(5, 513)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(181, 36)
        Me.btnPreview.TabIndex = 40
        Me.btnPreview.Text = "Accounts Payable Voucher"
        Me.btnPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'btnSaveOnly
        '
        Me.btnSaveOnly.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSaveOnly.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveOnly.Image = CType(resources.GetObject("btnSaveOnly.Image"), System.Drawing.Image)
        Me.btnSaveOnly.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSaveOnly.Location = New System.Drawing.Point(745, 513)
        Me.btnSaveOnly.Name = "btnSaveOnly"
        Me.btnSaveOnly.Size = New System.Drawing.Size(72, 36)
        Me.btnSaveOnly.TabIndex = 41
        Me.btnSaveOnly.Text = "&Save"
        Me.btnSaveOnly.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSaveOnly.UseVisualStyleBackColor = True
        '
        'btnAddPO
        '
        Me.btnAddPO.Image = Global.CSAcctg.My.Resources.Resources.add
        Me.btnAddPO.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAddPO.Location = New System.Drawing.Point(590, 513)
        Me.btnAddPO.Name = "btnAddPO"
        Me.btnAddPO.Size = New System.Drawing.Size(59, 36)
        Me.btnAddPO.TabIndex = 2
        Me.btnAddPO.Text = "P.O."
        Me.btnAddPO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAddPO.UseVisualStyleBackColor = True
        Me.btnAddPO.Visible = False
        '
        'frm_vend_EnterBills
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(1026, 558)
        Me.Controls.Add(Me.btnAddPO)
        Me.Controls.Add(Me.btnSaveOnly)
        Me.Controls.Add(Me.btnPreview)
        Me.Controls.Add(Me.BtnDelete)
        Me.Controls.Add(Me.chkRecurring)
        Me.Controls.Add(Me.txtCredittotal)
        Me.Controls.Add(Me.txtdebittotal)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.PanelBill)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.chkBillRcv)
        Me.Controls.Add(Me.rbtnCredit)
        Me.Controls.Add(Me.rbtnBill)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(902, 585)
        Me.Name = "frm_vend_EnterBills"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Voucher Register (Bills Entry)"
        Me.tb_Items.ResumeLayout(False)
        CType(Me.grdItems, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.PanelBill.ResumeLayout(False)
        Me.PanelBill.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.tb_Expenses.ResumeLayout(False)
        CType(Me.grdExpenses, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents rbtnBill As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnCredit As System.Windows.Forms.RadioButton
    Friend WithEvents chkBillRcv As System.Windows.Forms.CheckBox
    Friend WithEvents PanelBill As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lbladd As System.Windows.Forms.Label
    Friend WithEvents lblTerms As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtMemo As System.Windows.Forms.TextBox
    Friend WithEvents txtAddress As System.Windows.Forms.TextBox
    Friend WithEvents dteTransDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblamtdue As System.Windows.Forms.Label
    Friend WithEvents dteBillDue As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblBilldue As System.Windows.Forms.Label
    Friend WithEvents txtAmtDue As System.Windows.Forms.TextBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tb_Expenses As System.Windows.Forms.TabPage
    Friend WithEvents grdExpenses As System.Windows.Forms.DataGridView
    Friend WithEvents grdItems As System.Windows.Forms.DataGridView
    Friend WithEvents btnSelectPO As System.Windows.Forms.Button
    Friend WithEvents btnShowPO As System.Windows.Forms.Button
    Friend WithEvents btnRcvAll As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents lblbill As System.Windows.Forms.Label
    Friend WithEvents lblReceiptItem As System.Windows.Forms.Label
    Friend WithEvents lblDateDisc As System.Windows.Forms.Label
    Friend WithEvents lblDiscDate As System.Windows.Forms.Label
    Friend WithEvents txtRefNo As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtdebittotal As System.Windows.Forms.TextBox
    Friend WithEvents txtCredittotal As System.Windows.Forms.TextBox
    Friend WithEvents lblLoanNo As System.Windows.Forms.Label
    Friend WithEvents txtLoanNo As System.Windows.Forms.TextBox
    Friend WithEvents chkRecurring As System.Windows.Forms.CheckBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents BtnBrowse As System.Windows.Forms.Button
    Friend WithEvents txtClassName As System.Windows.Forms.TextBox
    Friend WithEvents txtClassRefNo As System.Windows.Forms.TextBox
    Friend WithEvents BtnDelete As System.Windows.Forms.Button
    Friend WithEvents ListDelete As System.Windows.Forms.ListView
    Friend WithEvents fxkeybillsexpensesdelete As System.Windows.Forms.ColumnHeader
    Friend WithEvents tb_Items As System.Windows.Forms.TabPage
    Friend WithEvents MtcboAPAcnt As MTGCComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents LblTransStatus As CSAcctg.modTransparentLabel
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents btnSaveOnly As System.Windows.Forms.Button
    Friend WithEvents toolStripViewPaymentHistory As System.Windows.Forms.ToolStripButton
    Friend WithEvents cboTerms_Multicombo As MTGCComboBox
    Friend WithEvents btnAddPO As System.Windows.Forms.Button
    Friend WithEvents cboMember As System.Windows.Forms.ComboBox
    Friend WithEvents btnDocNo As System.Windows.Forms.Button
End Class
