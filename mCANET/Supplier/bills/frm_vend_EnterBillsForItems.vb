Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_vend_EnterBillsForItems
    Private gCon As New Clsappconfiguration
    Dim sKeySupplier As String
    Private Sub frm_vend_EnterBillsForItems_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        m_loadSupplier(cbosupplier, cboTemp)
        m_ItemReceiptsList(grdItemReceipt)
        sKeySupplier = ""
    End Sub
    Private Sub m_ItemReceiptsList(ByVal grd As DataGridView)
        Dim sSQLCmd As String = "usp_t_billitems_Get "
        sSQLCmd &= "@fxKeyCompany='" & gCompanyID() & "'"
        If sKeySupplier <> "" Then
            sSQLCmd &= ",@fxKeySupplier='" & sKeySupplier & "'"
        Else
            sSQLCmd &= ",@fxKeySupplier=NULL"
        End If
        Try
            With grdItemReceipt
                .DataSource = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd).Tables(0)

                .Columns(0).Visible = False
                .Columns(1).Visible = False
                .Columns(2).Visible = False
                .Columns.Item(3).HeaderText = "Date"
                .Columns.Item(4).HeaderText = "Ref. No."
                '.Columns(5).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                .Columns.Item(5).HeaderText = "Memo"
            End With
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:ListItem Receipts")
        End Try
    End Sub

    Private Sub cbosupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbosupplier.SelectedIndexChanged
        If cbosupplier.SelectedIndex <> 1 Then
            sKeySupplier = cboTemp.Items(cbosupplier.SelectedIndex).ToString()
            m_ItemReceiptsList(grdItemReceipt)
        Else
            frm_vend_masterVendorAddEdit.Show()
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub grdItemReceipt_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdItemReceipt.CellContentClick

    End Sub

    Private Sub grdItemReceipt_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdItemReceipt.CellDoubleClick
        With grdItemReceipt
            frm_vend_EnterBills.SupplierBill = False
            frm_vend_EnterBills.Mode = 1
            frm_vend_EnterBills.KeyBill = .Item(0, .CurrentRow.Index).Value.ToString
            frm_vend_EnterBills.ShowDialog()
        End With
    End Sub
End Class