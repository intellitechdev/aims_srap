Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_vend_masterVendorAddEdit
    Private sKeySupplier As String
    Private sKeyAddress As String
    Private sKeyType As String
    Private sKeyTerms As String
    Private sKeyBillRate As String
    Private iCtr() As Integer
    Private sFormOrigin As String
    Private sKeyAPAcnt As String = ""

    Private gcon As New Clsappconfiguration
    Private gTrans As New clsTransactionFunctions

    Public Property KeySupplier() As String
        Get
            Return Me.sKeySupplier
        End Get
        Set(ByVal value As String)
            sKeySupplier = value
        End Set
    End Property

    Public Property KeyAddress() As String
        Get
            Return sKeyAddress
        End Get
        Set(ByVal value As String)
            sKeyAddress = value
        End Set
    End Property
    Public Property KeyFormOrigin() As String
        Get
            Return sFormOrigin
        End Get
        Set(ByVal value As String)
            sFormOrigin = value
        End Set
    End Property

    Private Sub frm_vend_masterVendorAddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If getUsersGroup(gUserName) = 3 Then
            If sKeySupplier = Nothing Then
                sKeySupplier = Guid.NewGuid.ToString
            End If

            Call RefreshForm()

            If sKeyAddress = Nothing Then
                sKeyAddress = Guid.NewGuid.ToString
            End If


            btnOk.Enabled = True
        Else
            If MsgBox("To have a full access on this module, pls contact your system administrator...", MsgBoxStyle.Information, Me.Text) = MsgBoxResult.Ok Then
                btnOk.Enabled = False
            End If
        End If
    End Sub

    Private Sub RefreshForm()
        m_loadSupplierType(cboTypeName, cboTypeID)
        m_loadTerms(cboTermsName, cboTermsID)
        LoadDefinedAcntsToMtcbo(MtcboAP, "Accounts Payable", True)
        MtcboAP.SelectedValue = "No Account"
        m_otherInfoValueGetVal(sKeySupplier, True)
        m_otherInfoValList(grdDefineFields)
        Call LoadCostCenter()
        Call LoadSupplier()

    End Sub

    Private Sub LoadCostCenter()
        Try
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, "usp_m_CostCenter_Load", _
                                        New SqlParameter("@companyID", gCompanyID()))

            cboCostCenter.DisplayMember = "fcCostCenter"
            cboCostCenter.ValueMember = "pkCostCenter"
            cboCostCenter.DataSource = ds.Tables(0).DefaultView
            cboCostCenter.SelectedIndex = -1

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Cost Center Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Dispose()
        Me.Close()
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        If InStr(txtSupplierName.Text, "'", CompareMethod.Binary) > 0 Then
            MsgBox("Found invalid character, pls remove the apostrophe at supplier name to continue.", MsgBoxStyle.Information)
        Else
            If Trim(txtSupplierName.Text) = "" Then
                MessageBox.Show("Supplier name cannot be empty!", "Field Validation")
                txtSupplierName.Select()

                Exit Sub
            End If

            If sKeyAPAcnt = "" Then
                MsgBox("Please select an account for Account payable.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Account payable not specified.")
                TabControl1.SelectTab("TabPage2")
                MtcboAP.Select()

                Exit Sub
            End If

            Call UpdateOtherInfo()
            Call InsertUpdateSupplier()

            Me.Close()

        End If
    End Sub


    Private Sub InsertUpdateSupplier()

        Dim xVal As Integer = 1
        Try

            Dim sSQL_CommandText As String = "SPU_Supplier_Update "
            sSQL_CommandText &= "@fxKeySupplier='" & sKeySupplier & "' "
            sSQL_CommandText &= ",@fcSupplierName='" & txtSupplierName.Text & "' "
            sSQL_CommandText &= ",@fdBalance='" & txtOpeningBal.Text & "' "
            sSQL_CommandText &= ",@fdDateAsOf='" & dteOpeningBalDate.Value & "' "
            sSQL_CommandText &= ",@fcCompanyName='" & txtCoName.Text & "' "
            sSQL_CommandText &= ",@fcSalutation='" & txtSalutation.Text & "' "
            sSQL_CommandText &= ",@fcLastName='" & txtLastName.Text & "' "
            sSQL_CommandText &= ",@fcFirstName='" & txtFirstName.Text & "' "
            sSQL_CommandText &= ",@fcMidName='" & txtMiddleInitial.Text & "' "
            sSQL_CommandText &= ",@fcAccountNo='" & txtAcctNo.Text & "' "
            sSQL_CommandText &= ",@fcTIN='" & txtTIN.Text & "' "
            sSQL_CommandText &= ",@fcContactPerson1='" & txtContact.Text & "' "
            sSQL_CommandText &= ",@fcContactPerson2='" & txtAltContact.Text & "' "
            sSQL_CommandText &= ",@fcTelNo1='" & txtPhone.Text & "' "
            sSQL_CommandText &= ",@fcTelNo2='" & txtAltPhone.Text & "' "
            sSQL_CommandText &= ",@fcFaxNo='" & txtFAX.Text & "' "
            sSQL_CommandText &= ",@fcEmail='" & txtEmail.Text & "' "
            sSQL_CommandText &= ",@fcPrintCheck='" & txtPrintOnChk.Text & "' "
            sSQL_CommandText &= ",@fdCreditLimit='" & txtCreditLimit.Text & "' "
            sSQL_CommandText &= ",@fbActive='" & xVal & "' "
            sSQL_CommandText &= ",@fxKeyCompany ='" & gCompanyID() & "' "
            sSQL_CommandText &= ",@fcAddress='" & txtAddress.Text & "' "
            sSQL_CommandText &= ",@fxKeyAddress=" & IIf(sKeyAddress = "", "NULL", "'" & sKeyAddress & "'") & " "

            If cboTypeID.SelectedIndex <> 0 And cboTypeID.SelectedIndex <> 1 Then
                sSQL_CommandText &= ",@fxKeySupplierType =" & IIf(cboTypeID.SelectedItem = "", "NULL", "'" & cboTypeID.SelectedItem & "'") & " "
            End If

            If cboTermsID.SelectedIndex <> 0 And cboTermsID.SelectedIndex <> 1 Then
                sSQL_CommandText &= ",@fxKeyTerms =" & IIf(cboTermsID.SelectedItem = "", "NULL", "'" & cboTermsID.SelectedItem & "'") & " "
            End If

            If cboBillingRateLvlID.SelectedIndex <> 0 And cboBillingRateLvlID.SelectedIndex <> 1 Then
                sSQL_CommandText &= ",@fxKeyBillRate =" & IIf(cboBillingRateLvlID.SelectedItem = "", "NULL", "'" & cboBillingRateLvlID.SelectedItem & "'") & " "
            End If
            sSQL_CommandText &= ",@fxKeyAPAccnt='" & sKeyAPAcnt & "' "
            sSQL_CommandText &= ",@IDno='" & txtIDNo.Text & "' "

            If cboCostCenter.SelectedIndex >= 0 Then
                sSQL_CommandText &= ",@fxKeyCostCenter =" & IIf(cboCostCenter.SelectedValue.ToString() = "", "NULL", "'" & cboCostCenter.SelectedValue.ToString & "'") & " "
            End If


            SqlHelper.ExecuteDataset(gcon.cnstring, Data.CommandType.Text, sSQL_CommandText)
            MessageBox.Show("Supplier successfully updated", "Add/Edit Supplier")
        Catch
            MessageBox.Show(Err.Description, "Add/Edit Supplier")
        End Try
    End Sub

    Private Sub UpdateOtherInfo()
        Dim iRow As Integer

        For iRow = 0 To (grdDefineFields.RowCount - 2)
            Try
                Dim sSQLCmd As String = "usp_m_otherInfoValue_update "
                sSQLCmd &= "@fxKeyID = '" & grdDefineFields.Rows(iRow).Cells(0).Value.ToString & "'"
                sSQLCmd &= ",@fxKeyOtherInfo = '" & grdDefineFields.Rows(iRow).Cells(1).Value.ToString & "'"
                sSQLCmd &= ",@fxKeyInfoValue = '" & grdDefineFields.Rows(iRow).Cells(2).Value.ToString & "'"
                sSQLCmd &= ",@fdRate = 0"
                sSQLCmd &= ",@fdPercentage = 0"
                sSQLCmd &= ",@fcNotes = '" & grdDefineFields.Rows(iRow).Cells(4).Value & "'"

                SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSQLCmd)
            Catch ex As Exception
                MessageBox.Show(Err.Description, "Update Other Info.")
            End Try
        Next
    End Sub

    Private Function isGrdEmpty(ByVal i As Integer) As Boolean
        Try
            If grdDefineFields.Rows(i).Cells(4).Value.ToString = 0 And _
                grdDefineFields.Rows(i).Cells(5).Value.ToString = 0 And _
                grdDefineFields.Rows(i).Cells(6).Value = "" Then

                Return True
            Else
                Return False
            End If
        Catch
        End Try

    End Function

    Public Sub LoadSupplier()

        Dim sSQLCmd As String = "SPU_Supplier_InformationList '" & sKeySupplier & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    txtIDNo.Text = rd.Item("IDNo")
                    txtSupplierName.Text = rd.Item("fcSupplierName")
                    txtOpeningBal.Text = Format(CDec(rd.Item("fdBalance")), "##,##0.00")
                    If rd.Item("fdDateAsOf").ToString <> "" Then
                        dteOpeningBalDate.Value = rd.Item("fdDateAsOf")
                    End If
                    chkInactive.Checked = IIf(rd.Item("fbActive") = True, False, True)
                    txtCoName.Text = rd.Item("fcCompanyName")
                    txtSalutation.Text = rd.Item("fcSalutation")
                    txtLastName.Text = rd.Item("fcLastName")
                    txtFirstName.Text = rd.Item("fcFirstName")
                    txtMiddleInitial.Text = rd.Item("fcMidName")
                    txtAddress.Text = mgetAddressOfSupplier(sKeySupplier)
                    txtAddress.Text = rd.Item("fcAddress").ToString

                    txtAcctNo.Text = rd.Item("fcAccountNo")
                    txtTIN.Text = rd.Item("fcTIN")
                    txtContact.Text = rd.Item("fcContactPerson1")
                    txtAltContact.Text = rd.Item("fcContactPerson2")
                    txtPhone.Text = rd.Item("fcTelNo1")
                    txtAltPhone.Text = rd.Item("fcTelNo2")
                    txtFAX.Text = rd.Item("fcFaxNo")
                    txtEmail.Text = rd.Item("fcEmail")
                    txtPrintOnChk.Text = rd.Item("fcPrintCheck")
                    txtCreditLimit.Text = rd.Item("fdCreditLimit")
                    sKeyType = (rd.Item("fxKeySupplierType").ToString)
                    sKeyTerms = (rd.Item("fxKeyTerms").ToString)
                    sKeyBillRate = rd.Item("fxKeyBillRate").ToString
                    sKeyAddress = rd.Item("fxKeyAddress").ToString
                    MtcboAP.SelectedValue = rd.Item("fxKeyAPAccnt").ToString
                    cboCostCenter.SelectedValue = rd.Item("fxKeyCostCenter").ToString()
                End If
            End Using

            selectedType()
            selectedTerm()
            selectedBillRate()

        Catch
            MessageBox.Show(Err.Description, "Load Supplier")
        End Try
    End Sub

    Private Sub selectedType()
        If Not sKeyType = Nothing Or Not sKeyType = "" Then
            cboTypeID.SelectedText = sKeyType
            cboTypeID.SelectedItem = sKeyType
            cboTypeName.SelectedIndex = cboTypeID.SelectedIndex
        Else
            cboTypeName.SelectedIndex = 0
            cboTypeID.SelectedIndex = 0
        End If
    End Sub

    Private Sub selectedTerm()
        If Not sKeyTerms = Nothing Or Not sKeyTerms = "" Then
            cboTermsID.SelectedText = sKeyTerms
            cboTermsID.SelectedItem = sKeyTerms
            cboTermsName.SelectedIndex = cboTermsID.SelectedIndex
        Else
            cboTermsName.SelectedIndex = 0
            cboTermsID.SelectedIndex = 0
        End If
    End Sub

    Private Sub selectedBillRate()
        If Not sKeyBillRate = Nothing Or Not sKeyBillRate = "" Then
            cboBillingRateLvlID.SelectedText = sKeyBillRate
            cboBillingRateLvlID.SelectedItem = sKeyBillRate
            cboBillingRateLvlName.SelectedIndex = cboBillingRateLvlID.SelectedIndex
            'Else
            'cboBillingRateLvlName.SelectedIndex = 0
            'cboBillingRateLvlID.SelectedIndex = 0
        End If
    End Sub

    Private Sub cboTypeName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTypeName.SelectedIndexChanged
        cboTypeID.SelectedIndex() = cboTypeName.SelectedIndex

        If cboTypeName.SelectedIndex = 1 Then
            frm_MF_vendorTypeAddEdit.KeySupplierType = Guid.NewGuid.ToString
            frm_MF_vendorTypeAddEdit.Show()
        End If
    End Sub

    Private Sub cboTermsName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTermsName.SelectedIndexChanged
        cboTermsID.SelectedIndex() = cboTermsName.SelectedIndex

        If cboTermsName.SelectedIndex = 1 Then
            frm_MF_termsAddEdit.Keyterms = Guid.NewGuid.ToString
            frm_MF_termsAddEdit.Show()
        End If
    End Sub

    Private Sub cboBillingRateLvlName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBillingRateLvlName.SelectedIndexChanged
        cboBillingRateLvlID.SelectedIndex = cboBillingRateLvlName.SelectedIndex

        If cboBillingRateLvlName.SelectedIndex = 1 Then
            frm_MF_billRateLevelAdd.KeyBillRate = Guid.NewGuid.ToString
            frm_MF_billRateLevelAdd.Show()
        End If
    End Sub

    Private Sub btnAddress_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddress.Click
        frm_vend_masterVendorAddress.KeyAddress = sKeyAddress
        frm_vend_masterVendorAddress.KeySupplier = sKeySupplier
        frm_cust_masterCustomerAddress.Show()
    End Sub

    Private Sub txtOpeningBal_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtOpeningBal.KeyPress
        e.Handled = Not numeric_Validate(e.KeyChar)
    End Sub

    Private Sub txtCreditLimit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCreditLimit.KeyPress
        e.Handled = Not numeric_Validate(e.KeyChar)
    End Sub

    Private Sub btnDefineField_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDefineField.Click
        frm_vend_masterVendorDefineFields.ShowDialog()
    End Sub

    Private Sub MtcboAP_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MtcboAP.SelectedIndexChanged
        If MtcboAP.SelectedItem.Col1 <> "No Account" Then
            sKeyAPAcnt = MtcboAP.SelectedItem.Col2
        Else
            sKeyAPAcnt = ""
        End If
    End Sub

    Private Sub frm_vend_masterVendorAddEdit_ResizeBegin(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeBegin
        Me.Opacity = 0.5
    End Sub

    Private Sub frm_vend_masterVendorAddEdit_ResizeEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeEnd
        Me.Opacity = 1
    End Sub
End Class