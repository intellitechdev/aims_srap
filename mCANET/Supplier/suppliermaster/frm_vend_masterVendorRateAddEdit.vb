Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_vend_masterVendorRateAddEdit
    Private gCon As New Clsappconfiguration

    Private sKeyRate As String

    Public Property KeyRate() As String
        Get
            Return sKeyRate
        End Get
        Set(ByVal value As String)
            sKeyRate = value
        End Set
    End Property

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub frm_vend_masterVendorRateAddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        loadRate()
    End Sub

    Private Sub loadRate()
        Dim sSQLCmd As String = "SELECT * FROM mSupplier02RateMaster "
        sSQLCmd &= " WHERE fxKeyRate = '" & sKeyRate & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    txtFieldName.Text = rd.Item("fcRateName")
                    txtFieldDescription.Text = rd.Item("fcRateDescription")
                    txtPercentage.Text = rd.Item("fdPercentage").ToString
                Else
                    txtFieldName.Text = ""
                    txtFieldDescription.Text = ""
                    txtPercentage.Text = "0"
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Supplier Rate Category")
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        If Trim(txtFieldName.Text) = "" Then
            MessageBox.Show("Rate Category name cannot be empty!", "Data Validation")
        Else
            updateRate()
            m_supplierRate(frm_vend_masterVendor.grdRate)
            Me.Close()
        End If
    End Sub

    Private Sub updateRate()
        Dim sSQLCmd As String = "SPU_SupplierRate_Update "
        sSQLCmd &= "@fxKeyRate = '" & sKeyRate & "'"
        sSQLCmd &= ",@fcRateName	= '" & txtFieldName.Text & "'"
        sSQLCmd &= ",@fcRateDescription = '" & txtFieldDescription.Text & "'"
        sSQLCmd &= ",@fdPercentage = '" & txtPercentage.Text & "'"
        sSQLCmd &= ",@fxKeyCompany = '" & gCompanyID() & "'"
        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            MessageBox.Show("Record successfully updated.", "Add/Edit Supplier Rate Category")
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Add/Edit Supplier Rate Category")
        End Try
    End Sub

    Private Sub txtPercentage_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPercentage.KeyPress
        e.Handled = Not numeric_Validate(e.KeyChar)
    End Sub
End Class
