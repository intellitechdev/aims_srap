<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_vend_masterVendorAddEdit
    Inherits System.Windows.Forms.Form


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_vend_masterVendorAddEdit))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dteOpeningBalDate = New System.Windows.Forms.DateTimePicker()
        Me.txtSupplierName = New System.Windows.Forms.TextBox()
        Me.txtOpeningBal = New System.Windows.Forms.TextBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.txtMiddleInitial = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txtPrintOnChk = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.btnAddress = New System.Windows.Forms.Button()
        Me.txtCC = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtAltContact = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtAltPhone = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtFAX = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtPhone = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtContact = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.txtLastName = New System.Windows.Forms.TextBox()
        Me.txtFirstName = New System.Windows.Forms.TextBox()
        Me.txtSalutation = New System.Windows.Forms.TextBox()
        Me.txtCoName = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.MtcboAP = New MTGCComboBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.btnDefineField = New System.Windows.Forms.Button()
        Me.grdDefineFields = New System.Windows.Forms.DataGridView()
        Me.cboBillingRateLvlName = New System.Windows.Forms.ComboBox()
        Me.cboBillingRateLvlID = New System.Windows.Forms.ComboBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cboTermsName = New System.Windows.Forms.ComboBox()
        Me.cboTermsID = New System.Windows.Forms.ComboBox()
        Me.cboTypeName = New System.Windows.Forms.ComboBox()
        Me.cboTypeID = New System.Windows.Forms.ComboBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtTIN = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtCreditLimit = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtAcctNo = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.btnOk = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.chkInactive = New System.Windows.Forms.CheckBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtIDNo = New System.Windows.Forms.TextBox()
        Me.cboCostCenter = New System.Windows.Forms.ComboBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.grdDefineFields, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(38, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Name:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 68)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(87, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Opening Balance"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(288, 70)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(31, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "as of"
        '
        'dteOpeningBalDate
        '
        Me.dteOpeningBalDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteOpeningBalDate.Location = New System.Drawing.Point(325, 66)
        Me.dteOpeningBalDate.Name = "dteOpeningBalDate"
        Me.dteOpeningBalDate.Size = New System.Drawing.Size(143, 21)
        Me.dteOpeningBalDate.TabIndex = 3
        '
        'txtSupplierName
        '
        Me.txtSupplierName.Location = New System.Drawing.Point(107, 39)
        Me.txtSupplierName.Name = "txtSupplierName"
        Me.txtSupplierName.Size = New System.Drawing.Size(361, 21)
        Me.txtSupplierName.TabIndex = 4
        '
        'txtOpeningBal
        '
        Me.txtOpeningBal.Location = New System.Drawing.Point(107, 66)
        Me.txtOpeningBal.Name = "txtOpeningBal"
        Me.txtOpeningBal.Size = New System.Drawing.Size(175, 21)
        Me.txtOpeningBal.TabIndex = 5
        Me.txtOpeningBal.Text = "0.00"
        Me.txtOpeningBal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(15, 110)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(467, 268)
        Me.TabControl1.TabIndex = 6
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.SystemColors.Window
        Me.TabPage1.Controls.Add(Me.txtMiddleInitial)
        Me.TabPage1.Controls.Add(Me.Label28)
        Me.TabPage1.Controls.Add(Me.txtPrintOnChk)
        Me.TabPage1.Controls.Add(Me.Label16)
        Me.TabPage1.Controls.Add(Me.btnAddress)
        Me.TabPage1.Controls.Add(Me.txtCC)
        Me.TabPage1.Controls.Add(Me.Label15)
        Me.TabPage1.Controls.Add(Me.txtEmail)
        Me.TabPage1.Controls.Add(Me.Label14)
        Me.TabPage1.Controls.Add(Me.txtAltContact)
        Me.TabPage1.Controls.Add(Me.Label13)
        Me.TabPage1.Controls.Add(Me.txtAltPhone)
        Me.TabPage1.Controls.Add(Me.Label12)
        Me.TabPage1.Controls.Add(Me.txtFAX)
        Me.TabPage1.Controls.Add(Me.Label11)
        Me.TabPage1.Controls.Add(Me.txtPhone)
        Me.TabPage1.Controls.Add(Me.Label10)
        Me.TabPage1.Controls.Add(Me.txtContact)
        Me.TabPage1.Controls.Add(Me.Label9)
        Me.TabPage1.Controls.Add(Me.txtAddress)
        Me.TabPage1.Controls.Add(Me.txtLastName)
        Me.TabPage1.Controls.Add(Me.txtFirstName)
        Me.TabPage1.Controls.Add(Me.txtSalutation)
        Me.TabPage1.Controls.Add(Me.txtCoName)
        Me.TabPage1.Controls.Add(Me.Label8)
        Me.TabPage1.Controls.Add(Me.Label7)
        Me.TabPage1.Controls.Add(Me.Label6)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(459, 242)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Address Info"
        '
        'txtMiddleInitial
        '
        Me.txtMiddleInitial.Location = New System.Drawing.Point(199, 32)
        Me.txtMiddleInitial.Name = "txtMiddleInitial"
        Me.txtMiddleInitial.Size = New System.Drawing.Size(43, 21)
        Me.txtMiddleInitial.TabIndex = 30
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(168, 36)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(27, 13)
        Me.Label28.TabIndex = 29
        Me.Label28.Text = "M.I."
        '
        'txtPrintOnChk
        '
        Me.txtPrintOnChk.Location = New System.Drawing.Point(251, 210)
        Me.txtPrintOnChk.Name = "txtPrintOnChk"
        Me.txtPrintOnChk.Size = New System.Drawing.Size(198, 21)
        Me.txtPrintOnChk.TabIndex = 26
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(248, 194)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(94, 13)
        Me.Label16.TabIndex = 25
        Me.Label16.Text = "Print on Check as:"
        '
        'btnAddress
        '
        Me.btnAddress.Location = New System.Drawing.Point(106, 209)
        Me.btnAddress.Name = "btnAddress"
        Me.btnAddress.Size = New System.Drawing.Size(94, 21)
        Me.btnAddress.TabIndex = 24
        Me.btnAddress.Text = "&Address Details"
        Me.btnAddress.UseVisualStyleBackColor = True
        Me.btnAddress.Visible = False
        '
        'txtCC
        '
        Me.txtCC.Location = New System.Drawing.Point(313, 161)
        Me.txtCC.Name = "txtCC"
        Me.txtCC.Size = New System.Drawing.Size(136, 21)
        Me.txtCC.TabIndex = 23
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(248, 164)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(23, 13)
        Me.Label15.TabIndex = 22
        Me.Label15.Text = "Cc:"
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(313, 135)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(136, 21)
        Me.txtEmail.TabIndex = 21
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(248, 138)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(35, 13)
        Me.Label14.TabIndex = 20
        Me.Label14.Text = "Email:"
        '
        'txtAltContact
        '
        Me.txtAltContact.Location = New System.Drawing.Point(313, 109)
        Me.txtAltContact.Name = "txtAltContact"
        Me.txtAltContact.Size = New System.Drawing.Size(136, 21)
        Me.txtAltContact.TabIndex = 19
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(248, 112)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(69, 13)
        Me.Label13.TabIndex = 18
        Me.Label13.Text = "Alt. Contact:"
        '
        'txtAltPhone
        '
        Me.txtAltPhone.Location = New System.Drawing.Point(313, 84)
        Me.txtAltPhone.Name = "txtAltPhone"
        Me.txtAltPhone.Size = New System.Drawing.Size(136, 21)
        Me.txtAltPhone.TabIndex = 17
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(248, 87)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(61, 13)
        Me.Label12.TabIndex = 16
        Me.Label12.Text = "Alt. Phone:"
        '
        'txtFAX
        '
        Me.txtFAX.Location = New System.Drawing.Point(313, 60)
        Me.txtFAX.Name = "txtFAX"
        Me.txtFAX.Size = New System.Drawing.Size(136, 21)
        Me.txtFAX.TabIndex = 15
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(248, 63)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(30, 13)
        Me.Label11.TabIndex = 14
        Me.Label11.Text = "FAX:"
        '
        'txtPhone
        '
        Me.txtPhone.Location = New System.Drawing.Point(313, 36)
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.Size = New System.Drawing.Size(136, 21)
        Me.txtPhone.TabIndex = 13
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(248, 39)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(41, 13)
        Me.Label10.TabIndex = 12
        Me.Label10.Text = "Phone:"
        '
        'txtContact
        '
        Me.txtContact.Location = New System.Drawing.Point(313, 12)
        Me.txtContact.Name = "txtContact"
        Me.txtContact.Size = New System.Drawing.Size(136, 21)
        Me.txtContact.TabIndex = 11
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(248, 15)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(49, 13)
        Me.Label9.TabIndex = 10
        Me.Label9.Text = "Contact:"
        '
        'txtAddress
        '
        Me.txtAddress.Location = New System.Drawing.Point(106, 109)
        Me.txtAddress.Multiline = True
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(136, 94)
        Me.txtAddress.TabIndex = 9
        '
        'txtLastName
        '
        Me.txtLastName.Location = New System.Drawing.Point(106, 82)
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(136, 21)
        Me.txtLastName.TabIndex = 8
        '
        'txtFirstName
        '
        Me.txtFirstName.Location = New System.Drawing.Point(106, 56)
        Me.txtFirstName.Name = "txtFirstName"
        Me.txtFirstName.Size = New System.Drawing.Size(136, 21)
        Me.txtFirstName.TabIndex = 7
        '
        'txtSalutation
        '
        Me.txtSalutation.Location = New System.Drawing.Point(106, 32)
        Me.txtSalutation.Name = "txtSalutation"
        Me.txtSalutation.Size = New System.Drawing.Size(43, 21)
        Me.txtSalutation.TabIndex = 6
        '
        'txtCoName
        '
        Me.txtCoName.Location = New System.Drawing.Point(106, 8)
        Me.txtCoName.Name = "txtCoName"
        Me.txtCoName.Size = New System.Drawing.Size(136, 21)
        Me.txtCoName.TabIndex = 5
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(6, 116)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(50, 13)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "Address:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 89)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(61, 13)
        Me.Label7.TabIndex = 3
        Me.Label7.Text = "Last Name:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 63)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(62, 13)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "First Name:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 39)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(60, 13)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Mr./Ms./..."
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 15)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(87, 18)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Company Name:"
        Me.Label4.UseCompatibleTextRendering = True
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.SystemColors.Window
        Me.TabPage2.Controls.Add(Me.MtcboAP)
        Me.TabPage2.Controls.Add(Me.Label23)
        Me.TabPage2.Controls.Add(Me.btnDefineField)
        Me.TabPage2.Controls.Add(Me.grdDefineFields)
        Me.TabPage2.Controls.Add(Me.cboBillingRateLvlName)
        Me.TabPage2.Controls.Add(Me.cboBillingRateLvlID)
        Me.TabPage2.Controls.Add(Me.Label22)
        Me.TabPage2.Controls.Add(Me.GroupBox1)
        Me.TabPage2.Controls.Add(Me.txtTIN)
        Me.TabPage2.Controls.Add(Me.Label19)
        Me.TabPage2.Controls.Add(Me.txtCreditLimit)
        Me.TabPage2.Controls.Add(Me.Label18)
        Me.TabPage2.Controls.Add(Me.txtAcctNo)
        Me.TabPage2.Controls.Add(Me.Label17)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(459, 242)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Additional Info"
        '
        'MtcboAP
        '
        Me.MtcboAP.ArrowBoxColor = System.Drawing.SystemColors.Control
        Me.MtcboAP.ArrowColor = System.Drawing.Color.Black
        Me.MtcboAP.BindedControl = CType(resources.GetObject("MtcboAP.BindedControl"), MTGCComboBox.ControlloAssociato)
        Me.MtcboAP.BorderStyle = MTGCComboBox.TipiBordi.Fixed3D
        Me.MtcboAP.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.MtcboAP.ColumnNum = 2
        Me.MtcboAP.ColumnWidth = "300; 0"
        Me.MtcboAP.DisabledArrowBoxColor = System.Drawing.SystemColors.Control
        Me.MtcboAP.DisabledArrowColor = System.Drawing.Color.LightGray
        Me.MtcboAP.DisabledBackColor = System.Drawing.SystemColors.Control
        Me.MtcboAP.DisabledBorderColor = System.Drawing.SystemColors.InactiveBorder
        Me.MtcboAP.DisabledForeColor = System.Drawing.SystemColors.GrayText
        Me.MtcboAP.DisplayMember = "Text"
        Me.MtcboAP.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.MtcboAP.DropDownBackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.MtcboAP.DropDownForeColor = System.Drawing.Color.Black
        Me.MtcboAP.DropDownStyle = MTGCComboBox.CustomDropDownStyle.DropDown
        Me.MtcboAP.DropDownWidth = 320
        Me.MtcboAP.GridLineColor = System.Drawing.Color.LightGray
        Me.MtcboAP.GridLineHorizontal = False
        Me.MtcboAP.GridLineVertical = False
        Me.MtcboAP.LoadingType = MTGCComboBox.CaricamentoCombo.ComboBoxItem
        Me.MtcboAP.Location = New System.Drawing.Point(102, 119)
        Me.MtcboAP.ManagingFastMouseMoving = True
        Me.MtcboAP.ManagingFastMouseMovingInterval = 30
        Me.MtcboAP.MaxDropDownItems = 15
        Me.MtcboAP.Name = "MtcboAP"
        Me.MtcboAP.SelectedItem = Nothing
        Me.MtcboAP.SelectedValue = Nothing
        Me.MtcboAP.Size = New System.Drawing.Size(120, 22)
        Me.MtcboAP.TabIndex = 30
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(6, 124)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(92, 13)
        Me.Label23.TabIndex = 29
        Me.Label23.Text = "Accounts Payable"
        '
        'btnDefineField
        '
        Me.btnDefineField.Location = New System.Drawing.Point(241, 206)
        Me.btnDefineField.Name = "btnDefineField"
        Me.btnDefineField.Size = New System.Drawing.Size(88, 21)
        Me.btnDefineField.TabIndex = 28
        Me.btnDefineField.Text = "&Define Fields"
        Me.btnDefineField.UseVisualStyleBackColor = True
        '
        'grdDefineFields
        '
        Me.grdDefineFields.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdDefineFields.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdDefineFields.Location = New System.Drawing.Point(241, 9)
        Me.grdDefineFields.Name = "grdDefineFields"
        Me.grdDefineFields.Size = New System.Drawing.Size(204, 191)
        Me.grdDefineFields.TabIndex = 27
        '
        'cboBillingRateLvlName
        '
        Me.cboBillingRateLvlName.FormattingEnabled = True
        Me.cboBillingRateLvlName.Location = New System.Drawing.Point(102, 92)
        Me.cboBillingRateLvlName.Name = "cboBillingRateLvlName"
        Me.cboBillingRateLvlName.Size = New System.Drawing.Size(120, 21)
        Me.cboBillingRateLvlName.TabIndex = 15
        '
        'cboBillingRateLvlID
        '
        Me.cboBillingRateLvlID.FormattingEnabled = True
        Me.cboBillingRateLvlID.Location = New System.Drawing.Point(102, 92)
        Me.cboBillingRateLvlID.Name = "cboBillingRateLvlID"
        Me.cboBillingRateLvlID.Size = New System.Drawing.Size(120, 21)
        Me.cboBillingRateLvlID.TabIndex = 16
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(7, 95)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(87, 13)
        Me.Label22.TabIndex = 14
        Me.Label22.Text = "Billing Rate Level"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cboTermsName)
        Me.GroupBox1.Controls.Add(Me.cboTermsID)
        Me.GroupBox1.Controls.Add(Me.cboTypeName)
        Me.GroupBox1.Controls.Add(Me.cboTypeID)
        Me.GroupBox1.Controls.Add(Me.Label21)
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 144)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(214, 83)
        Me.GroupBox1.TabIndex = 11
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Category"
        '
        'cboTermsName
        '
        Me.cboTermsName.FormattingEnabled = True
        Me.cboTermsName.Location = New System.Drawing.Point(78, 49)
        Me.cboTermsName.Name = "cboTermsName"
        Me.cboTermsName.Size = New System.Drawing.Size(130, 21)
        Me.cboTermsName.TabIndex = 15
        '
        'cboTermsID
        '
        Me.cboTermsID.FormattingEnabled = True
        Me.cboTermsID.Location = New System.Drawing.Point(78, 49)
        Me.cboTermsID.Name = "cboTermsID"
        Me.cboTermsID.Size = New System.Drawing.Size(130, 21)
        Me.cboTermsID.TabIndex = 16
        '
        'cboTypeName
        '
        Me.cboTypeName.FormattingEnabled = True
        Me.cboTypeName.Location = New System.Drawing.Point(78, 22)
        Me.cboTypeName.Name = "cboTypeName"
        Me.cboTypeName.Size = New System.Drawing.Size(130, 21)
        Me.cboTypeName.TabIndex = 13
        '
        'cboTypeID
        '
        Me.cboTypeID.FormattingEnabled = True
        Me.cboTypeID.Location = New System.Drawing.Point(78, 22)
        Me.cboTypeID.Name = "cboTypeID"
        Me.cboTypeID.Size = New System.Drawing.Size(130, 21)
        Me.cboTypeID.TabIndex = 16
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(6, 52)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(36, 13)
        Me.Label21.TabIndex = 14
        Me.Label21.Text = "Terms"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(6, 25)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(31, 13)
        Me.Label20.TabIndex = 10
        Me.Label20.Text = "Type"
        '
        'txtTIN
        '
        Me.txtTIN.Location = New System.Drawing.Point(88, 61)
        Me.txtTIN.Name = "txtTIN"
        Me.txtTIN.Size = New System.Drawing.Size(136, 21)
        Me.txtTIN.TabIndex = 10
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(7, 64)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(24, 13)
        Me.Label19.TabIndex = 9
        Me.Label19.Text = "TIN"
        '
        'txtCreditLimit
        '
        Me.txtCreditLimit.Location = New System.Drawing.Point(88, 35)
        Me.txtCreditLimit.Name = "txtCreditLimit"
        Me.txtCreditLimit.Size = New System.Drawing.Size(136, 21)
        Me.txtCreditLimit.TabIndex = 8
        Me.txtCreditLimit.Text = "0.00"
        Me.txtCreditLimit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(7, 38)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(60, 13)
        Me.Label18.TabIndex = 7
        Me.Label18.Text = "Credit Limit"
        '
        'txtAcctNo
        '
        Me.txtAcctNo.Location = New System.Drawing.Point(88, 9)
        Me.txtAcctNo.Name = "txtAcctNo"
        Me.txtAcctNo.Size = New System.Drawing.Size(136, 21)
        Me.txtAcctNo.TabIndex = 6
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(7, 12)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(62, 13)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Account No"
        '
        'btnOk
        '
        Me.btnOk.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.Image = Global.CSAcctg.My.Resources.Resources.floppy
        Me.btnOk.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnOk.Location = New System.Drawing.Point(325, 384)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(75, 29)
        Me.btnOk.TabIndex = 7
        Me.btnOk.Text = "Save"
        Me.btnOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.Location = New System.Drawing.Point(403, 384)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 29)
        Me.btnCancel.TabIndex = 8
        Me.btnCancel.Text = "Close"
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'chkInactive
        '
        Me.chkInactive.AutoSize = True
        Me.chkInactive.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkInactive.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkInactive.Location = New System.Drawing.Point(403, 93)
        Me.chkInactive.Name = "chkInactive"
        Me.chkInactive.Size = New System.Drawing.Size(65, 17)
        Me.chkInactive.TabIndex = 9
        Me.chkInactive.Text = "Inactive"
        Me.chkInactive.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkInactive.UseVisualStyleBackColor = True
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(12, 12)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(38, 13)
        Me.Label24.TabIndex = 10
        Me.Label24.Text = "ID No:"
        '
        'txtIDNo
        '
        Me.txtIDNo.Location = New System.Drawing.Point(107, 9)
        Me.txtIDNo.Name = "txtIDNo"
        Me.txtIDNo.Size = New System.Drawing.Size(99, 21)
        Me.txtIDNo.TabIndex = 11
        '
        'cboCostCenter
        '
        Me.cboCostCenter.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboCostCenter.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboCostCenter.FormattingEnabled = True
        Me.cboCostCenter.Location = New System.Drawing.Point(347, 9)
        Me.cboCostCenter.Name = "cboCostCenter"
        Me.cboCostCenter.Size = New System.Drawing.Size(121, 21)
        Me.cboCostCenter.TabIndex = 31
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(272, 12)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(69, 13)
        Me.Label25.TabIndex = 32
        Me.Label25.Text = "Cost Center:"
        '
        'frm_vend_masterVendorAddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.ClientSize = New System.Drawing.Size(494, 417)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.cboCostCenter)
        Me.Controls.Add(Me.txtIDNo)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.chkInactive)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOk)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.txtOpeningBal)
        Me.Controls.Add(Me.txtSupplierName)
        Me.Controls.Add(Me.dteOpeningBalDate)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frm_vend_masterVendorAddEdit"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "New Member/Supplier"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.grdDefineFields, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dteOpeningBalDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtSupplierName As System.Windows.Forms.TextBox
    Friend WithEvents txtOpeningBal As System.Windows.Forms.TextBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtLastName As System.Windows.Forms.TextBox
    Friend WithEvents txtFirstName As System.Windows.Forms.TextBox
    Friend WithEvents txtSalutation As System.Windows.Forms.TextBox
    Friend WithEvents txtCoName As System.Windows.Forms.TextBox
    Friend WithEvents txtAddress As System.Windows.Forms.TextBox
    Friend WithEvents txtContact As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtCC As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtAltContact As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtAltPhone As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtFAX As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtPhone As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtPrintOnChk As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents btnAddress As System.Windows.Forms.Button
    Friend WithEvents btnOk As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtCreditLimit As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtAcctNo As System.Windows.Forms.TextBox
    Friend WithEvents txtTIN As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cboTermsName As System.Windows.Forms.ComboBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents cboTypeName As System.Windows.Forms.ComboBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents cboBillingRateLvlName As System.Windows.Forms.ComboBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents chkInactive As System.Windows.Forms.CheckBox
    Friend WithEvents txtMiddleInitial As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents cboTypeID As System.Windows.Forms.ComboBox
    Friend WithEvents cboTermsID As System.Windows.Forms.ComboBox
    Friend WithEvents cboBillingRateLvlID As System.Windows.Forms.ComboBox
    Friend WithEvents btnDefineField As System.Windows.Forms.Button
    Friend WithEvents grdDefineFields As System.Windows.Forms.DataGridView
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents MtcboAP As MTGCComboBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtIDNo As System.Windows.Forms.TextBox
    Friend WithEvents cboCostCenter As System.Windows.Forms.ComboBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
End Class
