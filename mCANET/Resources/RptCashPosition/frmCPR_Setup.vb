﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmCPR_Setup

    Private con As New Clsappconfiguration

    Private Sub frmCPR_Setup_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _LoadData()
        _ClrFields()
    End Sub

    Private Sub _ClrFields()
        txtAcctCode.Text = ""
        txtID.Text = ""
        txtAcctTitle.Text = ""
        txtHeaderText.Text = ""
    End Sub

    Private Sub CloseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub _LoadData()
        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_CPR_Load")
            dgvlist.DataSource = ds.Tables(0)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub SaveToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveToolStripMenuItem.Click
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_CPR_InsertUpdate",
                                       New SqlParameter("@pkColID", txtID.Text),
                                       New SqlParameter("@fcAcctCode", txtAcctCode.Text),
                                       New SqlParameter("@fcAcctTitle", txtAcctTitle.Text),
                                       New SqlParameter("@fcHeaderText", txtHeaderText.Text))
            _LoadData()
            _ClrFields()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub DeleteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteToolStripMenuItem.Click
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_CPR_Delete",
                                       New SqlParameter("@pkColID", txtID.Text))
            _LoadData()
            _ClrFields()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub dgvlist_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvlist.Click
        With dgvlist.SelectedRows(0)
            txtID.Text = .Cells(0).Value.ToString
            txtAcctCode.Text = .Cells(1).Value.ToString
            txtAcctTitle.Text = .Cells(2).Value.ToString
            txtHeaderText.Text = .Cells(3).Value.ToString
        End With
    End Sub

    Private Sub NewToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NewToolStripMenuItem.Click
        _ClrFields()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        frmCPR_Accts.StartPosition = FormStartPosition.CenterScreen
        frmCPR_Accts.ShowDialog()
    End Sub

End Class