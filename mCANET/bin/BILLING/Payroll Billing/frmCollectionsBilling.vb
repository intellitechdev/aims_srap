﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Data.OleDb

Public Class frmCollectionsBilling

    Private con As New Clsappconfiguration
    Private mIndex As Integer = 0
    Private mIndex2 As Integer = 0

    Dim filenym As String
    Dim fpath As String

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub frmCollectionsBilling_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If _BootLoader() Then
            LoadDefaultAcct()
        End If
    End Sub

    Private Function _BootLoader() As Boolean
        Try
            mIndex = 0
            _LoadPayment_Frame()
            _LoadDetail_Frame()
            Return True
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "_BootLoader")
            Return False
        End Try
    End Function

#Region "BootLoader"
    Private Sub _LoadPayment_Frame()
        dgvPayment.DataSource = Nothing
        dgvPayment.Rows.Clear()
        dgvPayment.Columns.Clear()
        Dim empid As New DataGridViewTextBoxColumn
        Dim empname As New DataGridViewTextBoxColumn
        Dim amount As New DataGridViewTextBoxColumn
        Dim applied As New DataGridViewTextBoxColumn
        Dim overunder As New DataGridViewTextBoxColumn
        Dim totcollection As New DataGridViewTextBoxColumn
        With empid
            .Name = "empid"
            .HeaderText = "ID No."
        End With
        With empname
            .Name = "empname"
            .HeaderText = "Name"
        End With
        With amount
            .Name = "amount"
            .HeaderText = "Amount"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With applied
            .Name = "applied"
            .HeaderText = "Applied to Loan"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With overunder
            .Name = "overunder"
            .HeaderText = "(Over)/Under"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With totcollection
            .Name = "totcollection"
            .HeaderText = "Total Collection"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With dgvPayment
            .Columns.Add(empid)
            .Columns.Add(empname)
            .Columns.Add(amount)
            .Columns.Add(applied)
            .Columns.Add(overunder)
            .Columns.Add(totcollection)
        End With
    End Sub
    Private Sub _LoadDetail_Frame()
        dgvDetails.DataSource = Nothing
        dgvDetails.Rows.Clear()
        dgvDetails.Columns.Clear()
        Dim loanref As New DataGridViewTextBoxColumn
        Dim acctref As New DataGridViewTextBoxColumn
        Dim code As New DataGridViewTextBoxColumn
        Dim accttitle As New DataGridViewTextBoxColumn
        Dim currentdue As New DataGridViewTextBoxColumn
        Dim pastdue As New DataGridViewTextBoxColumn
        Dim billing As New DataGridViewTextBoxColumn
        Dim collections As New DataGridViewTextBoxColumn
        With loanref
            .Name = "loanref"
            .HeaderText = "Loan Ref."
        End With
        With acctref
            .Name = "acctref"
            .HeaderText = "Acct. Ref"
        End With
        With code
            .Name = "code"
            .HeaderText = "Code"
        End With
        With accttitle
            .Name = "accttitle"
            .HeaderText = "Acct. Title"
        End With
        With currentdue
            .Name = "currentdue"
            .HeaderText = "Current"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With pastdue
            .Name = "pastdue"
            .HeaderText = "Past"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With billing
            .Name = "billing"
            .HeaderText = "Billing"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With collections
            .Name = "collections"
            .HeaderText = "Collection"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With dgvDetails
            .Columns.Add(loanref)
            .Columns.Add(acctref)
            .Columns.Add(code)
            .Columns.Add(accttitle)
            .Columns.Add(currentdue)
            .Columns.Add(pastdue)
            .Columns.Add(billing)
            .Columns.Add(collections)
        End With
    End Sub
#End Region

    Private Sub btnGetBilling_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetBilling.Click
        frmSearch_Billing.xmode = "collect"
        frmSearch_Billing.StartPosition = FormStartPosition.CenterScreen
        frmSearch_Billing.ShowDialog()
    End Sub

    Private Sub _GetBilling_Client(ByVal key As String)
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_Billing_GetBilling",
                                                               New SqlParameter("@key", key))
            While rd.Read
                With dgvPayment
                    .Rows.Add()
                    .Item("empid", mIndex).Value = rd(0)
                    .Item("empname", mIndex).Value = rd(1)
                    lblBilldate.Text = rd(2)
                    _GetBilling_Amt(key, rd(0))
                    mIndex += 1
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "_GetBilling_Client")
        End Try
    End Sub
    Private Sub _GetBilling_Amt(ByVal key As String, ByVal idno As String)
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_Billing_GetBilling_Amount",
                                                               New SqlParameter("@key", key),
                                                               New SqlParameter("@IDNo", idno))
            While rd.Read
                With dgvPayment
                    .Item("amount", mIndex).Value = rd(0)
                    .Item("applied", mIndex).Value = "0.00"
                    .Item("overunder", mIndex).Value = "0.00"
                    .Item("totcollection", mIndex).Value = "0.00"
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "_GetBilling_Amt")
        End Try
    End Sub

    Private Sub txtBilling_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBilling.TextChanged
        If _BootLoader() Then
            _GetBilling_Client(txtBilling.Text)
        End If
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        If _BootLoader() Then
            txtBilling.Text = ""
            txtPath.Text = ""
            _ClearTotals()
        End If
    End Sub
    Private Sub _ClearTotals()
        txtTotAmount.Text = "0.00"
        txtTotApplied.Text = "0.00"
        txtTotBilling.Text = "0.00"
        txttotCollection.Text = "0.00"
        txtCollectionTot.Text = "0.00"
        txtCurrentDue.Text = "0.00"
        txtOverUnder.Text = "0.00"
        txtPastDue.Text = "0.00"
    End Sub

    Private Sub _GetBilling_Details(ByVal idno As String, ByVal key As String, ByVal collected As Decimal, ByVal underpay As Decimal)
        Try
            _LoadDetail_Frame()
            mIndex2 = 0
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_Billing_GetBilling_Details",
                                                               New SqlParameter("@key", key),
                                                               New SqlParameter("@idno", idno))
            While rd.Read
                With dgvDetails
                    .Rows.Add()
                    .Item("loanref", mIndex2).Value = rd(0)
                    .Item("acctref", mIndex2).Value = rd(1)
                    .Item("code", mIndex2).Value = rd(2)
                    .Item("accttitle", mIndex2).Value = rd(3)
                    .Item("currentdue", mIndex2).Value = rd(4)
                    .Item("pastdue", mIndex2).Value = rd(5)
                    .Item("billing", mIndex2).Value = rd(6)
                    If collected = 0 Then
                        .Item("collections", mIndex2).Value = "0.00"
                    Else
                        If .Item("billing", mIndex2).Value < collected Then
                            .Item("collections", mIndex2).Value = .Item("billing", mIndex2).Value
                            collected = collected - .Item("billing", mIndex2).Value
                        Else
                            .Item("collections", mIndex2).Value = .Item("billing", mIndex2).Value '- underpay '- collected
                            collected = collected - .Item("billing", mIndex2).Value
                        End If
                        If .Item("billing", mIndex2).Value = collected Then
                            .Item("collections", mIndex2).Value = .Item("billing", mIndex2).Value
                            collected = 0
                        End If
                    End If
                    mIndex2 += 1
                    collected = _GetBilling_Detail_Charges(rd(0), key, collected, underpay)
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "_GetBilling_Details")
        End Try
    End Sub
    Private Function _GetBilling_Detail_Charges(ByVal loanno As String, ByVal docnumber As String, ByVal collected As Decimal, ByVal underpay As Decimal) As Decimal
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_Billing_GetBilling_Details_Charges",
                                                               New SqlParameter("@LoanNo", loanno),
                                                               New SqlParameter("@DocNumber", docnumber))
            While rd.Read
                With dgvDetails
                    .Rows.Add()
                    .Item("loanref", mIndex2).Value = rd(0)
                    .Item("acctref", mIndex2).Value = rd(1)
                    .Item("code", mIndex2).Value = rd(2)
                    .Item("accttitle", mIndex2).Value = rd(3)
                    .Item("currentdue", mIndex2).Value = rd(4)
                    .Item("pastdue", mIndex2).Value = rd(5)
                    .Item("billing", mIndex2).Value = rd(6)
                    If collected = 0 Or .Item("billing", mIndex2).Value = 0 Then
                        .Item("collections", mIndex2).Value = "0.00"
                    Else
                        If .Item("billing", mIndex2).Value < collected Then
                            .Item("collections", mIndex2).Value = .Item("billing", mIndex2).Value
                            collected = collected - .Item("billing", mIndex2).Value
                        End If
                        If .Item("billing", mIndex2).Value > collected Then
                            .Item("collections", mIndex2).Value = .Item("billing", mIndex2).Value - underpay '- collected
                            collected = collected - .Item("billing", mIndex2).Value
                        End If
                        If .Item("billing", mIndex2).Value = collected Then
                            .Item("collections", mIndex2).Value = .Item("billing", mIndex2).Value
                            collected = 0
                        End If
                    End If
                    '.Item("collections", mIndex2).Value = "0.00" 'ongoing
                    mIndex2 += 1
                End With
            End While
            Return collected
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "_GetBilling_Detail_Charges")
            Return 0
        End Try
    End Function

    Private Sub dgvPayment_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPayment.CellValueChanged
        Dim totamout As Decimal = 0
        Dim totapplied As Decimal = 0
        Dim totoverunder As Decimal = 0
        Dim totcollection As Decimal = 0
        For i As Integer = 0 To dgvPayment.RowCount - 1
            With dgvPayment
                totamout = totamout + .Item("amount", i).Value
                totapplied = totapplied + .Item("applied", i).Value
                totoverunder = totoverunder + .Item("overunder", i).Value
                totcollection = totcollection + .Item("totcollection", i).Value
                .Item("amount", i).Value = Format(CDec(.Item("amount", i).Value), "##,##0.00")
                .Item("applied", i).Value = Format(CDec(.Item("applied", i).Value), "##,##0.00")
                .Item("overunder", i).Value = Format(CDec(.Item("overunder", i).Value), "##,##0.00")
                .Item("totcollection", i).Value = Format(CDec(.Item("totcollection", i).Value), "##,##0.00")
            End With
        Next
        txtTotAmount.Text = Format(totamout, "##,##0.00")
        txtTotApplied.Text = Format(totapplied, "##,##0.00")
        txtOverUnder.Text = Format(totoverunder, "##,##0.00")
        txtCollectionTot.Text = Format(totcollection, "##,##0.00")
    End Sub

    Private Sub dgvPayment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvPayment.Click
        With dgvPayment
            Try
                _GetBilling_Details(.SelectedRows(0).Cells(0).Value.ToString, txtBilling.Text, .CurrentRow.Cells("applied").Value, .CurrentRow.Cells("overunder").Value)
            Catch ex As Exception
                _LoadDetail_Frame()
            End Try
        End With
    End Sub

    Private Sub dgvDetails_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetails.CellValueChanged
        Dim totamout As Decimal = 0
        Dim totapplied As Decimal = 0
        Dim totoverunder As Decimal = 0
        Dim totcollection As Decimal = 0
        For i As Integer = 0 To dgvDetails.RowCount - 1
            With dgvDetails
                totamout = totamout + .Item("currentdue", i).Value
                totapplied = totapplied + .Item("pastdue", i).Value
                totoverunder = totoverunder + .Item("billing", i).Value
                totcollection = totcollection + .Item("collections", i).Value
                .Item("currentdue", i).Value = Format(CDec(.Item("currentdue", i).Value), "##,##0.00")
                .Item("pastdue", i).Value = Format(CDec(.Item("pastdue", i).Value), "##,##0.00")
                .Item("billing", i).Value = Format(CDec(.Item("billing", i).Value), "##,##0.00")
                .Item("collections", i).Value = Format(CDec(.Item("collections", i).Value), "##,##0.00")
            End With
        Next
        txtCurrentDue.Text = Format(totamout, "##,##0.00")
        txtPastDue.Text = Format(totapplied, "##,##0.00")
        txtTotBilling.Text = Format(totoverunder, "##,##0.00")
        txttotCollection.Text = Format(totcollection, "##,##0.00")
    End Sub

    Private Sub btnDefaultAcct_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDefaultAcct.Click
        If btnDefaultAcct.Text = "Default Account for Over Payment" Then
            frmBAccounts.xmode = "bill"
            frmBAccounts.StartPosition = FormStartPosition.CenterScreen
            frmBAccounts.ShowDialog()
            btnDefaultAcct.Text = "Save"
        Else
            SqlHelper.ExecuteNonQuery(con.cnstring, "_Billing_Insert_DefaultAcct",
                                       New SqlParameter("@fcCode", txtDefaulAcct.Tag),
                                       New SqlParameter("@fcAcctTitle", txtDefaulAcct.Text))
            btnDefaultAcct.Text = "Default Account for Over Payment"
        End If
    End Sub
    Private Sub LoadDefaultAcct()
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_Billing_LoadDefaultAcct")
        While rd.Read
            txtDefaulAcct.Tag = rd(0)
            txtDefaulAcct.Text = rd(1)
        End While
    End Sub
    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        If ImportCollections() Then
            btnImport.Text = "Import Collections"
            btnImport.ForeColor = Color.Black
        Else
            btnImport.Text = "Import Collections"
            btnImport.ForeColor = Color.Black
        End If
    End Sub

    Private Function ImportCollections() As Boolean
        Try
            OpenFileDialog1.Filter = "Microsoft Excel (*.xlsx)|*.xlsx|Microsoft Excel 97-2003 (*.xls)|*.xls"
            OpenFileDialog1.FileName = "Select Excel File"
            Dim x As DialogResult = OpenFileDialog1.ShowDialog()
            If x = Windows.Forms.DialogResult.Cancel Then
                Exit Function
            End If
            filenym = OpenFileDialog1.FileName
            fpath = Path.GetFullPath(filenym)
            txtPath.Text = fpath
            'iscolPrepare()
            btnImport.Text = "Importing..."
            btnImport.ForeColor = Color.Blue
            Import()
            Return True
        Catch ex As Exception
            ' MsgBox("User Cancelled!", vbInformation, "...")
            MsgBox(ex.Message, vbInformation, "Import")
            Return False
        End Try
    End Function

    Private Sub Import()
        Dim MyConnection As System.Data.OleDb.OleDbConnection
        MyConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + txtPath.Text.ToString + "';Extended Properties=Excel 8.0;")
        MyConnection.Open()
        Dim dbCommand As New OleDbCommand("select * from [Sheet1$]", MyConnection)
        Dim rd As OleDbDataReader
        rd = dbCommand.ExecuteReader

        '=========================
        While rd.Read
            For i As Integer = 0 To dgvPayment.RowCount - 1
                With dgvPayment
                    If .Item("empid", i).Value = rd(0) Then
                        If .Item("amount", i).Value > rd(6) Then
                            .Item("overunder", i).Value = .Item("amount", i).Value - rd(6)
                            .Item("applied", i).Value = rd(6)
                            .Item("totcollection", i).Value = rd(6)
                        End If
                        If .Item("amount", i).Value < rd(6) Then
                            .Item("overunder", i).Value = .Item("amount", i).Value - rd(6)
                            .Item("applied", i).Value = .Item("amount", i).Value
                            .Item("totcollection", i).Value = rd(6)
                        End If
                        If .Item("amount", i).Value = rd(6) Then
                            .Item("overunder", i).Value = "0.00"
                            .Item("applied", i).Value = rd(6)
                            .Item("totcollection", i).Value = rd(6)
                        End If
                    End If
                End With
            Next
        End While
        MyConnection.Close()
    End Sub

    Private Sub btnGenerateEntry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerateEntry.Click
        If xGenEntry() Then
            Me.Close()
        End If
    End Sub

    Private Function xGenEntry() As Boolean
        Try
            frmCollectionsIntegrator._AcctCode = txtDefaulAcct.Tag
            frmCollectionsIntegrator._AcctTitle = txtDefaulAcct.Text
            For i As Integer = 0 To dgvPayment.RowCount - 1
                With dgvPayment
                    frmCollectionsIntegrator._generate_Billing_Entry(.Item("empid", i).Value, .Item("applied", i).Value, txtBilling.Text, .Item("empname", i).Value, .Item("overunder", i).Value)
                End With
            Next
            Return True
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "xGenEntry")
            Return False
        End Try
    End Function

    Private Sub btnPrintDiff_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintDiff.Click
        If is_Data_Deleted() Then
            _Insert_Data_ForPrint()
            frmExportLoanDeductions.StartPosition = FormStartPosition.CenterScreen
            frmExportLoanDeductions._Print_Billing_Diff()
            frmExportLoanDeductions.ShowDialog()
        End If
    End Sub
    Private Sub _Insert_Data_ForPrint()
        Try
            For i As Integer = 0 To dgvPayment.RowCount - 1
                With dgvPayment
                    SqlHelper.ExecuteNonQuery(con.cnstring, "_Billing_InsertData_ForPrinting",
                                               New SqlParameter("@idno", .Item("empid", i).Value),
                                               New SqlParameter("@clientname", .Item("empname", i).Value),
                                               New SqlParameter("@fdamount", .Item("amount", i).Value),
                                               New SqlParameter("@fdapplied", .Item("applied", i).Value),
                                               New SqlParameter("@fdoverunder", .Item("overunder", i).Value),
                                               New SqlParameter("@fdcollected", .Item("totcollection", i).Value),
                                               New SqlParameter("@fccompany", frmMain.lblCompanyName.Text),
                                               New SqlParameter("@dtdate", CDate(lblBilldate.Text)))
                End With
            Next
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "_Printing")
        End Try
    End Sub
    Private Function is_Data_Deleted() As Boolean
        Try
            SqlHelper.ExecuteScalar(con.cnstring, CommandType.Text, "Delete FROM Billing_Temp")
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
End Class